import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { INVCAM_Fallecidos } from '../models/INVCAM_Fallecidos';
import { Observable, BehaviorSubject, observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class FallecidoService {

  myApUrl = environment.myApUrl;
  myApiUrl = 'api/INVCAM_Fallecidos/';

  private actualizarInfo = new BehaviorSubject<INVCAM_Fallecidos>({} as any);

  constructor(private http: HttpClient) { }

  obtenerFallecidos(){
    return this.http.get(this.myApUrl + this.myApiUrl);
  }

  guardarRegistro(fallecido: INVCAM_Fallecidos): Observable<INVCAM_Fallecidos>{

    return this.http.post<INVCAM_Fallecidos>(this.myApUrl + this.myApiUrl, fallecido);
  }

  actualizar(fallecido){
    this.actualizarInfo.asObservable();
  }

  obtenerInfo(): Observable<INVCAM_Fallecidos>{
    return this.actualizarInfo.asObservable();
  }
  
  actualizarFallecido(id: number, fallecido: INVCAM_Fallecidos): Observable<INVCAM_Fallecidos>{
    return this.http.patch<INVCAM_Fallecidos>(this.myApUrl + this.myApiUrl + id, fallecido)
  }

  eliminarReserva(id: number): Observable<INVCAM_Fallecidos>{
    return this.http.delete<INVCAM_Fallecidos>(this.myApUrl + this.myApiUrl + id)
  }

  filtrarFallecidos(nombreFallecido: string): Observable<INVCAM_Fallecidos>{
    return this.http.get<INVCAM_Fallecidos> (this.myApUrl + this.myApiUrl + nombreFallecido)
  }

  
}
