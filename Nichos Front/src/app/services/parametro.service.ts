import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ParametroService {

 /*myApUrl  = 'http://localhost:51567/';
  myApiUrl = 'api/INVCAM_Parametros/';*/

myApUrl = environment.myApUrl;
//myApUrl = 'http://192.168.1.100:8081/';
myApiUrl = 'api/INVCAM_Parametros/';

  constructor(private http: HttpClient) { }

  obtenerParametro(){      
    return this.http.get(this.myApUrl+ this.myApiUrl)
  }


}
