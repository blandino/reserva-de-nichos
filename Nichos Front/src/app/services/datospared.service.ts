import { Injectable } from '@angular/core';
import { observable, BehaviorSubject, Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DatosparedService {

  datos: any[] = []
  private datos$ = new BehaviorSubject<any>({} as any);
  constructor() { }

  ValidarDatos(datos: any[]){
    
  this.datos = datos;
  this.datos$.next(this.datos);
  }

  GetDatos$(): Observable<any>{    
    return this.datos$.asObservable();
  }
}
