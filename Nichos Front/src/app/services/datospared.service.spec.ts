import { TestBed } from '@angular/core/testing';

import { DatosparedService } from './datospared.service';

describe('DatosparedService', () => {
  let service: DatosparedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DatosparedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
