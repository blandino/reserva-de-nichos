import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VINVCAM_LOGIN } from '../models/VINVCAM_LOGIN';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  loginUserList: any[] = [];

/*myApUrl  = 'http://localhost:51567/';
myApiUrl = 'api/VINVCAM_LOGIN';*/

myApUrl = environment.myApUrl;
//myApUrl = 'http://192.168.1.100:8081/';
myApiUrl = 'api/VINVCAM_LOGIN';

  constructor(private http: HttpClient) { 
   // this.getLogin2();    
  }

   getLoginUsers(){ 
     return this.http.get(this.myApUrl + this.myApiUrl);
  }
 getLoginUser(usrid: any, pwl: any){ 
  return this.http.get(this.myApUrl + this.myApiUrl + '/'+usrid + '/'+ pwl);
}
getAdmin(usrid){
  return this.http.get(this.myApUrl + this.myApiUrl + '/'+usrid);
}

 getLogin2(){

   this.getLoginUsers()
   .subscribe((data: any) =>{

   this.loginUserList = data;
     /*console.log("Lista de usuarios logueados: ")
     console.log(this.loginUserList)*/
   })
  }


 getUserByName2(termino: string): any {

  let usersArr: VINVCAM_LOGIN[] = [];
  termino = termino.toLocaleLowerCase();

  for(let usuario of this.loginUserList){
    
    let userName = usuario.usrname.toLowerCase();

    if( userName.indexOf(termino) >=0 ){
      usersArr.push(usuario)
    }    
  } 

   return usersArr;

 }

}
