import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
@Injectable({
  providedIn: 'root'
})
export class SalespersonService {

  /*myApUrl  = 'http://localhost:51567/';
  myApiUrl = 'api/VINVCAM_SalesPerson/';*/

 myApUrl = environment.myApUrl;
  //myApUrl = 'http://192.168.1.100:8081/';
  myApiUrl = 'api/VINVCAM_SalesPerson/';

  constructor(private http: HttpClient) { }

  obtenerSalesPerson(){      
    return this.http.get(this.myApUrl+ this.myApiUrl)
  }
}
