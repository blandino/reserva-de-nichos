import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, observable } from 'rxjs';
import { INVCAM_Inventario } from '../models/INVCAM_Inventario';
import { environment } from '../../environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class CrudService {


//myApUrl = 'http://192.168.1.100:8081/';
myApUrl = environment.myApUrl
myApiUrl = 'api/INVCAM_Inventario/';


/*myApUrl  = 'http://localhost:51567/';
myApiUrl = 'api/INVCAM_Inventario/';*/

list: INVCAM_Inventario[];
private actualizarInfo = new BehaviorSubject<INVCAM_Inventario>({} as any);

  constructor(private http: HttpClient) { }

  guardarRegistro(inventario: INVCAM_Inventario): Observable<INVCAM_Inventario>{
    return this.http.post<INVCAM_Inventario>(this.myApUrl + this.myApiUrl, inventario);
  }

  obtenerRegistro(){
    return this.http.get(this.myApUrl + this.myApiUrl)
  }

  actualizar(inventario){
    this.actualizarInfo.next(inventario);
  }
  obtenerinfo(): Observable<INVCAM_Inventario>{
    return this.actualizarInfo.asObservable();
  }

  actualizarInventario(id: number, inventario: INVCAM_Inventario): Observable<INVCAM_Inventario>{
    return this.http.patch<INVCAM_Inventario>(this.myApUrl + this.myApiUrl + id, inventario);
   }

   eliminarReserva(id: number): Observable<INVCAM_Inventario>{  
     return this.http.delete<INVCAM_Inventario>(this.myApUrl + this.myApiUrl + id);   
   }

   filtrarFallecidos(nombreFallecido: string, prueba: number): Observable<INVCAM_Inventario>{
     return this.http.get<INVCAM_Inventario>(this.myApUrl + this.myApiUrl + nombreFallecido + '/' + prueba);
   }
  validarCodigo(codigo: string, parametroOpcional: string){
    return this.http.get(this.myApUrl + this.myApiUrl + codigo + '/'+ parametroOpcional);
  }


 
}


