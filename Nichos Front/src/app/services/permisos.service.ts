import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {INVCAM_Permisos} from '../models/INVCAM_Permisos'
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class PermisosService {

  myApUrl = environment.myApUrl;
  //myApUrl = 'http://192.168.1.100:8081/';
  myApiUrl = 'api/INVCAM_Permisos/';

  /*myApUrl  = 'http://localhost:51567/';
  myApiUrl = 'api/INVCAM_Permisos/';*/
  
  constructor( private http: HttpClient) { }

  guardarRegistro(permisos: INVCAM_Permisos): Observable<INVCAM_Permisos>{
    return this.http.post<INVCAM_Permisos>(this.myApUrl + this.myApiUrl, permisos);
  }

  eliminarRegistro(id: number): Observable<INVCAM_Permisos>{
    return this.http.delete<INVCAM_Permisos>(this.myApUrl + this.myApiUrl + id);
  }

  actualizarPermiso(id: number, permiso: INVCAM_Permisos): Observable<INVCAM_Permisos>{
    return this.http.patch<INVCAM_Permisos>(this.myApUrl + this.myApiUrl + id, permiso);
   }

   obtenerRegistro(){
    return this.http.get(this.myApUrl + this.myApiUrl)
  }
}
