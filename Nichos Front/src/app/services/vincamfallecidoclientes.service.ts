import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class VincamfallecidoclientesService {

  myApUrl = environment.myApUrl;
  myApiUrl = 'api/VINVCAM_FALLECIDOS_CLIENTES/' ;
  constructor(private http: HttpClient) { }

  obtenerFallecidosClientes(){
    return this.http.get(this.myApUrl + this.myApiUrl)
  }

  obtenerInventario(codigoInventario:string){
    return this.http.get(this.myApUrl + this.myApiUrl + codigoInventario)
  }

}
