import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, observable } from 'rxjs';
import { INVCAM_Paredes } from '../models/INVCAM_Paredes';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class ParedService {

myApUrl = environment.myApUrl;
//myApUrl = 'http://192.168.1.100:8081/';
myApiUrl = 'api/INVCAM_Paredes/';


/*myApUrl  =  'http://localhost:51567/';
myApiUrl = 'api/INVCAM_Paredes/';*/

private getInfo = new BehaviorSubject<INVCAM_Paredes>({} as any);

  constructor(private http: HttpClient) { }

  obtenerPared(){ 
    return this.http.get(this.myApUrl + this.myApiUrl);
 }

 EscucharInfo(pared){
  this.getInfo.next(pared);

}
obtenerinfo(): Observable<INVCAM_Paredes>{
  return this.getInfo.asObservable();
}
  
}
