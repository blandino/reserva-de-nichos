import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';



@Injectable({
  providedIn: 'root'
})
export class TipocolumbarioService {

  myApUrl  = environment.myApUrl;
  myApiUrl = 'api/Invcam_tipocolumbario' 

  constructor(private http:HttpClient) { }

  getTipoColumbario(){
    return this.http.get(this.myApUrl + this.myApiUrl)
  }
}
