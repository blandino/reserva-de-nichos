import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VINVCAM_LOGIN } from '../models/VINVCAM_LOGIN';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class GetusersService {

/*myApUrl  = 'http://localhost:51567/';
myApiUrl = 'api/VINVCAM_Usuarios/';*/

myApUrl = environment.myApUrl;
//myApUrl = 'http://192.168.1.100:8081/';
myApiUrl = 'api/VINVCAM_Usuarios/';

UserList: any[] = [];

  constructor(private http: HttpClient) {
    this.getUsers2();
  }


  getUsers(){ 
    return this.http.get(this.myApUrl + this.myApiUrl);
 }

 getUsers2(){

  this.getUsers()
  .subscribe((data: any) =>{

   this.UserList = data;

  })
 }

 getUserByName2(termino: string): any {

  let usersArr: VINVCAM_LOGIN[] = [];
  termino = termino.toLowerCase();

  for(let usuario of this.UserList){
   
    
    let userName = usuario.usrName.toLowerCase();

    if( userName.indexOf(termino) >=0 ){
      usersArr.push(usuario)
    }    
  } 
 
   return usersArr;

 }


}
