import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { INVCAM_Filas } from '../models/INVCAM_Filas';
import { Component } from '@angular/core';
import { environment } from '../../environments/environment.prod';



@Injectable({
  providedIn: 'root'
})
export class FilaService {

myApUrl = environment.myApUrl;
//myApUrl = 'http://192.168.1.100:8081/';
myApiUrl = 'api/INVCAM_Filas/';
myApiUrl2 = 'api/INVCAM_Etructura_Fila/'

/*myApUrl  =  'http://localhost:51567/';
myApiUrl = 'api/INVCAM_Filas/';*/

lista: INVCAM_Filas[];


  constructor(private http: HttpClient) { }
  
  obtenerFila(){ 
    return this.http.get(this.myApUrl + this.myApiUrl);
 }

 obtenerEstructuraFila(cementerio: string, etapa: string, pared: number){
  return this.http.get(this.myApUrl + this.myApiUrl2 + cementerio + '/' + etapa + '/'+ pared );
}
  
}

