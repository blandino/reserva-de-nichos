import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class LogServiceService {

  myApUrl = environment.myApUrl;
  myApiUrl = 'api/INVCAM_Log/';
  myApiUrl2 = 'api/INVCAM_Log/Get_Log';

  constructor(private http:HttpClient) { }

  GuardarUsuario(usuario : string ){
    return this.http.get(this.myApUrl + this.myApiUrl + usuario );
  }

  GetLog(){
    return this.http.get(this.myApUrl + this.myApiUrl2);
  }

  GetLogByDate(fechaInicial, fechaFinal){
    return this.http.get(this.myApUrl + this.myApiUrl + fechaInicial + "/" + fechaFinal);
  }

}
