import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class VInventarioService {
  
  myApUrl = environment.myApUrl
  myApiUrl = 'api/VINVCAM_Inventario/'

  constructor(private http: HttpClient) { }


  obtenerRegistros(){
    return this.http.get(this.myApUrl + this.myApiUrl)
  }

  obtenerInventario(codigoInventario:string){
    return this.http.get(this.myApUrl + this.myApiUrl + codigoInventario)
  }

}
