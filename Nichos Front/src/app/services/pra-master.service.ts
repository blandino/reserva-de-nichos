import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class PRAMASTERService {
  

  myAppUrl = environment.myApUrl;
 //myAppUrl = 'http://192.168.1.100:8081/';
  myApiUrl =  'api/INVCAM_PRA_MASTER1';
  myApiUrl2 =  'api/INVCAM_PRA_MASTER_CONTRACT';
  
  
 /* myAppUrl = 'http://localhost:51567/';
  myApiUrl =  'api/INVCAM_PRA_MASTER1';
  myApiUrl2 =  'api/INVCAM_PRA_MASTER_CONTRACT';*/



  datosUsuario$ = new EventEmitter<string>();

  constructor(private http: HttpClient) { }

  get_Pra_Master_info(){
  return this.http.get(this.myAppUrl + this.myApiUrl)
  }

  get_Pra_Master_info_ID(id: any){
    return this.http.get(this.myAppUrl + this.myApiUrl +'/'+ id)
    }

    get_Pra_Master_info_CONTRACT(id: any){
      return this.http.get(this.myAppUrl + this.myApiUrl2 +'/'+ id)
      }
  
}
