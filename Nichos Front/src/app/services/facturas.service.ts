import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {

  /*myApUrl = 'http://localhost:51567/';
  myApiUrl =  'api/VINVCAM_FACTURAS';*/

myApUrl = environment.myApUrl;
//myApUrl = 'http://192.168.1.100:8081/';
myApiUrl = 'api/VINVCAM_FACTURAS';


  constructor(private http: HttpClient) { }

  get_Factura(id: any){
    return this.http.get(this.myApUrl + this.myApiUrl +'/'+ id)
    }
}
