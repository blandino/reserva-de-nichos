import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';
import { Subject, observable, Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FiltrarEstadosService {

  myApUrl = environment.myApUrl;
  myApiUrl = 'api/FiltrarEstados/';
  estado: any;

  //private estado$ = new Subject; new BehaviorSubject<INVCAM_Inventario>({} as any);
   private estado$ =  new BehaviorSubject<any>({} as any);


  constructor(private http: HttpClient) { }

  validarCementerio(cementerio: string, estado:string){
    return this.http.get(this.myApUrl + this.myApiUrl + cementerio + '/' + estado);
  }

  validarEtapa(cementerio: string, estado, etapa:string){
    return this.http.get(this.myApUrl + this.myApiUrl + cementerio + '/' + estado + '/' + etapa)
  }

  validarParedes(cementerio: string, estado, etapa:string, pared: string){
    return this.http.get(this.myApUrl + this.myApiUrl + cementerio + '/' + estado + '/' + etapa + '/' + pared);
  }

  ValidarEstado(estado: any){
    this.estado = estado;
    this.estado$.next(this.estado);
  }

  getEstado$(): Observable<any>{
    return this.estado$.asObservable();
  }
}
