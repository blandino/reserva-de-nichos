import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';


@Injectable({
  providedIn: 'root'
})
export class ColumnaService {

//myApUrl = 'http://192.168.1.100:8081/';
myApUrl = environment.myApUrl
myApiUrl = 'api/INVCAM_Columnas/';
myApiUrl2 = 'api/INVCAM_Estructura_Columna/'

/*myApUrl  = 'http://localhost:51567/';
myApiUrl = 'api/INVCAM_Columnas/';
myApiUrl2 = 'api/INVCAM_Estructura_Columna/';*/


  constructor( private http: HttpClient) { }

  obtenerColumna(){ 
    return this.http.get(this.myApUrl + this.myApiUrl);
 }

 obtenerEstructuraColumna(cementerio: string, etapa: string, pared: number){
   return this.http.get(this.myApUrl + this.myApiUrl2 + cementerio + '/' + etapa + '/'+ pared );
 }

}
