import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared5CLComponent } from './pared5-cl.component';

describe('Pared5CLComponent', () => {
  let component: Pared5CLComponent;
  let fixture: ComponentFixture<Pared5CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared5CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared5CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
