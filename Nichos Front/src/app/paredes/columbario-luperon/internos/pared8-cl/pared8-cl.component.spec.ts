import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared8CLComponent } from './pared8-cl.component';

describe('Pared8CLComponent', () => {
  let component: Pared8CLComponent;
  let fixture: ComponentFixture<Pared8CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared8CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared8CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
