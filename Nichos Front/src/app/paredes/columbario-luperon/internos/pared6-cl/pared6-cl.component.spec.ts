import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared6CLComponent } from './pared6-cl.component';

describe('Pared6CLComponent', () => {
  let component: Pared6CLComponent;
  let fixture: ComponentFixture<Pared6CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared6CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared6CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
