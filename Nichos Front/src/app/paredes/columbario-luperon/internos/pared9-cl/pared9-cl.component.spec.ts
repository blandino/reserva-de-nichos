import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared9CLComponent } from './pared9-cl.component';

describe('Pared9CLComponent', () => {
  let component: Pared9CLComponent;
  let fixture: ComponentFixture<Pared9CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared9CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared9CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
