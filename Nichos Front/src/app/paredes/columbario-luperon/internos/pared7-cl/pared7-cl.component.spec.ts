import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared7CLComponent } from './pared7-cl.component';

describe('Pared7CLComponent', () => {
  let component: Pared7CLComponent;
  let fixture: ComponentFixture<Pared7CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared7CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared7CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
