import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared10CLComponent } from './pared10-cl.component';

describe('Pared10CLComponent', () => {
  let component: Pared10CLComponent;
  let fixture: ComponentFixture<Pared10CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared10CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared10CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
