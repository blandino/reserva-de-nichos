import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared11CLComponent } from './pared11-cl.component';

describe('Pared11CLComponent', () => {
  let component: Pared11CLComponent;
  let fixture: ComponentFixture<Pared11CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared11CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared11CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
