import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared1CLComponent } from './pared1-cl.component';

describe('Pared1CLComponent', () => {
  let component: Pared1CLComponent;
  let fixture: ComponentFixture<Pared1CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared1CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared1CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
