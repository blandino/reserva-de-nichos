import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared4CLComponent } from './pared4-cl.component';

describe('Pared4CLComponent', () => {
  let component: Pared4CLComponent;
  let fixture: ComponentFixture<Pared4CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared4CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared4CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
