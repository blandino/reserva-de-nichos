import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared3CLComponent } from './pared3-cl.component';

describe('Pared3CLComponent', () => {
  let component: Pared3CLComponent;
  let fixture: ComponentFixture<Pared3CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared3CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared3CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
