import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { from, observable, Subscription } from 'rxjs';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { element, promise } from 'protractor';
import { listenerCount, nextTick } from 'process';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { SlicePipe, CommonModule, getLocaleDateTimeFormat } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Http2ServerResponse } from 'http2';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalComponent } from '../../../../components/modal/modal.component';
import { CrudService } from '../../../../services/crud.service';
import { FilaService } from '../../../../services/fila.service';
import { ColumnaService } from '../../../../services/columna.service';
import { ParedService } from '../../../../services/pared.service';
import { INVCAM_Inventario } from '../../../../models/INVCAM_Inventario';
import { PRAMASTERService } from '../../../../services/pra-master.service';
import { SalespersonService } from '../../../../services/salesperson.service';
import { ParametroService } from '../../../../services/parametro.service';
import { GetusersService } from '../../../../services/getusers.service';
import { FacturasService } from '../../../../services/facturas.service';
import {Router} from '@angular/router'
import { TipocolumbarioService } from '../../../../services/tipocolumbario.service';
import { INVCAM_Fallecidos } from '../../../../models/INVCAM_Fallecidos';
import { FallecidoService } from '../../../../services/fallecido.service';
import { VInventarioService } from '../../../../services/v-inventario.service';
import { VincamfallecidoclientesService } from '../../../../services/vincamfallecidoclientes.service';


@Component({
  selector: 'app-pared2-cl',
  templateUrl: './pared2-cl.component.html',
  styleUrls: ['./pared2-cl.component.css']
})
export class Pared2CLComponent implements OnInit  {

  listaCementerios: any[] = ['Columbario Luperon'];
  codigoCementerio: any = 'CL'
  etapa: any[] = ['Externos'];
  paredCementerio: any ='2';
  

 
  listaPared: any[] = [];
  salespersonList: any[] = [];


  /////
  tipoColumbario: any[] = []
  columbarioSeleccionado: any; 
  idColumbario: any;
  ///////


  vIncamInventarioData: any[] = [];
  idFallecido: any;
  
  retornoValidacion: any;



  reservaEliminar: any [] = [];
  reservaEliminarId: any;
  paredReserva: any;
  columnaReserva: any;
  filaReserva: any;
 

  form: FormGroup;
  formNecesidad: FormGroup;
  formReservar: FormGroup;
  subscription: Subscription;

  listaFilas: any[] = [];  
  listaColumnas: any[] = [];
  listaInventario: any[] =[];

  parametroList: any[] = [];

  listaReserva: any[] = [];
  
  myLista: any[] = [];
  datos: any;
  result: any;  
  isChecked: boolean;
  estatus: any;  
  rojo: any;
  gris: any;
  verde: any;
    

  inventario : any;
  idInventario = 0;
  

   t : any;
  pared: any;
res: any[] = [];

listaMostrar: any[] = []
listaFactura: any[] = [];

listaMostrarcONTRACT: any[] = []

idCliente: any;
cliente: any;
 

  dataPanum: any;
  factura: any;
  fechavencimiento: any [] = [];

  nombreModal: any;
  tipoPlan: any [] = [];

  isAdmin: any; 
  
    constructor(private modal:         NgbModal,  
                private formBuilder:   FormBuilder,
                private crudService: CrudService,
                private filaService: FilaService,
                private columnaService: ColumnaService,
                public paredService:   ParedService,
                public toastr: ToastrService,
                private pRAMASTERService: PRAMASTERService,
                private salespersonService: SalespersonService,
                private getusersService: GetusersService,
                private parametroService: ParametroService,
                private facturasService: FacturasService,
                private router: Router,
                private TipocolumbarioService: TipocolumbarioService,
                private fallecidoService: FallecidoService,
                private vInventarioService: VInventarioService,
                private vincamfallecidoclientesService: VincamfallecidoclientesService) {  this.form = this.formBuilder.group({
                  id:0,
                  Codigo:[''],
                  Contrato: [''],
                  Panum: ['', [Validators.required]],
                  Cementerio: ['', [Validators.required]],
                  Pared: ['', [Validators.required]],
                  Fila: ['', [Validators.required]],
                  Columna: ['', [Validators.required]],
                  CodigoAnterior: [''],
                  Etapa: ['', [Validators.required]],
                  Estatus: [''],
                  IdCliente: [''],
                  Cliente: [''],
                  TipoColumbario: [''],
                  NombreFallecido: [''],
                  FechaNacimiento: [''],
                  FechaFallecimiento: [''],    
                  NombreFallecido_2: [''],
                  FechaNacimiento_2: [''],
                  FechaFallecimiento_2: [''],  
                  NombreFallecido_3: [''],
                  FechaNacimiento_3: [''],
                  FechaFallecimiento_3: [''],  
                  NombreFallecido_4: [''],
                  FechaNacimiento_4: [''],
                  FechaFallecimiento_4: [''],
                  NombreFallecido_5: [''],
                  FechaNacimiento_5: [''],
                  FechaFallecimiento_5: [''],    
                  NombreFallecido_6: [''],
                  FechaNacimiento_6: [''],
                  FechaFallecimiento_6: [''],  
                  NombreFallecido_7: [''],
                  FechaNacimiento_7: [''],
                  FechaFallecimiento_7: [''],  
                  NombreFallecido_8: [''],
                  FechaNacimiento_8: [''],
                  FechaFallecimiento_8: [''] 
                })       
                this.isAdmin = sessionStorage.getItem('admin')
                
                this.formNecesidad = this.formBuilder.group({
                  id:0,
                  Codigo:[''],                 
                  Cementerio: ['', [Validators.required]],
                  Pared: ['', [Validators.required]],
                  Fila: ['', [Validators.required]],
                  Columna: ['', [Validators.required]],
                  CodigoAnterior: [''],
                  Etapa: ['', [Validators.required]],
                  Estatus: [''],                  
                  NombreFallecido:[''],

                  Contrato: ['',[Validators.required]],
                  IdCliente: ['',[Validators.required]],
                  Cliente: ['',[Validators.required]],
                  TipoColumbario: [''],
                  FechaNacimiento: [''],
                  FechaFallecimiento: [''],
                  NombreFallecido_2: [''],
                  FechaNacimiento_2: [''],
                  FechaFallecimiento_2: [''],  
                  NombreFallecido_3: [''],
                  FechaNacimiento_3: [''],
                  FechaFallecimiento_3: [''],  
                  NombreFallecido_4: [''],
                  FechaNacimiento_4: [''],
                  FechaFallecimiento_4: [''],
                  NombreFallecido_5: [''],
                  FechaNacimiento_5: [''],
                  FechaFallecimiento_5: [''],    
                  NombreFallecido_6: [''],
                  FechaNacimiento_6: [''],
                  FechaFallecimiento_6: [''],  
                  NombreFallecido_7: [''],
                  FechaNacimiento_7: [''],
                  FechaFallecimiento_7: [''],  
                  NombreFallecido_8: [''],
                  FechaNacimiento_8: [''],
                  FechaFallecimiento_8: ['']  
                }) 

                this.formReservar = this.formBuilder.group({
                  id:0,                  
                  Vendedor: ['', [Validators.required]],                 
                  FechaVencimiento:['', [Validators.required]]
                }) 

                this.filaService.obtenerEstructuraFila(this.listaCementerios[0],
                                                       this.etapa[0],
                                                       this.paredCementerio )
                .subscribe((data: any) =>{
                  this.listaFilas = data;
                })   
                
                this.paredService.obtenerPared()
                .subscribe((data: any) => {
                
                  this.listaPared = data;
                  
                });

                this.salespersonService.obtenerSalesPerson()
                .subscribe((data: any) =>{

                  this.salespersonList = data;
              
                });
             

                this.columnaService.obtenerEstructuraColumna(this.listaCementerios[0],
                                                             this.etapa[0],
                                                              this.paredCementerio )
                .subscribe((data: any) =>{
                  this.listaColumnas = data;

                })
           
                
                this.parametroService.obtenerParametro()
                .subscribe((data: any)=>{
                  this.parametroList = data;
                  let fecha = new Date();
                  this.parametroList.forEach(element =>{
                   fecha.setDate(fecha.getDate() + element.diasReserva)                   
                   this.fechavencimiento.push(fecha) 
                  })
                });
                this.TipocolumbarioService.getTipoColumbario()
                .subscribe((data: any) =>{
                  this.tipoColumbario = data;
                  
                })
                
                

             
                
                setTimeout(()=>{
                //this.crudService.obtenerRegistro()
                //this.vInventarioService.obtenerRegistros()
                this.vincamfallecidoclientesService.obtenerFallecidosClientes()
                .subscribe((data: any) =>{    
                  this.listaInventario = data;     
                 this.listaInventario
                 this.listaFilas;
                 this.listaColumnas;
                
                 this.listaFilas.forEach(fil =>{

                  this.listaColumnas.forEach(col =>{
                 
                    const idlibre =  0;                       
                     if(this.buscarInventario(fil , col, this.paredCementerio, this.etapa) == true){
                       this.datos = fil + "-" + col.columna + "-" + this.paredCementerio;   
                  }
                  else{
                    this.datos = col + "-" + fil
                    this.myLista.push({id: idlibre,codigo:this.datos,fila: fil, columna: col, estado: this.verde, pared: this.paredCementerio, etapa:this.etapa[0]  } )
                  }                    
                  
                });  
                                
                  
                });
                  
                  this.result = this.myLista;                   
                 
                 
                
                 
                });}, 100)
            
             }
          
            
            
              ngOnInit(): void {                
                
                this.crudService.obtenerRegistro();
                this.isChecked = true;               
                this.rojo = 'ocupado';
                this.gris = 'vendido';
                this.verde = 'disponible';
                
              
  
              }

            
                        
              guardar(){
                if(this.idInventario ===0){
                  this.agregar();
                  }
                else{
                  this.editarInv();
                }   
                                    
             }
             
              agregar(){

                

                let fechaNacimiento    = this.form.get('FechaNacimiento').value;
                let fechaFallecimiento = this.form.get('FechaFallecimiento').value;

                let fechaNacimiento_2    = this.form.get('FechaNacimiento_2').value;
                let fechaFallecimiento_2 = this.form.get('FechaFallecimiento_2').value;

                let fechaNacimiento_3   = this.form.get('FechaNacimiento_3').value;
                let fechaFallecimiento_3 = this.form.get('FechaFallecimiento_3').value;

                let fechaNacimiento_4    = this.form.get('FechaNacimiento_4').value;
                let fechaFallecimiento_4 = this.form.get('FechaFallecimiento_4').value;

                let fechaNacimiento_5    = this.form.get('FechaNacimiento_5').value;
                let fechaFallecimiento_5 = this.form.get('FechaFallecimiento_5').value;

                let fechaNacimiento_6    = this.form.get('FechaNacimiento_6').value;
                let fechaFallecimiento_6 = this.form.get('FechaFallecimiento_6').value;

                let fechaNacimiento_7    = this.form.get('FechaNacimiento_7').value;
                let fechaFallecimiento_7 = this.form.get('FechaFallecimiento_7').value;

                let fechaNacimiento_8    = this.form.get('FechaNacimiento_8').value;
                let fechaFallecimiento_8 = this.form.get('FechaFallecimiento_8').value;

             



                let tipo ='PA'



                if (this.form.get('Panum').value == undefined){
                  return
                }
                if (this.form.get('NombreFallecido').value == undefined && this.form.get('NombreFallecido').value == null && this.isChecked == true){
                  this.toastr.error('Nombre fallecido vacio','Debe ingresar el nombre del fallecido')
                  return
                }           
                
                 if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value == '' && this.isChecked == true){
                  this.toastr.error('Debe ingresar la fecha de nacimiento y fallecimiento','Fechas vacias')
                  return
                } 
               

                let cementerio;
                               
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.form.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio;
                 
               }
               else{
                 cementerio = 'PC';
               }       
               
               //Fallecido 1
               if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value ==''){
                 fechaNacimiento    =  new Date();
                 fechaFallecimiento =  new Date(); 
               }

                //Fallecido 2               
                 if (this.form.get('FechaNacimiento_2').value == '' && this.form.get('FechaFallecimiento_2').value == ''){
                  fechaNacimiento_2    =  new Date();
                  fechaFallecimiento_2 =  new Date(); 
                } 

                //Fallecido 3
                 if (this.form.get('FechaNacimiento_3').value == '' && this.form.get('FechaFallecimiento_3').value == ''){
                  fechaNacimiento_3    =  new Date();
                  fechaFallecimiento_3 =  new Date(); 
                } 

                //Fallecido 4               
                 if (this.form.get('FechaNacimiento_4').value == '' && this.form.get('FechaFallecimiento_4').value == ''){
                  fechaNacimiento_4    =  new Date();
                  fechaFallecimiento_4 =  new Date(); 
                } 

                 //Fallecido 5               
                 if (this.form.get('FechaNacimiento_5').value == '' && this.form.get('FechaFallecimiento_5').value == ''){
                  fechaNacimiento_5    =  new Date();
                  fechaFallecimiento_5 =  new Date(); 
                } 

                 //Fallecido 6               
                 if (this.form.get('FechaNacimiento_6').value == '' && this.form.get('FechaFallecimiento_6').value == ''){
                  fechaNacimiento_6    =  new Date();
                  fechaFallecimiento_6 =  new Date(); 
                } 

                 //Fallecido 7               
                 if (this.form.get('FechaNacimiento_7').value == '' && this.form.get('FechaFallecimiento_7').value == ''){
                  fechaNacimiento_7    =  new Date();
                  fechaFallecimiento_7 =  new Date(); 
                } 

                 //Fallecido 8               
                 if (this.form.get('FechaNacimiento_8').value == '' && this.form.get('FechaFallecimiento_8').value == ''){
                  fechaNacimiento_8    =  new Date();
                  fechaFallecimiento_8 =  new Date(); 
                } 

                let codigo =  cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value              
                this.validarCodigo(codigo);
    
                if (this.retornoValidacion == true){
                  this.toastr.error('Error','El registro existe en la base de datos')
                  return;
                }
               
               
  
               const inventario: INVCAM_Inventario = {              
                 Codigo: cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
                 Contrato: this.form.get('Contrato').value,
                 Panum: this.form.get('Panum').value,
                 Cementerio: this.form.get('Cementerio').value,
                 Pared: this.form.get('Pared').value,
                 Fila: this.form.get('Fila').value,
                 Columna: this.form.get('Columna').value,
                 CodigoAnterior: 'N/A',
                 Etapa: this.form.get('Etapa').value,
                 Estatus: this.estatus,
                 Vendedor: 'N/A',
                 FechaReserva: new Date("2021/02/20"),
                 IdCliente: this.form.get('IdCliente').value,
                 NombreCliente: this.form.get('Cliente').value,
                 //NombreFallecido: this.form.get('NombreFallecido').value,                 
                 FechaRegistro: new Date(),
                 //FechaNacimiento: fechaNacimiento,
                 //FechaFallecimiento: fechaFallecimiento,
                 Tipo: tipo,
                 TipoColumbario:       this.form.get('TipoColumbario').value
                    
               } 
               
               const fallecido: INVCAM_Fallecidos = {
                codigoInventario      :cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
                fallecido1           :this.form.get('NombreFallecido').value, 
                fechaNacimiento1     :fechaNacimiento,
                fechaFallecimiento1  :fechaFallecimiento,

                fallecido2           :this.form.get('NombreFallecido_2').value, 
                fechaNacimiento2     :fechaNacimiento_2,
                fechaFallecimiento2  :fechaFallecimiento_2,

                fallecido3           :this.form.get('NombreFallecido_3').value,
                fechaNacimiento3     :fechaNacimiento_3,
                fechaFallecimiento3  :fechaFallecimiento_3,

                fallecido4           :this.form.get('NombreFallecido_4').value,
                fechaNacimiento4     :fechaNacimiento_4,
                fechaFallecimiento4  :fechaFallecimiento_4,
                
                fallecido5           :this.form.get('NombreFallecido_5').value,
                fechaNacimiento5     :fechaNacimiento_5,
                fechaFallecimiento5  :fechaFallecimiento_5,

                fallecido6           :this.form.get('NombreFallecido_6').value,
                fechaNacimiento6     :fechaNacimiento_6,
                fechaFallecimiento6  :fechaFallecimiento_6,

                fallecido7           :this.form.get('NombreFallecido_7').value,
                fechaNacimiento7     :fechaNacimiento_7,
                fechaFallecimiento7  :fechaFallecimiento_7,

                fallecido8           :this.form.get('NombreFallecido_8').value,
                fechaNacimiento8     :fechaNacimiento_8,
                fechaFallecimiento8  :fechaFallecimiento_8,
               }

               this.crudService.guardarRegistro(inventario).subscribe(data =>{   
                this.toastr.success('Registro Creado','El registro se guardo correctamente')
                this.listaMostrar =['']
                this.form.reset()

               
                setTimeout(()=>{
                  //this.refresh();
                }, 2000)
               }, err =>{
                
                 if(this.form.invalid){
                  err= this.toastr.error('Error','Favor completar todos los campos')
                }
                else{
                  err=  this.toastr.error('Error','Error al guardar el registro')                  

                }
               })

               if (fallecido.fallecido1 != null){

                this.fallecidoService.guardarRegistro(fallecido).subscribe(data =>{   
                  this.toastr.success('Registro Creado','El registro se guardo correctamente')
                  this.listaMostrar =['']
                  this.form.reset()
  
                 
                  setTimeout(()=>{
                   // this.refresh();
                  }, 5000)
                 }, err =>{
                  
                   if(this.form.invalid){
                    err= this.toastr.error('Error','Favor completar todos los campos')
                  }
                  else{
                    err=  this.toastr.error('Error','Error al guardar el Fallecido')                  
  
                  }
                 })

               }
              
        
               
                 
                
                if(this.form.invalid){
                  return Object.values(this.form.controls).forEach(control =>{
                    control.markAsTouched();
                  })
                }
                //

                this.refresh();
     
            }
  
            editarInv(){
             


              let fechaNacimiento    = this.form.get('FechaNacimiento').value;
              let fechaFallecimiento = this.form.get('FechaFallecimiento').value;

              let fechaNacimiento_2    = this.form.get('FechaNacimiento_2').value;
              let fechaFallecimiento_2 = this.form.get('FechaFallecimiento_2').value;

              let fechaNacimiento_3   = this.form.get('FechaNacimiento_3').value;
              let fechaFallecimiento_3 = this.form.get('FechaFallecimiento_3').value;

              let fechaNacimiento_4    = this.form.get('FechaNacimiento_4').value;
              let fechaFallecimiento_4 = this.form.get('FechaFallecimiento_4').value;

              let fechaNacimiento_5    = this.form.get('FechaNacimiento_5').value;
              let fechaFallecimiento_5 = this.form.get('FechaFallecimiento_5').value;

              let fechaNacimiento_6    = this.form.get('FechaNacimiento_6').value;
              let fechaFallecimiento_6 = this.form.get('FechaFallecimiento_6').value;

              let fechaNacimiento_7    = this.form.get('FechaNacimiento_7').value;
              let fechaFallecimiento_7 = this.form.get('FechaFallecimiento_7').value;

              let fechaNacimiento_8    = this.form.get('FechaNacimiento_8').value;
              let fechaFallecimiento_8 = this.form.get('FechaFallecimiento_8').value;

              let cementerio;
              let tipo ='PA';
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.form.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio
                 
               }
               else{
                 cementerio = 'PC';
               }    
            
                //Fallecido 1
                if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value ==''){
                  fechaNacimiento    =  new Date();
                  fechaFallecimiento =  new Date(); 
                }
 
                 //Fallecido 2               
                  if (this.form.get('FechaNacimiento_2').value == '' && this.form.get('FechaFallecimiento_2').value == ''){
                   fechaNacimiento_2    =  new Date();
                   fechaFallecimiento_2 =  new Date(); 
                 } 
 
                 //Fallecido 3
                  if (this.form.get('FechaNacimiento_3').value == '' && this.form.get('FechaFallecimiento_3').value == ''){
                   fechaNacimiento_3    =  new Date();
                   fechaFallecimiento_3 =  new Date(); 
                 } 
 
                 //Fallecido 4               
                  if (this.form.get('FechaNacimiento_4').value == '' && this.form.get('FechaFallecimiento_4').value == ''){
                   fechaNacimiento_4    =  new Date();
                   fechaFallecimiento_4 =  new Date(); 
                 } 
 
                  //Fallecido 5               
                  if (this.form.get('FechaNacimiento_5').value == '' && this.form.get('FechaFallecimiento_5').value == ''){
                   fechaNacimiento_5    =  new Date();
                   fechaFallecimiento_5 =  new Date(); 
                 } 
 
                  //Fallecido 6               
                  if (this.form.get('FechaNacimiento_6').value == '' && this.form.get('FechaFallecimiento_6').value == ''){
                   fechaNacimiento_6    =  new Date();
                   fechaFallecimiento_6 =  new Date(); 
                 } 
 
                  //Fallecido 7               
                  if (this.form.get('FechaNacimiento_7').value == '' && this.form.get('FechaFallecimiento_7').value == ''){
                   fechaNacimiento_7    =  new Date();
                   fechaFallecimiento_7 =  new Date(); 
                 } 
 
                  //Fallecido 8               
                  if (this.form.get('FechaNacimiento_8').value == '' && this.form.get('FechaFallecimiento_8').value == ''){
                   fechaNacimiento_8    =  new Date();
                   fechaFallecimiento_8 =  new Date(); 
                 } 
  
               const inventario: INVCAM_Inventario = {
                 Id: this.inventario.id,
                 Codigo: cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
                 Contrato: this.form.get('Contrato').value,
                 Panum: this.form.get('Panum').value,
                 Cementerio: this.form.get('Cementerio').value,
                 Pared: this.form.get('Pared').value,
                 Fila: this.form.get('Fila').value,
                 Columna: this.form.get('Columna').value,
                 CodigoAnterior: 'N/A',
                 Etapa: this.form.get('Etapa').value,
                 Estatus: this.estatus,
                 Vendedor: 'N/A',
                 FechaReserva: new Date("2021/02/20"),
                 IdCliente: this.form.get('IdCliente').value,
                 NombreCliente: this.form.get('Cliente').value,
                 //NombreFallecido: this.form.get('NombreFallecido').value,
                 FechaRegistro: new Date(),
                 //FechaNacimiento: this.form.get('FechaNacimiento').value,
                 //FechaFallecimiento: this.form.get('FechaFallecimiento').value,
                 Tipo: tipo,
                 TipoColumbario:       this.form.get('TipoColumbario').value
               }

               const fallecido: INVCAM_Fallecidos = {
                Id: this.idFallecido, 
                codigoInventario      :cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
                fallecido1           :this.form.get('NombreFallecido').value, 
                fechaNacimiento1     :fechaNacimiento,
                fechaFallecimiento1  :fechaFallecimiento,

                fallecido2           :this.form.get('NombreFallecido_2').value, 
                fechaNacimiento2     :fechaNacimiento_2,
                fechaFallecimiento2  :fechaFallecimiento_2,

                fallecido3           :this.form.get('NombreFallecido_3').value,
                fechaNacimiento3     :fechaNacimiento_3,
                fechaFallecimiento3  :fechaFallecimiento_3,

                fallecido4           :this.form.get('NombreFallecido_4').value,
                fechaNacimiento4     :fechaNacimiento_4,
                fechaFallecimiento4  :fechaFallecimiento_4,
                
                fallecido5           :this.form.get('NombreFallecido_5').value,
                fechaNacimiento5     :fechaNacimiento_5,
                fechaFallecimiento5  :fechaFallecimiento_5,

                fallecido6           :this.form.get('NombreFallecido_6').value,
                fechaNacimiento6     :fechaNacimiento_6,
                fechaFallecimiento6  :fechaFallecimiento_6,

                fallecido7           :this.form.get('NombreFallecido_7').value,
                fechaNacimiento7     :fechaNacimiento_7,
                fechaFallecimiento7  :fechaFallecimiento_7,

                fallecido8           :this.form.get('NombreFallecido_8').value,
                fechaNacimiento8     :fechaNacimiento_8,
                fechaFallecimiento8  :fechaFallecimiento_8,
        
               }



               this.crudService.actualizarInventario(this.idInventario, inventario).subscribe(data => {
                this.toastr.info('Registro Actualizado','El registro se actualizo correctamente')            
                
                setTimeout(()=>{
                  //this.refresh();
                }, 2000)
                //this.crudService.obtenerinfo();               
                this.idInventario = 0;                
               }, err =>{              
               
             
                 err=  this.toastr.error('Error','Error guardando registro')    
               
              }) 

              if(fallecido.fallecido1 != null){
                this.fallecidoService.actualizarFallecido(this.idFallecido, fallecido).subscribe(data =>{
                  this.toastr.info('Fallecido Actualizado', 'El fallecido se actualizo correctamente ')
  
                  /*setTimeout(()=>{
                    this.refresh();
                  }, 3000)*/
                  this.idFallecido = 0;
                }, err =>{
                  err = this.toastr.error('Error', 'Error al actualizar fallecido')
                })
              }

             

               if(this.form.invalid){
                return Object.values(this.form.controls).forEach(control =>{
                  control.markAsTouched();
                })
              }

              setTimeout(()=>{
                this.refresh();
              }, 3000)
  
            }

             guardarNecesidad(){
                if(this.idInventario ===0){
                  this.agregarNecesidad();
                  }
                else{
                  this.editarInvNecesidad();
                }   
                                    
             }

             agregarNecesidad(){
              let tipo ='N'

              let fechaNacimiento    = this.formNecesidad.get('FechaNacimiento').value;
              let fechaFallecimiento = this.formNecesidad.get('FechaFallecimiento').value;

              let fechaNacimiento_2    = this.formNecesidad.get('FechaNacimiento_2').value;
              let fechaFallecimiento_2 = this.formNecesidad.get('FechaFallecimiento_2').value;

              let fechaNacimiento_3   = this.formNecesidad.get('FechaNacimiento_3').value;
              let fechaFallecimiento_3 = this.formNecesidad.get('FechaFallecimiento_3').value;

              let fechaNacimiento_4    = this.formNecesidad.get('FechaNacimiento_4').value;
              let fechaFallecimiento_4 = this.formNecesidad.get('FechaFallecimiento_4').value;

              let fechaNacimiento_5    = this.formNecesidad.get('FechaNacimiento_5').value;
              let fechaFallecimiento_5 = this.formNecesidad.get('FechaFallecimiento_5').value;

              let fechaNacimiento_6    = this.formNecesidad.get('FechaNacimiento_6').value;
              let fechaFallecimiento_6 = this.formNecesidad.get('FechaFallecimiento_6').value;

              let fechaNacimiento_7    = this.formNecesidad.get('FechaNacimiento_7').value;
              let fechaFallecimiento_7 = this.formNecesidad.get('FechaFallecimiento_7').value;

              let fechaNacimiento_8    = this.formNecesidad.get('FechaNacimiento_8').value;
              let fechaFallecimiento_8 = this.formNecesidad.get('FechaFallecimiento_8').value;


              if (this.formNecesidad.get('NombreFallecido').value == undefined && this.formNecesidad.get('NombreFallecido').value == null && this.isChecked == true){
                this.toastr.error('Nombre fallecido vacio','Debe ingresar el nombre del fallecido')
                return
              }

              if (this.formNecesidad.get('FechaNacimiento').value == undefined || this.formNecesidad.get('FechaFallecimiento').value == undefined && this.isChecked == true){
                this.toastr.error('Debe ingresar la fecha de nacimiento y fallecimiento','Fechas vacias')
                return
              } 

              if (this.formNecesidad.get('Contrato').value == undefined || this.formNecesidad.get('Contrato').value == ''){
                this.toastr.error('Debe el ingresar el numero de factura','No.Facutra Vacio')
                return
              }

              if (this.formNecesidad.get('IdCliente').value == undefined){
                this.toastr.error('Debe el ingresar el ID del cliente','ID de cliente Vacio')
                return
              }

              if (this.formNecesidad.get('Cliente').value == undefined){
                this.toastr.error('Debe el ingresar el nombre del cliente','Nombre de cliente Vacio')
                return
              }


              let cementerio;
              
             if (this.isChecked == true){
               this.estatus = 'ocupado';
             }
             else{
               this.estatus = 'vendido';
             }

             if(this.formNecesidad.get('Cementerio').value == this.listaCementerios[0]){
               cementerio = this.codigoCementerio
               
             }
             else{
               cementerio = 'PC';
             }    
             
             //Fallecido 1
              if (this.formNecesidad.get('FechaNacimiento').value == undefined && this.formNecesidad.get('FechaFallecimiento').value == undefined){
               fechaNacimiento    =  new Date();
               fechaFallecimiento =  new Date(); 
             }

             //Fallecido 2               
             if (this.formNecesidad.get('FechaNacimiento_2').value == '' && this.formNecesidad.get('FechaFallecimiento_2').value == ''){
              fechaNacimiento_2    =  new Date();
              fechaFallecimiento_2 =  new Date(); 
            } 

            //Fallecido 3
             if (this.formNecesidad.get('FechaNacimiento_3').value == '' && this.formNecesidad.get('FechaFallecimiento_3').value == ''){
              fechaNacimiento_3    =  new Date();
              fechaFallecimiento_3 =  new Date(); 
            } 

            //Fallecido 4               
             if (this.formNecesidad.get('FechaNacimiento_4').value == '' && this.formNecesidad.get('FechaFallecimiento_4').value == ''){
              fechaNacimiento_4    =  new Date();
              fechaFallecimiento_4 =  new Date(); 
            } 

             //Fallecido 5               
             if (this.formNecesidad.get('FechaNacimiento_5').value == '' && this.formNecesidad.get('FechaFallecimiento_5').value == ''){
              fechaNacimiento_5    =  new Date();
              fechaFallecimiento_5 =  new Date(); 
            } 

             //Fallecido 6               
             if (this.formNecesidad.get('FechaNacimiento_6').value == '' && this.formNecesidad.get('FechaFallecimiento_6').value == ''){
              fechaNacimiento_6    =  new Date();
              fechaFallecimiento_6 =  new Date(); 
            } 

             //Fallecido 7               
             if (this.formNecesidad.get('FechaNacimiento_7').value == '' && this.formNecesidad.get('FechaFallecimiento_7').value == ''){
              fechaNacimiento_7    =  new Date();
              fechaFallecimiento_7 =  new Date(); 
            } 

             //Fallecido 8               
             if (this.formNecesidad.get('FechaNacimiento_8').value == '' && this.formNecesidad.get('FechaFallecimiento_8').value == ''){
              fechaNacimiento_8    =  new Date();
              fechaFallecimiento_8 =  new Date(); 
            } 

            let codigo =  cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value              
            this.validarCodigo(codigo);

            if (this.retornoValidacion == true){
              this.toastr.error('Error','El registro existe en la base de datos')
              return;
            }


             const inventario: INVCAM_Inventario = {              
               Codigo: cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
               Contrato: this.formNecesidad.get('Contrato').value,
               Panum: "Necesidad",
               Cementerio: this.formNecesidad.get('Cementerio').value,
               Pared: this.formNecesidad.get('Pared').value,
               Fila: this.formNecesidad.get('Fila').value,
               Columna: this.formNecesidad.get('Columna').value,
               CodigoAnterior: "Necesidad",
               Etapa: this.formNecesidad.get('Etapa').value,
               Estatus: this.estatus,
               Vendedor: 'Necesidad',
               FechaReserva: new Date(),
               IdCliente: this.formNecesidad.get('IdCliente').value,
               NombreCliente: this.formNecesidad.get('Cliente').value,
               //NombreFallecido: this.formNecesidad.get('NombreFallecido').value,
               FechaRegistro: new Date(),
               //FechaNacimiento: fechaNacimiento,
               //FechaFallecimiento: fechaFallecimiento,
               Tipo: tipo,
               TipoColumbario:       this.formNecesidad.get('TipoColumbario').value,
               
                        
             }

             const fallecido: INVCAM_Fallecidos = {
              codigoInventario      :cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
              fallecido1           :this.formNecesidad.get('NombreFallecido').value, 
              fechaNacimiento1     :fechaNacimiento,
              fechaFallecimiento1  :fechaFallecimiento,

              fallecido2           :this.formNecesidad.get('NombreFallecido_2').value, 
              fechaNacimiento2     :fechaNacimiento_2,
              fechaFallecimiento2  :fechaFallecimiento_2,

              fallecido3           :this.formNecesidad.get('NombreFallecido_3').value,
              fechaNacimiento3     :fechaNacimiento_3,
              fechaFallecimiento3  :fechaFallecimiento_3,

              fallecido4           :this.formNecesidad.get('NombreFallecido_4').value,
              fechaNacimiento4     :fechaNacimiento_4,
              fechaFallecimiento4  :fechaFallecimiento_4,
              
              fallecido5           :this.formNecesidad.get('NombreFallecido_5').value,
              fechaNacimiento5     :fechaNacimiento_5,
              fechaFallecimiento5  :fechaFallecimiento_5,

              fallecido6           :this.formNecesidad.get('NombreFallecido_6').value,
              fechaNacimiento6     :fechaNacimiento_6,
              fechaFallecimiento6  :fechaFallecimiento_6,

              fallecido7           :this.formNecesidad.get('NombreFallecido_7').value,
              fechaNacimiento7     :fechaNacimiento_7,
              fechaFallecimiento7  :fechaFallecimiento_7,

              fallecido8           :this.formNecesidad.get('NombreFallecido_8').value,
              fechaNacimiento8     :fechaNacimiento_8,
              fechaFallecimiento8  :fechaFallecimiento_8,
             }

                this.crudService.guardarRegistro(inventario).subscribe(data =>{   
                  this.toastr.success('Registro Creado','El registro se guardo correctamente')
                  this.listaMostrar =['']
                  this.form.reset()
                 
                  setTimeout(()=>{
                    this.refresh();    
                  }, 2000)
                   
                 }, err =>{
                   if(this.form.invalid){
                    err= this.toastr.error('Error','Favor completar todos los campos')
                  }
                  else{
                    err=  this.toastr.error('Error','El registro existe en al base de datos')                   

                  }
                  

                 })

                 if(fallecido.fallecido1 != null){

                  this.fallecidoService.guardarRegistro(fallecido).subscribe(data =>{   
                    this.toastr.success('Registro Creado','El registro se guardo correctamente')
                    this.listaMostrar =['']
                    this.form.reset()
    
                   
                    setTimeout(()=>{
                      this.refresh();
                    }, 5000)
                   }, err =>{
                    
                     if(this.form.invalid){
                      err= this.toastr.error('Error','Favor completar todos los campos')
                    }
                    else{
                      err=  this.toastr.error('Error','Error al guardar el Fallecido')                  
    
                    }
                   })

                 }

         
          }
             
              /*agregarNecesidad(){
                let tipo ='N'

                let fechaNacimiento    = this.formNecesidad.get('FechaNacimiento').value;
                let fechaFallecimiento = this.formNecesidad.get('FechaFallecimiento').value;

                let fechaNacimiento_2    = this.formNecesidad.get('FechaNacimiento_2').value;
                let fechaFallecimiento_2 = this.formNecesidad.get('FechaFallecimiento_2').value;

                let fechaNacimiento_3   = this.formNecesidad.get('FechaNacimiento_3').value;
                let fechaFallecimiento_3 = this.formNecesidad.get('FechaFallecimiento_3').value;

                let fechaNacimiento_4    = this.formNecesidad.get('FechaNacimiento_4').value;
                let fechaFallecimiento_4 = this.formNecesidad.get('FechaFallecimiento_4').value;

                let fechaNacimiento_5    = this.formNecesidad.get('FechaNacimiento_5').value;
                let fechaFallecimiento_5 = this.formNecesidad.get('FechaFallecimiento_5').value;

                let fechaNacimiento_6    = this.formNecesidad.get('FechaNacimiento_6').value;
                let fechaFallecimiento_6 = this.formNecesidad.get('FechaFallecimiento_6').value;

                let fechaNacimiento_7    = this.formNecesidad.get('FechaNacimiento_7').value;
                let fechaFallecimiento_7 = this.formNecesidad.get('FechaFallecimiento_7').value;

                let fechaNacimiento_8    = this.formNecesidad.get('FechaNacimiento_8').value;
                let fechaFallecimiento_8 = this.formNecesidad.get('FechaFallecimiento_8').value;


                if (this.formNecesidad.get('NombreFallecido').value == undefined && this.formNecesidad.get('NombreFallecido').value == null && this.isChecked == true){
                  this.toastr.error('Nombre fallecido vacio','Debe ingresar el nombre del fallecido')
                  return
                }

                if (this.formNecesidad.get('FechaNacimiento').value == undefined || this.formNecesidad.get('FechaFallecimiento').value == undefined && this.isChecked == true){
                  this.toastr.error('Debe ingresar la fecha de nacimiento y fallecimiento','Fechas vacias')
                  return
                } 

                if (this.formNecesidad.get('Contrato').value == undefined || this.formNecesidad.get('Contrato').value == ''){
                  this.toastr.error('Debe el ingresar el numero de factura','No.Facutra Vacio')
                  return
                }

                if (this.formNecesidad.get('IdCliente').value == undefined){
                  this.toastr.error('Debe el ingresar el ID del cliente','ID de cliente Vacio')
                  return
                }

                if (this.formNecesidad.get('Cliente').value == undefined){
                  this.toastr.error('Debe el ingresar el nombre del cliente','Nombre de cliente Vacio')
                  return
                }


                let cementerio;
                
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.formNecesidad.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio
                 
               }
               else{
                 cementerio = 'PC';
               }    
               
                if (this.formNecesidad.get('FechaNacimiento').value == undefined && this.formNecesidad.get('FechaFallecimiento').value == undefined){
                 fechaNacimiento    =  new Date();
                 fechaFallecimiento =  new Date(); 
               }
  
               const inventario: INVCAM_Inventario = {              
                 Codigo: cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
                 Contrato: this.formNecesidad.get('Contrato').value,
                 Panum: "Necesidad",
                 Cementerio: this.formNecesidad.get('Cementerio').value,
                 Pared: this.formNecesidad.get('Pared').value,
                 Fila: this.formNecesidad.get('Fila').value,
                 Columna: this.formNecesidad.get('Columna').value,
                 CodigoAnterior: "Necesidad",
                 Etapa: this.formNecesidad.get('Etapa').value,
                 Estatus: this.estatus,
                 Vendedor: 'Necesidad',
                 FechaReserva: new Date(),
                 IdCliente: this.formNecesidad.get('IdCliente').value,
                 NombreCliente: this.formNecesidad.get('Cliente').value,
                 NombreFallecido: this.formNecesidad.get('NombreFallecido').value,
                 FechaRegistro: new Date(),
                 FechaNacimiento: fechaNacimiento,
                 FechaFallecimiento: fechaFallecimiento,
                 Tipo: tipo,
                 TipoColumbario:       this.formNecesidad.get('TipoColumbario').value
                          
               }
                  this.crudService.guardarRegistro(inventario).subscribe(data =>{   
                    this.toastr.success('Registro Creado','El registro se guardo correctamente')
                    this.listaMostrar =['']
                    this.form.reset()
                   
                    setTimeout(()=>{
                      this.refresh();    
                    }, 2000)
                     
                   }, err =>{
                     if(this.form.invalid){
                      err= this.toastr.error('Error','Favor completar todos los campos')
                    }
                    else{
                      err=  this.toastr.error('Error','El registro existe en al base de datos')                   

                    }
                    
                    

                   })
                if(this.form.invalid){
                  return Object.values(this.form.controls).forEach(control =>{
                    control.markAsTouched();
                  })
                }
            }*/
  
            editarInvNecesidad(){


              let fechaNacimiento    = this.formNecesidad.get('FechaNacimiento').value;
              let fechaFallecimiento = this.formNecesidad.get('FechaFallecimiento').value;

              let fechaNacimiento_2    = this.formNecesidad.get('FechaNacimiento_2').value;
              let fechaFallecimiento_2 = this.formNecesidad.get('FechaFallecimiento_2').value;

              let fechaNacimiento_3   = this.formNecesidad.get('FechaNacimiento_3').value;
              let fechaFallecimiento_3 = this.formNecesidad.get('FechaFallecimiento_3').value;

              let fechaNacimiento_4    = this.formNecesidad.get('FechaNacimiento_4').value;
              let fechaFallecimiento_4 = this.formNecesidad.get('FechaFallecimiento_4').value;

              let fechaNacimiento_5    = this.formNecesidad.get('FechaNacimiento_5').value;
              let fechaFallecimiento_5 = this.formNecesidad.get('FechaFallecimiento_5').value;

              let fechaNacimiento_6    = this.formNecesidad.get('FechaNacimiento_6').value;
              let fechaFallecimiento_6 = this.formNecesidad.get('FechaFallecimiento_6').value;

              let fechaNacimiento_7    = this.formNecesidad.get('FechaNacimiento_7').value;
              let fechaFallecimiento_7 = this.formNecesidad.get('FechaFallecimiento_7').value;

              let fechaNacimiento_8    = this.formNecesidad.get('FechaNacimiento_8').value;
              let fechaFallecimiento_8 = this.formNecesidad.get('FechaFallecimiento_8').value;

              let cementerio;
              let tipo ='N'
              
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.formNecesidad.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio
                 
               }
               else{
                 cementerio = 'PC';
               }
               
                 //Fallecido 1
              if (this.formNecesidad.get('FechaNacimiento').value == undefined && this.formNecesidad.get('FechaFallecimiento').value == undefined){
                fechaNacimiento    =  new Date();
                fechaFallecimiento =  new Date(); 
              }
 
              //Fallecido 2               
              if (this.formNecesidad.get('FechaNacimiento_2').value == '' && this.formNecesidad.get('FechaFallecimiento_2').value == ''){
               fechaNacimiento_2    =  new Date();
               fechaFallecimiento_2 =  new Date(); 
             } 
 
             //Fallecido 3
              if (this.formNecesidad.get('FechaNacimiento_3').value == '' && this.formNecesidad.get('FechaFallecimiento_3').value == ''){
               fechaNacimiento_3    =  new Date();
               fechaFallecimiento_3 =  new Date(); 
             } 
 
             //Fallecido 4               
              if (this.formNecesidad.get('FechaNacimiento_4').value == '' && this.formNecesidad.get('FechaFallecimiento_4').value == ''){
               fechaNacimiento_4    =  new Date();
               fechaFallecimiento_4 =  new Date(); 
             } 
 
              //Fallecido 5               
              if (this.formNecesidad.get('FechaNacimiento_5').value == '' && this.formNecesidad.get('FechaFallecimiento_5').value == ''){
               fechaNacimiento_5    =  new Date();
               fechaFallecimiento_5 =  new Date(); 
             } 
 
              //Fallecido 6               
              if (this.formNecesidad.get('FechaNacimiento_6').value == '' && this.formNecesidad.get('FechaFallecimiento_6').value == ''){
               fechaNacimiento_6    =  new Date();
               fechaFallecimiento_6 =  new Date(); 
             } 
 
              //Fallecido 7               
              if (this.formNecesidad.get('FechaNacimiento_7').value == '' && this.formNecesidad.get('FechaFallecimiento_7').value == ''){
               fechaNacimiento_7    =  new Date();
               fechaFallecimiento_7 =  new Date(); 
             } 
 
              //Fallecido 8               
              if (this.formNecesidad.get('FechaNacimiento_8').value == '' && this.formNecesidad.get('FechaFallecimiento_8').value == ''){
               fechaNacimiento_8    =  new Date();
               fechaFallecimiento_8 =  new Date(); 
             } 
 
               
               const inventario: INVCAM_Inventario = {
                Id: this.inventario.id,
                Codigo: cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
                Contrato: this.formNecesidad.get('Contrato').value,
                Panum: '',
                Cementerio: this.formNecesidad.get('Cementerio').value,
                Pared: this.formNecesidad.get('Pared').value,
                Fila: this.formNecesidad.get('Fila').value,
                Columna: this.formNecesidad.get('Columna').value,
                CodigoAnterior: 'Necesidad',
                Etapa: this.formNecesidad.get('Etapa').value,
                Estatus: this.estatus,
                Vendedor: 'Necesidad',
                FechaReserva: new Date("2021/02/20"),
                IdCliente: this.formNecesidad.get('IdCliente').value,
                NombreCliente: this.formNecesidad.get('Cliente').value,
                //NombreFallecido: this.formNecesidad.get('NombreFallecido').value,
                FechaRegistro: new Date(),
                //FechaNacimiento: this.formNecesidad.get('FechaNacimiento').value,
                //FechaFallecimiento: this.formNecesidad.get('FechaFallecimiento').value,
                Tipo: tipo,
                TipoColumbario:       this.formNecesidad.get('TipoColumbario').value             
              }

              const fallecido: INVCAM_Fallecidos = {
                Id: this.idFallecido, 
                codigoInventario      :cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
                fallecido1           :this.formNecesidad.get('NombreFallecido').value, 
                fechaNacimiento1     :fechaNacimiento,
                fechaFallecimiento1  :fechaFallecimiento,
  
                fallecido2           :this.formNecesidad.get('NombreFallecido_2').value, 
                fechaNacimiento2     :fechaNacimiento_2,
                fechaFallecimiento2  :fechaFallecimiento_2,
  
                fallecido3           :this.formNecesidad.get('NombreFallecido_3').value,
                fechaNacimiento3     :fechaNacimiento_3,
                fechaFallecimiento3  :fechaFallecimiento_3,
  
                fallecido4           :this.formNecesidad.get('NombreFallecido_4').value,
                fechaNacimiento4     :fechaNacimiento_4,
                fechaFallecimiento4  :fechaFallecimiento_4,
                
                fallecido5           :this.formNecesidad.get('NombreFallecido_5').value,
                fechaNacimiento5     :fechaNacimiento_5,
                fechaFallecimiento5  :fechaFallecimiento_5,
  
                fallecido6           :this.formNecesidad.get('NombreFallecido_6').value,
                fechaNacimiento6     :fechaNacimiento_6,
                fechaFallecimiento6  :fechaFallecimiento_6,
  
                fallecido7           :this.formNecesidad.get('NombreFallecido_7').value,
                fechaNacimiento7     :fechaNacimiento_7,
                fechaFallecimiento7  :fechaFallecimiento_7,
  
                fallecido8           :this.formNecesidad.get('NombreFallecido_8').value,
                fechaNacimiento8     :fechaNacimiento_8,
                fechaFallecimiento8  :fechaFallecimiento_8,
              
               }

  
               this.crudService.actualizarInventario(this.idInventario, inventario).subscribe(data => {
                this.toastr.info('Registro Actualizado','El registro se actualizo correctamente')            
                
                setTimeout(()=>{
                  this.refresh();
                }, 2000)
                this.crudService.obtenerinfo();
                this.idInventario = 0;                
               })

              if(fallecido.fallecido1 != null){

                this.fallecidoService.actualizarFallecido(this.idFallecido, fallecido).subscribe(data =>{
                  this.toastr.info('Fallecido Actualizado', 'El fallecido se actualizo correctamente ')
  
                  setTimeout(()=>{
                    this.refresh();
                  }, 3000)
                  this.idFallecido = 0;
                }, err =>{
                  err = this.toastr.error('Error', 'Error al actualizar fallecido')
                })
              } 


            
  
            }

            guardarReservar(){
              if(this.idInventario ===0){
                this.agregarReservar();
                }
              else{
                this.editarInvReservar();
              }   
                                  
           }
           
            agregarReservar(){  
              let tipo ='R'
            
              
              let cementerio;
              
             if (this.isChecked == true){
               this.estatus = 'ocupado';
             }
             else{
               this.estatus = 'vendido';
             }

             if(this.form.get('Cementerio').value == this.listaCementerios[0]){
               cementerio = this.codigoCementerio
               
             }
             else{
               cementerio = this.codigoCementerio;
             } 
                  
          

             this.listaReserva.forEach(element =>{
               this.paredReserva = element.pared;
               this.columnaReserva = element.columna;
               this.filaReserva = element.fila
              
             })

             
              const inventario: INVCAM_Inventario = {              
                Codigo: cementerio +'-'+  this.paredReserva + '-' +  this.columnaReserva + '-' + this.filaReserva,
                Contrato: "Reserva",
                Panum: "Reserva",
                Cementerio: this.listaCementerios[0],
                Pared: this.paredReserva,
                Fila: this.filaReserva,
                Columna: this.columnaReserva,
                CodigoAnterior: "Reserva",
                Etapa: this.etapa[0],
                Estatus: "Reservado",
                Vendedor: this.formReservar.get('Vendedor').value,
                FechaReserva: this.formReservar.get('FechaVencimiento').value,
                IdCliente: "0",
                NombreCliente: "Reservado",
               // NombreFallecido: "Reservado",
                FechaRegistro: new Date(), 
                //FechaNacimiento: fechaNacimiento,
                //FechaFallecimiento: fechaFallecimiento,
                //FechaNacimiento: new Date(),
                //FechaFallecimiento: new Date(),
                Tipo: tipo,
                //TipoColumbario:       'Reservado'                
              }              
             
             

             this.crudService.guardarRegistro(inventario).subscribe(data =>{   
              this.toastr.success('Registro Creado','El registro se guardo correctamente')
              this.listaMostrar =['']
              //this.form.reset()
             
              setTimeout(()=>{
                this.refresh();
              }, 2000) 
             }, err =>{

              
              err=  this.toastr.error('Error','El registro existe en al base de datos')   
               /*if(this.form.invalid){
                err= this.toastr.error('Error','Favor completar todos los campos')
              }
              else{
                err=  this.toastr.error('Error','El registro existe en al base de datos')                   

              }*/
             })

             
              /*if(this.form.invalid){
                return Object.values(this.form.controls).forEach(control =>{
                  control.markAsTouched();
                })
              } */          
          }

          editarInvReservar(){
            let cementerio;
            let tipo = 'R';

             if (this.isChecked == true){
               this.estatus = 'ocupado';
             }
             else{
               this.estatus = 'vendido';
             }

             if(this.form.get('Cementerio').value == this.listaCementerios[0]){
               cementerio = this.codigoCementerio
               
             }
             else{
               cementerio = 'PC';
             }             

             const inventario: INVCAM_Inventario = {
               Id: this.inventario.id,
               Codigo: cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
               Contrato: this.form.get('Contrato').value,
               Panum: this.form.get('Panum').value,
               Cementerio: this.form.get('Cementerio').value,
               Pared: this.form.get('Pared').value,
               Fila: this.form.get('Fila').value,
               Columna: this.form.get('Columna').value,
               CodigoAnterior: "N/A",
               Etapa: this.form.get('Etapa').value,
               Estatus: this.estatus,
               Vendedor: this.formReservar.get('Vendedor').value,
               FechaReserva: this.formReservar.get('FechaVencimiento').value,
               IdCliente: this.form.get('IdCliente').value,
               NombreCliente: this.form.get('Cliente').value,
               //NombreFallecido: this.form.get('NombreFallecido').value,
               FechaRegistro: new Date(),
               //FechaNacimiento: this.form.get('FechaNacimiento').value,
               //FechaFallecimiento: this.form.get('FechaFallecimiento').value,
               Tipo: tipo,
               TipoColumbario:       'Reservado'
             }
             this.crudService.actualizarInventario(this.idInventario, inventario).subscribe(data => {
              this.toastr.info('Registro Actualizado','El registro se actualizo correctamente')            
              
              setTimeout(()=>{
                this.refresh();
              }, 2000)           
              this.crudService.obtenerinfo();
              this.idInventario = 0;                
             })

             if(this.form.invalid){
              return Object.values(this.form.controls).forEach(control =>{
                control.markAsTouched();
              })
            }

          }


          NoDisponible(){

                var fila;
                var columna;


           this.reservaEliminar.forEach(data =>{
            fila = data.fila;
            columna = data.columna
              
           })
           

           const inventario: INVCAM_Inventario = {  
             Codigo:this.codigoCementerio +'-'+ columna  + '-' + fila,
             Contrato: 'No disponible',
             Panum: 'No disponible',
             Cementerio: this.listaCementerios[0],
             Pared: this.paredCementerio[0],
             Fila: fila,
             Columna: columna,
             CodigoAnterior: 'No disponible',
             Etapa: this.etapa[0],
             Estatus: 'No disponible',
             Vendedor: 'NO DISPONIBLE',
             FechaReserva: new Date("2021/02/20"),
             IdCliente: 'No disponible',
             NombreCliente: 'No disponible',
            // NombreFallecido: 'No disponible',                 
             FechaRegistro: new Date(),
             //FechaNacimiento:  new Date(),
             //FechaFallecimiento:  new Date(),
             Tipo: 'No disponible',
             //TipoColumbario:       'No disponible'
           }    
          
           if(confirm('Seguro que desea marcar el registro no disponible')){

            this.crudService.guardarRegistro(inventario).subscribe(data =>{   
              this.toastr.success('Registro Creado','El registro se guardo correctamente')
              this.listaMostrar =['']
              this.form.reset()

             
              setTimeout(()=>{
                this.refresh();
               
              }, 2000)
               
             }, err =>{
                err=  this.toastr.error('Error','Error marcando como disponible')                
             })
           }
              
        }
             
           
              refresh(){
                window.location.reload();
               }
            
              openSM(contenido, contenidoReservar ,data)
              {  
                if (this.isAdmin == 'false'){
                  return
                }

                this.tipoPlan.push(data);
                this.tipoPlan.forEach(element =>{
                  this.t = element.Tipo
                
                })               

                
                if(this.t =='PA'){
                  this.nombreModal = contenido;                 

                }
                if(this.t =='N'){
                 this.nombreModal = contenidoReservar;                       

               }
                
                this.form.reset()          
                                                        
               
                this.subscription = this.crudService.obtenerinfo().subscribe(data =>{
               
                  this.inventario =  data;
                  this.dataPanum = this.inventario.panum 
                  this.factura = this.inventario.contrato
                  this.idColumbario;
                  if (this.inventario.estado == 'ocupado'){
                    this.isChecked = true;
                  }
                  else{
                    this.isChecked = false;
                  }              
                
                  //this.vInventarioService.obtenerInventario(this.inventario.codigo).subscribe((data: any) =>{       
                    this.vincamfallecidoclientesService.obtenerInventario(this.inventario.codigo).subscribe((data: any) =>{                
                  this.vIncamInventarioData = data;
             
                
                  if (data.idFallecido == null){
                    this.idFallecido = 0
                  }
                  else {
                    this.idFallecido = data.idFallecido
                  }


                
        

                  let fallecido1 = data.fallecido1;
                  let fechaNacimiento1 = data.fechaNacimiento1
                  let fechaFallecimiento1 = data.fechaFallecimiento1

                  let fallecido2 = data.fallecido2;
                  let fechaNacimiento2 = data.fechaNacimiento2;
                  let fechaFallecimiento2 = data.fechaFallecimiento2;

                  let fallecido3 = data.fallecido3;
                  let fechaNacimiento3 = data.fechaNacimiento3;
                  let fechaFallecimiento3 = data.fechaFallecimiento3;

                  let fallecido4 = data.fallecido4;
                  let fechaNacimiento4 = data.fechaNacimiento4
                  let fechaFallecimiento4 = data.fechaFallecimiento4

                  let fallecido5 = data.fallecido5;
                  let fechaNacimiento5 = data.fechaNacimiento5
                  let fechaFallecimiento5 = data.fechaFallecimiento5

                  let fallecido6 = data.fallecido6;
                  let fechaNacimiento6 = data.fechaNacimiento6
                  let fechaFallecimiento6 = data.fechaFallecimiento6

                  let fallecido7 = data.fallecido7;
                  let fechaNacimiento7 = data.fechaNacimiento7
                  let fechaFallecimiento7 = data.fechaFallecimiento7

                  let fallecido8 = data.fallecido8;
                  let fechaNacimiento8 = data.fechaNacimiento8
                  let fechaFallecimiento8 = data.fechaFallecimiento8

                  this.columbarioSeleccionado = this.inventario.tipoColumbario; 
                                 

                  if(this.t =='PA'){
                                 
                    this.form.patchValue({
                      Codigo: this.inventario.codigo,
                      Contrato: this.inventario.contrato,   
                      Panum: this.inventario.panum,
                      Cementerio: this.inventario.cementerio,
                      Pared: this.inventario.pared,
                      Fila: this.inventario.fila,
                      Columna: this.inventario.columna,
                      CodigoAnterior: "N/A",
                      Etapa: this.inventario.etapa,
                     
                      Estatus: this.isChecked,
                      NombreFallecido: this.inventario.nombreFallecido,
                      FechaNacimiento: this.inventario.fechaNacimiento,
                      FechaFallecimiento:this.inventario.fechaFallecimiento,
                      //TipoColumbario:   this.idColumbario                      
                      TipoColumbario:    this.inventario.tipoColumbario,
                      NombreFallecido_2: fallecido2,
                      FechaNacimiento_2: fechaNacimiento2,
                      FechaFallecimiento_2: fechaFallecimiento2,  
                      NombreFallecido_3: fallecido3,
                      FechaNacimiento_3: fechaNacimiento3,
                      FechaFallecimiento_3: fechaFallecimiento3,  
                      NombreFallecido_4: fallecido4,
                      FechaNacimiento_4: fechaNacimiento4,
                      FechaFallecimiento_4: fechaFallecimiento4,
                      NombreFallecido_5: fallecido5,
                      FechaNacimiento_5: fechaNacimiento5,
                      FechaFallecimiento_5: fechaFallecimiento5,    
                      NombreFallecido_6: fallecido6,
                      FechaNacimiento_6: fechaNacimiento6,
                      FechaFallecimiento_6: fechaFallecimiento6,  
                      NombreFallecido_7: fallecido7,
                      FechaNacimiento_7: fechaNacimiento7,
                      FechaFallecimiento_7: fechaFallecimiento7,  
                      NombreFallecido_8: fallecido8,
                      FechaNacimiento_8: fechaNacimiento8,
                      FechaFallecimiento_8: fechaFallecimiento8  

                      
                    })
                  }

                  if(this.t =='N'){

                    this.formNecesidad.patchValue({                     
                      Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
                      Contrato: this.inventario.contrato,   
                      Cementerio: this.listaCementerios[0],
                      Pared: this.inventario.pared,
                      Fila: this.inventario.fila,
                      Columna: this.inventario.columna,
                      CodigoAnterior: "N/A",
                      Etapa:this.inventario.etapa,                     
                      Estatus:   this.isChecked,
                      IdCliente: this.inventario.idCliente,
                      Cliente: this.inventario.nombreCliente,           
                      NombreFallecido: this.inventario.nombreFallecido,
                      FechaNacimiento: this.inventario.fechaNacimiento,
                      FechaFallecimiento:this.inventario.fechaFallecimiento,
                      TipoColumbario:    this.inventario.tipoColumbario,
                      NombreFallecido_2: fallecido2,
                      FechaNacimiento_2: fechaNacimiento2,
                      FechaFallecimiento_2: fechaFallecimiento2,  
                      NombreFallecido_3: fallecido3,
                      FechaNacimiento_3: fechaNacimiento3,
                      FechaFallecimiento_3: fechaFallecimiento3,  
                      NombreFallecido_4: fallecido4,
                      FechaNacimiento_4: fechaNacimiento4,
                      FechaFallecimiento_4: fechaFallecimiento4,
                      NombreFallecido_5: fallecido5,
                      FechaNacimiento_5: fechaNacimiento5,
                      FechaFallecimiento_5: fechaFallecimiento5,    
                      NombreFallecido_6: fallecido6,
                      FechaNacimiento_6: fechaNacimiento6,
                      FechaFallecimiento_6: fechaFallecimiento6,  
                      NombreFallecido_7: fallecido7,
                      FechaNacimiento_7: fechaNacimiento7,
                      FechaFallecimiento_7: fechaFallecimiento7,  
                      NombreFallecido_8: fallecido8,
                      FechaNacimiento_8: fechaNacimiento8,
                      FechaFallecimiento_8: fechaFallecimiento8  
                    })                               
   
                  }

                  
                
                  
                })

                if(  this.inventario.estado == 'vendido'){
                  this.form.patchValue({
                    Codigo: this.inventario.codigo,
                    Contrato: this.inventario.contrato,   
                    Panum: this.inventario.panum,
                    Cementerio: this.inventario.cementerio,
                    Pared: this.inventario.pared,
                    Fila: this.inventario.fila,
                    Columna: this.inventario.columna,
                    CodigoAnterior: "N/A",
                    Etapa: this.inventario.etapa,
                   
                    Estatus: this.isChecked,
                    NombreFallecido: this.inventario.nombreFallecido,
                    FechaNacimiento: this.inventario.fechaNacimiento,
                    FechaFallecimiento:this.inventario.fechaFallecimiento,                                        
                    TipoColumbario:    this.inventario.tipoColumbario,   
                    NombreFallecido_2: null,
                      FechaNacimiento_2: new Date(),
                      FechaFallecimiento_2: new Date(),  
                      NombreFallecido_3: null,
                      FechaNacimiento_3: new Date(),
                      FechaFallecimiento_3: new Date(),  
                      NombreFallecido_4: null,
                      FechaNacimiento_4: new Date(),
                      FechaFallecimiento_4: new Date(),
                      NombreFallecido_5: null,
                      FechaNacimiento_5:new Date(),
                      FechaFallecimiento_5: new Date(),    
                      NombreFallecido_6: null,
                      FechaNacimiento_6: new Date(),
                      FechaFallecimiento_6: new Date(),  
                      NombreFallecido_7: null,
                      FechaNacimiento_7: new Date(),
                      FechaFallecimiento_7: new Date(),  
                      NombreFallecido_8: null,
                      FechaNacimiento_8: new Date(),
                      FechaFallecimiento_8: new Date()  
                  })
                  
                }

                  
                  this.idInventario = this.inventario.id                                   
                
                })                
                
               
                this.modal.open(this.nombreModal, { size: 'xl'});    
                
                this.editar(data)           
                this.obtenerDatosPRA(this.dataPanum);  
                this.obtenerFactura(this.factura)
                         
              }

              openSM2(contenido)
              {    
                                
                this.subscription = this.crudService.obtenerinfo().subscribe(data =>{
                  
                  this.inventario =  data;      
                  this.dataPanum = this.inventario.panum 
                  

                  if (this.inventario.estado == 'ocupado'){
                    this.isChecked = true;
                  }
                  else{
                    this.isChecked = false;
                  }
                
                this.isChecked = true;
                
                 this.form.patchValue({
                  Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
                  Cementerio: this.listaCementerios[0],
                  Pared: this.inventario.pared,
                  Fila: this.inventario.fila,
                  Columna: this.inventario.columna,
                  CodigoAnterior: "N/A",
                  Etapa: this.inventario.etapa,
                  Estatus: this.isChecked,
                  IdCliente: this.inventario.idCliente,
                  Cliente: this.inventario.nombreCliente,
                  NombreFallecido: this.inventario.nombreFallecido,
                  FechaNacimiento: this.inventario.fechaNacimiento,
                  FechaFallecimiento:this.inventario.fechaFallecimiento,
                  //TipoClumbario: this.inventario.tipoColumbario
                 
                })            
                  

                  this.idInventario = this.inventario.id                                   
                  
                })
                 this.form.get('Panum').reset();
                 this.form.get('NombreFallecido').reset();
                  
                 this.modal.open(contenido, { size: 'xl'});  
                 this.listaMostrar =[''];               
                 
              }   
              
              openSM3(contenidoReservar)
              {    
                               
                
                this.subscription = this.crudService.obtenerinfo().subscribe(data =>{
                  
                  this.inventario =  data;      
                  this.dataPanum = this.inventario.panum 
                  

                  if (this.inventario.estado == 'ocupado'){
                    this.isChecked = true;
                  }
                  else{
                    this.isChecked = false;
                  }
                this.isChecked = true;
                
                 this.formNecesidad.patchValue({
                  Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
                  Cementerio: this.listaCementerios[0],
                  Pared: this.inventario.pared,
                  Fila: this.inventario.fila,
                  Columna: this.inventario.columna,
                  CodigoAnterior: "CristoR",
                  Etapa: this.inventario.etapa,
                  Estatus: this.isChecked,   
                  IdCliente: this.inventario.idCliente,
                  Cliente: this.inventario.nombreCliente,           
                  FechaNacimiento: this.inventario.fechaNacimiento,
                  FechaFallecimiento:this.inventario.fechaFallecimiento
                 
                })            
                  

                  this.idInventario = this.inventario.id                                   
                  
                })
                 this.formNecesidad.get('NombreFallecido').reset();
                  
                 this.modal.open(contenidoReservar, { size: 'xl'});  

                 //
                    this.obtenerFactura(this.formNecesidad.get('Contrato'));
                 //
                 this.listaMostrar =[''];               
                 
              } 

              
              openReservar(reservar,data){
                this.modal.open(reservar, {size:'lg'})
                this.listaReserva.push(data);    
              }

              openPlan(plan, data){                
                this.modal.open(plan, {size:'sm'})                 
                this.editar(data)
              }

  
              buscarInventario(fila:any, columna:any, pared:any, etapa:any) : boolean{
                let flag: boolean;
                flag =false;
                let cementerio: any;
                
                this.listaInventario.forEach (element=>{                 
  
                  if(element.fila == fila && element.columna == columna && element.pared== pared && element.cementerio == this.listaCementerios[0] && element.etapa == etapa) {
                    flag = true;
                    if(element.cementerio == this.listaCementerios[0]){
                      cementerio = this.codigoCementerio
                    }
                    else{
                      cementerio ='PC'
                    }                    
  
                    this.datos = cementerio + '-' + element.pared + '-' + element.columna + '-' + element.fila;                    
  
                    this.myLista.push({
                     // id: element.id,
                      id: element.idInventario,
                      codigo: this.datos,
                      contrato: element.contrato,
                      panum: element.panum,
                      pared: element.pared,
                      fila: element.fila, 
                      columna: element.columna, 
                      codigoAnterior: element.codigoAnterior,
                      etapa: element.etapa,
                      estado: element.estatus, 
                      cementerio: element.cementerio,
                      //nombreFallecido: element.nombreFallecido,
                      nombreFallecido: element.fallecido1,
                      vendedor: element.vendedor,
                      //fechaNacimiento: element.fechaNacimiento,
                      //fechaFallecimiento: element.fechaFallecimiento,
                      fechaNacimiento: element.fechaNacimiento1,
                      fechaFallecimiento: element.fechaFallecimiento1,
                      Tipo: element.tipo,
                      idCliente: element.idCliente,
                      nombreCliente: element.nombreCliente,
                      tipoColumbario: element.tipoColumbario,

                    })
                  }
                  
                });
         
                
                return flag
              }
  
  
              editar(inventario){                
                
                this.crudService.actualizar(inventario); 
                this.listaMostrar =[''];  
                this.reservaEliminar.push(inventario);   
                this.reservaEliminar.forEach(data =>{
                  this.reservaEliminarId = data.id;
                })
              }  
              obtenerDatosPRA(dato:any){                

               this.pRAMASTERService.get_Pra_Master_info_ID(dato).subscribe((x: any) =>{
                this.res = x   
                
                this.listaMostrar.push(this.res)      
                });                  
                this.listaMostrar =[];
                
                return this.res;   

              }    

              obtenerFactura(dato:any){                

                this.facturasService.get_Factura(dato).subscribe((x: any) =>{       
                 
                 this.listaFactura.push(x);  
               
                 });                  
                 this.listaFactura =[];
                 
                 return this.listaFactura;   
 
               } 


              
             /* obtenerDatosPRAcONTRACT(dato:any){
                
                this.pRAMASTERService.get_Pra_Master_info_CONTRACT(dato).subscribe((x: any) =>{
                 this.res = x   
                 
                 this.listaMostrarcONTRACT.push(this.res)               
                 
                 });                  
                 this.listaMostrarcONTRACT =[];
                
                 return this.res;   
 
               }*/

               eliminarReserva(id: number){

                if (id == 0){
                  return;
                }

                if(confirm('Esta seguro que desea Eliminar el registro?')){
                  this.crudService.eliminarReserva(id).subscribe(data =>{
                    this.toastr.warning('Registro Eliminado', 'Registro eliminado del sistema');

                    setTimeout(()=>{
                      this.refresh();                                   
                      
                    }, 2000)
           
                  })
           
                }           
              }            

               obtenerFecha(){
                 let fecha = new Date();
                 this.parametroList.forEach(element =>{
                   element.diasReserva
                   fecha.setDate(fecha.getDate() + element.diasReserva) 
                   
                   this.fechavencimiento.push(fecha)                  
                 })
               }

               validarCodigo(codigo){
                let parametroOpcional = 'A';             

                this.crudService.validarCodigo(codigo, parametroOpcional)
                .subscribe((data: any) =>{       
                  if (data == true){
                    this.retornoValidacion = true;
                    this.toastr.error('Error','El registro existe en la base de datos')
                    return;
                  }
                })
               }

               get contratoValido(){
                 return this.form.get('Contrato').invalid && this.form.get('Contrato').touched
               }
               get paValido(){
                return this.form.get('Panum').invalid && this.form.get('Panum').touched
              }
              get cementerioValido(){
                return this.form.get('Cementerio').invalid && this.form.get('Cementerio').touched
              }
              get etapaValido(){
                return this.form.get('Etapa').invalid && this.form.get('Etapa').touched
              }
              get paredValido(){
                return this.form.get('Pared').invalid && this.form.get('Pared').touched
              }
              get filaValido(){
                return this.form.get('Fila').invalid && this.form.get('Fila').touched
              }
              get columnaValido(){
                return this.form.get('Columna').invalid && this.form.get('Columna').touched
              }
              get nombreFallecidoValido(){
                return this.form.get('NombreFallecido').invalid &&  this.form.get('NombreFallecido').touched
              }
            
            
            }
            
