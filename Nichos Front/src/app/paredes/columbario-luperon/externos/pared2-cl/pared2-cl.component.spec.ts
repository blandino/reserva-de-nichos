import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared2CLComponent } from './pared2-cl.component';

describe('Pared2CLComponent', () => {
  let component: Pared2CLComponent;
  let fixture: ComponentFixture<Pared2CLComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared2CLComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared2CLComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
