import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared1ISComponent } from './pared1-is.component';

describe('Pared1ISComponent', () => {
  let component: Pared1ISComponent;
  let fixture: ComponentFixture<Pared1ISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared1ISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared1ISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
