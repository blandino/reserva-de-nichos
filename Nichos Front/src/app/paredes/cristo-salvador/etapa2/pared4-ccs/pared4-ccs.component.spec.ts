import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared4CCSComponent } from './pared4-ccs.component';

describe('Pared4CCSComponent', () => {
  let component: Pared4CCSComponent;
  let fixture: ComponentFixture<Pared4CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared4CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared4CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
