import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared7CCSComponent } from './pared7-ccs.component';

describe('Pared7CCSComponent', () => {
  let component: Pared7CCSComponent;
  let fixture: ComponentFixture<Pared7CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared7CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared7CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
