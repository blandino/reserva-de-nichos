import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared11CCSComponent } from './pared11-ccs.component';

describe('Pared11CCSComponent', () => {
  let component: Pared11CCSComponent;
  let fixture: ComponentFixture<Pared11CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared11CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared11CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
