import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared9CCSComponent } from './pared9-ccs.component';

describe('Pared9CCSComponent', () => {
  let component: Pared9CCSComponent;
  let fixture: ComponentFixture<Pared9CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared9CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared9CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
