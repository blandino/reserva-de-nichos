import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared10CCSComponent } from './pared10-ccs.component';

describe('Pared10CCSComponent', () => {
  let component: Pared10CCSComponent;
  let fixture: ComponentFixture<Pared10CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared10CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared10CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
