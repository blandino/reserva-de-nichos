import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared8CCSComponent } from './pared8-ccs.component';

describe('Pared8CCSComponent', () => {
  let component: Pared8CCSComponent;
  let fixture: ComponentFixture<Pared8CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared8CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared8CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
