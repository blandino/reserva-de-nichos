import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared12CCSComponent } from './pared12-ccs.component';

describe('Pared12CCSComponent', () => {
  let component: Pared12CCSComponent;
  let fixture: ComponentFixture<Pared12CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared12CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared12CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
