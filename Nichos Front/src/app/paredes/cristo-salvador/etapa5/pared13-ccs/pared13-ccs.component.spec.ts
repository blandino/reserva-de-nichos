import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared13CCSComponent } from './pared13-ccs.component';

describe('Pared13CCSComponent', () => {
  let component: Pared13CCSComponent;
  let fixture: ComponentFixture<Pared13CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared13CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared13CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
