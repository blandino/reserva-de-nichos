import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared6CCSComponent } from './pared6-ccs.component';

describe('Pared6CCSComponent', () => {
  let component: Pared6CCSComponent;
  let fixture: ComponentFixture<Pared6CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared6CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared6CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
