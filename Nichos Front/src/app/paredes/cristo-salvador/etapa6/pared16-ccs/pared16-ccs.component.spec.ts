import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared16CCSComponent } from './pared16-ccs.component';

describe('Pared16CCSComponent', () => {
  let component: Pared16CCSComponent;
  let fixture: ComponentFixture<Pared16CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared16CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared16CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
