import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared14CCSComponent } from './pared14-ccs.component';

describe('Pared14CCSComponent', () => {
  let component: Pared14CCSComponent;
  let fixture: ComponentFixture<Pared14CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared14CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared14CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
