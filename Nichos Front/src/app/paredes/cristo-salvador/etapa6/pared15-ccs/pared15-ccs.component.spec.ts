import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared15CCSComponent } from './pared15-ccs.component';

describe('Pared15CCSComponent', () => {
  let component: Pared15CCSComponent;
  let fixture: ComponentFixture<Pared15CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared15CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared15CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
