import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared3CCSComponent } from './pared3-ccs.component';

describe('Pared3CCSComponent', () => {
  let component: Pared3CCSComponent;
  let fixture: ComponentFixture<Pared3CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared3CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared3CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
