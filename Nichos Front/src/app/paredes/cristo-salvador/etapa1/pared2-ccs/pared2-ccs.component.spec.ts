import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared2CCSComponent } from './pared2-ccs.component';

describe('Pared2CCSComponent', () => {
  let component: Pared2CCSComponent;
  let fixture: ComponentFixture<Pared2CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared2CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared2CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
