import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared1CCSComponent } from './pared1-ccs.component';

describe('Pared1CCSComponent', () => {
  let component: Pared1CCSComponent;
  let fixture: ComponentFixture<Pared1CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared1CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared1CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
