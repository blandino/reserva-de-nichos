import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared5CCSComponent } from './pared5-ccs.component';

describe('Pared5CCSComponent', () => {
  let component: Pared5CCSComponent;
  let fixture: ComponentFixture<Pared5CCSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared5CCSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared5CCSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
