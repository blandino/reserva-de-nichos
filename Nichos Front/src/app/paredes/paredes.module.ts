import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { ParedesRoutingModule } from './paredes-routing.module';

//Columbario Ozama
//Etapa
//import { Pared1COComponent } from './columbario-ozama/Etapa1/pared1-co/pared1-co.component';

///Columbario Santiago
//Etapa1
import { Pared1CSComponent } from '../paredes/columbario-santiago/etapa1/pared1-cs/pared1-cs.component';
import { Pared2CSComponent } from '../paredes/columbario-santiago/etapa1/pared2-cs/pared2-cs.component';
import { Pared3CSComponent } from '../paredes/columbario-santiago/etapa1/pared3-cs/pared3-cs.component';
import { Pared4CSComponent } from '../paredes/columbario-santiago/etapa1/pared4-cs/pared4-cs.component';
import { Pared5CSComponent } from '../paredes/columbario-santiago/etapa1/pared5-cs/pared5-cs.component';
//Isla Central
import { Pared6CSComponent } from '../paredes/columbario-santiago/isla-central/pared6-cs/pared6-cs.component';
import { Pared7CSComponent } from '../paredes/columbario-santiago/isla-central/pared7-cs/pared7-cs.component';


//Columbarios Luperon
//Externos
/*import { Pared1CLComponent } from '../paredes/columbario-luperon/externos/pared1-cl/pared1-cl.component';
import { Pared2CLComponent } from '../paredes/columbario-luperon/externos/pared2-cl/pared2-cl.component';
import { Pared3CLComponent } from '../paredes/columbario-luperon/externos/pared3-cl/pared3-cl.component';
import { Pared4CLComponent } from '../paredes/columbario-luperon/externos/pared4-cl/pared4-cl.component';
import { Pared5CLComponent } from '../paredes/columbario-luperon/internos/pared5-cl/pared5-cl.component';
import { Pared6CLComponent } from '../paredes/columbario-luperon/internos/pared6-cl/pared6-cl.component';
import { Pared7CLComponent } from '../paredes/columbario-luperon/internos/pared7-cl/pared7-cl.component';
import { Pared8CLComponent } from '../paredes/columbario-luperon/internos/pared8-cl/pared8-cl.component';
import { Pared9CLComponent } from '../paredes/columbario-luperon/internos/pared9-cl/pared9-cl.component';
import { Pared10CLComponent } from '../paredes/columbario-luperon/familiar/pared10-cl/pared10-cl.component';
import { Pared11CLComponent } from '../paredes/columbario-luperon/familiar/pared11-cl/pared11-cl.component';*/

//Ingenio Santiago
//Etapa1
//import { Pared1ISComponent } from '../paredes/ingenio-santiago/etapa1/pared1-is/pared1-is.component';

//Maximo Gomez
//Etapa1
//import { Pared1MGComponent } from '../paredes/maximo-gomez/etapa1/pared1-mg/pared1-mg.component';
//Etapa2
//import { Pared2MGComponent } from '../paredes/maximo-gomez/etapa2/pared2-mg/pared2-mg.component';

//Cristo Salvador
//Etapa1
//import { Pared1CCSComponent } from '../paredes/cristo-salvador/etapa1/pared1-ccs/pared1-ccs.component';
//import { Pared2CCSComponent } from '../paredes/cristo-salvador/etapa1/pared2-ccs/pared2-ccs.component';
//import { Pared3CCSComponent } from '../paredes/cristo-salvador/etapa1/pared3-ccs/pared3-ccs.component';
//Etapa 2
//import { Pared4CCSComponent } from '../paredes/cristo-salvador/etapa2/pared4-ccs/pared4-ccs.component';
//Etapa3
//import { Pared5CCSComponent } from '../paredes/cristo-salvador/etapa3/pared5-ccs/pared5-ccs.component';
//Etapa4
//import { Pared6CCSComponent } from '../paredes/cristo-salvador/etapa4/pared6-ccs/pared6-ccs.component';
//Etapa 5
//import { Pared7CCSComponent } from '../paredes/cristo-salvador/etapa5/pared7-ccs/pared7-ccs.component';
//import { Pared8CCSComponent } from '../paredes/cristo-salvador/etapa5/pared8-ccs/pared8-ccs.component';
//import { Pared9CCSComponent } from '../paredes/cristo-salvador/etapa5/pared9-ccs/pared9-ccs.component';
//import { Pared10CCSComponent } from '../paredes/cristo-salvador/etapa5/pared10-ccs/pared10-ccs.component';
//import { Pared11CCSComponent } from '../paredes/cristo-salvador/etapa5/pared11-ccs/pared11-ccs.component';
//import { Pared12CCSComponent } from '../paredes/cristo-salvador/etapa5/pared12-ccs/pared12-ccs.component';
//import { Pared13CCSComponent } from '../paredes/cristo-salvador/etapa5/pared13-ccs/pared13-ccs.component';

//Etapa 6
//import { Pared14CCSComponent } from '../paredes/cristo-salvador/etapa6/pared14-ccs/pared14-ccs.component';
//import { Pared15CCSComponent } from '../paredes/cristo-salvador/etapa6/pared15-ccs/pared15-ccs.component';
//import { Pared16CCSComponent } from '../paredes/cristo-salvador/etapa6/pared16-ccs/pared16-ccs.component';







@NgModule({
  declarations: [//Pared1CCSComponent, 
                //Pared2CCSComponent, 
                //Pared3CCSComponent, 
                //Pared4CCSComponent, 
               // Pared5CCSComponent, 
                //Pared6CCSComponent, 
                //Pared7CCSComponent, 
                //Pared8CCSComponent, 
                //Pared9CCSComponent, 
                //Pared10CCSComponent, 
                //Pared11CCSComponent, 
                //Pared12CCSComponent, 
                //Pared13CCSComponent, 
                //Pared14CCSComponent, 
                //Pared15CCSComponent, 
                //Pared16CCSComponent
                //lPared1COComponent,
                /*Pared1CSComponent,
                Pared2CSComponent,
                Pared3CSComponent,
                Pared4CSComponent,
                Pared5CSComponent,
                Pared6CSComponent,
                Pared7CSComponent,*/
                /*Pared1CLComponent,
                Pared2CLComponent,
                Pared3CLComponent,
                Pared4CLComponent,
                Pared5CLComponent,
                Pared6CLComponent,
                Pared7CLComponent,
                Pared8CLComponent,
                Pared9CLComponent,
                Pared10CLComponent,
                Pared11CLComponent,*/
               // Pared1ISComponent,
              //  Pared1MGComponent,
               // Pared2MGComponent
                              ],
  imports: [
    CommonModule,
    ParedesRoutingModule
  ]
})
export class ParedesModule { }
