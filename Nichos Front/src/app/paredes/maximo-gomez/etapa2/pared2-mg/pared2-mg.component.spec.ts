import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared2MGComponent } from './pared2-mg.component';

describe('Pared2MGComponent', () => {
  let component: Pared2MGComponent;
  let fixture: ComponentFixture<Pared2MGComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared2MGComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared2MGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
