import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared1MGComponent } from './pared1-mg.component';

describe('Pared1MGComponent', () => {
  let component: Pared1MGComponent;
  let fixture: ComponentFixture<Pared1MGComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared1MGComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared1MGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
