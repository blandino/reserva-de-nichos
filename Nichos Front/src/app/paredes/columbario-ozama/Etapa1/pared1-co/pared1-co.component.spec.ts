import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared1COComponent } from './pared1-co.component';

describe('Pared1COComponent', () => {
  let component: Pared1COComponent;
  let fixture: ComponentFixture<Pared1COComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared1COComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared1COComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
