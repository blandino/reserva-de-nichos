import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared3CSComponent } from './pared3-cs.component';

describe('Pared3CSComponent', () => {
  let component: Pared3CSComponent;
  let fixture: ComponentFixture<Pared3CSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared3CSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared3CSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
