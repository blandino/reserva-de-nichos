import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared4CSComponent } from './pared4-cs.component';

describe('Pared4CSComponent', () => {
  let component: Pared4CSComponent;
  let fixture: ComponentFixture<Pared4CSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared4CSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared4CSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
