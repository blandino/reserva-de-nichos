import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared5CSComponent } from './pared5-cs.component';

describe('Pared5CSComponent', () => {
  let component: Pared5CSComponent;
  let fixture: ComponentFixture<Pared5CSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared5CSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared5CSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
