import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared2CSComponent } from './pared2-cs.component';

describe('Pared2CSComponent', () => {
  let component: Pared2CSComponent;
  let fixture: ComponentFixture<Pared2CSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared2CSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared2CSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
