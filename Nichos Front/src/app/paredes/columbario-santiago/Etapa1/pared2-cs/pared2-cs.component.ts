import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { from, observable, Subscription } from 'rxjs';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { element, promise } from 'protractor';
import { listenerCount, nextTick } from 'process';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { SlicePipe, CommonModule, getLocaleDateTimeFormat } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Http2ServerResponse } from 'http2';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalComponent } from '../../../../components/modal/modal.component';
import { CrudService } from '../../../../services/crud.service';
import { FilaService } from '../../../../services/fila.service';
import { ColumnaService } from '../../../../services/columna.service';
import { ParedService } from '../../../../services/pared.service';
import { INVCAM_Inventario } from '../../../../models/INVCAM_Inventario';
import { PRAMASTERService } from '../../../../services/pra-master.service';
import { SalespersonService } from '../../../../services/salesperson.service';
import { ParametroService } from '../../../../services/parametro.service';
import { GetusersService } from '../../../../services/getusers.service';
import { FacturasService } from '../../../../services/facturas.service';
import {Router} from '@angular/router'

@Component({
  selector: 'app-pared2-cs',
  templateUrl: './pared2-cs.component.html',
  styleUrls: ['./pared2-cs.component.css']
})
export class Pared2CSComponent implements OnInit {

  
  listaCementerios: any[] = ['Columbario Santiago'];
  codigoCementerio: any = 'CS'
  etapa: any[] = ['Etapa1'];
  paredCementerio: any ='2';
ngOnInit() {
  //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
  //Add 'implements OnInit' to the class.
  
}}