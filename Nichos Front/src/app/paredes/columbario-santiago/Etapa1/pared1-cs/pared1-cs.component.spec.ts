import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared1CSComponent } from './pared1-cs.component';

describe('Pared1CSComponent', () => {
  let component: Pared1CSComponent;
  let fixture: ComponentFixture<Pared1CSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared1CSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared1CSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
