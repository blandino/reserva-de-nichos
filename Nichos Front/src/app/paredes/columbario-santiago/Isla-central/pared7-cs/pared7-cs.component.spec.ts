import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared7CSComponent } from './pared7-cs.component';

describe('Pared7CSComponent', () => {
  let component: Pared7CSComponent;
  let fixture: ComponentFixture<Pared7CSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared7CSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared7CSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
