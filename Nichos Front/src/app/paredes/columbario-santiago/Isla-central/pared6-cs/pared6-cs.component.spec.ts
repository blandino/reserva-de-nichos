import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared6CSComponent } from './pared6-cs.component';

describe('Pared6CSComponent', () => {
  let component: Pared6CSComponent;
  let fixture: ComponentFixture<Pared6CSComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared6CSComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared6CSComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
