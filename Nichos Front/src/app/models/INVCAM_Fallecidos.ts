export class INVCAM_Fallecidos{

        Id?                   :number;
        codigoInventario      :string;
        fallecido1           :string; 
        fechaNacimiento1     :Date; 
        fechaFallecimiento1  :Date; 
        fallecido2           :string; 
        fechaNacimiento2     :Date; 
        fechaFallecimiento2  :Date; 
        fallecido3           :string;
        fechaNacimiento3     :Date;
        fechaFallecimiento3  :Date;
        fallecido4           :string;
        fechaNacimiento4     :Date;
        fechaFallecimiento4  :Date;
        fallecido5           :string;
        fechaNacimiento5     :Date;
        fechaFallecimiento5  :Date;
        fallecido6           :string;
        fechaNacimiento6     :Date;
        fechaFallecimiento6  :Date;
        fallecido7           :string;
        fechaNacimiento7     :Date;
        fechaFallecimiento7  :Date;
        fallecido8           :string;
        fechaNacimiento8     :Date;
        fechaFallecimiento8  :Date;
        
}