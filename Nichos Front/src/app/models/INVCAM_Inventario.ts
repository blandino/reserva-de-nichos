export class INVCAM_Inventario
{
        Id?: number;
        Codigo: string;
        Contrato: string;
        Panum: string;
        Cementerio: string;
        Pared: string;
        Fila: string;
        Columna: string;
        CodigoAnterior: string;
        Etapa: string;
        Estatus: string;
        Vendedor: string;
        FechaReserva: Date;
        IdCliente: string
        NombreCliente: string;
       // NombreFallecido: string;
        FechaRegistro: Date;
       // FechaNacimiento: Date;
        //FechaFallecimiento    :Date;
        Tipo                  :string;
        TipoColumbario?: string;
       /* NombreFallecido_2     :string; 
        FechaNacimiento_2     :Date; 
        FechaFallecimiento_2  :Date; 
        NombreFallecido_3     :string;
        FechaNacimiento_3     :Date;
        FechaFallecimiento_3  :Date;
        NombreFallecido_4     :string;
        FechaNacimiento_4     :Date;
        FechaFallecimiento_4  :Date;
        TipoColumbario        :string;
        NombreFallecido_2: string;
        NombreFallecido_3: string;
        NombreFallecido_4: string;
        */
}