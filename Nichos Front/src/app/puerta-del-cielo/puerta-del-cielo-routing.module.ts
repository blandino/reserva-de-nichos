import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Pared1Component } from '../components/shared/paredesPC/pared1/pared1.component';
import { Pared2Component } from '../components/shared/paredesPC/pared2/pared2.component';
import { Pared3Component } from '../components/shared/paredesPC/pared3/pared3.component';
import { Pared4Component } from '../components/shared/paredesPC/pared4/pared4.component';
import { Pared5Component } from '../components/shared/paredesPC/pared5/pared5.component';
import { Pared6Component } from '../components/shared/paredesPC/pared6/pared6.component';
import { Pared7Component } from '../components/shared/paredesPC/pared7/pared7.component';
import { Pared8Component } from '../components/shared/paredesPC/pared8/pared8.component';

const routes: Routes = [

  {path: 'pared1',  component: Pared1Component},
  {path: 'pared2',  component: Pared2Component},
  {path: 'pared3',  component: Pared3Component},
  {path: 'pared4',  component: Pared4Component},
  {path: 'pared5',  component: Pared5Component},
  {path: 'pared6',  component: Pared6Component},
  {path: 'pared7',  component: Pared7Component},
  {path: 'pared8',  component: Pared8Component},
  
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PuertaDelCieloRoutingModule { }
