import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CementerioPuertaRComponent } from './cementerio-puerta-r.component';

describe('CementerioPuertaRComponent', () => {
  let component: CementerioPuertaRComponent;
  let fixture: ComponentFixture<CementerioPuertaRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CementerioPuertaRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CementerioPuertaRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
