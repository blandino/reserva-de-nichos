import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PuertaDelCieloRoutingModule } from './puerta-del-cielo-routing.module';
import { CementerioPuertaRComponent } from './cementerio-puerta-r/cementerio-puerta-r.component';


@NgModule({
  declarations: [CementerioPuertaRComponent],
  imports: [
    CommonModule,
    PuertaDelCieloRoutingModule
  ]
})
export class PuertaDelCieloModule { }
