import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumbarioOzamaComponent } from './columbario-ozama.component';

describe('ColumbarioOzamaComponent', () => {
  let component: ColumbarioOzamaComponent;
  let fixture: ComponentFixture<ColumbarioOzamaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColumbarioOzamaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumbarioOzamaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
