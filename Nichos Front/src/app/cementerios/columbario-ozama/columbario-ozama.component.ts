import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../services/filtrar-estados.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-columbario-ozama',
  templateUrl: './columbario-ozama.component.html',
  styleUrls: ['./columbario-ozama.component.css']
})
export class ColumbarioOzamaComponent implements OnInit {

  
  visibilidadEtapa1: boolean;
  cementerio = 'Columbario Ozama';
  estado: any;

  estado$:Observable<any>;
  
  constructor(private filtrarEstadosService: FiltrarEstadosService) {    
   }

  ngOnInit(): void {
   
     this.filtrarEstadosService.getEstado$()
    .subscribe(data =>{      
      this.estado = data
      this.validarVisibilidadEtapa();
    })    
  }

  validarVisibilidadEtapa(){
    
    var etapa1 = 'Etapa1'

  this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa1)
    .subscribe((data:any)=>{
      this.visibilidadEtapa1 = data;
    });
  }
}
