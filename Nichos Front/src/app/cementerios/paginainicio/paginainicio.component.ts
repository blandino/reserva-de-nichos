import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-paginainicio',
  templateUrl: './paginainicio.component.html',
  styleUrls: ['./paginainicio.component.css']
})
export class PaginainicioComponent implements OnInit {
  img: string = 'https://blandinoprearreglo.com.do/wp-content/uploads/2020/08/logo-e1602165189516.png'; 
  constructor() { }

  ngOnInit(): void {
  }

}
