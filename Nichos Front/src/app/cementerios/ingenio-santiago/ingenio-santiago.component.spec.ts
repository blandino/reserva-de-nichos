import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngenioSantiagoComponent } from './ingenio-santiago.component';

describe('IngenioSantiagoComponent', () => {
  let component: IngenioSantiagoComponent;
  let fixture: ComponentFixture<IngenioSantiagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngenioSantiagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngenioSantiagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
