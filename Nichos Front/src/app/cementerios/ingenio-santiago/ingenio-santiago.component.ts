import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../services/filtrar-estados.service';
import { Observable, Subject, Subscription } from 'rxjs';


@Component({
  selector: 'app-ingenio-santiago',
  templateUrl: './ingenio-santiago.component.html',
  styleUrls: ['./ingenio-santiago.component.css']
})
export class IngenioSantiagoComponent implements OnInit {
  visibilidadEtapa1: boolean;

  cementerio = 'Ingenio Santiago';
  estado: any;
  estado$:Observable<any>;
  
  constructor(private filtrarEstadosService: FiltrarEstadosService) {    
   }

  ngOnInit(): void {
   
     this.filtrarEstadosService.getEstado$()
    .subscribe(data =>{      
      this.estado = data
      this.validarVisibilidadEtapa();
    })
    
  }

  validarVisibilidadEtapa(){
    
    var etapa1 = 'Etapa1';   
  this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa1)
    .subscribe((data:any)=>{
      this.visibilidadEtapa1 = data;
    });
  }
}
