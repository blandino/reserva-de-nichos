import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CementerioCristoRedentorComponent } from './cementerio-cristo-redentor.component';

describe('CementerioCristoRedentorComponent', () => {
  let component: CementerioCristoRedentorComponent;
  let fixture: ComponentFixture<CementerioCristoRedentorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CementerioCristoRedentorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CementerioCristoRedentorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
