import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../services/filtrar-estados.service';
import { Observable, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-cementerio-cristo-redentor',
  templateUrl: './cementerio-cristo-redentor.component.html',
  styleUrls: ['./cementerio-cristo-redentor.component.css']
})
export class CementerioCristoRedentorComponent implements OnInit {
  visibilidadEtapa1: boolean;
  visibilidadEtapa2: boolean;
  visibilidadEtapa3: boolean;
  visibilidadEtapa4: boolean;
  visibilidadEtapa5: boolean;
  visibilidadEtapa6: boolean;
  visibilidadEtapa7: boolean;
  visibilidadEtapa8: boolean;
  visibilidadEtapa9: boolean;
 

  estadoDropDown: any;

  cementerio = 'Cristo Redentor';
  estado: any;

  estado$:Observable<any>;
  
  constructor(private filtrarEstadosService: FiltrarEstadosService) {    
   }

  ngOnInit(): void {
   
     this.filtrarEstadosService.getEstado$()
    .subscribe(data =>{      
      this.estado = data
      this.validarVisibilidadEtapa();
    })
    
  }

  validarVisibilidadEtapa(){
    
   var etapa1 = 'Etapa1';
   var etapa2 = 'Etapa2';
   var etapa3 = 'Etapa3';
   var etapa4 = 'Etapa4';
   var etapa5 = 'Etapa5';
   var etapa6 = 'Etapa6';
   var etapa7 = 'Etapa7';
   var etapa8 = 'Etapa8';
   var etapa9 = 'Etapa9';

  this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa1)
    .subscribe((data:any)=>{
      this.visibilidadEtapa1 = data;
    });

   this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa2)
    .subscribe((data:any)=>{
      this.visibilidadEtapa2 = data;
    });

    
    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa3)
    .subscribe((data:any)=>{
      this.visibilidadEtapa3 = data;
    });

    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa4)
    .subscribe((data:any)=>{
      this.visibilidadEtapa4 = data;
    });

    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa5)
    .subscribe((data:any)=>{
      this.visibilidadEtapa5 = data;
    });

    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa6)
    .subscribe((data:any)=>{
      this.visibilidadEtapa6 = data;
    });

    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa7)
    .subscribe((data:any)=>{
      this.visibilidadEtapa7 = data;
    });
    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa8)
    .subscribe((data:any)=>{
      this.visibilidadEtapa8 = data;
    });
    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa9)
    .subscribe((data:any)=>{
      this.visibilidadEtapa9 = data;
    });


  }
}
