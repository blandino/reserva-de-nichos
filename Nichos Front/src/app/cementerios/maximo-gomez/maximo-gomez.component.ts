import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../services/filtrar-estados.service';
import { Observable, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-maximo-gomez',
  templateUrl: './maximo-gomez.component.html',
  styleUrls: ['./maximo-gomez.component.css']
})
export class MaximoGomezComponent implements OnInit {

  visibilidadEtapa1: boolean;
  visibilidadEtapa2: boolean;

 

  estadoDropDown: any;

  cementerio = 'Maximo Gomez';
  estado: any;

  estado$:Observable<any>;
  
  constructor(private filtrarEstadosService: FiltrarEstadosService) {    
   }

  ngOnInit(): void {
   
     this.filtrarEstadosService.getEstado$()
    .subscribe(data =>{      
      this.estado = data
      this.validarVisibilidadEtapa();
    })
    
  }

  validarVisibilidadEtapa(){
    
    var Etapa1 = 'Etapa1';
    var Etapa2 = 'Etapa2';
 

  this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, Etapa1)
    .subscribe((data:any)=>{
      this.visibilidadEtapa1 = data;
    });

   this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, Etapa2)
    .subscribe((data:any)=>{
      this.visibilidadEtapa2 = data;
    });    
  }
}
