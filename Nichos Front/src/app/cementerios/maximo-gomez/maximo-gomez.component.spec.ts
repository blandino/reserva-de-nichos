import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaximoGomezComponent } from './maximo-gomez.component';

describe('MaximoGomezComponent', () => {
  let component: MaximoGomezComponent;
  let fixture: ComponentFixture<MaximoGomezComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaximoGomezComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaximoGomezComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
