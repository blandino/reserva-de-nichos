import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorComponent } from './cristo-salvador.component';

describe('CristoSalvadorComponent', () => {
  let component: CristoSalvadorComponent;
  let fixture: ComponentFixture<CristoSalvadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
