import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CementeriosRoutingModule } from './cementerios-routing.module';
import { ColumbarioSantiagoComponent } from './columbario-santiago/columbario-santiago.component';
import { ColumbarioOzamaComponent } from './columbario-ozama/columbario-ozama.component';
import { ColumbarioLuperonComponent } from './columbario-luperon/columbario-luperon.component';
import { IngenioSantiagoComponent } from './ingenio-santiago/ingenio-santiago.component';
import { MaximoGomezComponent } from './maximo-gomez/maximo-gomez.component';
import { CristoSalvadorComponent } from './cristo-salvador/cristo-salvador.component';
import {CementerioCristoRedentorComponent} from './cementerio-cristo-redentor/cementerio-cristo-redentor.component';
import { PaginainicioComponent } from './paginainicio/paginainicio.component'


 


@NgModule({
  declarations: [ColumbarioSantiagoComponent, ColumbarioOzamaComponent, ColumbarioLuperonComponent, IngenioSantiagoComponent, MaximoGomezComponent, 
    CristoSalvadorComponent, CementerioCristoRedentorComponent, PaginainicioComponent],
  imports: [
    CommonModule,
    CementeriosRoutingModule
  ]
})
export class CementeriosModule { }
