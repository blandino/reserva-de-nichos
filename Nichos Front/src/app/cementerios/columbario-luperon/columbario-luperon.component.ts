import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../services/filtrar-estados.service';
import { Observable, Subject, Subscription } from 'rxjs';





@Component({
  selector: 'app-columbario-luperon',
  templateUrl: './columbario-luperon.component.html',
  styleUrls: ['./columbario-luperon.component.css']
})
export class ColumbarioLuperonComponent implements OnInit {

  visibilidadExternos: boolean;
  visibilidadInternos: boolean;
  visibilidadFamiliar: boolean;
 

  estadoDropDown: any;

  cementerio = 'Columbario Luperon';
  estado: any;

  estado$:Observable<any>;
  
  constructor(private filtrarEstadosService: FiltrarEstadosService) {    
   }

  ngOnInit(): void {
   
     this.filtrarEstadosService.getEstado$()
    .subscribe(data =>{      
      this.estado = data
      this.validarVisibilidadEtapa();
    })
    
  }

  validarVisibilidadEtapa(){
    
    var externos = 'Externos';
    var internos = 'Internos';
    var familiar = 'Familiar';

  this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, externos)
    .subscribe((data:any)=>{
      this.visibilidadExternos = data;
    });

   this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, internos)
    .subscribe((data:any)=>{
      this.visibilidadInternos = data;
    });

    
    this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, familiar)
    .subscribe((data:any)=>{
      this.visibilidadFamiliar = data;
    });
  }
}
