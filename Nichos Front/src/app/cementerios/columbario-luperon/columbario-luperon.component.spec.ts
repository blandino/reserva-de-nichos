import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumbarioLuperonComponent } from './columbario-luperon.component';

describe('ColumbarioLuperonComponent', () => {
  let component: ColumbarioLuperonComponent;
  let fixture: ComponentFixture<ColumbarioLuperonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColumbarioLuperonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumbarioLuperonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
