import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../services/filtrar-estados.service';
import { Observable, Subject, Subscription } from 'rxjs';

@Component({
  selector: 'app-columbario-santiago',
  templateUrl: './columbario-santiago.component.html',
  styleUrls: ['./columbario-santiago.component.css']
})
export class ColumbarioSantiagoComponent implements OnInit {

  visibilidadEtapa1: boolean;
  visibilidadIslaCentral: boolean;

  cementerio = 'Columbario Santiago';
  estado: any;

  estado$:Observable<any>;
  
  constructor(private filtrarEstadosService: FiltrarEstadosService) {    
   }

  ngOnInit(): void {
   
     this.filtrarEstadosService.getEstado$()
    .subscribe(data =>{      
      this.estado = data
      this.validarVisibilidadEtapa();
    })
    
  }

  validarVisibilidadEtapa(){
    
    var etapa1      = 'Etapa1';
    var islaCentral = 'Isla Central';

  this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, etapa1)
    .subscribe((data:any)=>{
      this.visibilidadEtapa1 = data;
    });

   this.filtrarEstadosService.validarEtapa(this.cementerio, this.estado, islaCentral)
    .subscribe((data:any)=>{
      this.visibilidadIslaCentral = data;
    });
  }
}
