import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColumbarioSantiagoComponent } from './columbario-santiago.component';

describe('ColumbarioSantiagoComponent', () => {
  let component: ColumbarioSantiagoComponent;
  let fixture: ComponentFixture<ColumbarioSantiagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ColumbarioSantiagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ColumbarioSantiagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
