import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//luperon
import { ExternosComponent } from '../listparedes/lista-luperon/externos/externos.component';
import { InternosComponent } from '../listparedes/lista-luperon/internos/internos.component';
import { FamiliarComponent } from '../listparedes/lista-luperon/familiar/familiar.component';

//ozama
import { OzamaEtapa1Component } from '../listparedes/lista-ozama/ozama-etapa1/ozama-etapa1.component';

//santiago
import { IslaCentralComponent } from '../listparedes/lista-santiago/isla-central/isla-central.component';
import { SantiagoEtapa1Component } from '../listparedes/lista-santiago/santiago-etapa1/santiago-etapa1.component';

//ingenio Santiago
import { IngenioSantiagoEtapa1Component } from '../listparedes/lista-ingenio-santiago/ingenio-santiago-etapa1/ingenio-santiago-etapa1.component';

//Maximo Gomez
import { MaximoGomezEtapa1Component } from '../listparedes/lista-maximo-gomez/maximo-gomez-etapa1/maximo-gomez-etapa1.component';
import { MaximoGomezEtapa2Component } from '../listparedes/lista-maximo-gomez/maximo-gomez-etapa2/maximo-gomez-etapa2.component';

//Cristo Salvador
import { CristoSalvadorEtapa1Component } from '../listparedes/lista-cristo-salvador/cristo-salvador-etapa1/cristo-salvador-etapa1.component';
import { CristoSalvadorEtapa2Component } from '../listparedes/lista-cristo-salvador/cristo-salvador-etapa2/cristo-salvador-etapa2.component';
import { CristoSalvadorEtapa3Component } from '../listparedes/lista-cristo-salvador/cristo-salvador-etapa3/cristo-salvador-etapa3.component';
import { CristoSalvadorEtapa4Component } from '../listparedes/lista-cristo-salvador/cristo-salvador-etapa4/cristo-salvador-etapa4.component';
import { CristoSalvadorEtapa5Component } from '../listparedes/lista-cristo-salvador/cristo-salvador-etapa5/cristo-salvador-etapa5.component';
import { CristoSalvadorEtapa6Component } from '../listparedes/lista-cristo-salvador/cristo-salvador-etapa6/cristo-salvador-etapa6.component';

//CristoRedentor

import {CristoRedentorEtapa1Component} from '../listparedes/listaCristoredentor/cristo-redentor-etapa1/cristo-redentor-etapa1.component'
import {CristoRedentorEtapa2Component} from '../listparedes/listaCristoredentor/cristo-redentor-etapa2/cristo-redentor-etapa2.component'
import {CristoRedentorEtapa3Component} from '../listparedes/listaCristoredentor/cristo-redentor-etapa3/cristo-redentor-etapa3.component'
import {CristoRedentorEtapa4Component} from '../listparedes/listaCristoredentor/cristo-redentor-etapa4/cristo-redentor-etapa4.component'
import {CristoRedentorEtapa5Component} from '../listparedes/listaCristoredentor/cristo-redentor-etapa5/cristo-redentor-etapa5.component'
import {CristoRedentorEtapa6Component} from '../listparedes/listaCristoredentor/cristo-redentor-etapa6/cristo-redentor-etapa6.component'
import {CristoRedentorEtapa7Component } from '../listparedes/listaCristoredentor/cristo-redentor-etapa7/cristo-redentor-etapa7.component';
import {CristoRedentorEtapa8Component } from '../listparedes/listaCristoredentor/cristo-redentor-etapa8/cristo-redentor-etapa8.component';
import { CristoRedentorEtapa9Component } from '../listparedes/listaCristoredentor/cristo-redentor-etapa9/cristo-redentor-etapa9.component';
import { CristoSalvadorEtapa7Component } from '../listparedes/lista-cristo-salvador/cristo-salvador-etapa7/cristo-salvador-etapa7.component';









const routes: Routes = [

  //santiago

  {
    path: 'LSantiagoEtapa1', 
    component: SantiagoEtapa1Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LSantiagoIslaCentral', 
    component: IslaCentralComponent,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  //ozama
  {
    path: 'LOzamaEtapa1', 
    component: OzamaEtapa1Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  //Luperon
  {
    path: 'LLuperonExternos', 
    component: ExternosComponent,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LLuperonInternos', 
    component: InternosComponent,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LLuperonFamiliar', 
    component: FamiliarComponent,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  //ingenio santiago
  {
    path: 'LIngenioSantiagoEtapa1', 
    component: IngenioSantiagoEtapa1Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  //Maximo Gomez
  {
    path: 'LMaximoGomezEtapa1', 
    component: MaximoGomezEtapa1Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LMaximoGomezEtapa2', 
    component: MaximoGomezEtapa2Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  //Cristo Salvador
  {
    path: 'LCristoSalvadorEtapa1', 
    component: CristoSalvadorEtapa1Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoSalvadorEtapa2', 
    component: CristoSalvadorEtapa2Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoSalvadorEtapa3', 
    component: CristoSalvadorEtapa3Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoSalvadorEtapa4', 
    component: CristoSalvadorEtapa4Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoSalvadorEtapa5', 
    component: CristoSalvadorEtapa5Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoSalvadorEtapa6', 
    component: CristoSalvadorEtapa6Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path:'LCristoSalvadorEtapa7',
    component: CristoSalvadorEtapa7Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  
  


  //CristoRedentor
  {
    path: 'LCristoRedentorEtapa1', 
    component: CristoRedentorEtapa1Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoRedentorEtapa2', 
    component: CristoRedentorEtapa2Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoRedentorEtapa3', 
    component: CristoRedentorEtapa3Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoRedentorEtapa4', 
    component: CristoRedentorEtapa4Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoRedentorEtapa5', 
    component: CristoRedentorEtapa5Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoRedentorEtapa6', 
    component: CristoRedentorEtapa6Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoRedentorEtapa7', 
    component: CristoRedentorEtapa7Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

  {
    path: 'LCristoRedentorEtapa8', 
    component: CristoRedentorEtapa8Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },
  {
    path: 'LCristoRedentorEtapa9', 
    component: CristoRedentorEtapa9Component,
    loadChildren: () => import('../listparedes/listparedes.module').then(c => c.ListparedesModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CementeriosRoutingModule { }
