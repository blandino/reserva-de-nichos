import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModalComponent } from './components/modal/modal.component';
import { CementerioPuertaRComponent } from './puerta-del-cielo/cementerio-puerta-r/cementerio-puerta-r.component';
import { CementerioCristoRComponent } from './cristo-redentor/cementerio-cristo-r/cementerio-cristo-r.component';

//Carpeta cementerios
import { ColumbarioSantiagoComponent } from './cementerios/columbario-santiago/columbario-santiago.component';
import { ColumbarioOzamaComponent } from './cementerios/columbario-ozama/columbario-ozama.component';
import { ColumbarioLuperonComponent } from './cementerios/columbario-luperon/columbario-luperon.component';
import { IngenioSantiagoComponent } from './cementerios/ingenio-santiago/ingenio-santiago.component';
import { MaximoGomezComponent } from './cementerios/maximo-gomez/maximo-gomez.component';
import { CristoSalvadorComponent } from './cementerios/cristo-salvador/cristo-salvador.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { LoginComponent } from './components/login/login.component';
import { CementerioCristoRedentorComponent } from './cementerios/cementerio-cristo-redentor/cementerio-cristo-redentor.component';
//import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { BarranComponent } from './barranavegacion/barran/barran.component';
import { ReporteComponent } from './components/reporte/reporte.component';



const routes: Routes = [

{
  path: 'Barranavegacion',
  component: BarranComponent,
  loadChildren: () => import('./barranavegacion/barranavegacion.module').then(b => b.BarranavegacionModule)
},

  {
    path: 'CCristoR', 
    component: CementerioCristoRComponent,
    loadChildren: () => import('./cristo-redentor/cristo-redentor.module').then(c => c.CristoRedentorModule)
  },

  {
    path: 'CPuertaDC',  
    component: CementerioPuertaRComponent,
    loadChildren: () => import('./puerta-del-cielo/puerta-del-cielo.module').then(p => p.PuertaDelCieloModule)
  },

  {
    path: 'CSantiago',  
    component: ColumbarioSantiagoComponent,
    loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  {
    path: 'COzama',  
    component: ColumbarioOzamaComponent,
    loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  {
    path: 'CLuperon',  
    component: ColumbarioLuperonComponent,
    loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  
  {
    path: 'CIngenioSantiago',  
    component: IngenioSantiagoComponent,
    loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

   
   {
    path: 'CMaximoGomez',  
    component: MaximoGomezComponent,
    loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  
  {
    path: 'CCristoSalvador',  
    component: CristoSalvadorComponent,
    loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  {
    path: 'CementerioCristoRedentor',  
    component: CementerioCristoRedentorComponent,
    loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
  },
 
  {path: 'Modal',  component: ModalComponent},
  {path: 'buscar/:termino', component: BuscadorComponent},
  {path: 'Login', component: LoginComponent},
  //{path:'Navbar', component: NavbarComponent},
  {path: 'Reporte', component: ReporteComponent},
  
  {path: '**', pathMatch: 'full', redirectTo: 'Barranavegacion'},
  
  ////
  /*{
    path:'',
    component: NavbarComponent,
    children:[

      {
        path: 'CCristoR', 
        component: CementerioCristoRComponent,
        loadChildren: () => import('./cristo-redentor/cristo-redentor.module').then(c => c.CristoRedentorModule)
      },
    
      {
        path: 'CPuertaDC',  
        component: CementerioPuertaRComponent,
        loadChildren: () => import('./puerta-del-cielo/puerta-del-cielo.module').then(p => p.PuertaDelCieloModule)
      },
    
      {
        path: 'CSantiago',  
        component: ColumbarioSantiagoComponent,
        loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
      },
    
      {
        path: 'COzama',  
        component: ColumbarioOzamaComponent,
        loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
      },
    
      {
        path: 'CLuperon',  
        component: ColumbarioLuperonComponent,
        loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
      },
    
      
      {
        path: 'CIngenioSantiago',  
        component: IngenioSantiagoComponent,
        loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
      },
    
       
       {
        path: 'CMaximoGomez',  
        component: MaximoGomezComponent,
        loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
      },
    
      
      {
        path: 'CCristoSalvador',  
        component: CristoSalvadorComponent,
        loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
      },
    
      {
        path: 'CementerioCristoRedentor',  
        component: CementerioCristoRedentorComponent,
        loadChildren: () => import('./cementerios/cementerios.module').then(p => p.CementeriosModule)
      }

    ]
  }*/


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
