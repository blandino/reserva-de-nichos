import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CristoRedentorRoutingModule } from './cristo-redentor-routing.module';
import { CementerioCristoRComponent } from './cementerio-cristo-r/cementerio-cristo-r.component';


@NgModule({
  declarations: [CementerioCristoRComponent],
  imports: [
    CommonModule,
    CristoRedentorRoutingModule
  ]
})
export class CristoRedentorModule { }
