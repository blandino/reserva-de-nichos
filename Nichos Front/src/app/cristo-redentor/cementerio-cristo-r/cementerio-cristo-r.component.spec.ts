import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CementerioCristoRComponent } from './cementerio-cristo-r.component';

describe('CementerioCristoRComponent', () => {
  let component: CementerioCristoRComponent;
  let fixture: ComponentFixture<CementerioCristoRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CementerioCristoRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CementerioCristoRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
