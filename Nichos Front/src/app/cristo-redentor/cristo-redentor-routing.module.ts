import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GrillaComponent } from '../components/shared/grilla/grilla.component';
import { Grilla2Component } from '../components/shared/grilla2/grilla2.component';
import { Grilla3Component } from '../components/shared/grilla3/grilla3.component';
import { Grilla4Component } from '../components/shared/grilla4/grilla4.component';
import { Grilla5Component } from '../components/shared/grilla5/grilla5.component';
import { Grilla6Component } from '../components/shared/grilla6/grilla6.component';
import { Grilla7Component } from '../components/shared/grilla7/grilla7.component';
import { Grilla8Component } from '../components/shared/grilla8/grilla8.component';

const routes: Routes = [
  
  {path: 'Grilla1',   component: GrillaComponent},
  {path: 'Grilla2',  component: Grilla2Component},
  {path: 'Grilla3',  component: Grilla3Component},
  {path: 'Grilla4',  component: Grilla4Component},
  {path: 'Grilla5',  component: Grilla5Component},
  {path: 'Grilla6',  component: Grilla6Component},
  {path: 'Grilla7',  component: Grilla7Component},
  {path: 'Grilla8',  component: Grilla8Component},

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CristoRedentorRoutingModule { }
