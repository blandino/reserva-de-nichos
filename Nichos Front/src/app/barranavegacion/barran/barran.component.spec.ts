import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarranComponent } from './barran.component';

describe('BarranComponent', () => {
  let component: BarranComponent;
  let fixture: ComponentFixture<BarranComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarranComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarranComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
