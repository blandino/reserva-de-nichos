import { Component, OnInit } from '@angular/core';
import { GetusersService } from './../../services/getusers.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from './../../services/login.service';
import { INVCAM_Permisos } from 'src/app/models/INVCAM_Permisos';
import { PermisosService } from './../../services/permisos.service';
import { ToastrService } from 'ngx-toastr';
import { VINVCAM_LOGIN } from './../../models/VINVCAM_LOGIN';
import {Router} from '@angular/router'
import { CrudService } from '../../services/crud.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { from, observable, Subscription, BehaviorSubject, Observable } from 'rxjs';
import { PRAMASTERService } from '../../services/pra-master.service';
import { FacturasService } from '../../services/facturas.service';
import { VincamfallecidoclientesService } from '../../services/vincamfallecidoclientes.service';
import { TipocolumbarioService } from '../../services/tipocolumbario.service'; 
import { FiltrarEstadosService } from '../../services/filtrar-estados.service';
import { nextTick } from 'process';







@Component({
  selector: 'app-barran',
  templateUrl: './barran.component.html',
  styleUrls: ['./barran.component.css']
})
export class BarranComponent implements OnInit {


  parametros: any = [{texto:'Fallecido', valor:1 }, {texto:'Contrato', valor:2 }, {texto:'Cliente', valor:3 }];
  nprueba: any;
  usersList: any[] = [];
  loginUserList: any[] = [];
  addUserList: any[] = []
  ListaPermisos: any[] = [];
  listaUsuariosPermisos: any[] = [];
  textoBuscar: string = '';
  listaFallecidos: any[] = [];

  form: FormGroup;
  inventario : any;
  subscription: Subscription;
  prueba: any [] = [];
  t : any;
  nombreModal: any;
  isChecked: boolean;
  formNecesidad: FormGroup;
  listaCementerios: any[] = ['Columbario Luperon'];
  codigoCementerio: any = 'CL';
  
  dataPanum: any;
  factura: any;


  isAdmin: any;
  isLogged: any;
  idInventario = 0;
  res: any[] = [];
  listaMostrar: any[] = [];
  listaFactura: any[] = [];
  columbarioSeleccionado: any; 
  tipoColumbario: any[] = []
  vIncamInventarioData: any;
  tipoPlan: any [] = [];
  codigoInventario: any;

  cementerio:any;
  etapa:any;
  pared:any;
  fila:any;
  columna:any;

  listaEstado: any[]=['Todos','Reservado', 'Ocupado', 'Vendido','Disponible'];
  estadosDropdown: any;

  visibilidadcolumbarioOzama    :boolean;
  visibilidadColumbarioSantiago :boolean;
  visibilidadColumbarioLuperon  :boolean;
  visibilidadIngenioSantiago    :boolean;
  visibilidadMaximoGomez        :boolean;
  visibilidadCristoSalvador     :boolean;
  visibilidadCristoRedentor     :boolean;

   visibilidadcolumbarioOzama$       = new BehaviorSubject<any>({} as any)
   visibilidadColumbarioSantiago$    = new BehaviorSubject<any>({} as any)
   visibilidadColumbarioLuperon$     = new BehaviorSubject<any>({} as any)
   visibilidadIngenioSantiago$       = new BehaviorSubject<any>({} as any)
   visibilidadMaximoGomez$           = new BehaviorSubject<any>({} as any)
   visibilidadCristoSalvador$        = new BehaviorSubject<any>({} as any)
   visibilidadCristoRedentor$        = new BehaviorSubject<any>({} as any)


  constructor(
              private getusersService: GetusersService,
              private modal: NgbModal,
              private loginService: LoginService,
              private permisosService: PermisosService,
              private toastr: ToastrService,
              private router: Router,
              private crudService: CrudService,
              private formBuilder: FormBuilder,
              private pRAMASTERService: PRAMASTERService,
              private facturasService: FacturasService,
              private vincamfallecidoclientesService: VincamfallecidoclientesService,
              private TipocolumbarioService: TipocolumbarioService,
              private filtrarEstadosService: FiltrarEstadosService) { 

                this.form = this.formBuilder.group({
                  id:0,
                  Codigo:[''],
                  Contrato: [''],
                  Panum: ['', [Validators.required]],
                  Cementerio: ['', [Validators.required]],
                  Pared: ['', [Validators.required]],
                  Fila: ['', [Validators.required]],
                  Columna: ['', [Validators.required]],
                  CodigoAnterior: [''],
                  Etapa: ['', [Validators.required]],
                  Estatus: [''],
                  IdCliente: [''],
                  Cliente: [''],
                  TipoColumbario: [''],
                  NombreFallecido: [''],
                  FechaNacimiento: [''],
                  FechaFallecimiento: [''],    
                  NombreFallecido_2: [''],
                  FechaNacimiento_2: [''],
                  FechaFallecimiento_2: [''],  
                  NombreFallecido_3: [''],
                  FechaNacimiento_3: [''],
                  FechaFallecimiento_3: [''],  
                  NombreFallecido_4: [''],
                  FechaNacimiento_4: [''],
                  FechaFallecimiento_4: [''],
                  NombreFallecido_5: [''],
                  FechaNacimiento_5: [''],
                  FechaFallecimiento_5: [''],    
                  NombreFallecido_6: [''],
                  FechaNacimiento_6: [''],
                  FechaFallecimiento_6: [''],  
                  NombreFallecido_7: [''],
                  FechaNacimiento_7: [''],
                  FechaFallecimiento_7: [''],  
                  NombreFallecido_8: [''],
                  FechaNacimiento_8: [''],
                  FechaFallecimiento_8: ['']     
                })

                this.formNecesidad = this.formBuilder.group({
                  id:0,
                  Codigo:[''],                 
                  Cementerio: ['', [Validators.required]],
                  Pared: ['', [Validators.required]],
                  Fila: ['', [Validators.required]],
                  Columna: ['', [Validators.required]],
                  CodigoAnterior: [''],
                  Etapa: ['', [Validators.required]],
                  Estatus: [''],                  
                  NombreFallecido:[''],

                  Contrato: ['',[Validators.required]],
                  IdCliente: ['',[Validators.required]],
                  Cliente: ['',[Validators.required]],
                  TipoColumbario: [''],
                  FechaNacimiento: [''],
                  FechaFallecimiento: [''],
                  NombreFallecido_2: [''],
                  FechaNacimiento_2: [''],
                  FechaFallecimiento_2: [''],  
                  NombreFallecido_3: [''],
                  FechaNacimiento_3: [''],
                  FechaFallecimiento_3: [''],  
                  NombreFallecido_4: [''],
                  FechaNacimiento_4: [''],
                  FechaFallecimiento_4: [''],
                  NombreFallecido_5: [''],
                  FechaNacimiento_5: [''],
                  FechaFallecimiento_5: [''],    
                  NombreFallecido_6: [''],
                  FechaNacimiento_6: [''],
                  FechaFallecimiento_6: [''],  
                  NombreFallecido_7: [''],
                  FechaNacimiento_7: [''],
                  FechaFallecimiento_7: [''],  
                  NombreFallecido_8: [''],
                  FechaNacimiento_8: [''],
                  FechaFallecimiento_8: ['']    
                }) 

                this.getUsers();
                this.getLogin();
                this.getPermissions();
                this.getParams();

               
                this.TipocolumbarioService.getTipoColumbario()
                .subscribe((data: any) =>{
                  this.tipoColumbario = data;
                  
                })

                if(!sessionStorage.getItem('login')){
                  this.router.navigate(['Login']);
                }
             }

  ngOnInit(): void {
   // this.isAdmin = sessionStorage.getItem('admin')
    //this.isLogged=sessionStorage.getItem('login');
    //this.getFallecidosByName();    
    this.cambiarEstado('Todos');
  }

  cambiarEstado(estado){
    //sessionStorage.setItem('Estado', estado)
    this.estadosDropdown = estado;
    
    this.observarCambiosDeEstado(estado);
    this.validarEstadoCementerio(); 
    //this.validarEstadoSinValores$()
   
  }

  validarEstadoSinValores$(): Observable<any>{

   var visibilidadcolumbarioOzama;
   var visibilidadColumbarioSantiago;
   var visibilidadColumbarioLuperon;
   var visibilidadIngenioSantiago;
   var visibilidadMaximoGomez;
   var visibilidadCristoSalvador;
   var visibilidadCristoRedentor;
   
   this.visibilidadcolumbarioOzama$.asObservable().subscribe((data: any)=>{
     visibilidadcolumbarioOzama = data;
   });

   this.visibilidadColumbarioSantiago$.asObservable().subscribe((data: any)=>{
    visibilidadColumbarioSantiago = data
   })

   this.visibilidadColumbarioLuperon$.asObservable().subscribe((data:any)=>{
    visibilidadColumbarioLuperon = data;
   })
   
   this.visibilidadIngenioSantiago$.asObservable().subscribe((data:any)=>{
    visibilidadIngenioSantiago = data;
   })

   this.visibilidadMaximoGomez$.asObservable().subscribe((data:any)=>{
    visibilidadMaximoGomez = data;
   })
 
   this.visibilidadCristoSalvador$.asObservable().subscribe((data:any)=>{
    visibilidadCristoSalvador = data;
   })

   this.visibilidadCristoRedentor$.asObservable().subscribe((data:any)=>{
    visibilidadCristoRedentor = data;
   })

   if (visibilidadcolumbarioOzama    == false &&
       visibilidadColumbarioSantiago == false &&
       visibilidadColumbarioLuperon  == false &&
       visibilidadIngenioSantiago    == false &&
       visibilidadMaximoGomez        == false &&
       visibilidadCristoSalvador     == false &&
       visibilidadCristoRedentor     == false ){

        this.toastr.error('Error', 'No se han encontrado registros con el estatus indicado')
        //this.router.navigate(['Barranavegacion/PaginaVacia'])
   
   }
   return
  }

  observarCambiosDeEstado(estadoObservar){
    this.filtrarEstadosService.ValidarEstado(estadoObservar)    
    //this.validarEstadoSinValores();
  }

  validarEstadoCementerio(){
    //this.filtrarEstadosService.validarCementerio();
    var columbarioOzama    = 'Columbario Ozama';
    var columbarioSantiago = 'Columbario Santiago';
    var columbarioLuperon  = 'Columbario Luperon';
    var ingenioSantiago    = 'Ingenio Santiago';
    var maximoGomez        = 'Maximo Gomez';
    var cristoSalvador     = 'Cristo Salvador';
    var cristoRedentor     = 'Cristo Redentor';
   
    this.filtrarEstadosService.validarCementerio( columbarioOzama, this.estadosDropdown)
    .subscribe((data:boolean)=>{
      this.visibilidadcolumbarioOzama = data;    
      this.visibilidadcolumbarioOzama$.next(this.visibilidadcolumbarioOzama);
     
    })

     this.filtrarEstadosService.validarCementerio( columbarioSantiago, this.estadosDropdown)
    .subscribe((data:boolean)=>{
      this.visibilidadColumbarioSantiago = data;
      this.visibilidadColumbarioSantiago$.next(this.visibilidadColumbarioSantiago);
    })      

    this.filtrarEstadosService.validarCementerio( columbarioLuperon, this.estadosDropdown)
    .subscribe((data:boolean)=>{
      this.visibilidadColumbarioLuperon  = data; 
      this.visibilidadColumbarioLuperon$.next(this.visibilidadColumbarioLuperon); 
    })

    this.filtrarEstadosService.validarCementerio( ingenioSantiago, this.estadosDropdown)
    .subscribe((data:boolean)=>{
      this.visibilidadIngenioSantiago = data;
      this.visibilidadIngenioSantiago$.next(this.visibilidadIngenioSantiago);
    })

    this.filtrarEstadosService.validarCementerio( maximoGomez, this.estadosDropdown)
    .subscribe((data:boolean)=>{
      this.visibilidadMaximoGomez  = data;
      this.visibilidadMaximoGomez$.next(this.visibilidadMaximoGomez);
    })
   
    this.filtrarEstadosService.validarCementerio( cristoSalvador, this.estadosDropdown)
    .subscribe((data:boolean)=>{
      this.visibilidadCristoSalvador = data;
      this.visibilidadCristoSalvador$.next(this.visibilidadCristoSalvador);
    })

    this.filtrarEstadosService.validarCementerio( cristoRedentor, this.estadosDropdown)
    .subscribe((data:boolean)=>{
      this.visibilidadCristoRedentor = data;
      this.visibilidadCristoRedentor$.next(this.visibilidadCristoRedentor);
    }) 
    //this.validarEstadoSinValores$();
    //this.validarEstadoSinValores()
     
  }

  getParams(){
    this.isAdmin = sessionStorage.getItem('admin');
    this.isLogged=sessionStorage.getItem('login');

    
  }
  

  openModal(users){
    this.modal.open(users, { size: 'xl'});   
  }

  openModalFallecidos(Modalfallecidos){
    this.modal.open(Modalfallecidos, {size:'xl'})
    this.listaFallecidos = [''];
  }                   


  guardar(data){


    const permiso: INVCAM_Permisos = {      
      usuarioID: data,
      estatus:  true ,
      isAdmin:  false

    }
    this.permisosService.guardarRegistro(permiso).subscribe(data =>{
    this.toastr.success('Permiso Otorgado de manera satisfactoria', 'Permiso otrogado')

    this.getUsers();
    this.getLogin();

    setTimeout(()=>{      
                      
      
    }, 2000)                    
    

    }, err =>{
      this.toastr.warning('Permiso denegado','No se pudo otorgar el permiso')
    })
  }

  refresh(){
    window.location.reload();
   }

   eliminar(id: number){
     if(confirm('Esta seguro que desea quitar el permiso')){
       this.permisosService.eliminarRegistro(id).subscribe(data =>{
         this.toastr.warning('Usuario Eliminado', 'Usiaro elimanado del sistema');

         this.getUsers();
         this.getLogin();

        //this.getusersService.getUsers();
        //this.loginService.getLoginUsers();
    

       })

     }

   }

   actualizar(id, isAdmin, usrid ){
     
    let admin;

    if (isAdmin == true){
      admin = false
    }

    if (isAdmin == false){
    admin = true
    }

    const permiso: INVCAM_Permisos = {  
      permisoID: id,
      usuarioID: usrid,
      estatus:  true ,
      isAdmin:  admin
    }

    this.permisosService.actualizarPermiso(id, permiso).subscribe(data => {

      if(admin){
        this.toastr.info('Ahora es Administrador','Permiso Adminsitrador otorgado') 
      }

      if(admin == false){
        this.toastr.error('Usuario con permisos limitados','Permisos Limitados')
      }
             
      
      
      this.getUsers();
      this.getLogin();
      
      setTimeout(()=>{
        
       
      }, 2000)
                     
     })

   }

   buscarUsuario(termino: string){    
    
    this.router.navigate(['/buscar',termino]);

   }

   buscarUsuario2(termino: string){    

       
    let usersArr: VINVCAM_LOGIN[] = [];
    termino = termino.toLowerCase();

    for(let usuario of this.usersList){
   
    
      let userName = usuario.usrName.toLowerCase();
  
      if( userName.indexOf(termino) >=0 ){
        usersArr.push(usuario)
      }    
    } 
   
    this.usersList = usersArr

    if(termino ==''){
      this.getusersService.getUsers()
    .subscribe((data: any) =>{
  
      this.usersList = data;    
  
    }); 

    }
    
     return usersArr;
   }

   getUsers(){
    this.getusersService.getUsers()
    .subscribe((data: any) =>{
  
      this.usersList = data;    
  
    }); 

   }
  
   
   getLogin(){

    this.loginService.getLoginUsers()
    .subscribe((data: any) =>{

      this.loginUserList = data;   

    })

   }

   getFallecidosByName(nombreFallecido: string/*, parametro:any*/){
     //console.log(parametro)
     this.crudService.filtrarFallecidos(nombreFallecido, this.nprueba).subscribe((data: any) =>{
       
       this.listaFallecidos = data;
     })
   }

   cambiarParametrosBusqueda(event: any){
      this.nprueba = event
   
      var nombrePlaceHolder;

      if(this.nprueba ==1){
        nombrePlaceHolder = 'Nombre del Fallecido';        
      }
      else if(this.nprueba == 2){
        nombrePlaceHolder = 'Numero de contrato'
      }
      else{
        nombrePlaceHolder = 'Nombre del fallecido'
      }
   }

   getPermissions(){
     this.permisosService.obtenerRegistro()
     .subscribe((data: any) =>{
       this.ListaPermisos = data;
     
     })
   }

   getListPermissions(){
     this.ListaPermisos.forEach(lp => {
      
      this.loginUserList.forEach(lg =>{

        if(lp.usuarioID == lg.usrname){

          this.listaUsuariosPermisos.push({})

        }
      })

     })
     

   }

 

   getUserByName(termino: string): any {

    let usersArr: VINVCAM_LOGIN[] = [];
    termino = termino.toLocaleLowerCase();
  
    for(let usuario of this.loginUserList){
      
      let userName = usuario.usrname.toLowerCase();

      if( userName.indexOf(termino) >=0 ){
        usersArr.push(usuario)
      }

      
    } 
  
     return usersArr;
  
   }

   editar(inventario){   
    this.crudService.actualizar(inventario); 
  } 
  
  openSM(contenido, contenidoReservar ,datos)
  {  
    if (this.isAdmin == 'false'){
      return
    }

    this.tipoPlan.push(datos);
                this.tipoPlan.forEach(element =>{
                  this.t = element.tipo   
                  //this.codigoInventario = element.codigo             
                })               

                
                if(this.t =='PA'){
                  this.nombreModal = contenido;                 

                }
                if(this.t =='N'){
                 this.nombreModal = contenidoReservar;                       

               }


    this.form.reset();
    this.formNecesidad.reset();   
    
    this.subscription = this.crudService.obtenerinfo().subscribe(data =>{                   
                  this.inventario =  data;
                  this.dataPanum = this.inventario.panum; 
                  this.factura = this.inventario.contrato;
                  this.cementerio = this.inventario.cementerio;
                  this.etapa = this.inventario.etapa;
                  this.pared = this.inventario.pared;                  
                  this.fila = this.inventario.fila;
                  this.columna = this.inventario.columna;

     
                  //this.idColumbario;
                  if (this.inventario.estatus == 'ocupado'){
                    this.isChecked = true;
                  }
                  else{
                    this.isChecked = false;
                  } 
         

      this.vincamfallecidoclientesService.obtenerInventario(this.inventario.codigo).subscribe((Ifallecido: any) =>{                
        this.vIncamInventarioData = Ifallecido;
        console.log("inventario de fallecidos");
        console.log(this.vIncamInventarioData)
        
        let fallecido1 = this.vIncamInventarioData.fallecido1;
        let fechaNacimiento1 = this.vIncamInventarioData.fechaNacimiento1
        let fechaFallecimiento1 = this.vIncamInventarioData.fechaFallecimiento1     

        let fallecido2 = this.vIncamInventarioData.fallecido2;
        let fechaNacimiento2 = this.vIncamInventarioData.fechaNacimiento2;
        let fechaFallecimiento2 = this.vIncamInventarioData.fechaFallecimiento2;

        let fallecido3 = this.vIncamInventarioData.fallecido3;
        let fechaNacimiento3 = this.vIncamInventarioData.fechaNacimiento3;
        let fechaFallecimiento3 = this.vIncamInventarioData.fechaFallecimiento3;

        let fallecido4 = this.vIncamInventarioData.fallecido4;
        let fechaNacimiento4 = this.vIncamInventarioData.fechaNacimiento4
        let fechaFallecimiento4 = this.vIncamInventarioData.fechaFallecimiento4

        let fallecido5 = this.vIncamInventarioData.fallecido5;
        let fechaNacimiento5 = this.vIncamInventarioData.fechaNacimiento5
        let fechaFallecimiento5 = this.vIncamInventarioData.fechaFallecimiento5

        let fallecido6 = this.vIncamInventarioData.fallecido6;
        let fechaNacimiento6 = this.vIncamInventarioData.fechaNacimiento6
        let fechaFallecimiento6 = this.vIncamInventarioData.fechaFallecimiento6

        let fallecido7 = this.vIncamInventarioData.fallecido7;
        let fechaNacimiento7 = this.vIncamInventarioData.fechaNacimiento7
        let fechaFallecimiento7 = this.vIncamInventarioData.fechaFallecimiento7

        let fallecido8 = this.vIncamInventarioData.fallecido8;
        let fechaNacimiento8 = this.vIncamInventarioData.fechaNacimiento8
        let fechaFallecimiento8 = this.vIncamInventarioData.fechaFallecimiento8

        this.columbarioSeleccionado = this.vIncamInventarioData.tipoColumbario; 
        this.dataPanum = this.vIncamInventarioData.panum;
        this.factura = this.vIncamInventarioData.contrato;
                       

        if(this.t =='PA'){
                       
          this.form.patchValue({
            Codigo: this.inventario.codigo,
            //Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
            Contrato: this.inventario.contrato,   
            Panum: this.inventario.panum,
            Cementerio: this.inventario.cementerio,
            Pared: this.inventario.pared,
            Fila: this.inventario.fila,
            Columna: this.inventario.columna,
            CodigoAnterior: "N/A",
            Etapa: this.inventario.etapa,
           
            Estatus: this.isChecked,
            NombreFallecido: this.inventario.nombreFallecido,
            FechaNacimiento: this.inventario.fechaNacimiento,
            FechaFallecimiento:this.inventario.fechaFallecimiento,                           
            //TipoColumbario:    this.vIncamInventarioData.tipoColumbario,
            TipoColumbario:    this.inventario.tipoColumbario,
            NombreFallecido_2: fallecido2,
            FechaNacimiento_2: fechaNacimiento2,
            FechaFallecimiento_2: fechaFallecimiento2,  
            NombreFallecido_3: fallecido3,
            FechaNacimiento_3: fechaNacimiento3,
            FechaFallecimiento_3: fechaFallecimiento3,  
            NombreFallecido_4: fallecido4,
            FechaNacimiento_4: fechaNacimiento4,
            FechaFallecimiento_4: fechaFallecimiento4,
            NombreFallecido_5: fallecido5,
            FechaNacimiento_5: fechaNacimiento5,
            FechaFallecimiento_5: fechaFallecimiento5,    
            NombreFallecido_6: fallecido6,
            FechaNacimiento_6: fechaNacimiento6,
            FechaFallecimiento_6: fechaFallecimiento6,  
            NombreFallecido_7: fallecido7,
            FechaNacimiento_7: fechaNacimiento7,
            FechaFallecimiento_7: fechaFallecimiento7,  
            NombreFallecido_8: fallecido8,
            FechaNacimiento_8: fechaNacimiento8,
            FechaFallecimiento_8: fechaFallecimiento8  

            
          })
        }

        if(this.t =='N'){

          this.formNecesidad.patchValue({     
            Codigo: this.inventario.codigo,                
            //Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
                      Contrato: this.inventario.contrato,   
                      //Cementerio: this.listaCementerios[0],
                      Cementerio: this.inventario.cementerio,
                      Pared: this.inventario.pared,
                      Fila: this.inventario.fila,
                      Columna: this.inventario.columna,
                      CodigoAnterior: "N/A",
                      Etapa:this.inventario.etapa,                     
                      Estatus:   this.isChecked,
                      IdCliente: this.inventario.idCliente,
                      Cliente: this.inventario.nombreCliente,           
                      NombreFallecido: this.inventario.nombreFallecido,
                      FechaNacimiento: this.inventario.fechaNacimiento,
                      FechaFallecimiento:this.inventario.fechaFallecimiento,
                      TipoColumbario:    this.inventario.tipoColumbario,
            NombreFallecido_2: fallecido2,
            FechaNacimiento_2: fechaNacimiento2,
            FechaFallecimiento_2: fechaFallecimiento2,  
            NombreFallecido_3: fallecido3,
            FechaNacimiento_3: fechaNacimiento3,
            FechaFallecimiento_3: fechaFallecimiento3,  
            NombreFallecido_4: fallecido4,
            FechaNacimiento_4: fechaNacimiento4,
            FechaFallecimiento_4: fechaFallecimiento4,
            NombreFallecido_5: fallecido5,
            FechaNacimiento_5: fechaNacimiento5,
            FechaFallecimiento_5: fechaFallecimiento5,    
            NombreFallecido_6: fallecido6,
            FechaNacimiento_6: fechaNacimiento6,
            FechaFallecimiento_6: fechaFallecimiento6,  
            NombreFallecido_7: fallecido7,
            FechaNacimiento_7: fechaNacimiento7,
            FechaFallecimiento_7: fechaFallecimiento7,  
            NombreFallecido_8: fallecido8,
            FechaNacimiento_8: fechaNacimiento8,
            FechaFallecimiento_8: fechaFallecimiento8  
          })                               

        }

        
      
        
      })
      
                      
    
    })                
    
   
    this.modal.open(this.nombreModal, { size: 'xl'});    
    
    this.editar(datos)           
    this.obtenerDatosPRA(this.dataPanum);  
    this.obtenerFactura(this.factura)
             
  }

 
  obtenerDatosPRA(dato:any){                

    this.pRAMASTERService.get_Pra_Master_info_ID(dato).subscribe((x: any) =>{
     this.res = x   
     
     this.listaMostrar.push(this.res)               
      
   
     });                  
     this.listaMostrar =[];
     
     return this.res;   

   } 

   obtenerFactura(dato:any){                

    this.facturasService.get_Factura(dato).subscribe((x: any) =>{ 

     this.listaFactura.push(x)

     });                  
     this.listaFactura =[];
     
     return this.listaFactura;   

   }
   

   CerrarSesion(){
     sessionStorage.clear();
     setTimeout(()=>{
      this.refresh();
     
    }, 2000)
   }

}
