import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BarranavegacionRoutingModule } from './barranavegacion-routing.module';
//import { BarranComponent } from './barran/barran.component';


@NgModule({
  declarations: [/*BarranComponent*/],
  imports: [
    CommonModule,
    BarranavegacionRoutingModule
  ]
})
export class BarranavegacionModule { }
