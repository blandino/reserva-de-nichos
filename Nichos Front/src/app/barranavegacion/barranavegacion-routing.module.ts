import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ColumbarioSantiagoComponent } from '../cementerios/columbario-santiago/columbario-santiago.component';
import { ColumbarioOzamaComponent } from '../cementerios/columbario-ozama/columbario-ozama.component';
import { ColumbarioLuperonComponent } from '../cementerios/columbario-luperon/columbario-luperon.component';
import { IngenioSantiagoComponent } from '../cementerios/ingenio-santiago/ingenio-santiago.component';
import { MaximoGomezComponent } from '../cementerios/maximo-gomez/maximo-gomez.component';
import { CristoSalvadorComponent } from '../cementerios/cristo-salvador/cristo-salvador.component';
import { CementerioCristoRedentorComponent } from '../cementerios/cementerio-cristo-redentor/cementerio-cristo-redentor.component';
import { PaginainicioComponent } from '../cementerios/paginainicio/paginainicio.component';
import { PaginavaciaComponent } from '../components/paginavacia/paginavacia.component';






const routes: Routes = [

  {
    path: 'CSantiago1',  
    component: ColumbarioSantiagoComponent,
    loadChildren: () => import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  {
    path: 'COzama1',  
    component: ColumbarioOzamaComponent,
    loadChildren: () => import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },
  
  {
    path: 'CLuperon1',  
    component: ColumbarioLuperonComponent,
    loadChildren: () => import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  
  {
    path: 'CIngenioSantiago1',  
    component: IngenioSantiagoComponent,
    loadChildren: () => import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

   
   {
    path: 'CMaximoGomez1',  
    component: MaximoGomezComponent,
    loadChildren: () => import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  
  {
    path: 'CCristoSalvador1',  
    component: CristoSalvadorComponent,
    loadChildren: () => import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },

  {
    path: 'CementerioCristoRedentor1',  
    component: CementerioCristoRedentorComponent,
    loadChildren: () => import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },
  {
    path:'PaginaInicio',
    component: PaginainicioComponent,
    loadChildren: ()=> import('../cementerios/cementerios.module').then(p => p.CementeriosModule)
  },
  {path:'PaginaVacia', component: PaginavaciaComponent},

    {path: '**', pathMatch: 'full', redirectTo: 'Barranavegacion/PaginaInicio'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BarranavegacionRoutingModule { }
