import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SantiagoEtapa1Component } from './santiago-etapa1.component';

describe('SantiagoEtapa1Component', () => {
  let component: SantiagoEtapa1Component;
  let fixture: ComponentFixture<SantiagoEtapa1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SantiagoEtapa1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SantiagoEtapa1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
