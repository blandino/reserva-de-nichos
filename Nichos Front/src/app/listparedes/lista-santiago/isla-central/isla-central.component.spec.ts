import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IslaCentralComponent } from './isla-central.component';

describe('IslaCentralComponent', () => {
  let component: IslaCentralComponent;
  let fixture: ComponentFixture<IslaCentralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IslaCentralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IslaCentralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
