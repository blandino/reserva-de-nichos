import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa3Component } from './cristo-redentor-etapa3.component';

describe('CristoRedentorEtapa3Component', () => {
  let component: CristoRedentorEtapa3Component;
  let fixture: ComponentFixture<CristoRedentorEtapa3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
