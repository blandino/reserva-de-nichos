import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa8Component } from './cristo-redentor-etapa8.component';

describe('CristoRedentorEtapa8Component', () => {
  let component: CristoRedentorEtapa8Component;
  let fixture: ComponentFixture<CristoRedentorEtapa8Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa8Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
