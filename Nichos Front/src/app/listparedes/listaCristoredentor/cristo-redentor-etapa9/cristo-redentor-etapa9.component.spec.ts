import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa9Component } from './cristo-redentor-etapa9.component';

describe('CristoRedentorEtapa9Component', () => {
  let component: CristoRedentorEtapa9Component;
  let fixture: ComponentFixture<CristoRedentorEtapa9Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa9Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
