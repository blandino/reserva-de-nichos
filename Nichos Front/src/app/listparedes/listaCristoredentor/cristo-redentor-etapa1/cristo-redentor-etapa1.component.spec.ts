import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa1Component } from './cristo-redentor-etapa1.component';

describe('CristoRedentorEtapa1Component', () => {
  let component: CristoRedentorEtapa1Component;
  let fixture: ComponentFixture<CristoRedentorEtapa1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
