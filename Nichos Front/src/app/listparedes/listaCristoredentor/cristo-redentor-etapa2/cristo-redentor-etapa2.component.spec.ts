import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa2Component } from './cristo-redentor-etapa2.component';

describe('CristoRedentorEtapa2Component', () => {
  let component: CristoRedentorEtapa2Component;
  let fixture: ComponentFixture<CristoRedentorEtapa2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
