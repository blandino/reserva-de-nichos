import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa7Component } from './cristo-redentor-etapa7.component';

describe('CristoRedentorEtapa7Component', () => {
  let component: CristoRedentorEtapa7Component;
  let fixture: ComponentFixture<CristoRedentorEtapa7Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa7Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
