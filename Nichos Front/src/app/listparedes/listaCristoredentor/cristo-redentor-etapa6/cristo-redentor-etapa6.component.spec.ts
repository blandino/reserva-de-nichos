import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa6Component } from './cristo-redentor-etapa6.component';

describe('CristoRedentorEtapa6Component', () => {
  let component: CristoRedentorEtapa6Component;
  let fixture: ComponentFixture<CristoRedentorEtapa6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
