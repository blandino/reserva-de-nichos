import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa5Component } from './cristo-redentor-etapa5.component';

describe('CristoRedentorEtapa5Component', () => {
  let component: CristoRedentorEtapa5Component;
  let fixture: ComponentFixture<CristoRedentorEtapa5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
