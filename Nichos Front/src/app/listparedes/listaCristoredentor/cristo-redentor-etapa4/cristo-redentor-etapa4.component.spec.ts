import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRedentorEtapa4Component } from './cristo-redentor-etapa4.component';

describe('CristoRedentorEtapa4Component', () => {
  let component: CristoRedentorEtapa4Component;
  let fixture: ComponentFixture<CristoRedentorEtapa4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRedentorEtapa4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRedentorEtapa4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
