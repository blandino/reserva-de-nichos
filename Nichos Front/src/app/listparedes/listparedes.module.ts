import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListparedesRoutingModule } from './listparedes-routing.module';
import { IslaCentralComponent } from './lista-santiago/isla-central/isla-central.component';
import { SantiagoEtapa1Component } from './lista-santiago/santiago-etapa1/santiago-etapa1.component';
import { MaximoGomezEtapa1Component } from './lista-maximo-gomez/maximo-gomez-etapa1/maximo-gomez-etapa1.component';
import { MaximoGomezEtapa2Component } from './lista-maximo-gomez/maximo-gomez-etapa2/maximo-gomez-etapa2.component';
import { CristoSalvadorEtapa1Component } from './lista-cristo-salvador/cristo-salvador-etapa1/cristo-salvador-etapa1.component';
import { CristoSalvadorEtapa2Component } from './lista-cristo-salvador/cristo-salvador-etapa2/cristo-salvador-etapa2.component';
import { CristoSalvadorEtapa3Component } from './lista-cristo-salvador/cristo-salvador-etapa3/cristo-salvador-etapa3.component';
import { CristoSalvadorEtapa4Component } from './lista-cristo-salvador/cristo-salvador-etapa4/cristo-salvador-etapa4.component';
import { CristoSalvadorEtapa5Component } from './lista-cristo-salvador/cristo-salvador-etapa5/cristo-salvador-etapa5.component';
import { CristoSalvadorEtapa6Component } from './lista-cristo-salvador/cristo-salvador-etapa6/cristo-salvador-etapa6.component';
import { CristoSalvadorEtapa7Component } from './lista-cristo-salvador/cristo-salvador-etapa7/cristo-salvador-etapa7.component';
import { ExternosComponent } from './lista-luperon/externos/externos.component';
import { FamiliarComponent } from './lista-luperon/familiar/familiar.component';
import { InternosComponent } from './lista-luperon/internos/internos.component';
import { IngenioSantiagoEtapa1Component } from './lista-ingenio-santiago/ingenio-santiago-etapa1/ingenio-santiago-etapa1.component';
import { OzamaEtapa1Component } from './lista-ozama/ozama-etapa1/ozama-etapa1.component';
import { CristoRedentorEtapa1Component } from './listaCristoredentor/cristo-redentor-etapa1/cristo-redentor-etapa1.component';
import { CristoRedentorEtapa2Component } from './listaCristoredentor/cristo-redentor-etapa2/cristo-redentor-etapa2.component';
import { CristoRedentorEtapa3Component } from './listaCristoredentor/cristo-redentor-etapa3/cristo-redentor-etapa3.component';
import { CristoRedentorEtapa4Component } from './listaCristoredentor/cristo-redentor-etapa4/cristo-redentor-etapa4.component';
import { CristoRedentorEtapa5Component } from './listaCristoredentor/cristo-redentor-etapa5/cristo-redentor-etapa5.component';
import { CristoRedentorEtapa6Component } from './listaCristoredentor/cristo-redentor-etapa6/cristo-redentor-etapa6.component';
import { InicioComponent } from './inicio/inicio.component';
import { CristoRedentorEtapa7Component } from './listaCristoredentor/cristo-redentor-etapa7/cristo-redentor-etapa7.component';
import { CristoRedentorEtapa8Component } from './listaCristoredentor/cristo-redentor-etapa8/cristo-redentor-etapa8.component';
import { CristoRedentorEtapa9Component } from './listaCristoredentor/cristo-redentor-etapa9/cristo-redentor-etapa9.component';








@NgModule({
  declarations: [  IslaCentralComponent, 
                   SantiagoEtapa1Component, 
                   MaximoGomezEtapa1Component, 
                   MaximoGomezEtapa2Component, 
                   CristoSalvadorEtapa1Component, 
                   CristoSalvadorEtapa2Component,                   
                   CristoSalvadorEtapa3Component, 
                   CristoSalvadorEtapa4Component, 
                   CristoSalvadorEtapa5Component,
                   CristoSalvadorEtapa6Component,
                   CristoSalvadorEtapa7Component,
                   ExternosComponent,
                   FamiliarComponent,
                   InternosComponent,
                   IngenioSantiagoEtapa1Component,
                   OzamaEtapa1Component,
                   CristoRedentorEtapa1Component,
                   CristoRedentorEtapa2Component,
                   CristoRedentorEtapa3Component,
                   CristoRedentorEtapa4Component,
                   CristoRedentorEtapa5Component,
                   CristoRedentorEtapa6Component,
                   InicioComponent,
                   CristoRedentorEtapa7Component,
                   CristoRedentorEtapa8Component,
                   CristoRedentorEtapa9Component,
  ],
  imports: [
    CommonModule,
    ListparedesRoutingModule
  ]
})
export class ListparedesModule { }
