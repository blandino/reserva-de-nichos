import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaximoGomezEtapa2Component } from './maximo-gomez-etapa2.component';

describe('MaximoGomezEtapa2Component', () => {
  let component: MaximoGomezEtapa2Component;
  let fixture: ComponentFixture<MaximoGomezEtapa2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaximoGomezEtapa2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaximoGomezEtapa2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
