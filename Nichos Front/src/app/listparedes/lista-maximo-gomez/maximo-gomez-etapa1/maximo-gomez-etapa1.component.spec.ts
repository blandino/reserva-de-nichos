import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaximoGomezEtapa1Component } from './maximo-gomez-etapa1.component';

describe('MaximoGomezEtapa1Component', () => {
  let component: MaximoGomezEtapa1Component;
  let fixture: ComponentFixture<MaximoGomezEtapa1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaximoGomezEtapa1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaximoGomezEtapa1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
