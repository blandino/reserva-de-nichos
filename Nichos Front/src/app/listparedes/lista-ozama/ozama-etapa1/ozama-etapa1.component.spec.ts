import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OzamaEtapa1Component } from './ozama-etapa1.component';

describe('OzamaEtapa1Component', () => {
  let component: OzamaEtapa1Component;
  let fixture: ComponentFixture<OzamaEtapa1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OzamaEtapa1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OzamaEtapa1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
