import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorEtapa4Component } from './cristo-salvador-etapa4.component';

describe('CristoSalvadorEtapa4Component', () => {
  let component: CristoSalvadorEtapa4Component;
  let fixture: ComponentFixture<CristoSalvadorEtapa4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorEtapa4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorEtapa4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
