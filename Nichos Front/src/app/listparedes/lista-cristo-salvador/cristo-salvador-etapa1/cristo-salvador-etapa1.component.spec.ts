import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorEtapa1Component } from './cristo-salvador-etapa1.component';

describe('CristoSalvadorEtapa1Component', () => {
  let component: CristoSalvadorEtapa1Component;
  let fixture: ComponentFixture<CristoSalvadorEtapa1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorEtapa1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorEtapa1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
