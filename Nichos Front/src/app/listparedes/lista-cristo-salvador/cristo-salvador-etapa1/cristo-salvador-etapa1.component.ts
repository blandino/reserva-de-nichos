import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../../services/filtrar-estados.service';
import { DatosparedService } from '../../../services/datospared.service';


@Component({
  selector: 'app-cristo-salvador-etapa1',
  templateUrl: './cristo-salvador-etapa1.component.html',
  styleUrls: ['./cristo-salvador-etapa1.component.css']
})
export class CristoSalvadorEtapa1Component implements OnInit {

 //Datos: any[] = ['Cristo Salvador', 'CCS', 'Etapa1', '1'];
 //Datos: any = {cementerio:"Cristo Salvador", codigoCementerio:"CCS", Etapa:"Etapa1", pared:"1" };
 Datos:any;
 /*listaCementerios: any[] = [''];
  codigoCementerio: any = 'CCS'
  etapa: any[] = ['Etapa1'];
  paredCementerio: any ='1';*/

  estado: any
  paredes: any[] = ['1','2','3']
  cementerio = 'Cristo Salvador';


  visibilidadPared: any[] = [];

  constructor( private filtrarEstadosService: FiltrarEstadosService,
              private  datosparedService: DatosparedService) { 
                //this.Datos = ['Cristo Salvador', 'CCS', 'Etapa1', '1'];
              }

  ngOnInit(): void {

    this.filtrarEstadosService.getEstado$()
    .subscribe((data: any) =>{
      this.estado =  data;
      this.validarVisibilidadParedes()      
    })

    //this.EnviarDatosParedes();  
  }

  validarVisibilidadParedes(){

    var etapa = 'Etapa1'
    for (let index = 0; index <=2; index++) {

      this.filtrarEstadosService.validarParedes(this.cementerio, this.estado, etapa, this.paredes[index])
      .subscribe((data: any)=>{
        this.visibilidadPared[index] = data
    
      })          
    }
  }

  EnviarDatosParedes(cementerio_dato, codigoCementerio_dato, Etapa_dato, pared_dato ){

   this.Datos = {cementerio: cementerio_dato, codigoCementerio: codigoCementerio_dato, Etapa: Etapa_dato, pared: pared_dato };
 
    this.datosparedService.ValidarDatos(this.Datos);
    console.log(this.Datos)
  }

}
