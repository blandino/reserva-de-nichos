import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorEtapa5Component } from './cristo-salvador-etapa5.component';

describe('CristoSalvadorEtapa5Component', () => {
  let component: CristoSalvadorEtapa5Component;
  let fixture: ComponentFixture<CristoSalvadorEtapa5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorEtapa5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorEtapa5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
