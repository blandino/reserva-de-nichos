import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorEtapa7Component } from './cristo-salvador-etapa7.component';

describe('CristoSalvadorEtapa7Component', () => {
  let component: CristoSalvadorEtapa7Component;
  let fixture: ComponentFixture<CristoSalvadorEtapa7Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorEtapa7Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorEtapa7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
