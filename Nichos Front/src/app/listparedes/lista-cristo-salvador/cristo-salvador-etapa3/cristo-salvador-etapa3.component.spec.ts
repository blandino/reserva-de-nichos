import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorEtapa3Component } from './cristo-salvador-etapa3.component';

describe('CristoSalvadorEtapa3Component', () => {
  let component: CristoSalvadorEtapa3Component;
  let fixture: ComponentFixture<CristoSalvadorEtapa3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorEtapa3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorEtapa3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
