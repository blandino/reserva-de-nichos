import { Component, OnInit } from '@angular/core';
import { FiltrarEstadosService } from '../../../services/filtrar-estados.service';
import { DatosparedService } from '../../../services/datospared.service';

@Component({
  selector: 'app-cristo-salvador-etapa3',
  templateUrl: './cristo-salvador-etapa3.component.html',
  styleUrls: ['./cristo-salvador-etapa3.component.css']
})
export class CristoSalvadorEtapa3Component implements OnInit {

  Datos:any;
  estado: any
  paredes: any[] = ['5']
  cementerio = 'Cristo Salvador';


  visibilidadPared: any[] = [];

  constructor( private filtrarEstadosService: FiltrarEstadosService,
               private  datosparedService: DatosparedService) { }

  ngOnInit(): void {
    
    this.filtrarEstadosService.getEstado$()
    .subscribe((data: any) =>{
      this.estado =  data;
      this.validarVisibilidadParedes()
    })
  }

  validarVisibilidadParedes(){

    var etapa = 'Etapa3'
    for (let index = 0; index < 1; index++) {

      this.filtrarEstadosService.validarParedes(this.cementerio, this.estado, etapa, this.paredes[index])
      .subscribe((data: any)=>{
        this.visibilidadPared[index] = data
   
      })          
    }

  }

  EnviarDatosParedes(cementerio_dato, codigoCementerio_dato, Etapa_dato, pared_dato ){

    this.Datos = {cementerio: cementerio_dato, codigoCementerio: codigoCementerio_dato, Etapa: Etapa_dato, pared: pared_dato };
  
     this.datosparedService.ValidarDatos(this.Datos);
     console.log(this.Datos)
   }


}
