import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorEtapa6Component } from './cristo-salvador-etapa6.component';

describe('CristoSalvadorEtapa6Component', () => {
  let component: CristoSalvadorEtapa6Component;
  let fixture: ComponentFixture<CristoSalvadorEtapa6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorEtapa6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorEtapa6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
