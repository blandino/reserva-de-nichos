import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoSalvadorEtapa2Component } from './cristo-salvador-etapa2.component';

describe('CristoSalvadorEtapa2Component', () => {
  let component: CristoSalvadorEtapa2Component;
  let fixture: ComponentFixture<CristoSalvadorEtapa2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoSalvadorEtapa2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoSalvadorEtapa2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
