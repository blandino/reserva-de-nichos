import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngenioSantiagoEtapa1Component } from './ingenio-santiago-etapa1.component';

describe('IngenioSantiagoEtapa1Component', () => {
  let component: IngenioSantiagoEtapa1Component;
  let fixture: ComponentFixture<IngenioSantiagoEtapa1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngenioSantiagoEtapa1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngenioSantiagoEtapa1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
