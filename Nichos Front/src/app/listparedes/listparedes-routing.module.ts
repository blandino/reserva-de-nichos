import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GrillaComponent } from '../components/shared/grilla/grilla.component';
import {Grilla2Component} from '../components/shared/grilla2/grilla2.component';
import {Grilla3Component} from '../components/shared/grilla3/grilla3.component';
import {Grilla4Component} from '../components/shared/grilla4/grilla4.component';
import {Grilla5Component} from '../components/shared/grilla5/grilla5.component';
import {Grilla6Component} from '../components/shared/grilla6/grilla6.component';
import {Grilla7Component} from '../components/shared/grilla7/grilla7.component';
import {Grilla8Component} from '../components/shared/grilla8/grilla8.component';
import {Grilla9Component} from '../components/shared/grilla9/grilla9.component';
import {Grilla10Component} from '../components/shared/grilla10/grilla10.component';
import {Grilla11Component} from '../components/shared/grilla11/grilla11.component';
import {Grilla12Component} from '../components/shared/grilla12/grilla12.component';
import {Grilla13Component} from '../components/shared/grilla13/grilla13.component';

import {  Grillacs1Component } from '../components/shared/grillacs1/grillacs1.component';
import {  Grillacs2Component } from '../components/shared/grillacs2/grillacs2.component';
import {  Grillacs3Component } from '../components/shared/grillacs3/grillacs3.component';
import {  Grillacs4Component } from '../components/shared/grillacs4/grillacs4.component';
import {  Grillacs5Component } from '../components/shared/grillacs5/grillacs5.component';
import {  Grillacs6Component } from '../components/shared/grillacs6/grillacs6.component';
import {  Grillacs7Component } from '../components/shared/grillacs7/grillacs7.component';



//Columbario Ozama
//Etapa1
import { Pared1COComponent } from '../paredes/columbario-ozama/Etapa1/pared1-co/pared1-co.component';


//Columbario Santiago
//Etapa1
import { Pared1CSComponent } from '../paredes/columbario-santiago/etapa1/pared1-cs/pared1-cs.component';
import { Pared2CSComponent } from '../paredes/columbario-santiago/etapa1/pared2-cs/pared2-cs.component';
import { Pared3CSComponent } from '../paredes/columbario-santiago/etapa1/pared3-cs/pared3-cs.component';
import { Pared4CSComponent } from '../paredes/columbario-santiago/etapa1/pared4-cs/pared4-cs.component';
import { Pared5CSComponent } from '../paredes/columbario-santiago/etapa1/pared5-cs/pared5-cs.component';
//Isla Central
import { Pared6CSComponent } from '../paredes/columbario-santiago/isla-central/pared6-cs/pared6-cs.component';
import { Pared7CSComponent } from '../paredes/columbario-santiago/isla-central/pared7-cs/pared7-cs.component';


//Columbarios Luperon
//Externos
import { Pared1CLComponent } from '../paredes/columbario-luperon/externos/pared1-cl/pared1-cl.component';
import { Pared2CLComponent } from '../paredes/columbario-luperon/externos/pared2-cl/pared2-cl.component';
import { Pared3CLComponent } from '../paredes/columbario-luperon/externos/pared3-cl/pared3-cl.component';
import { Pared4CLComponent } from '../paredes/columbario-luperon/externos/pared4-cl/pared4-cl.component';
import { Pared5CLComponent } from '../paredes/columbario-luperon/internos/pared5-cl/pared5-cl.component';
import { Pared6CLComponent } from '../paredes/columbario-luperon/internos/pared6-cl/pared6-cl.component';
import { Pared7CLComponent } from '../paredes/columbario-luperon/internos/pared7-cl/pared7-cl.component';
import { Pared8CLComponent } from '../paredes/columbario-luperon/internos/pared8-cl/pared8-cl.component';
import { Pared9CLComponent } from '../paredes/columbario-luperon/internos/pared9-cl/pared9-cl.component';
import { Pared10CLComponent } from '../paredes/columbario-luperon/familiar/pared10-cl/pared10-cl.component';
import { Pared11CLComponent } from '../paredes/columbario-luperon/familiar/pared11-cl/pared11-cl.component';

//Ingenio Santiago
//Etapa1
import { Pared1ISComponent } from '../paredes/ingenio-santiago/etapa1/pared1-is/pared1-is.component';

//Maximo Gomez
//Etapa1
import { Pared1MGComponent } from '../paredes/maximo-gomez/etapa1/pared1-mg/pared1-mg.component';
//Etapa2
import { Pared2MGComponent } from '../paredes/maximo-gomez/etapa2/pared2-mg/pared2-mg.component';

//Cristo Salvador
//Etapa1
import { Pared1CCSComponent } from '../paredes/cristo-salvador/etapa1/pared1-ccs/pared1-ccs.component';
import { Pared2CCSComponent } from '../paredes/cristo-salvador/etapa1/pared2-ccs/pared2-ccs.component';
import { Pared3CCSComponent } from '../paredes/cristo-salvador/etapa1/pared3-ccs/pared3-ccs.component';
//Etapa 2
import { Pared4CCSComponent } from '../paredes/cristo-salvador/etapa2/pared4-ccs/pared4-ccs.component';
//Etapa3
import { Pared5CCSComponent } from '../paredes/cristo-salvador/etapa3/pared5-ccs/pared5-ccs.component';
//Etapa4
import { Pared6CCSComponent } from '../paredes/cristo-salvador/etapa4/pared6-ccs/pared6-ccs.component';
//Etapa 5
import { Pared7CCSComponent } from '../paredes/cristo-salvador/etapa5/pared7-ccs/pared7-ccs.component';
import { Pared8CCSComponent } from '../paredes/cristo-salvador/etapa5/pared8-ccs/pared8-ccs.component';
import { Pared9CCSComponent } from '../paredes/cristo-salvador/etapa5/pared9-ccs/pared9-ccs.component';
import { Pared10CCSComponent } from '../paredes/cristo-salvador/etapa5/pared10-ccs/pared10-ccs.component';
import { Pared11CCSComponent } from '../paredes/cristo-salvador/etapa5/pared11-ccs/pared11-ccs.component';
import { Pared12CCSComponent } from '../paredes/cristo-salvador/etapa5/pared12-ccs/pared12-ccs.component';
import { Pared13CCSComponent } from '../paredes/cristo-salvador/etapa5/pared13-ccs/pared13-ccs.component';

//Etapa 6
import { Pared14CCSComponent } from '../paredes/cristo-salvador/etapa6/pared14-ccs/pared14-ccs.component';
import { Pared15CCSComponent } from '../paredes/cristo-salvador/etapa6/pared15-ccs/pared15-ccs.component';
import { Pared16CCSComponent } from '../paredes/cristo-salvador/etapa6/pared16-ccs/pared16-ccs.component';
import { ParedCompletaComponent } from '../components/pared-completa/pared-completa.component';
import { ParedCompletaCementerioComponent } from '../components/pared-completa-cementerio/pared-completa-cementerio.component';




const routes: Routes = [

  
  /*
   *  
  Maestro de rutas
  *
  */

    /*Columbario Ozama*/

  /*Etapa1*/
  {path: 'Pared1COEtapa1', component: ParedCompletaComponent},

  /*Columbario Ozama*/

   /*Columbario Santiago*/

  /*Etapa1*/
  {path: 'Pared1CSEtapa1', component: ParedCompletaComponent},

  /*Isla Central*/
  {path: 'Pared6CSIslaCentral', component: ParedCompletaComponent},
  {path: 'Pared7CSIslaCentral', component: ParedCompletaComponent},

  /*Columbario Santiago*/

    /*Columbario Luperon*/

  /*Externos*/
  {path: 'Pared1CLExternos', component: ParedCompletaComponent},
  {path: 'Pared2CLExternos', component: ParedCompletaComponent},
  {path: 'Pared3CLExternos', component: ParedCompletaComponent},
  {path: 'Pared4CLExternos', component: ParedCompletaComponent},

  /*Internos*/
  {path: 'Pared5CLInternos', component: ParedCompletaComponent},
  {path: 'Pared6CLInternos', component: ParedCompletaComponent},

  /*Familiar*/
  {path: 'Pared7CLFamiliar', component: ParedCompletaComponent},
  /*Columbario Luperon*/

   /*Ingenio Santiago*/

  /*Etapa1*/
  {path: 'Pared1ISEtapa1', component: ParedCompletaCementerioComponent},

  /*Ingenio Santiago*/


  /*Maximo Gomez*/

  /*Etapa1*/
  {path: 'Pared1MGEtapa1', component: ParedCompletaCementerioComponent},

  
  /*Etapa2*/
  {path: 'Pared2MGEtapa2', component: ParedCompletaCementerioComponent},

  /*Maximo Gomez*/

  
  /*Cristo Redentor*/

   /*Etapa1*/
   {path: 'Pared1CREtapa1', component: ParedCompletaCementerioComponent},
   {path: 'Pared2CREtapa1', component: ParedCompletaCementerioComponent},
   {path: 'Pared3CREtapa1', component: ParedCompletaCementerioComponent},

   /*Etapa2*/
   {path: 'Pared4CREtapa2', component: ParedCompletaCementerioComponent},
   {path: 'Pared5CREtapa2', component: ParedCompletaCementerioComponent},
 
    /*Etapa3*/
    {path: 'Pared6CREtapa3', component: ParedCompletaCementerioComponent},
    {path: 'Pared7CREtapa3', component: ParedCompletaCementerioComponent},

    /*Etapa4*/
    {path: 'Pared8CREtapa4', component: ParedCompletaCementerioComponent},
    {path: 'Pared9CREtapa4', component: ParedCompletaCementerioComponent},

     /*Etapa5*/
     {path: 'Pared10CREtapa5', component: ParedCompletaCementerioComponent},

     /*Etapa6*/
     {path: 'Pared11CREtapa6', component: ParedCompletaCementerioComponent},
     {path: 'Pared12CREtapa6', component: ParedCompletaCementerioComponent},
     {path: 'Pared13CREtapa6', component: ParedCompletaCementerioComponent},

     /*Etapa7*/
     {path: 'Pared14CREtapa7', component: ParedCompletaCementerioComponent},

     /*Etapa8*/
     {path: 'Pared15CREtapa8', component: ParedCompletaCementerioComponent},

     /*Etapa9*/
     {path: 'Pared16CREtapa9', component: ParedCompletaCementerioComponent},
     {path: 'Pared17CREtapa9', component: ParedCompletaCementerioComponent},
   
   /*Cristo Redentor*/



  /*Cristosalvador */

  /*Etapa1*/
  {path: 'Pared1CCSEtapa1', component: ParedCompletaCementerioComponent},
  {path: 'Pared2CCSEtapa1', component: ParedCompletaCementerioComponent},
  {path: 'Pared3CCSEtapa1', component: ParedCompletaCementerioComponent},
  {path: 'ParedPrueba4'   , component: ParedCompletaCementerioComponent},

  /*Etapa2*/
  {path: 'Pared4CCSEtapa2', component: ParedCompletaCementerioComponent},

   /*Etapa3*/
   {path: 'Pared5CCSEtapa3', component: ParedCompletaCementerioComponent},

   /*Etapa4*/
   {path: 'Pared6CCSEtapa4', component: ParedCompletaCementerioComponent},

    /*Etapa5*/
    {path: 'Pared7CCSEtapa5' ,  component: ParedCompletaCementerioComponent},
    {path: 'Pared8CCSEtapa5' ,  component: ParedCompletaCementerioComponent},
    {path: 'Pared9CCSEtapa5' ,  component: ParedCompletaCementerioComponent},
    {path: 'Pared10CCSEtapa5',  component: ParedCompletaCementerioComponent},
    {path: 'Pared11CCSEtapa5',  component: ParedCompletaCementerioComponent},
    {path: 'Pared12CCSEtapa5',  component: ParedCompletaCementerioComponent},
    {path: 'Pared13CCSEtapa5',  component: ParedCompletaCementerioComponent},

    /*Etapa6*/
    {path: 'Pared14CCSEtapa6' ,  component: ParedCompletaCementerioComponent},
    {path: 'Pared15CCSEtapa6' ,  component: ParedCompletaCementerioComponent},
    {path: 'Pared16CCSEtapa6' ,  component: ParedCompletaCementerioComponent},

    /*Etapa7*/
    {path:'Pared17CCSEtapa7', component:ParedCompletaCementerioComponent},
    {path:'Pared18CCSEtapa7', component:ParedCompletaCementerioComponent},
    {path:'Pared19CCSEtapa7', component:ParedCompletaCementerioComponent},

   /*Cristosalvador */

  /*
   *  
  Maestro de rutas
  *
  */

//CristoRedentor
//Etapa1
 {path: 'Grilla1',   component: GrillaComponent},
 {path: 'Grilla2',   component: Grilla2Component},
 {path: 'Grilla3',   component: Grilla3Component},

 //Etapa2
 {path: 'Grilla4',   component: Grilla4Component},
 {path: 'Grilla5',   component: Grilla5Component},

 //Etapa3
 {path: 'Grilla6',   component: Grilla6Component},
 {path: 'Grilla7',   component: Grilla7Component},

 //Etapa4
 {path: 'Grilla8',   component: Grilla8Component},
 {path: 'Grilla9',   component: Grilla9Component},

 //Etapa5
 {path: 'Grilla10',   component: Grilla10Component},

 //Etapa6
 {path: 'Grilla11',   component: Grilla11Component},

 {path: 'Grilla12',   component: Grilla12Component},
 {path: 'Grilla13',   component: Grilla13Component},



 //SAntiAGO COLUMBARIO
 {path: 'GrillaCS1',   component: Grillacs1Component},
 {path: 'GrillaCS2',   component: Grillacs2Component},
 {path: 'GrillaCS3',   component: Grillacs3Component},
 {path: 'GrillaCS4',   component: Grillacs4Component},
 {path: 'GrillaCS5',   component: Grillacs5Component},
 {path: 'GrillaCS6',   component: Grillacs6Component},
 {path: 'GrillaCS7',   component: Grillacs7Component},


//Columbario Luperon
{path: 'Pared1CO',    component: Pared1COComponent},

//Columbario Santiago
 {path: 'Pared1CS',    component: Pared1CSComponent},
 {path: 'Pared2CS',    component: Pared2CSComponent},
 {path: 'Pared3CS',    component: Pared3CSComponent},
 {path: 'Pared4CS',    component: Pared4CSComponent},
 {path: 'Pared5CS',    component: Pared5CSComponent},
 {path: 'Pared6CS',    component: Pared6CSComponent},
 {path: 'Pared7CS',    component: Pared7CSComponent},

 //Columbario Luperon
 //EXTERNOS
 {path: 'Pared1CL',    component: Pared1CLComponent},
 {path: 'Pared2CL',    component: Pared2CLComponent},
 {path: 'Pared3CL',    component: Pared3CLComponent},
 {path: 'Pared4CL',    component: Pared4CLComponent},
 //INTERNOS
 {path: 'Pared5CL',    component: Pared5CLComponent},
 {path: 'Pared6CL',    component: Pared6CLComponent},
 {path: 'Pared7CL',    component: Pared7CLComponent},
 {path: 'Pared8CL',    component: Pared8CLComponent},
 {path: 'Pared9CL',    component: Pared9CLComponent},
 //Familiar
 {path: 'Pared10CL',   component: Pared10CLComponent},
 {path: 'Pared11CL',   component: Pared11CLComponent},

 //Ingenio Santiago
 {path: 'Pared1IS',   component: Pared1ISComponent},

 //Maximo Gomez
 {path: 'Pared1MG',   component: Pared1MGComponent},
 {path: 'Pared2MG',   component: Pared2MGComponent},

 //Cristo Salvador
 //Etapa1
 {path: 'Pared1CCS',   component: Pared1CCSComponent},
 {path: 'Pared2CCS',   component: Pared2CCSComponent},
 {path: 'Pared3CCS',   component: Pared3CCSComponent},
 //Etapa2
 {path: 'Pared4CCS',   component: Pared4CCSComponent},
 //Etapa3
 {path: 'Pared5CCS',   component: Pared5CCSComponent},
 //Etapa4
 {path: 'Pared6CCS',   component: Pared6CCSComponent},
 //Etapa5
 {path: 'Pared7CCS',    component: Pared7CCSComponent},
 {path: 'Pared8CCS',    component: Pared8CCSComponent},
 {path: 'Pared9CCS',    component: Pared9CCSComponent},
 {path: 'Pared10CCS',   component: Pared10CCSComponent},
 {path: 'Pared11CCS',   component: Pared11CCSComponent},
 {path: 'Pared12CCS',   component: Pared12CCSComponent},
 {path: 'Pared13CCS',   component: Pared13CCSComponent},
//Etapa6
 {path: 'Pared14CCS',   component: Pared14CCSComponent},
 {path: 'Pared15CCS',   component: Pared15CCSComponent},
 {path: 'Pared16CCS',   component: Pared16CCSComponent},

 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListparedesRoutingModule { }
