import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { GrillaComponent } from './components/shared/grilla/grilla.component';
import { CristoRComponent } from './components/cristo-r/cristo-r.component';
import { PuertaDCComponent } from './components/puerta-dc/puerta-dc.component';
import { MenuComponent } from './components/shared/menu/menu.component';
import { ModalComponent } from './components/modal/modal.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { Grilla2Component } from './components/shared/grilla2/grilla2.component';
import { Grilla3Component } from './components/shared/grilla3/grilla3.component';
import { Grilla4Component } from './components/shared/grilla4/grilla4.component';
import { Grilla5Component } from './components/shared/grilla5/grilla5.component';
import { Grilla6Component } from './components/shared/grilla6/grilla6.component';
import { Grilla7Component } from './components/shared/grilla7/grilla7.component';
import { Grilla8Component } from './components/shared/grilla8/grilla8.component';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MenuPCComponent } from './components/shared/menu-pc/menu-pc.component';
import { Pared1Component } from './components/shared/paredesPC/pared1/pared1.component';
import { Pared2Component } from './components/shared/paredesPC/pared2/pared2.component';
import { Pared3Component } from './components/shared/paredesPC/pared3/pared3.component';
import { Pared4Component } from './components/shared/paredesPC/pared4/pared4.component';
import { Pared5Component } from './components/shared/paredesPC/pared5/pared5.component';
import { Pared6Component } from './components/shared/paredesPC/pared6/pared6.component';
import { Pared7Component } from './components/shared/paredesPC/pared7/pared7.component';
import { Pared8Component } from './components/shared/paredesPC/pared8/pared8.component';
import { CementerioCristoRComponent } from './cristo-redentor/cementerio-cristo-r/cementerio-cristo-r.component';
import { CementerioPuertaRComponent } from './puerta-del-cielo/cementerio-puerta-r/cementerio-puerta-r.component';
import { CementeriosModule } from './cementerios/cementerios.module';
import { ListparedesModule } from './listparedes/listparedes.module';
import { ParedesModule } from './paredes/paredes.module';


////Columbario Santiago
import { Pared1COComponent } from './paredes/columbario-ozama/Etapa1/pared1-co/pared1-co.component';


///Columbario Santiago
//Etapa1


import { Pared1CSComponent } from './paredes/columbario-santiago/Etapa1/pared1-cs/pared1-cs.component';
import { Pared2CSComponent } from './paredes/columbario-santiago/Etapa1/pared2-cs/pared2-cs.component';
import { Pared3CSComponent } from './paredes/columbario-santiago/etapa1/pared3-cs/pared3-cs.component';
import { Pared4CSComponent } from './paredes/columbario-santiago/etapa1/pared4-cs/pared4-cs.component';
import { Pared5CSComponent } from './paredes/columbario-santiago/etapa1/pared5-cs/pared5-cs.component';
//Isla Central
import { Pared6CSComponent } from './paredes/columbario-santiago/isla-central/pared6-cs/pared6-cs.component';
import { Pared7CSComponent } from './paredes/columbario-santiago/isla-central/pared7-cs/pared7-cs.component';


//Columbarios Luperon
//Externos
import { Pared1CLComponent } from './paredes/columbario-luperon/externos/pared1-cl/pared1-cl.component';
import { Pared2CLComponent } from './paredes/columbario-luperon/externos/pared2-cl/pared2-cl.component';
import { Pared3CLComponent } from './paredes/columbario-luperon/externos/pared3-cl/pared3-cl.component';
import { Pared4CLComponent } from './paredes/columbario-luperon/externos/pared4-cl/pared4-cl.component';
import { Pared5CLComponent } from './paredes/columbario-luperon/internos/pared5-cl/pared5-cl.component';
import { Pared6CLComponent } from './paredes/columbario-luperon/internos/pared6-cl/pared6-cl.component';
import { Pared7CLComponent } from './paredes/columbario-luperon/internos/pared7-cl/pared7-cl.component';
import { Pared8CLComponent } from './paredes/columbario-luperon/internos/pared8-cl/pared8-cl.component';
import { Pared9CLComponent } from './paredes/columbario-luperon/internos/pared9-cl/pared9-cl.component';
import { Pared10CLComponent } from './paredes/columbario-luperon/familiar/pared10-cl/pared10-cl.component';
import { Pared11CLComponent } from './paredes/columbario-luperon/familiar/pared11-cl/pared11-cl.component';

//Ingenio Santiago
//Etapa1
import { Pared1ISComponent } from './paredes/ingenio-santiago/etapa1/pared1-is/pared1-is.component';

//Maximo Gomez
//Etapa1
import { Pared1MGComponent } from './paredes/maximo-gomez/etapa1/pared1-mg/pared1-mg.component';
//Etapa2
import { Pared2MGComponent } from './paredes/maximo-gomez/etapa2/pared2-mg/pared2-mg.component';

//Cristo Salvador
//Etapa1
import { Pared1CCSComponent } from './paredes/cristo-salvador/etapa1/pared1-ccs/pared1-ccs.component';
import { Pared2CCSComponent } from './paredes/cristo-salvador/etapa1/pared2-ccs/pared2-ccs.component';
import { Pared3CCSComponent } from './paredes/cristo-salvador/etapa1/pared3-ccs/pared3-ccs.component';
//Etapa 2
import { Pared4CCSComponent } from './paredes/cristo-salvador/etapa2/pared4-ccs/pared4-ccs.component';
//Etapa3
import { Pared5CCSComponent } from './paredes/cristo-salvador/etapa3/pared5-ccs/pared5-ccs.component';
//Etapa4
import { Pared6CCSComponent } from './paredes/cristo-salvador/etapa4/pared6-ccs/pared6-ccs.component';
//Etapa 5
import { Pared7CCSComponent } from './paredes/cristo-salvador/etapa5/pared7-ccs/pared7-ccs.component';
import { Pared8CCSComponent } from './paredes/cristo-salvador/etapa5/pared8-ccs/pared8-ccs.component';
import { Pared9CCSComponent } from './paredes/cristo-salvador/etapa5/pared9-ccs/pared9-ccs.component';
import { Pared10CCSComponent } from './paredes/cristo-salvador/etapa5/pared10-ccs/pared10-ccs.component';
import { Pared11CCSComponent } from './paredes/cristo-salvador/etapa5/pared11-ccs/pared11-ccs.component';
import { Pared12CCSComponent } from './paredes/cristo-salvador/etapa5/pared12-ccs/pared12-ccs.component';
import { Pared13CCSComponent } from './paredes/cristo-salvador/etapa5/pared13-ccs/pared13-ccs.component';

//Etapa 6
import { Pared14CCSComponent } from './paredes/cristo-salvador/etapa6/pared14-ccs/pared14-ccs.component';
import { Pared15CCSComponent } from './paredes/cristo-salvador/etapa6/pared15-ccs/pared15-ccs.component';
import { Pared16CCSComponent } from './paredes/cristo-salvador/etapa6/pared16-ccs/pared16-ccs.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { LoginComponent } from './components/login/login.component';
import { Grilla9Component } from './components/shared/grilla9/grilla9.component';
import { Grilla10Component } from './components/shared/grilla10/grilla10.component';
import { Grilla11Component } from './components/shared/grilla11/grilla11.component';
import { Grilla12Component } from './components/shared/grilla12/grilla12.component';
import { Grilla13Component } from './components/shared/grilla13/grilla13.component';

import { Grillacs1Component } from './components/shared/grillacs1/grillacs1.component';
import { Grillacs2Component } from './components/shared/grillacs2/grillacs2.component';
import { Grillacs3Component } from './components/shared/grillacs3/grillacs3.component';
import { Grillacs4Component } from './components/shared/grillacs4/grillacs4.component';
import { Grillacs5Component } from './components/shared/grillacs5/grillacs5.component';
import { Grillacs6Component } from './components/shared/grillacs6/grillacs6.component';
import { Grillacs7Component } from './components/shared/grillacs7/grillacs7.component';
import { BarranComponent } from './barranavegacion/barran/barran.component';
import { BarranavegacionModule } from './barranavegacion/barranavegacion.module';
import { IndexComponent } from './components/listparedes/index/index.component';
import { ParedCompletaComponent } from './components/pared-completa/pared-completa.component';
import { ParedCompletaDatosComponent } from './components/pared-completa-datos/pared-completa-datos.component';
import { ParedCompletaDatosCementerioComponent } from './components/pared-completa-datos-cementerio/pared-completa-datos-cementerio.component';
import { ParedCompletaCementerioComponent } from './components/pared-completa-cementerio/pared-completa-cementerio.component';
import { PaginavaciaComponent } from './components/paginavacia/paginavacia.component';
import { ReporteComponent } from './components/reporte/reporte.component';
















@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    GrillaComponent,
    CristoRComponent,
    PuertaDCComponent,
    MenuComponent,
    ModalComponent,
    Grilla2Component,
    Grilla3Component,
    Grilla4Component,
    Grilla5Component,
    Grilla6Component,
    Grilla7Component,
    Grilla8Component,
    MenuPCComponent,
    Pared1Component,
    Pared2Component,
    Pared3Component,
    Pared4Component,
    Pared5Component,
    Pared6Component,
    Pared7Component,
    Pared8Component,
    Pared1COComponent,  
    Pared1CLComponent,
    Pared2CLComponent,
    Pared3CLComponent,
    Pared4CLComponent,
    Pared5CLComponent,
    Pared6CLComponent,
    Pared7CLComponent,
    Pared8CLComponent,
    Pared9CLComponent,
    Pared10CLComponent,
    Pared11CLComponent,
    BuscadorComponent,
    LoginComponent,  
    Pared1ISComponent, 
    Pared1MGComponent,
    Pared2MGComponent,
    Pared1CCSComponent,
    Pared2CCSComponent,
    Pared3CCSComponent,
    Pared4CCSComponent,
    Pared5CCSComponent,
    Pared6CCSComponent,
    Pared7CCSComponent,
    Pared8CCSComponent,
    Pared9CCSComponent, 
    Pared10CCSComponent,   
    Pared11CCSComponent,
    Pared12CCSComponent,
    Pared13CCSComponent,
    Pared14CCSComponent,
    Pared15CCSComponent,
    Pared16CCSComponent,
    Grilla9Component,
    Grilla10Component,
    Grilla11Component,
    Grilla12Component,
    Grilla13Component,
    Grillacs1Component,
    Grillacs2Component,
    Grillacs3Component,
    Grillacs4Component,
    Grillacs5Component,
    Grillacs6Component,
    Grillacs7Component,
    BarranComponent,
    IndexComponent,
    ParedCompletaComponent,
    ParedCompletaDatosComponent,
    ParedCompletaDatosCementerioComponent,
    ParedCompletaCementerioComponent,
    PaginavaciaComponent,
    ReporteComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    CementeriosModule,
    ListparedesModule,
    ParedesModule,
    BarranavegacionModule
    
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
