import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParedCompletaCementerioComponent } from './pared-completa-cementerio.component';

describe('ParedCompletaCementerioComponent', () => {
  let component: ParedCompletaCementerioComponent;
  let fixture: ComponentFixture<ParedCompletaCementerioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParedCompletaCementerioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParedCompletaCementerioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
