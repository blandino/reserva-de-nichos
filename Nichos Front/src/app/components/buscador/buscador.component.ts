import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { LoginService } from '../../services/login.service';
import { GetusersService } from '../../services/getusers.service';





@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {
  
  usuariosFiltrados: any[] =[];

  constructor( private activatedRoute: ActivatedRoute,
               private login: LoginService,
               private getusersService: GetusersService) {                  
               }

  ngOnInit() {

    this.activatedRoute.params.subscribe(params =>{
      this.usuariosFiltrados = this.getusersService.getUserByName2(params['termino'])      

    })
  }

}
