import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParedCompletaDatosCementerioComponent } from './pared-completa-datos-cementerio.component';

describe('ParedCompletaDatosCementerioComponent', () => {
  let component: ParedCompletaDatosCementerioComponent;
  let fixture: ComponentFixture<ParedCompletaDatosCementerioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParedCompletaDatosCementerioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParedCompletaDatosCementerioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
