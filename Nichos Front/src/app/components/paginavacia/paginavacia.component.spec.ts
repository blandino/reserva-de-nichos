import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginavaciaComponent } from './paginavacia.component';

describe('PaginavaciaComponent', () => {
  let component: PaginavaciaComponent;
  let fixture: ComponentFixture<PaginavaciaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PaginavaciaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginavaciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
