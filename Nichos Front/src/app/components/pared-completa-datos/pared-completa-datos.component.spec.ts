import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParedCompletaDatosComponent } from './pared-completa-datos.component';

describe('ParedCompletaDatosComponent', () => {
  let component: ParedCompletaDatosComponent;
  let fixture: ComponentFixture<ParedCompletaDatosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParedCompletaDatosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParedCompletaDatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
