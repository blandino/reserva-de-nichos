import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PuertaDCComponent } from './puerta-dc.component';

describe('PuertaDCComponent', () => {
  let component: PuertaDCComponent;
  let fixture: ComponentFixture<PuertaDCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PuertaDCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PuertaDCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
