import { Component, OnInit } from '@angular/core';
import { GetusersService } from '../../../services/getusers.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from '../../../services/login.service';
import { INVCAM_Permisos } from 'src/app/models/INVCAM_Permisos';
import { PermisosService } from '../../../services/permisos.service';
import { ToastrService } from 'ngx-toastr';
import { VINVCAM_LOGIN } from '../../../models/VINVCAM_LOGIN';
import {Router} from '@angular/router'






@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  usersList: any[] = [];
  loginUserList: any[] = [];
  addUserList: any[] = []
  ListaPermisos: any[] = [];
  listaUsuariosPermisos: any[] = [];
  textoBuscar: string = '';

  isAdmin: any;
  isLogged: any;
 

  constructor(
              private getusersService: GetusersService,
              private modal: NgbModal,
              private loginService: LoginService,
              private permisosService: PermisosService,
              private toastr: ToastrService,
              private router: Router) { 

                this.getUsers();
                this.getLogin();
                this.getPermissions();

                if(!sessionStorage.getItem('login')){
                  this.router.navigate(['Login']);
                }
                
                this.isAdmin = sessionStorage.getItem('admin')
                this.isLogged=sessionStorage.getItem('login');

             }

           

  ngOnInit(): void {
   // this.isAdmin = sessionStorage.getItem('admin')
    //this.isLogged=sessionStorage.getItem('login');
  }

  getParams(){
    this.isAdmin = sessionStorage.getItem('admin')
    this.isLogged=sessionStorage.getItem('login');
  }
  

  openModal(users){
    this.modal.open(users, { size: 'xl'});   
  }

  guardar(data){


    const permiso: INVCAM_Permisos = {      
      usuarioID: data,
      estatus:  true ,
      isAdmin:  false

    }
    this.permisosService.guardarRegistro(permiso).subscribe(data =>{
    this.toastr.success('Permiso Otorgado de manera satisfactoria', 'Permiso otrogado')

    this.getUsers();
    this.getLogin();

    setTimeout(()=>{      
      console.log("mensaje")                      
      
    }, 2000)                    
     console.log("Guardado exitosamente");

    }, err =>{
      this.toastr.warning('Permiso denegado','No se pudo otorgar el permiso')
    })
  }

  refresh(){
    window.location.reload();
   }

   eliminar(id: number){
     if(confirm('Esta seguro que desea quitar el permiso')){
       this.permisosService.eliminarRegistro(id).subscribe(data =>{
         this.toastr.warning('Usuario Eliminado', 'Usiaro elimanado del sistema');

         this.getUsers();
         this.getLogin();

        //this.getusersService.getUsers();
        //this.loginService.getLoginUsers();
    

       })

     }

   }

   actualizar(id, isAdmin, usrid ){
     
    let admin;

    if (isAdmin == true){
      admin = false
    }

    if (isAdmin == false){
    admin = true
    }

    const permiso: INVCAM_Permisos = {  
      permisoID: id,
      usuarioID: usrid,
      estatus:  true ,
      isAdmin:  admin
    }

    this.permisosService.actualizarPermiso(id, permiso).subscribe(data => {

      if(admin){
        this.toastr.info('Ahora es Administrador','Permiso Adminsitrador otorgado') 
      }

      if(admin == false){
        this.toastr.error('Usuario con permisos limitados','Permisos Limitados')
      }
             
      
      
      this.getUsers();
      this.getLogin();
      
      setTimeout(()=>{
        
        console.log("mensaje")
      }, 2000)
                     
     })

   }

   buscarUsuario(termino: string){    
    /*console.log("termino")
    console.log(termino)*/
    this.router.navigate(['/buscar',termino]);

   }

   buscarUsuario2(termino: string){    

       
    let usersArr: VINVCAM_LOGIN[] = [];
    termino = termino.toLowerCase();

    for(let usuario of this.usersList){
   
    
      let userName = usuario.usrName.toLowerCase();
  
      if( userName.indexOf(termino) >=0 ){
        usersArr.push(usuario)
      }    
    } 
    console.log("Arreglo de usuarios");
    console.log(usersArr) 
    this.usersList = usersArr

    if(termino ==''){
      this.getusersService.getUsers()
    .subscribe((data: any) =>{
  
      this.usersList = data;    
  
    }); 

    }
    
     return usersArr;
   }

   getUsers(){
    this.getusersService.getUsers()
    .subscribe((data: any) =>{
  
      this.usersList = data;    
  
    }); 

   }
   

   getLogin(){

    this.loginService.getLoginUsers()
    .subscribe((data: any) =>{

      this.loginUserList = data;
      /*console.log("Lista de usuarios logueados: ")
      console.log(this.loginUserList)*/

    })

   }

   getPermissions(){
     this.permisosService.obtenerRegistro()
     .subscribe((data: any) =>{
       this.ListaPermisos = data;
       /*console.log("Lista de permisos")
       console.log(this.ListaPermisos)*/
     })
   }

   getListPermissions(){
     this.ListaPermisos.forEach(lp => {
      
      this.loginUserList.forEach(lg =>{

        if(lp.usuarioID == lg.usrname){

          this.listaUsuariosPermisos.push({})

        }
      })

     })
     

   }

 

   getUserByName(termino: string): any {

    let usersArr: VINVCAM_LOGIN[] = [];
    termino = termino.toLocaleLowerCase();
  
    for(let usuario of this.loginUserList){
      
      let userName = usuario.usrname.toLowerCase();

      if( userName.indexOf(termino) >=0 ){
        usersArr.push(usuario)
      }

      
    } 
    console.log(usersArr) 
     return usersArr;
  
   }


}
