import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grillacs3Component } from './grillacs3.component';

describe('Grillacs3Component', () => {
  let component: Grillacs3Component;
  let fixture: ComponentFixture<Grillacs3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grillacs3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grillacs3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
