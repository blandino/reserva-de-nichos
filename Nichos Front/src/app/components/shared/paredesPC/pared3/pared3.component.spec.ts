import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared3Component } from './pared3.component';

describe('Pared3Component', () => {
  let component: Pared3Component;
  let fixture: ComponentFixture<Pared3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
