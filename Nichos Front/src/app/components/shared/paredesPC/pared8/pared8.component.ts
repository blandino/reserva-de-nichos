import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { ModalComponent } from '../../../modal/modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { from, observable, Subscription } from 'rxjs';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { CrudService } from '../../../../services/crud.service';
import { FilaService } from '../../../../services/fila.service';
import { ColumnaService } from '../../../../services/columna.service';
import { ParedService } from '../../../../services/pared.service';
import { INVCAM_Inventario } from '../../../../models/INVCAM_Inventario';
import { element } from 'protractor';
import { nextTick } from 'process';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { SlicePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { PRAMASTERService } from '../../../../services/pra-master.service';

@Component({
  selector: 'app-pared8',
  templateUrl: './pared8.component.html',
  styleUrls: ['./pared8.component.css']
})
export class Pared8Component  {}

 