import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared8Component } from './pared8.component';

describe('Pared8Component', () => {
  let component: Pared8Component;
  let fixture: ComponentFixture<Pared8Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared8Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
