import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared6Component } from './pared6.component';

describe('Pared6Component', () => {
  let component: Pared6Component;
  let fixture: ComponentFixture<Pared6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
