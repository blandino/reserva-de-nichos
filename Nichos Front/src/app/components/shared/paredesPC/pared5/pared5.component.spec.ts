import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared5Component } from './pared5.component';

describe('Pared5Component', () => {
  let component: Pared5Component;
  let fixture: ComponentFixture<Pared5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
