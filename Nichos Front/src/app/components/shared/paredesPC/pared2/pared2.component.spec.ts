import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared2Component } from './pared2.component';

describe('Pared2Component', () => {
  let component: Pared2Component;
  let fixture: ComponentFixture<Pared2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
