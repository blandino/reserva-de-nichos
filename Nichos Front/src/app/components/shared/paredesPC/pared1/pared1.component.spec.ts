import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared1Component } from './pared1.component';

describe('Pared1Component', () => {
  let component: Pared1Component;
  let fixture: ComponentFixture<Pared1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
