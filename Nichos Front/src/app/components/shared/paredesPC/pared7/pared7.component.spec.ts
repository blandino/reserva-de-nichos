import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared7Component } from './pared7.component';

describe('Pared7Component', () => {
  let component: Pared7Component;
  let fixture: ComponentFixture<Pared7Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared7Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
