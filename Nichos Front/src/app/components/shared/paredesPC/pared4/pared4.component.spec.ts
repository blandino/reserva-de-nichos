import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pared4Component } from './pared4.component';

describe('Pared4Component', () => {
  let component: Pared4Component;
  let fixture: ComponentFixture<Pared4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pared4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pared4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
