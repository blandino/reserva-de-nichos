import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grillacs4Component } from './grillacs4.component';

describe('Grillacs4Component', () => {
  let component: Grillacs4Component;
  let fixture: ComponentFixture<Grillacs4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grillacs4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grillacs4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
