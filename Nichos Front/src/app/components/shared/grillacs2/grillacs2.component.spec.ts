import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grillacs2Component } from './grillacs2.component';

describe('Grillacs2Component', () => {
  let component: Grillacs2Component;
  let fixture: ComponentFixture<Grillacs2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grillacs2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grillacs2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
