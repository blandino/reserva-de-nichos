import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CrudService } from '../../../services/crud.service';
import { ParedService } from '../../../services/pared.service';
import { from, observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-menu-pc',
  templateUrl: './menu-pc.component.html',
  styleUrls: ['./menu-pc.component.css']
})
export class MenuPCComponent implements OnInit {
  listaPared: any;
  pared: string;
  subscription: Subscription;
  inventario: any;


  mcr: boolean;
  mpc: boolean;

  
  @Output()
  propagar = new EventEmitter<string>();
 

  constructor(public crudService: CrudService, public  paredService: ParedService) {
    this.paredService.obtenerPared()
              .subscribe((data: any) => {
              
                this.listaPared = data;

                console.log("bienvenido senior pared")
                console.log(this.listaPared);
                
              });
              this.propagar = new EventEmitter();

              
  }

  ngOnInit()  {
    this.subscription = this.crudService.obtenerinfo().subscribe(data => {
      this.inventario =  data;
      console.log("Walter")      
      console.log (this.inventario)
      
     
    })

  }

  onPropagar(pared: string){    
    this.pared = pared;
    this.propagar.emit(this.pared)
    console.log("Propagar: ")
    console.log(this.pared)
    return this.pared
   
  }

  cambiarPared(pared: any){
    this.pared = pared;
    console.log("su pared es: ")
    console.log(this.pared)
    return this.pared
   
  }
  editar(pared){
    this.paredService.EscucharInfo(pared);
  }

  menu(){
    this.mcr = true;
    this.mpc = false;
  }
  
  menuPc(){  
    this.mpc = true;
    this.mcr = false;
  }

 
}
