import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPCComponent } from './menu-pc.component';

describe('MenuPCComponent', () => {
  let component: MenuPCComponent;
  let fixture: ComponentFixture<MenuPCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuPCComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuPCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
