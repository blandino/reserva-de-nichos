import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla12Component } from './grilla12.component';

describe('Grilla12Component', () => {
  let component: Grilla12Component;
  let fixture: ComponentFixture<Grilla12Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla12Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla12Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
