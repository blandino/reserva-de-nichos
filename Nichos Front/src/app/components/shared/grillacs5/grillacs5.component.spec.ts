import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grillacs5Component } from './grillacs5.component';

describe('Grillacs5Component', () => {
  let component: Grillacs5Component;
  let fixture: ComponentFixture<Grillacs5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grillacs5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grillacs5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
