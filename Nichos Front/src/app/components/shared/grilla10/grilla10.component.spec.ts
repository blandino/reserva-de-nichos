import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla10Component } from './grilla10.component';

describe('Grilla10Component', () => {
  let component: Grilla10Component;
  let fixture: ComponentFixture<Grilla10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
