import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grillacs6Component } from './grillacs6.component';

describe('Grillacs6Component', () => {
  let component: Grillacs6Component;
  let fixture: ComponentFixture<Grillacs6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grillacs6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grillacs6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
