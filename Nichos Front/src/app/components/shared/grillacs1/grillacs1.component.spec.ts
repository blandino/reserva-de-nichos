import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grillacs1Component } from './grillacs1.component';

describe('Grillacs1Component', () => {
  let component: Grillacs1Component;
  let fixture: ComponentFixture<Grillacs1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grillacs1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grillacs1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
