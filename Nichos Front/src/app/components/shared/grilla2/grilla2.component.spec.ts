import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla2Component } from './grilla2.component';

describe('Grilla2Component', () => {
  let component: Grilla2Component;
  let fixture: ComponentFixture<Grilla2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
