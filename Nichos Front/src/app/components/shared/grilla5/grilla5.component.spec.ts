import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla5Component } from './grilla5.component';

describe('Grilla5Component', () => {
  let component: Grilla5Component;
  let fixture: ComponentFixture<Grilla5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla5Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
