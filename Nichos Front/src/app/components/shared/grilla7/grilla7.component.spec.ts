import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla7Component } from './grilla7.component';

describe('Grilla7Component', () => {
  let component: Grilla7Component;
  let fixture: ComponentFixture<Grilla7Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla7Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
