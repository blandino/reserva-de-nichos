import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla11Component } from './grilla11.component';

describe('Grilla11Component', () => {
  let component: Grilla11Component;
  let fixture: ComponentFixture<Grilla11Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla11Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
