import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { from, observable, Subscription } from 'rxjs';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { element, promise } from 'protractor';
import { listenerCount, nextTick } from 'process';
import { positionElements } from '@ng-bootstrap/ng-bootstrap/util/positioning';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { SlicePipe, CommonModule, getLocaleDateTimeFormat } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { Http2ServerResponse } from 'http2';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalComponent } from '../../../components/modal/modal.component';
import { CrudService } from '../../../services/crud.service';
import { FilaService } from '../../../services/fila.service';
import { ColumnaService } from '../../../services/columna.service';
import { ParedService } from '../../../services/pared.service';
import { INVCAM_Inventario } from '../../../models/INVCAM_Inventario';
import { PRAMASTERService } from '../../../services/pra-master.service';
import { SalespersonService } from '../../../services/salesperson.service';
import { ParametroService } from '../../../services/parametro.service';
import { GetusersService } from '../../../services/getusers.service';
import { FacturasService } from '../../../services/facturas.service';
import { INVCAM_Fallecidos } from '../../../models/INVCAM_Fallecidos';
import { FallecidoService } from '../../../services/fallecido.service';
import { VInventarioService } from '../../../services/v-inventario.service';
import {Router} from '@angular/router';
import { VincamfallecidoclientesService } from '../../../services/vincamfallecidoclientes.service';

@Component({
  selector: 'app-grilla13',
  templateUrl: './grilla13.component.html',
  styleUrls: ['./grilla13.component.css']
})
export class Grilla13Component implements OnInit {

  listaCementerios: any[] = ['Cristo Redentor'];
  codigoCementerio: any = 'CR'
  etapa: any[] = ['Etapa6'];
  paredCementerio: any ='13';

  reservaEliminar: any [] = [];
  reservaEliminarId: any;
  paredReserva: any;
  columnaReserva: any;
  filaReserva: any;
  listaprueba: any[] = [];
  listaprueba2: any[] = [];

  retornoValidacion: any;
  idFallecido: any;  
  vIncamInventarioData: any[] = [];

  form: FormGroup;
  formNecesidad: FormGroup;
  formReservar: FormGroup;
  subscription: Subscription;
  miArreglo: any[] = [];
  listaFilas: any[] = [];
  
  listaColumnas: any[] = [];
  listaPared: any[] = [];
  listaInventario: any[] =[];
  listaInventarioFila: any[] =[];
  listaInventarioColumna: any[] =[];
  listaInventarioPared: any[] =[];
  salespersonList: any[] = [];
  parametroList: any[] = [];

  listaReserva: any[] = [];
  
  myLista: any[] = [];
  datos: any;
  result: any;
  result2: string[] =[]
  
  isChecked: boolean;
  estatus: any;
  
  rojo: any;
  gris: any;
  verde: any;
  
  disponible: any;
  
  //Formulario
  inventario : any;
  idInventario = 0;
  

   t : any;
  pared: any;
res: any[] = [];

listaMostrar: any[] = []
listaFactura: any[] = [];

listaMostrarcONTRACT: any[] = []

idCliente: any;
cliente: any;
  datosUsuario:any[] = ['Walter Garcia'];
  
  isContract: boolean
  isPA: boolean

  formulario:any;

  contract: any[] = [];
  custic: any;

  dataPanum: any;
  factura: any;
  fechavencimiento: any [] = [];
  usersList: any[] = [];

  nombreModal: any;
  prueba: any [] = [];

  isAdmin: any; 
  
    constructor(private modal:         NgbModal,  
                private formBuilder:   FormBuilder,
                private crudService: CrudService,
                private filaService: FilaService,
                private columnaService: ColumnaService,
                public paredService:   ParedService,
                public toastr: ToastrService,
                private pRAMASTERService: PRAMASTERService,
                private salespersonService: SalespersonService,
                private getusersService: GetusersService,
                private parametroService: ParametroService,
                private facturasService: FacturasService,
                private fallecidoService: FallecidoService,
                private vInventarioService: VInventarioService,
                private router: Router,
                private vincamfallecidoclientesService: VincamfallecidoclientesService) {  this.form = this.formBuilder.group({
                  id:0,
                  Codigo:[''],
                  Contrato: [''],
                  Panum: ['', [Validators.required]],
                  Cementerio: ['', [Validators.required]],
                  Pared: ['', [Validators.required]],
                  Fila: ['', [Validators.required]],
                  Columna: ['', [Validators.required]],
                  CodigoAnterior: [''],
                  Etapa: ['', [Validators.required]],
                  Estatus: [''],
                  IdCliente: [''],
                  Cliente: [''] ,
                  NombreFallecido: [''],
                  FechaNacimiento: [''],
                  FechaFallecimiento: [''] ,    
                })       
                this.isAdmin = sessionStorage.getItem('admin')
                
                this.formNecesidad = this.formBuilder.group({
                  id:0,
                  Codigo:[''],                 
                  Cementerio: ['', [Validators.required]],
                  Pared: ['', [Validators.required]],
                  Fila: ['', [Validators.required]],
                  Columna: ['', [Validators.required]],
                  CodigoAnterior: [''],
                  Etapa: ['', [Validators.required]],
                  Estatus: [''],                  
                  NombreFallecido:[''],

                  Contrato: ['',[Validators.required]],
                  IdCliente: ['',[Validators.required]],
                  Cliente: ['',[Validators.required]],
                  FechaNacimiento: [''],
                  FechaFallecimiento: [''] ,   
                }) 

                this.formReservar = this.formBuilder.group({
                  id:0,                  
                  Vendedor: ['', [Validators.required]],                 
                  FechaVencimiento:['', [Validators.required]]
                }) 

               

                //console.log('Formulario',this.form)   
            
                this.filaService.obtenerFila()
                .subscribe((data: any) => {
                
                  //this.listaFilas = data;
                  
                });

                this.filaService.obtenerEstructuraFila(this.listaCementerios[0],
                                                       this.etapa[0],
                                                       this.paredCementerio )
                .subscribe((data: any) =>{
                  this.listaFilas = data;
                })
                          
                          
                this.columnaService.obtenerColumna()
                .subscribe((data: any) => {                
                  
                  //this.listaColumnas = data;
                  //this.listaprueba = data;
                 
                });

                this.columnaService.obtenerEstructuraColumna(this.listaCementerios[0],
                                                             this.etapa[0],
                                                              this.paredCementerio )
                .subscribe((data: any) =>{
                  this.listaColumnas = data;

                })
            
                this.paredService.obtenerPared()
                .subscribe((data: any) => {
                
                  this.listaPared = data;
                  
                });

                this.salespersonService.obtenerSalesPerson()
                .subscribe((data: any) =>{

                  this.salespersonList = data;
                  //console.log("Lista de vendedores");
                  //console.log(this.salespersonList)
                });
                
                this.parametroService.obtenerParametro()
                .subscribe((data: any)=>{
                  this.parametroList = data;
                  let fecha = new Date();
                  this.parametroList.forEach(element =>{

                    //element.diasReserva
                   fecha.setDate(fecha.getDate() + element.diasReserva)
                   
                   this.fechavencimiento.push(fecha)
                   //console.log(this.fechavencimiento)

                  })
                });

                this.getusersService.getUsers()
                .subscribe((data: any) =>{

                  this.usersList = data;
                  ////console.log("Lista de usuarios: ")
                  ////console.log(this.usersList);
                });  
                

             
                
                setTimeout(()=>{
                    //console.log("TimeOut")
                
  
                //this.crudService.obtenerRegistro()
                this.vincamfallecidoclientesService.obtenerFallecidosClientes()
                .subscribe((data: any) =>{    

                 
                 
                  this.listaInventario = data;  
                  //console.log("lista vendedor") 
                  //console.log(this.listaInventario)          
                  
                  
                  this.listaInventarioFila =  this.listaInventario.map(data => data.fila);
               
                  this.listaInventarioColumna = this.listaInventario.map(data => data.columna);
                  
                  this.listaInventarioPared = this.listaInventario.map(data => data.pared);
             
                  
                 this.listaInventario
                 this.listaFilas;
                 this.listaColumnas;
                 this.listaInventarioFila;
                 this.listaInventarioColumna;
                 this.listaInventarioPared;      
  
                 this.listaFilas.forEach(fil =>{

                  this.listaColumnas.forEach(col =>{
                 
                     
                    //const pared = "1" 
                    const idlibre =  0;
                    
                                                              
                     if(this.buscarInventario(fil , col, this.paredCementerio, this.etapa) == true){
                       this.datos = fil + "-" + col.columna + "-" + this.paredCementerio;                    
                     
                  }
                  else{
                   
                    this.datos = col + "-" + fil
                    this.myLista.push({id: idlibre,codigo:this.datos,fila: fil, columna: col, estado: this.verde, pared: this.paredCementerio, etapa:this.etapa[0]  } )
                  
                  }                    
                  
                });  
                                
                  
                });
                  
                  this.result = this.myLista;                   
                 
                 //console.table(this.result)           
                
                 
                });}, 100)
             }
          
            
            
              ngOnInit(): void {                
                
                this.crudService.obtenerRegistro();
                this.isChecked = true;
                ////console.log(this.isChecked);
                this.rojo = 'ocupado';
                this.gris = 'vendido';
                this.verde = 'disponible';
                
                //console.log('Formulario OnInit',this.form)  
  
              }

                        
              guardar(){
                //console.log("ID")
                //console.log(this.idInventario)
                if(this.idInventario ===0){
                  this.agregar();
                  }
                else{
                  this.editarInv();
                }   
                                    
             }
             
              /*agregar(){

               
                

                let fechaNacimiento    = this.form.get('FechaNacimiento').value;
                let fechaFallecimiento = this.form.get('FechaFallecimiento').value;
                let tipo ='PA'



                if (this.form.get('Panum').value == undefined){
                  return
                }
                if (this.form.get('NombreFallecido').value == undefined && this.form.get('NombreFallecido').value == null && this.isChecked == true){
                  this.toastr.error('Nombre fallecido vacio','Debe ingresar el nombre del fallecido')
                  return
                }           
                
                 if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value == '' && this.isChecked == true){
                  this.toastr.error('Debe ingresar la fecha de nacimiento y fallecimiento','Fechas vacias')
                  return
                } 

                if (this.form.get('NombreFallecido').value == undefined && this.form.get('NombreFallecido').value == null && this.isChecked == false){
                  fechaNacimiento    =  new Date();
                  fechaFallecimiento =  new Date(); 
                }           
                
                 if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value == '' && this.isChecked == false){
                  fechaNacimiento    =  new Date();
                  fechaFallecimiento =  new Date(); 
                } 

                let cementerio;
                               
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.form.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio;
                 
               }
               else{
                 cementerio = 'PC';
               }       
               
               if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value ==''){
                 fechaNacimiento    =  new Date();
                 fechaFallecimiento =  new Date(); 
               }
               
               
  
               const inventario: INVCAM_Inventario = {              
                 Codigo: cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
                 Contrato: this.form.get('Contrato').value,
                 Panum: this.form.get('Panum').value,
                 Cementerio: this.form.get('Cementerio').value,
                 Pared: this.form.get('Pared').value,
                 Fila: this.form.get('Fila').value,
                 Columna: this.form.get('Columna').value,
                 CodigoAnterior: 'N/A',
                 Etapa: this.form.get('Etapa').value,
                 Estatus: this.estatus,
                 Vendedor: 'N/A',
                 FechaReserva: new Date("2021/02/20"),
                 IdCliente: this.form.get('IdCliente').value,
                 NombreCliente: this.form.get('Cliente').value,
                 NombreFallecido: this.form.get('NombreFallecido').value,                 
                 FechaRegistro: new Date(),
                 FechaNacimiento: fechaNacimiento,
                 FechaFallecimiento: fechaFallecimiento,
                 Tipo: tipo
                 
                 
             
               }    
               //console.log(inventario)
               //console.log(this.form)  


              
                  this.crudService.guardarRegistro(inventario).subscribe(data =>{   
                    this.toastr.success('Registro Creado','El registro se guardo correctamente')
                    this.listaMostrar =['']
                    this.form.reset()

                   
                    setTimeout(()=>{
                      this.refresh();
                      //console.log("mensaje")                      
                      
                    }, 2000)
                    
                     //console.log("Guardado exitosamente");
                     
                   }, err =>{
                    
                     if(this.form.invalid){
                      err= this.toastr.error('Error','Favor completar todos los campos')
                    }
                    else{
                      err=  this.toastr.error('Error','El registro existe en al base de datos')
                   

                    }
                    
                    

                   })
                
                if(this.form.invalid){
                  return Object.values(this.form.controls).forEach(control =>{
                    control.markAsTouched();
                  })
                }
                //
     
            }*/
  
            agregar(){

              let fechaNacimiento    = this.form.get('FechaNacimiento').value;
              let fechaFallecimiento = this.form.get('FechaFallecimiento').value;

              let fechaNacimiento_2    = new Date();
              let fechaFallecimiento_2 = new Date();

              let fechaNacimiento_3   = new Date();
              let fechaFallecimiento_3 = new Date();

              let fechaNacimiento_4    = new Date();
              let fechaFallecimiento_4 = new Date();

              let fechaNacimiento_5    = new Date();
              let fechaFallecimiento_5 = new Date();

              let fechaNacimiento_6    = new Date();
              let fechaFallecimiento_6 = new Date();

              let fechaNacimiento_7    = new Date();
              let fechaFallecimiento_7 = new Date();

              let fechaNacimiento_8    = new Date();
              let fechaFallecimiento_8 = new Date();

               
                

              //let fechaNacimiento    = this.form.get('FechaNacimiento').value;
              //let fechaFallecimiento = this.form.get('FechaFallecimiento').value;
              let tipo ='PA'



              if (this.form.get('Panum').value == undefined){
                return
              }
              if (this.form.get('NombreFallecido').value == undefined && this.form.get('NombreFallecido').value == null && this.isChecked == true){
                this.toastr.error('Nombre fallecido vacio','Debe ingresar el nombre del fallecido')
                return
              }           
              
               if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value == '' && this.isChecked == true){
                this.toastr.error('Debe ingresar la fecha de nacimiento y fallecimiento','Fechas vacias')
                return
              } 

              if (this.form.get('NombreFallecido').value == undefined && this.form.get('NombreFallecido').value == null && this.isChecked == false){
                fechaNacimiento    =  new Date();
                fechaFallecimiento =  new Date(); 
              }           
              
               if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value == '' && this.isChecked == false){
                fechaNacimiento    =  new Date();
                fechaFallecimiento =  new Date(); 
              } 

              

              let cementerio;
                             
             if (this.isChecked == true){
               this.estatus = 'ocupado';
             }
             else{
               this.estatus = 'vendido';
             }

             if(this.form.get('Cementerio').value == this.listaCementerios[0]){
               cementerio = this.codigoCementerio;
               
             }
             else{
               cementerio = 'PC';
             }       
             
             if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value ==''){
               fechaNacimiento    =  new Date();
               fechaFallecimiento =  new Date(); 
             }
             
             let codigo =  cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value              
                this.validarCodigo(codigo);
    
                if (this.retornoValidacion == true){
                  this.toastr.error('Error','El registro existe en la base de datos')
                  return;
                }
             

             const inventario: INVCAM_Inventario = {              
               Codigo: cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
               Contrato: this.form.get('Contrato').value,
               Panum: this.form.get('Panum').value,
               Cementerio: this.form.get('Cementerio').value,
               Pared: this.form.get('Pared').value,
               Fila: this.form.get('Fila').value,
               Columna: this.form.get('Columna').value,
               CodigoAnterior: 'N/A',
               Etapa: this.form.get('Etapa').value,
               Estatus: this.estatus,
               Vendedor: 'N/A',
               FechaReserva: new Date("2021/02/20"),
               IdCliente: this.form.get('IdCliente').value,
               NombreCliente: this.form.get('Cliente').value,
               //NombreFallecido: this.form.get('NombreFallecido').value,                 
               FechaRegistro: new Date(),
               //FechaNacimiento: fechaNacimiento,
               //FechaFallecimiento: fechaFallecimiento,
               Tipo: tipo   
             }   
             
             
             const fallecido: INVCAM_Fallecidos = {
              codigoInventario      :cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
              fallecido1           :this.form.get('NombreFallecido').value, 
              fechaNacimiento1     :fechaNacimiento,
              fechaFallecimiento1  :fechaFallecimiento,

              fallecido2           :null, 
              fechaNacimiento2     :fechaNacimiento_2,
              fechaFallecimiento2  :fechaFallecimiento_2,

              fallecido3           :null,
              fechaNacimiento3     :fechaNacimiento_3,
              fechaFallecimiento3  :fechaFallecimiento_3,

              fallecido4           :null,
              fechaNacimiento4     :fechaNacimiento_4,
              fechaFallecimiento4  :fechaFallecimiento_4,
              
              fallecido5           :null,
              fechaNacimiento5     :fechaNacimiento_5,
              fechaFallecimiento5  :fechaFallecimiento_5,

              fallecido6           :null,
              fechaNacimiento6     :fechaNacimiento_6,
              fechaFallecimiento6  :fechaFallecimiento_6,

              fallecido7           :null,
              fechaNacimiento7     :fechaNacimiento_7,
              fechaFallecimiento7  :fechaFallecimiento_7,

              fallecido8           :null,
              fechaNacimiento8     :fechaNacimiento_8,
              fechaFallecimiento8  :fechaFallecimiento_8,
             }

             //console.log(inventario)
             //console.log(this.form)  


                this.crudService.guardarRegistro(inventario).subscribe(data =>{   
                  this.toastr.success('Registro Creado','El registro se guardo correctamente')
                  this.listaMostrar =['']
                  this.form.reset()

                 
                  setTimeout(()=>{
                    this.refresh();
                    //console.log("mensaje")                      
                    
                  }, 2000)
                  
                   //console.log("Guardado exitosamente");
                   
                 }, err =>{                 
                    err=  this.toastr.error('Error','Error guardando el registro')              
                 })

                 if (fallecido.fallecido1 != null){

                  this.fallecidoService.guardarRegistro(fallecido).subscribe(data =>{   
                    this.toastr.success('Registro Creado','El registro se guardo correctamente')
                    this.listaMostrar =['']
                    this.form.reset()
    
                   
                    setTimeout(()=>{
                      this.refresh();
                    }, 5000)
                   }, err =>{
                    
                      err=  this.toastr.error('Error','Error al guardar el Fallecido')                  
    
                    
                   })

                 }



              
              if(this.form.invalid){
                return Object.values(this.form.controls).forEach(control =>{
                  control.markAsTouched();
                  this.toastr.error('Error','Favor completar todos los campos')
                })
              }
              

              setTimeout(()=>{
                this.refresh();
              }, 5000)
   
          }
            editarInv(){

              let fechaNacimiento      = new Date();
              let fechaFallecimiento   = new Date();

              let fechaNacimiento_2    = new Date();
              let fechaFallecimiento_2 = new Date();

              let fechaNacimiento_3   = new Date();
              let fechaFallecimiento_3 = new Date();

              let fechaNacimiento_4    = new Date();
              let fechaFallecimiento_4 = new Date();

              let fechaNacimiento_5    = new Date();
              let fechaFallecimiento_5 = new Date();

              let fechaNacimiento_6    = new Date();
              let fechaFallecimiento_6 = new Date();

              let fechaNacimiento_7    = new Date();
              let fechaFallecimiento_7 = new Date();

              let fechaNacimiento_8    = new Date();
              let fechaFallecimiento_8 = new Date();

              let cementerio;
              let tipo ='PA'

     
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.form.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio
                 
               }
               else{
                 cementerio = 'PC';
               }             
  
               const inventario: INVCAM_Inventario = {
                 Id: this.inventario.id,
                 Codigo: cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
                 Contrato: this.form.get('Contrato').value,
                 Panum: this.form.get('Panum').value,
                 Cementerio: this.form.get('Cementerio').value,
                 Pared: this.form.get('Pared').value,
                 Fila: this.form.get('Fila').value,
                 Columna: this.form.get('Columna').value,
                 CodigoAnterior: 'N/A',
                 Etapa: this.form.get('Etapa').value,
                 Estatus: this.estatus,
                 Vendedor: 'N/A',
                 FechaReserva: new Date("2021/02/20"),
                 IdCliente: this.form.get('IdCliente').value,
                 NombreCliente: this.form.get('Cliente').value,
                 //NombreFallecido: this.form.get('NombreFallecido').value,
                 FechaRegistro: new Date(),
                 //FechaNacimiento: this.form.get('FechaNacimiento').value,
                 //FechaFallecimiento: this.form.get('FechaFallecimiento').value,
                 Tipo: tipo
             
               }
               const fallecido: INVCAM_Fallecidos = {
                Id: this.idFallecido, 
                codigoInventario      :cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
                fallecido1           :this.form.get('NombreFallecido').value, 
                fechaNacimiento1     :fechaNacimiento,
                fechaFallecimiento1  :fechaFallecimiento,

                fallecido2           :null, 
                fechaNacimiento2     :fechaNacimiento_2,
                fechaFallecimiento2  :fechaFallecimiento_2,

                fallecido3           :null,
                fechaNacimiento3     :fechaNacimiento_3,
                fechaFallecimiento3  :fechaFallecimiento_3,

                fallecido4           :null,
                fechaNacimiento4     :fechaNacimiento_4,
                fechaFallecimiento4  :fechaFallecimiento_4,
                
                fallecido5           :null,
                fechaNacimiento5     :fechaNacimiento_5,
                fechaFallecimiento5  :fechaFallecimiento_5,

                fallecido6           :null,
                fechaNacimiento6     :fechaNacimiento_6,
                fechaFallecimiento6  :fechaFallecimiento_6,

                fallecido7           :null,
                fechaNacimiento7     :fechaNacimiento_7,
                fechaFallecimiento7  :fechaFallecimiento_7,

                fallecido8           :null,
                fechaNacimiento8     :fechaNacimiento_8,
                fechaFallecimiento8  :fechaFallecimiento_8,
              }
               this.crudService.actualizarInventario(this.idInventario, inventario).subscribe(data => {
                this.toastr.info('Registro Actualizado','El registro se actualizo correctamente')            
                
                setTimeout(()=>{
                  this.refresh();
                  //console.log("mensaje")
                }, 2000)
                //this.refresh();
                //console.log("Guardado exitosamente....");
                this.crudService.obtenerinfo();
                //this.form.reset();
                this.idInventario = 0;                
               }, err =>{             
                 err=  this.toastr.error('Error','Eerror actualizando registro')
               //this.listaMostrar =['']
               //this.form.reset()               
              }) 

              if(fallecido.fallecido1 != null){

                this.fallecidoService.actualizarFallecido(this.idFallecido, fallecido).subscribe(data =>{
                  this.toastr.info('Fallecido Actualizado', 'El fallecido se actualizo correctamente ')
  
                  setTimeout(()=>{
                    this.refresh();
                  }, 3000)
                  this.idFallecido = 0;
                }, err =>{
                  err = this.toastr.error('Error', 'Error al actualizar fallecido')
                })

              }

            

               if(this.form.invalid){
                return Object.values(this.form.controls).forEach(control =>{
                  control.markAsTouched();
                  this.toastr.error('Error','Favor completar todos los campos a editar')
                })
              }

              setTimeout(()=>{
                this.refresh();
              }, 3000)
  
            }

             guardarNecesidad(){
                //console.log("ID")
                //console.log(this.idInventario)
                if(this.idInventario ===0){
                  this.agregarNecesidad();
                  }
                else{
                  this.editarInvNecesidad();
                }   
                                    
             }
             
              agregarNecesidad(){

                let tipo ='N'

                let fechaNacimiento    = this.formNecesidad.get('FechaNacimiento').value;
                let fechaFallecimiento = this.formNecesidad.get('FechaFallecimiento').value;

                let fechaNacimiento_2    = new Date();
                let fechaFallecimiento_2 = new Date();
  
                let fechaNacimiento_3   = new Date();
                let fechaFallecimiento_3 = new Date();
  
                let fechaNacimiento_4    = new Date();
                let fechaFallecimiento_4 = new Date();
  
                let fechaNacimiento_5    = new Date();
                let fechaFallecimiento_5 = new Date();
  
                let fechaNacimiento_6    = new Date();
                let fechaFallecimiento_6 = new Date();
  
                let fechaNacimiento_7    = new Date();
                let fechaFallecimiento_7 = new Date();
  
                let fechaNacimiento_8    = new Date();
                let fechaFallecimiento_8 = new Date();

                if (this.formNecesidad.get('NombreFallecido').value == undefined && this.formNecesidad.get('NombreFallecido').value == null && this.isChecked == true){
                  this.toastr.error('Nombre fallecido vacio','Debe ingresar el nombre del fallecido')
                  return
                }

                if (this.formNecesidad.get('FechaNacimiento').value == undefined || this.formNecesidad.get('FechaFallecimiento').value == undefined && this.isChecked == true){
                  this.toastr.error('Debe ingresar la fecha de nacimiento y fallecimiento','Fechas vacias')
                  return
                } 

                if (this.formNecesidad.get('Contrato').value == undefined || this.formNecesidad.get('Contrato').value == ''){
                  this.toastr.error('Debe el ingresar el numero de factura','No.Facutra Vacio')
                  return
                }

                if (this.formNecesidad.get('IdCliente').value == undefined){
                  this.toastr.error('Debe el ingresar el ID del cliente','ID de cliente Vacio')
                  return
                }

                if (this.formNecesidad.get('Cliente').value == undefined){
                  this.toastr.error('Debe el ingresar el nombre del cliente','Nombre de cliente Vacio')
                  return
                }


                let cementerio;
                
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.formNecesidad.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio
                 
               }
               else{
                 cementerio = 'PC';
               }    
               
                if (this.formNecesidad.get('FechaNacimiento').value == undefined && this.formNecesidad.get('FechaFallecimiento').value == undefined){
                 fechaNacimiento    =  new Date();
                 fechaFallecimiento =  new Date(); 
               }
  
               const inventario: INVCAM_Inventario = {              
                 Codigo: cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
                 Contrato: this.formNecesidad.get('Contrato').value,
                 Panum: "Necesidad",
                 Cementerio: this.formNecesidad.get('Cementerio').value,
                 Pared: this.formNecesidad.get('Pared').value,
                 Fila: this.formNecesidad.get('Fila').value,
                 Columna: this.formNecesidad.get('Columna').value,
                 CodigoAnterior: "Necesidad",
                 Etapa: this.formNecesidad.get('Etapa').value,
                 Estatus: this.estatus,
                 Vendedor: 'Necesidad',
                 FechaReserva: new Date(),
                 IdCliente: this.formNecesidad.get('IdCliente').value,
                 NombreCliente: this.formNecesidad.get('Cliente').value,
                 //NombreFallecido: this.formNecesidad.get('NombreFallecido').value,
                 FechaRegistro: new Date(),
                 //FechaNacimiento: fechaNacimiento,
                 //FechaFallecimiento: fechaFallecimiento,
                 Tipo: tipo
             
               }

               const fallecido: INVCAM_Fallecidos = {
                codigoInventario      :cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
                fallecido1           :this.formNecesidad.get('NombreFallecido').value, 
                fechaNacimiento1     :fechaNacimiento,
                fechaFallecimiento1  :fechaFallecimiento,
  
                fallecido2           :null, 
                fechaNacimiento2     :fechaNacimiento_2,
                fechaFallecimiento2  :fechaFallecimiento_2,
  
                fallecido3           :null,
                fechaNacimiento3     :fechaNacimiento_3,
                fechaFallecimiento3  :fechaFallecimiento_3,
  
                fallecido4           :null,
                fechaNacimiento4     :fechaNacimiento_4,
                fechaFallecimiento4  :fechaFallecimiento_4,
                
                fallecido5           :null,
                fechaNacimiento5     :fechaNacimiento_5,
                fechaFallecimiento5  :fechaFallecimiento_5,
  
                fallecido6           :null,
                fechaNacimiento6     :fechaNacimiento_6,
                fechaFallecimiento6  :fechaFallecimiento_6,
  
                fallecido7           :null,
                fechaNacimiento7     :fechaNacimiento_7,
                fechaFallecimiento7  :fechaFallecimiento_7,
  
                fallecido8           :null,
                fechaNacimiento8     :fechaNacimiento_8,
                fechaFallecimiento8  :fechaFallecimiento_8,
               }

                  this.crudService.guardarRegistro(inventario).subscribe(data =>{   
                    this.toastr.success('Registro Creado','El registro se guardo correctamente')
                    this.listaMostrar =['']
                    this.form.reset()
                   
                    setTimeout(()=>{
                      this.refresh();
                      //console.log("mensaje")                      
                      
                    }, 2000)                    
                     //console.log("Guardado exitosamente");
                     
                   }, err =>{
                     /*err=  this.toastr.error('Error','El registro existe en al base de datos')
                     this.listaMostrar =['']
                     this.form.reset()*/                   
                      err=  this.toastr.error('Error','Error guardando el registro')  
                   })

                   if(fallecido.fallecido1 !=  null){
                    this.fallecidoService.guardarRegistro(fallecido).subscribe(data =>{   
                      this.toastr.success('Registro Creado','El registro se guardo correctamente')
                      this.listaMostrar =['']
                      this.form.reset()
      
                     
                      setTimeout(()=>{
                        this.refresh();
                      }, 5000)
                     }, err =>{                    
                        err=  this.toastr.error('Error','Error al guardar el Fallecido')     
                     })

                   }

                   

                if(this.form.invalid){
                  return Object.values(this.form.controls).forEach(control =>{
                    control.markAsTouched();
                  })
                }

                setTimeout(()=>{
                  this.refresh();
                }, 5000)
            }
  
            editarInvNecesidad(){

              let fechaNacimiento      = new Date();
              let fechaFallecimiento   = new Date();

              let fechaNacimiento_2    = new Date();
              let fechaFallecimiento_2 = new Date();

              let fechaNacimiento_3   = new Date();
              let fechaFallecimiento_3 = new Date();

              let fechaNacimiento_4    = new Date();
              let fechaFallecimiento_4 = new Date();

              let fechaNacimiento_5    = new Date();
              let fechaFallecimiento_5 = new Date();

              let fechaNacimiento_6    = new Date();
              let fechaFallecimiento_6 = new Date();

              let fechaNacimiento_7    = new Date();
              let fechaFallecimiento_7 = new Date();

              let fechaNacimiento_8    = new Date();
              let fechaFallecimiento_8 = new Date();

              let cementerio;
              let tipo ='N'      
              
               if (this.isChecked == true){
                 this.estatus = 'ocupado';
               }
               else{
                 this.estatus = 'vendido';
               }
  
               if(this.formNecesidad.get('Cementerio').value == this.listaCementerios[0]){
                 cementerio = this.codigoCementerio
                 
               }
               else{
                 cementerio = 'PC';
               }
               
               const inventario: INVCAM_Inventario = {
                Id: this.inventario.id,
                Codigo: cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
                Contrato: this.formNecesidad.get('Contrato').value,
                Panum: '',
                Cementerio: this.formNecesidad.get('Cementerio').value,
                Pared: this.formNecesidad.get('Pared').value,
                Fila: this.formNecesidad.get('Fila').value,
                Columna: this.formNecesidad.get('Columna').value,
                CodigoAnterior: 'Necesidad',
                Etapa: this.formNecesidad.get('Etapa').value,
                Estatus: this.estatus,
                Vendedor: 'Necesidad',
                FechaReserva: new Date("2021/02/20"),
                IdCliente: this.formNecesidad.get('IdCliente').value,
                NombreCliente: this.formNecesidad.get('Cliente').value,
                //NombreFallecido: this.formNecesidad.get('NombreFallecido').value,
                FechaRegistro: new Date(),
                //FechaNacimiento: this.formNecesidad.get('FechaNacimiento').value,
                //FechaFallecimiento: this.formNecesidad.get('FechaFallecimiento').value,
                Tipo: tipo            
              }
  
                const fallecido: INVCAM_Fallecidos = {
                Id: this.idFallecido, 
                codigoInventario      :cementerio+'-'+  this.formNecesidad.get('Pared').value + '-' + this.formNecesidad.get('Columna').value  + '-' +this.formNecesidad.get('Fila').value,
                fallecido1           :this.formNecesidad.get('NombreFallecido').value, 
                fechaNacimiento1     :fechaNacimiento,
                fechaFallecimiento1  :fechaFallecimiento,

                fallecido2           :null, 
                fechaNacimiento2     :fechaNacimiento_2,
                fechaFallecimiento2  :fechaFallecimiento_2,

                fallecido3           :null,
                fechaNacimiento3     :fechaNacimiento_3,
                fechaFallecimiento3  :fechaFallecimiento_3,

                fallecido4           :null,
                fechaNacimiento4     :fechaNacimiento_4,
                fechaFallecimiento4  :fechaFallecimiento_4,
                
                fallecido5           :null,
                fechaNacimiento5     :fechaNacimiento_5,
                fechaFallecimiento5  :fechaFallecimiento_5,

                fallecido6           :null,
                fechaNacimiento6     :fechaNacimiento_6,
                fechaFallecimiento6  :fechaFallecimiento_6,

                fallecido7           :null,
                fechaNacimiento7     :fechaNacimiento_7,
                fechaFallecimiento7  :fechaFallecimiento_7,

                fallecido8           :null,
                fechaNacimiento8     :fechaNacimiento_8,
                fechaFallecimiento8  :fechaFallecimiento_8,
              }
               this.crudService.actualizarInventario(this.idInventario, inventario).subscribe(data => {
                this.toastr.info('Registro Actualizado','El registro se actualizo correctamente')            
                
                setTimeout(()=>{
                  this.refresh();
                  //console.log("mensaje")
                }, 2000)
                //this.refresh();
                //console.log("Guardado exitosamente....");
                this.crudService.obtenerinfo();
                //this.form.reset();
                this.idInventario = 0;                
               }, err =>{
                 err = this.toastr.error('Error', 'Error actualizando registro')
               })


               if (fallecido.fallecido1 !=  null) {

                this.fallecidoService.actualizarFallecido(this.idFallecido, fallecido).subscribe(data =>{
                  this.toastr.info('Fallecido Actualizado', 'El fallecido se actualizo correctamente ')
  
                  setTimeout(()=>{
                    this.refresh();
                  }, 3000)
                  this.idFallecido = 0;
                }, err =>{
                  err = this.toastr.error('Error', 'Error al actualizar fallecido')
                })
               }
          
               if(this.form.invalid){
                return Object.values(this.form.controls).forEach(control =>{
                  control.markAsTouched();
                })
              }

              setTimeout(()=>{
                this.refresh();
              }, 3000)
  
            }

            guardarReservar(){
              //console.log("ID")
              //console.log(this.idInventario)
              if(this.idInventario ===0){
                this.agregarReservar();
                }
              else{
                this.editarInvReservar();
              }   
                                  
           }
           
            agregarReservar(){  
              let tipo ='R'

              let fechaNacimiento    = this.form.get('FechaNacimiento').value;
              let fechaFallecimiento = this.form.get('FechaFallecimiento').value;
              
              
              let cementerio;
              
             if (this.isChecked == true){
               this.estatus = 'ocupado';
             }
             else{
               this.estatus = 'vendido';
             }

             if(this.form.get('Cementerio').value == this.listaCementerios[0]){
               cementerio = this.codigoCementerio
               
             }
             else{
               cementerio = this.codigoCementerio;
             } 
                  
             if (this.form.get('FechaNacimiento').value == '' && this.form.get('FechaFallecimiento').value ==''){
              fechaNacimiento    =  new Date();
              fechaFallecimiento =  new Date(); 
            }

             this.listaReserva.forEach(element =>{
               this.paredReserva = element.pared;
               this.columnaReserva = element.columna;
               this.filaReserva = element.fila
              
             })

             
              const inventario: INVCAM_Inventario = {              
                Codigo: cementerio +'-'+  this.paredReserva + '-' +  this.columnaReserva + '-' + this.filaReserva,
                Contrato: "Reserva",
                Panum: "Reserva",
                Cementerio: this.listaCementerios[0],
                Pared: this.paredReserva,
                Fila: this.filaReserva,
                Columna: this.columnaReserva,
                CodigoAnterior: "Reserva",
                Etapa: this.etapa[0],
                Estatus: "Reservado",
                Vendedor: this.formReservar.get('Vendedor').value,
                FechaReserva: this.formReservar.get('FechaVencimiento').value,
                IdCliente: "0",
                NombreCliente: "Reservado",
                //NombreFallecido: "Reservado",
                FechaRegistro: new Date(),
                //FechaNacimiento: this.form.get('FechaNacimiento').value,
                //FechaFallecimiento: this.form.get('FechaFallecimiento').value         
                
                //FechaNacimiento: fechaNacimiento,
                //FechaFallecimiento: fechaFallecimiento,
                Tipo: tipo
              }              
             
             

             this.crudService.guardarRegistro(inventario).subscribe(data =>{   
              this.toastr.success('Registro Creado','El registro se guardo correctamente')
              this.listaMostrar =['']
              this.form.reset()
             
              setTimeout(()=>{
                this.refresh();
                //console.log("mensaje")                      
                
              }, 2000)                    
               //console.log("Guardado exitosamente");
               
             }, err =>{
             
               if(this.form.invalid){
                err= this.toastr.error('Error','Favor completar todos los campos')
              }
              else{
                err=  this.toastr.error('Error','El registro existe en al base de datos')                   

              }
             })

             
              if(this.form.invalid){
                return Object.values(this.form.controls).forEach(control =>{
                  control.markAsTouched();
                })
              }           
          }

          editarInvReservar(){
            let cementerio;
            let tipo = 'R';

             if (this.isChecked == true){
               this.estatus = 'ocupado';
             }
             else{
               this.estatus = 'vendido';
             }

             if(this.form.get('Cementerio').value == this.listaCementerios[0]){
               cementerio = this.codigoCementerio
               
             }
             else{
               cementerio = 'PC';
             }             

             const inventario: INVCAM_Inventario = {
               Id: this.inventario.id,
               Codigo: cementerio+'-'+  this.form.get('Pared').value + '-' + this.form.get('Columna').value  + '-' +this.form.get('Fila').value,
               Contrato: this.form.get('Contrato').value,
               Panum: this.form.get('Panum').value,
               Cementerio: this.form.get('Cementerio').value,
               Pared: this.form.get('Pared').value,
               Fila: this.form.get('Fila').value,
               Columna: this.form.get('Columna').value,
               CodigoAnterior: "N/A",
               Etapa: this.form.get('Etapa').value,
               Estatus: this.estatus,
               Vendedor: this.formReservar.get('Vendedor').value,
               FechaReserva: this.formReservar.get('FechaVencimiento').value,
               IdCliente: this.form.get('IdCliente').value,
               NombreCliente: this.form.get('Cliente').value,
               //NombreFallecido: this.form.get('NombreFallecido').value,
               FechaRegistro: new Date(),
               //FechaNacimiento: this.form.get('FechaNacimiento').value,
               //FechaFallecimiento: this.form.get('FechaFallecimiento').value,
               Tipo: tipo


           
             }
             this.crudService.actualizarInventario(this.idInventario, inventario).subscribe(data => {
              this.toastr.info('Registro Actualizado','El registro se actualizo correctamente')            
              
              setTimeout(()=>{
                this.refresh();
                //console.log("mensaje")
              }, 2000)
              //this.refresh();
              //console.log("Guardado exitosamente....");
              this.crudService.obtenerinfo();
              //this.form.reset();
              this.idInventario = 0;                
             })

             if(this.form.invalid){
              return Object.values(this.form.controls).forEach(control =>{
                control.markAsTouched();
              })
            }

          }


          NoDisponible(){

                var fila;
                var columna;


           this.reservaEliminar.forEach(data =>{
            fila = data.fila;
            columna = data.columna
              
           })
           

           const inventario: INVCAM_Inventario = {              
             //Codigo:this.codigoCementerio[0]+'-'+  this.paredCementerio + '-' + columna  + '-' + fila,
             Codigo:this.codigoCementerio +'-'+ columna  + '-' + fila,
             Contrato: 'No disponible',
             Panum: 'No disponible',
             Cementerio: this.listaCementerios[0],
             Pared: this.paredCementerio[0],
             Fila: fila,
             Columna: columna,
             CodigoAnterior: 'No disponible',
             Etapa: this.etapa[0],
             Estatus: 'No disponible',
             Vendedor: 'NO DISPONIBLE',
             FechaReserva: new Date("2021/02/20"),
             IdCliente: 'No disponible',
             NombreCliente: 'No disponible',
             //NombreFallecido: 'No disponible',                 
             FechaRegistro: new Date(),
             //FechaNacimiento:  new Date(),
             //FechaFallecimiento:  new Date(),
             Tipo: 'No disponible'
             
             
         
           }    
           //console.log(inventario)
           //console.log(this.form)  



           if(confirm('Seguro que desea marcar el registro no disponible')){

            this.crudService.guardarRegistro(inventario).subscribe(data =>{   
              this.toastr.success('Registro Creado','El registro se guardo correctamente')
              this.listaMostrar =['']
              this.form.reset()

             
              setTimeout(()=>{
                this.refresh();
                //console.log("mensaje")                      
                
              }, 2000)
              
               //console.log("Guardado exitosamente");
               
             }, err =>{
             
                err=  this.toastr.error('Error','Error marcando como no disponible')

             })
           }
              
        }
             
           
              refresh(){
                window.location.reload();
               }
            
              openSM(contenido, contenidoReservar ,data)
              {  
                if (this.isAdmin == 'false'){
                  return
                }

                this.prueba.push(data);
                this.prueba.forEach(element =>{
                  this.t = element.Tipo
                  //console.log(element)
                })               

                
                if(this.t =='PA'){
                  this.nombreModal = contenido;                 

                }
                if(this.t =='N'){
                 this.nombreModal = contenidoReservar;                       

               }
                
                this.form.reset()          
                                                        
               
                this.subscription = this.crudService.obtenerinfo().subscribe(data =>{
                  //console.log(data)                  

                  this.inventario =  data;      
                  //console.log("Inventario");
                  //console.log(this.inventario)
                  this.dataPanum = this.inventario.panum 
                  this.factura = this.inventario.contrato
                  

                  if (this.inventario.estado == 'ocupado'){
                    this.isChecked = true;
                  }
                  else{
                    this.isChecked = false;
                  }

                  this.vincamfallecidoclientesService.obtenerInventario(this.inventario.codigo).subscribe((data: any) =>{                   
                    this.vIncamInventarioData = data;
                    this.idFallecido = data.idFallecido
                  })
                
                 
                  if(this.t =='PA'){
                                 
                    this.form.patchValue({
                      Codigo: this.inventario.codigo,
                      Contrato: this.inventario.contrato,   
                      Panum: this.inventario.panum,
                      Cementerio: this.inventario.cementerio,
                      Pared: this.inventario.pared,
                      Fila: this.inventario.fila,
                      Columna: this.inventario.columna,
                      CodigoAnterior: "CristoR",
                      Etapa: this.inventario.etapa,
                      //Estatus: this.estatus
                      Estatus: this.isChecked,
                      NombreFallecido: this.inventario.nombreFallecido,
                      FechaNacimiento: this.inventario.fechaNacimiento,
                      FechaFallecimiento:this.inventario.fechaFallecimiento                
                     
                    })
                  }

                  if(this.t =='N'){

                    this.formNecesidad.patchValue({
                      //Codigo: this.inventario.codigo,
                      Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
                      Contrato: this.inventario.contrato,   
                      //Panum: this.inventario.panum,
                      Cementerio: this.listaCementerios[0],
                      Pared: this.inventario.pared,
                      Fila: this.inventario.fila,
                      Columna: this.inventario.columna,
                      CodigoAnterior: "CristoR",
                      Etapa: this.inventario.etapa,
                      //Estatus: this.estatus
                      Estatus: this.isChecked,    
                      
                      IdCliente: this.inventario.idCliente,
                      Cliente: this.inventario.nombreCliente,           
                      NombreFallecido: this.inventario.nombreFallecido,
                      FechaNacimiento: this.inventario.fechaNacimiento,
                      FechaFallecimiento:this.inventario.fechaFallecimiento
                     
                    })                               
   
                  }
                  
                  
                  this.idInventario = this.inventario.id                                   
                
                })                
                
               
                this.modal.open(this.nombreModal, { size: 'xl'});    
                
                this.editar(data)           
                this.obtenerDatosPRA(this.dataPanum);  
                this.obtenerFactura(this.factura)
                         
              }

              openSM2(contenido)
              {    
                                
                this.subscription = this.crudService.obtenerinfo().subscribe(data =>{
                  
                  this.inventario =  data;      
                  this.dataPanum = this.inventario.panum 
                  

                  if (this.inventario.estado == 'ocupado'){
                    this.isChecked = true;
                  }
                  else{
                    this.isChecked = false;
                  }
                 /*if(this.inventario.estado == 'vendido'){
                   this.isChecked = false;
                 }*/
                this.isChecked = true;
                
                 this.form.patchValue({
                  //Codigo: this.inventario.codigo,
                  Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
                  //Contrato: this.inventario.contrato,   
                  //Panum: this.inventario.panum,
                  Cementerio: this.listaCementerios[0],
                  Pared: this.inventario.pared,
                  Fila: this.inventario.fila,
                  Columna: this.inventario.columna,
                  CodigoAnterior: "CristoR",
                  Etapa: this.inventario.etapa,
                  //Estatus: this.estatus
                  Estatus: this.isChecked,
                  IdCliente: this.inventario.idCliente,
                  Cliente: this.inventario.nombreCliente,
                  NombreFallecido: this.inventario.nombreFallecido,
                  FechaNacimiento: this.inventario.fechaNacimiento,
                  FechaFallecimiento:this.inventario.fechaFallecimiento
                 
                })            
                  

                  this.idInventario = this.inventario.id                                   
                  
                })                
                  //this.form.reset()  
                 this.form.get('Panum').reset();
                 this.form.get('NombreFallecido').reset();
                  
                 this.modal.open(contenido, { size: 'xl'});  
                 this.listaMostrar =[''];               
                 
              }   
              
              openSM3(contenidoReservar)
              {    
                               
                
                this.subscription = this.crudService.obtenerinfo().subscribe(data =>{
                  
                  this.inventario =  data;      
                  this.dataPanum = this.inventario.panum 
                  

                  if (this.inventario.estado == 'ocupado'){
                    this.isChecked = true;
                  }
                  else{
                    this.isChecked = false;
                  }
                 /*if(this.inventario.estado == 'vendido'){
                   this.isChecked = false;
                 }*/
                this.isChecked = true;
                
                 this.formNecesidad.patchValue({
                  //Codigo: this.inventario.codigo,
                  Codigo: this.codigoCementerio +'-' + this.inventario.pared +'-' + this.inventario.columna + '-' + this.inventario.fila ,
                  //Contrato: this.inventario.contrato,   
                  //Panum: this.inventario.panum,
                  Cementerio: this.listaCementerios[0],
                  Pared: this.inventario.pared,
                  Fila: this.inventario.fila,
                  Columna: this.inventario.columna,
                  CodigoAnterior: "CristoR",
                  Etapa: this.inventario.etapa,
                  //Estatus: this.estatus
                  Estatus: this.isChecked,   
                  IdCliente: this.inventario.idCliente,
                  Cliente: this.inventario.nombreCliente,               
                  //NombreFallecido: this.inventario.nombreFallecido
                  FechaNacimiento: this.inventario.fechaNacimiento,
                  FechaFallecimiento:this.inventario.fechaFallecimiento
                 
                })            
                  

                  this.idInventario = this.inventario.id                                   
                  
                })                
                  //this.form.reset()  
                 //this.form.get('Panum').reset();
                 this.formNecesidad.get('NombreFallecido').reset();
                  
                 this.modal.open(contenidoReservar, { size: 'xl'});  

                 //
                    this.obtenerFactura(this.formNecesidad.get('Contrato'));
                 //
                 this.listaMostrar =[''];               
                 
              } 

              
              openReservar(reservar,data){
                this.modal.open(reservar, {size:'lg'})
                this.listaReserva.push(data);    
              }
              

             /* openPlan(plan){                
                this.modal.open(plan, {size:'sm'})                               
              }*/

              openPlan(plan, data){                
                this.modal.open(plan, {size:'sm'})                 
                this.editar(data)
              }

  
              buscarInventario(fila:any, columna:any, pared:any, etapa:any) : boolean{
                let flag: boolean;
                flag =false;
                let cementerio: any;
                
                this.listaInventario.forEach (element=>{                 
  
                  if(element.fila == fila && element.columna == columna && element.pared== pared && element.cementerio == this.listaCementerios[0] && element.etapa == etapa) {
                    flag = true;
                    //this.datos = element.fila + "-" + element.columna + "-" + element.pared; 
                    if(element.cementerio == this.listaCementerios[0]){
                      cementerio = this.codigoCementerio
                    }
                    else{
                      cementerio ='PC'
                    }                    
  
                    this.datos = cementerio + '-' + element.pared + '-' + element.columna + '-' + element.fila;                    
  
                    this.myLista.push({
                      id: element.idInventario,
                      codigo: this.datos,
                      contrato: element.contrato,
                      panum: element.panum,
                      pared: element.pared,
                      fila: element.fila, 
                      columna: element.columna, 
                      codigoAnterior: element.codigoAnterior,
                      etapa: element.etapa,
                      estado: element.estatus, 
                      //cementerio: cementerio})
                      cementerio: element.cementerio,
                      //nombreFallecido: element.nombreFallecido,
                      nombreFallecido: element.fallecido1,
                      vendedor: element.vendedor,
                      //fechaNacimiento: element.fechaNacimiento,
                      //fechaFallecimiento: element.fechaFallecimiento,
                      fechaNacimiento: element.fechaNacimiento1,
                      fechaFallecimiento: element.fechaFallecimiento1,
                      Tipo: element.tipo,
                      idCliente: element.idCliente,
                      nombreCliente: element.nombreCliente})
                  }
                  
                });
                
                return flag
              }
  
  
              editar(inventario){                
                
                this.crudService.actualizar(inventario); 
                this.listaMostrar =[''];  
                
                //console.log("datos no disponible")
                //console.log(inventario)
                
                
               
                this.reservaEliminar.push(inventario);   
                this.reservaEliminar.forEach(data =>{
                  this.reservaEliminarId = data.id;
                })
              }   
              
              pruebadatos(){

              }
  
              cambiarPared(pared: any){
                //console.log(pared);
  
                this.pared = pared;
                //console.log("PR: ")
                //console.log(this.pared)
                return this.pared
               
              }
              mostrarDatos(mensaje:string){
                //console.log("su mensaje es: ")
                //console.log(mensaje)
              }
              procesar(dato){
              //console.log(dato)
              }

              

              obtenerDatosPRA(dato:any){                

               this.pRAMASTERService.get_Pra_Master_info_ID(dato).subscribe((x: any) =>{
                this.res = x   
                
                this.listaMostrar.push(this.res)               
                //console.log(this.listaMostrar)     
              
                });                  
                this.listaMostrar =[];
                
                return this.res;   

              }    

              obtenerFactura(dato:any){                

                this.facturasService.get_Factura(dato).subscribe((x: any) =>{       
                  
                  let factura = x
                  //console.log(factura)
                  //console.log('datos factura')
                  //console.log(dato)
                  //console.log(x)            
                 
                 this.listaFactura.push(x)     
                 //console.log("Lista de facturas: ")          
                 //console.log(this.listaFactura)     
               
                 });                  
                 this.listaFactura =[];
                 
                 return this.listaFactura;   
 
               } 


              
              obtenerDatosPRAcONTRACT(dato:any){
                
                this.pRAMASTERService.get_Pra_Master_info_CONTRACT(dato).subscribe((x: any) =>{
                 this.res = x   
                 
                 this.listaMostrarcONTRACT.push(this.res)               
                 
                 });                  
                 this.listaMostrarcONTRACT =[];
                
                 return this.res;   
 
               }

               eliminarReserva(id: number){

                if (id == 0){
                  return;
                }

                if(confirm('Esta seguro que desea Eliminar el registro?')){
                  this.crudService.eliminarReserva(id).subscribe(data =>{
                    this.toastr.warning('Registro Eliminado', 'Registro eliminado del sistema');

                    setTimeout(()=>{
                      this.refresh();
                      //console.log("mensaje")                      
                      
                    }, 2000)
           
                  })
           
                }           
              }            
              
              validarCodigo(codigo){
                let parametroOpcional = 'A';             

                this.crudService.validarCodigo(codigo, parametroOpcional)
                .subscribe((data: any) =>{       
                  if (data == true){
                    this.retornoValidacion = true;
                    this.toastr.error('Error','El registro existe en la base de datos')
                    return;
                  }
                })
               }

               obtenerFecha(){
                 let fecha = new Date();
                 this.parametroList.forEach(element =>{
                   element.diasReserva
                   fecha.setDate(fecha.getDate() + element.diasReserva) 
                   
                   this.fechavencimiento.push(fecha)
                   //console.log(this.fechavencimiento)

                   /*let fecha = new Date();
                  fecha.setDate(fecha.getDate() + data.diasReserva)                  
                  //console.log(this.parametroList)
                  //console.log(data)*/
                 })
               }

               get contratoValido(){
                 return this.form.get('Contrato').invalid && this.form.get('Contrato').touched
               }
               get paValido(){
                return this.form.get('Panum').invalid && this.form.get('Panum').touched
              }
              get cementerioValido(){
                return this.form.get('Cementerio').invalid && this.form.get('Cementerio').touched
              }
              get etapaValido(){
                return this.form.get('Etapa').invalid && this.form.get('Etapa').touched
              }
              get paredValido(){
                return this.form.get('Pared').invalid && this.form.get('Pared').touched
              }
              get filaValido(){
                return this.form.get('Fila').invalid && this.form.get('Fila').touched
              }
              get columnaValido(){
                return this.form.get('Columna').invalid && this.form.get('Columna').touched
              }
              get nombreFallecidoValido(){
                return this.form.get('NombreFallecido').invalid &&  this.form.get('NombreFallecido').touched
              }
            
            
            }
            
