import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla13Component } from './grilla13.component';

describe('Grilla13Component', () => {
  let component: Grilla13Component;
  let fixture: ComponentFixture<Grilla13Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla13Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla13Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
