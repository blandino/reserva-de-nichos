import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla9Component } from './grilla9.component';

describe('Grilla9Component', () => {
  let component: Grilla9Component;
  let fixture: ComponentFixture<Grilla9Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla9Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla9Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
