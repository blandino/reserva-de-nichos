import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla6Component } from './grilla6.component';

describe('Grilla6Component', () => {
  let component: Grilla6Component;
  let fixture: ComponentFixture<Grilla6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla6Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
