import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla3Component } from './grilla3.component';

describe('Grilla3Component', () => {
  let component: Grilla3Component;
  let fixture: ComponentFixture<Grilla3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
