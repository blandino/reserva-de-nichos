import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla8Component } from './grilla8.component';

describe('Grilla8Component', () => {
  let component: Grilla8Component;
  let fixture: ComponentFixture<Grilla8Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla8Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
