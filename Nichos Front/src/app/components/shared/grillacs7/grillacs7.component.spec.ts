import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grillacs7Component } from './grillacs7.component';

describe('Grillacs7Component', () => {
  let component: Grillacs7Component;
  let fixture: ComponentFixture<Grillacs7Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grillacs7Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grillacs7Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
