import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Grilla4Component } from './grilla4.component';

describe('Grilla4Component', () => {
  let component: Grilla4Component;
  let fixture: ComponentFixture<Grilla4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Grilla4Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Grilla4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
