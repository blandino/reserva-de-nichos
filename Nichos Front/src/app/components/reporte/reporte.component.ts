import { Component, OnInit } from '@angular/core';
import { VincamfallecidoclientesService } from '../../services/vincamfallecidoclientes.service';
import { LogServiceService } from 'src/app/services/log-service.service';



@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.css']
})


export class ReporteComponent implements OnInit {
  
  vincamfallecidoclientesDatos: any[] =[];
  logData: any[] = [];
  fileName= 'ExcelSheet.xlsx';  
  fechainnio: Date;

  constructor(private vincamfallecidoclientesService: VincamfallecidoclientesService,
              private logServiceService: LogServiceService) { 
    this.getvincamfallecidoclientesDatos();
  }

  ngOnInit(): void {
this.getLog();
  }
  getvincamfallecidoclientesDatos(){
    this.vincamfallecidoclientesService.obtenerFallecidosClientes().subscribe((data:any)=>{
      this.vincamfallecidoclientesDatos = data;
      //console.log(this.vincamfallecidoclientesDatos) 
    })
  }

  getLog(){
    this.logServiceService.GetLog().subscribe((data:any) =>{
      this.logData = data;
      console.log(this.logData)
    })
  }

  getLogByDate(fuchaInicio, fechaFinal){

    this.logServiceService.GetLogByDate(fuchaInicio,fechaFinal).subscribe((data:any) =>{
    this.logData = [''];
    this.logData = data; 
    })

    console.log(fuchaInicio + " " + fechaFinal)

  }



}
