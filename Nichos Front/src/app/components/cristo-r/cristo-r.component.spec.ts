import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristoRComponent } from './cristo-r.component';

describe('CristoRComponent', () => {
  let component: CristoRComponent;
  let fixture: ComponentFixture<CristoRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristoRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristoRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
