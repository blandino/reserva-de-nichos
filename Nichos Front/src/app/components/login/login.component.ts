import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
import { LogServiceService } from 'src/app/services/log-service.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUserList: any[] = [];
  form: FormGroup;
  img: string = 'https://blandinoprearreglo.com.do/wp-content/uploads/2020/08/logo-e1602165189516.png';  
  loginUser: any [] = [];
  isLogin: boolean = false;
  isAdmin: boolean;
  




  constructor(private loginService: LoginService,
              private formBuilder:   FormBuilder,
              public toastr: ToastrService,
              private router: Router,
              private logServiceService: LogServiceService) {

    this.form = this.formBuilder.group({
      UserId: [''],
      Password: [''],
      RememberMe: ['']
    })     
    //this.getAdmin();   
    this.getLogin();

   }

  ngOnInit(): void {
  }

  getLogin(){

    this.loginService.getLoginUser(this.form.get('UserId').value, this.form.get('Password').value)
    .subscribe((data: any) =>{

     /* if(this.form.get('UserId').value || this.form.get('Password').value){

      }*/

      if(data == true){
        this.getAdmin()
        this.toastr.success('Usuario y contraseña correctos','Logueado')
        //this.router.navigate(['CCristoR/Grilla1']);
        //this.router.navigate(['Navbar']);
        sessionStorage.setItem('login', data)
        this.guardarUsuario();
      
       // this.router.navigate(['Barranavegacion']);
        /*sessionStorage.setItem('login', data)
        this.getAdmin()*/
      }
      else{
        this.toastr.error('Usuario y contraseña incorrectos','Datos incorrectos')
      }     
    })
    
    
    
   }

   getAdmin(){
     this.loginService.getAdmin(this.form.get('UserId').value).subscribe((dataAdmin: any)=>{
       /*if (data =='true'){
       }*/
       //var Usuario = this.form.get('UserId').value;
       sessionStorage.setItem('admin', dataAdmin)
      // sessionStorage.setItem('UsrName', Usuario)
       //this.router.navigate(['Barranavegacion']);
       this.router.navigate(['Barranavegacion/PaginaInicio']);
       
     })
    }

   login(){
    this.loginUserList.forEach(element=>{

      if(element.usrid == this.form.get('UserName').value && element.pwl == this.form.get('Password').value ){
        this.isLogin = true;
        this.isAdmin = element.isAdmin;
        this.toastr.success('Usuario y contraseña correctos','Logueado')
        this.router.navigate(['CCristoR/Grilla1']);
        this.loginUser.push(element)
      }

      /*else{
        this.toastr.error('Usuario y contraseña incorrectos','No Login')      
      }*/

    })
    let usuario = this.form.get('UserName').value;
    let contrasena = this.form.get('Password').value;     

     this.saveData( this.isLogin, this.isAdmin)
   }

   saveData(login: any, admin: any){

     sessionStorage.setItem('login', login);
     sessionStorage.setItem('admin', admin);
   }
  guardarUsuario(){
    var usuario = this.form.get('UserId').value;
    sessionStorage.setItem('UsrName', usuario)
    //var usuario = sessionStorage.getItem('UsrName');
    this.logServiceService.GuardarUsuario(usuario).subscribe();
  }

}
