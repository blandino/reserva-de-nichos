import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParedCompletaComponent } from './pared-completa.component';

describe('ParedCompletaComponent', () => {
  let component: ParedCompletaComponent;
  let fixture: ComponentFixture<ParedCompletaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParedCompletaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParedCompletaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
