import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Reserva de nichos';

  mcr: boolean;
  mpc: boolean;
  

//
constructor(){
  this.menu();
  this.menuPc();

}
menu(){
  this.mcr = true;
  this.mpc = false;
}

menuPc(){  
  this.mpc = true;
  this.mcr = false;
}
//

}
