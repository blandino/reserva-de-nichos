﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Log
    {
        public int Id { get; set; }
        public string Usuario { get; set; }
        public string Detalles { get; set; }
        public DateTime Fecha { get; set; }
    }
}
