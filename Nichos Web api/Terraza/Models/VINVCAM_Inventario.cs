﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
	public class VINVCAM_Inventario
	{
		[Key]
		public int Id { get; set; } 
		public string Codigo { get; set; }
		public string Contrato { get; set; }
		public string PANUM { get; set; }
		public string Cementerio { get; set; }
		public string Pared { get; set; }
		public string Fila { get; set; }
		public string Columna { get; set; }
		public string Etapa { get; set; }
		public string Estatus { get; set; }
		public string Vendedor { get; set; }
		public DateTime FechaReserva { get; set; }
		public string IdCliente { get; set; }
		public string NombreCliente { get; set; }
		public string Tipo { get; set; }
		public string TipoColumbario { get; set; }
		public string codigoInventario { get; set; }
		public string fallecido1 { get; set; }
		public DateTime fechaNacimiento1 { get; set; }
		public DateTime fechaFallecimiento1 { get; set; }
		public string fallecido2 { get; set; }
		public DateTime fechaNacimiento2 { get; set; }
		public DateTime fechaFallecimiento2 { get; set; }
		public string fallecido3 { get; set; }
		public DateTime fechaNacimiento3 { get; set; }
		public DateTime fechaFallecimiento3 { get; set; }
		public string fallecido4 { get; set; }
		public DateTime fechaNacimiento4 { get; set; }
		public DateTime fechaFallecimiento4 { get; set; }
		public string fallecido5 { get; set; }
		public DateTime fechaNacimiento5 { get; set; }
		public DateTime fechaFallecimiento5 { get; set; }
		public string fallecido6 { get; set; }
		public DateTime fechaNacimiento6 { get; set; }
		public DateTime fechaFallecimiento6 { get; set; }
		public string fallecido7 { get; set; }
		public DateTime fechaNacimiento7 { get; set; }
		public DateTime fechaFallecimiento7 { get; set; }
		public string fallecido8 { get; set; }
		public DateTime fechaNacimiento8 {get;set;} 
		public DateTime fechaFallecimiento8 { get; set; }
	}
}
