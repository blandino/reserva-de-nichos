﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Estructura_Columna
    {
        [Key]
        public int ID { get; set; }
        public string Type { get; set; }
        public int Desde { get; set; }
        public int Hasta { get; set; }
        public string Cementerio { get; set; }
        public string Etapa { get; set; }
        public int Pared { get; set; }
    }
}
