﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class INVCAM_ParedesDto
    {
       
        public int Id { get; set; }
        [Required(ErrorMessage = "La Pared es requerida")]
        public string Pared { get; set; }
    }
}
