﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class INVCAM_ColumnasDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "La Columna es requerida")]
        public string Columna { get; set; }
        public string IdCementerio { get; set; }
        public string Etapa { get; set; }
        public string Pared { get; set; }
    }
}
