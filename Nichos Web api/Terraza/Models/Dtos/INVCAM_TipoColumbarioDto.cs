﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class INVCAM_TipoColumbarioDto
    {
        public int Id { get; set; }
        public string Identificador { get; set; }
        public string Tipo { get; set; }
    }
}
