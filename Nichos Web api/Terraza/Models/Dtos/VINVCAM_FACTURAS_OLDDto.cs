﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class VINVCAM_FACTURAS_OLDDto
    {
        public string SOPNUMBE { get; set; }
        public string ORIGNUMB { get; set; }
        public DateTime DOCDATE { get; set; }
        public string CUSTNMBR { get; set; }
        public string CUSTNAME { get; set; }
    }
}
