﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class INVCAM_Estructura_ColumnaDto
    {
        public int ID { get; set; }
        [Required(ErrorMessage = "El ID es requerido")]
        public string Type { get; set; }
        public int Desde { get; set; }
        public int Hasta { get; set; }
        public string Cementerio { get; set; }
        public string Etapa { get; set; }
        public int Pared { get; set; }
    }
}
