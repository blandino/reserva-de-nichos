﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class INVCAM_FilasDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "La fila es requerida")]
        public string Fila { get; set; }
        public string IdCementerio { get; set; }
    }
}
