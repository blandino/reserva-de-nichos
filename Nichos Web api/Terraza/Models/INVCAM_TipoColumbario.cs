﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_TipoColumbario
    {
        [Key]
        public int    Id { get; set; }
        public string Identificador { get; set; }
        public string Tipo { get; set; }
    }
}
