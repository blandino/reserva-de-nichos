﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Columnas
    {
        [Key]
        public int    Id           { get; set; }
        public string Columna      { get; set; }
        public string IdCementerio { get; set; }
        public string Etapa        { get; set; }
        public string Pared        { get; set; }
    }
}
