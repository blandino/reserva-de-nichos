﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Data;

namespace Terraza
{
    [Route("api/[controller]")]
    [ApiController]
    public class FiltrarEstadosController : ControllerBase
    {
        public ApplicationDbContext _context;
        public int numeroTest = 3;

        public FiltrarEstadosController(ApplicationDbContext context) 
        {
            _context = context;

        }

        [HttpGet("{cementerio}/{estado}", Name = "validarCementerio")]
        public bool validarCementerio(string cementerio, string estado) 
        {
            var todo = "Todos";
            var disponibilidad = "disponible";
                      

            if (estado.ToLower() == disponibilidad.ToLower())
            {
                if (validarDisponibilidadDeNichos(cementerio))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }           

            if (estado.ToLower() == todo.ToLower())
            {
                return true;
            }

            /*var validarRegistroExistente = _context.INVCAM_Inventario.ToList().Where(c => c.Estatus.ToLower().Trim() == estado.ToLower().Trim());
            var datos = validarRegistroExistente;

            if (datos == null)
            {

                return false;
            }*/           

           var result = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower() && c.Estatus.ToLower() == estado.ToLower()).FirstOrDefault();
                var Data = result;

            if (Data != null)
            {
                return true;
            }

            return false;

        }

        [HttpGet("{cementerio}/{estado}/{etapa}", Name = "validarEtapas")]
        public bool validarEtapas(string cementerio, string estado, string etapa)
        {
            var todo = "Todos";
           
            
            if (estado.ToLower() == todo.ToLower()) 
            {
                return true;
            }

            var disponibilidad = "Disponible";

            if (disponibilidad.ToLower() == estado.ToLower())
            {

                if (validarDisponibilidadDeNichos(cementerio, estado, etapa)) 
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
           

            var result = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower() && c.Estatus.ToLower() == estado.ToLower() && (c.Etapa.ToLower() == etapa.ToLower() )).FirstOrDefault();
          
            var Data = result;

            if (Data != null)
            {
                return true;
            }

            return false;

        }

        [HttpGet("{cementerio}/{estado}/{etapa}/{pared}", Name = "ValidarParedes")]
        public bool ValidarParedes(string cementerio, string estado, string etapa, string pared) 
        {
            var todo = "Todos";

            if (estado.ToLower() == todo.ToLower()) 
            {
                return true;
            }

            var disponible = "Disponible";

            if (  disponible.ToLower() == estado.ToLower())
            {
                if (validarDisponibilidadDeNichos(cementerio, estado, etapa, pared))
                {
                    return true;
                }
                else 
                {
                    return false;                
                }
            }

            var result = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower() && c.Estatus.ToLower() == estado.ToLower() && (c.Etapa.ToLower() == etapa.ToLower()) && (pared == c.Pared) ).FirstOrDefault();

            var Data = result;

            if (Data != null)
            {
                return true;
            }

            return false;
        }


        //Produccion
        public bool validarDisponibilidadDeNichos(string cementerio) 
        {
            bool resultado = false;            
            //Cementerios
            var columbarioOzama = "Columbario Ozama";
            var columbarioSantiago = "Columbario Santiago";
            var columbarioLuperon = "Columbario Luperon";
            var ingenioSantiago = "Ingenio Santiago";
            var maximoGomez = "Maximo Gomez";
            var cristoSalvador = "Cristo Salvador";
            var cristoRedentor = "Cristo Redentor";

            var numeroDeNichos = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower()).Count();

           
                switch (cementerio) 
                {
                    case "Columbario Ozama":
                        //ColumbarioOzama
                        if (cementerio == columbarioOzama && numeroDeNichos < (20 * 8))
                        {
                            resultado = true;                           
                        }
                        else
                        {
                            resultado =  false;
                        }
                        break;

                        //Columbario Santiago
                    case "Columbario Santiago":
                        if (cementerio == columbarioSantiago && numeroDeNichos < ((61 * 7) + (4 * 10) + (4 * 10)))
                        {
                            resultado = true;
                        }
                        else 
                        {
                            resultado = false;
                        }
                        break;

                    case "Columbario Luperon":
                        if (cementerio == columbarioLuperon && numeroDeNichos < ((10 * 11) + (10 * 7) + (11 * 8)
                            + (10 * 7) + (22 * 6) + (20 * 11) + (12 * 3)))
                        {
                            resultado = true;
                        }
                        else 
                        {
                            resultado = false;
                        }
                        break;

                    case "Ingenio Santiago":
                        if (cementerio == ingenioSantiago && numeroDeNichos < 5)
                        {
                           resultado = true;
                        }
                        else 
                        {
                           resultado = false;
                        }
                        break;

                   
                    case "Maximo Gomez":
                        if (cementerio == maximoGomez && numeroDeNichos < ((5 * 6) + (2 * 6)))
                        {
                            resultado = true;
                        }
                        else 
                        {
                            resultado = false;
                        }
                        break;

                    case "Cristo Salvador":
                        if (cementerio == cristoSalvador && numeroDeNichos < ((1 * 5) + (4 * 5) + (1 * 5) + (4 * 5)
                            + (6 * 5) + (6 * 5) + (13 * 6) + (4 * 6) + (12 * 6) + (5 * 5) + (12 * 6) + (10 * 6) 
                            + (4 * 5) + (7 * 5) + (9 * 5) + (7 * 5) + (7 * 5) + (7 * 5) + (7 * 5)) )
                        {
                            resultado = true;
                        }
                        else 
                        {
                            resultado = false;
                        }
                        break;

                    case "Cristo Redentor":
                        if (cementerio == cristoRedentor && numeroDeNichos < ((14 * 6) + (5 * 6) + (14 * 6) + (7 * 7) 
                            + (7 * 6) + (4 * 6) + (1 * 1) + (6 * 6) + (6 * 6) + (7 * 6) + (5 * 6) + (6 * 6) + (6 * 1) + (6 * 4) + (6 * 10) + (6 * 5) + (6 * 2)))
                        {
                            resultado = true;
                        }
                        else 
                        {
                        resultado = false;
                        }
                        break;
                }
           

            return resultado;
        }

        public bool validarDisponibilidadDeNichos(string cementerio, string estado, string etapa)
        {
            bool resultado = false;

            //Columbario Ozaman y sus etapas
            var columbarioOzama = "Columbario Ozama";
            var columbarioOzamaetapa1 = "Etapa1";
            //Columbario Ozaman y sus etapas

            //Columbario Santiago y sus etapas
            var columbarioSantiago = "Columbario Santiago";
            var columbarioSantiagoetapa1 = "Etapa1";
            var colubarioSantiagoIslaCentral = "Isla Central";
            //Columbario Santiago y sus etapas

            //Colubario Luperon y sus etapas
            var columbarioLuperon = "Columbario Luperon";
            var columbarioLuperonExternos = "Externos";
            var columbarioLuperonInternos = "Internos";
            var columbarioLuperonFamiliar = "Familiar";
            //Colubario Luperon y sus etapas


            //Ingenio Santiago y sus etapas
            var ingenioSantiago = "Ingenio Santiago";
            var ingenioSantiagoEtapa = "Etapa1";
            //Ingenio Santiago y sus etapas

            //Maximo Gomez y sus etapas
            var maximoGomez = "Maximo Gomez";
            var maximoGomezEtapa1 = "Etapa1";
            var maximoGomezEtapa2 = "Etapa2";
            //Maximo Gomez y sus etapas

            //Cristo Salvador y sus etapas
            var cristoSalvador = "Cristo Salvador";
            var cristoSalvadorEtapa1 = "Etapa1";
            var cristoSalvadorEtapa2 = "Etapa2";
            var cristoSalvadorEtapa3 = "Etapa3";
            var cristoSalvadorEtapa4 = "Etapa4";
            var cristoSalvadorEtapa5 = "Etapa5";
            var cristoSalvadorEtapa6 = "Etapa6";
            var cristoSalvadorEtapa7 = "Etapa7";
            //Cristo Salvador y sus etapas

            //Cristo Redentor y sus etapas
            var cristoRedentor = "Cristo Redentor";
            var cristoRedentorEtapa1 = "Etapa1";
            var cristoRedentorEtapa2 = "Etapa2";
            var cristoRedentorEtapa3 = "Etapa3";
            var cristoRedentorEtapa4 = "Etapa4";
            var cristoRedentorEtapa5 = "Etapa5";
            var cristoRedentorEtapa6 = "Etapa6";
            //etapas agregadas 8/13/2021
            var cristoRedentorEtapa7 = "Etapa7";
            var cristoRedentorEtapa8 = "Etapa8";
            //etapas agregadas 9/3/2021
            var cristoRedentorEtapa9 = "Etapa9";


            //Cristo Redentor y sus etapas




            var numeroNichosPorParedes = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower() && (c.Etapa == etapa)).Count();



            switch (cementerio)
            {
                case "Columbario Ozama":
                    if (cementerio.ToLower() == columbarioOzama.ToLower() && etapa.ToLower() == columbarioOzamaetapa1.ToLower()
                                                                          && numeroNichosPorParedes < (20 * 8))
                    {
                        resultado = true;
                    }
                  
                    break;

                case "Columbario Santiago":
                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == columbarioSantiagoetapa1.ToLower()
                                                                             && numeroNichosPorParedes < (61 * 7))
                    {
                        resultado = true;
                    }

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == colubarioSantiagoIslaCentral.ToLower()
                                                                             && numeroNichosPorParedes < (4 * 10) + (4 * 10))
                    {
                        resultado = true;
                    }

                 
                    break;

                case "Columbario Luperon":
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                  && numeroNichosPorParedes < ((10 * 11) + (10 * 7) + (11 * 8) + (10 * 7)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonInternos.ToLower()
                                                                  && numeroNichosPorParedes < ((22 * 6) + (20 * 11)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonFamiliar.ToLower()
                                                                  && numeroNichosPorParedes < (12 * 3))
                    {
                        resultado = true;
                    }
                   

                    break;

                case "Ingenio Santiago":

                    if (cementerio.ToLower() == ingenioSantiago.ToLower() && etapa.ToLower() == ingenioSantiagoEtapa.ToLower()
                                                                          && numeroNichosPorParedes < (1 * 5))
                    {
                        resultado = true;
                    }
                 
                    break;

                case "Maximo Gomez":
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa1.ToLower()
                                                                      && numeroNichosPorParedes < (5 * 6))
                    {
                        resultado = true;

                    }
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa2.ToLower()
                                                                       && numeroNichosPorParedes < (2 * 6))
                    {
                        resultado = true;

                    }
                  
                    break;

                case "Cristo Salvador":
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && numeroNichosPorParedes < ((1 * 5) + (4 * 5) + (1 * 5)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa2.ToLower()
                                                                         && numeroNichosPorParedes < (4 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa3.ToLower()
                                                                         && numeroNichosPorParedes < (6 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa4.ToLower()
                                                                         && numeroNichosPorParedes < (6 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                          && numeroNichosPorParedes < ((13 * 6) + (4 * 6) + (12 * 6) + (5 * 5) +
                                                                          (12 * 6) + (10 * 6) + (4 * 5)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                                         && numeroNichosPorParedes < ((7 * 5) + (9 * 5) + (7 * 5)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa7.ToLower()
                                                                         && numeroNichosPorParedes < ((7 * 5) + (7 * 5) + (7 * 5)))
                    {
                        resultado = true;
                    }

                    break;

                case "Cristo Redentor":
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                         && numeroNichosPorParedes < ((14 * 6) + (5 * 6) + (14 * 6)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa2.ToLower()
                                                                         && numeroNichosPorParedes < ((7 * 7) + (7 * 6)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa3.ToLower()
                                                                         && numeroNichosPorParedes < ((4 * 6) + (4 * 1)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa4.ToLower()
                                                                     && numeroNichosPorParedes < ((6 * 6) + (6 * 6)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa5.ToLower()
                                                                     && numeroNichosPorParedes < ((7 * 6)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                                     && numeroNichosPorParedes < ((5 * 6) + (6 * 6) + (6 * 1)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa7.ToLower()
                                                                   && numeroNichosPorParedes < ((6 * 4)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa8.ToLower()
                                                                  && numeroNichosPorParedes < ((6 * 10)))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa9.ToLower()
                                                               && numeroNichosPorParedes < ((6 * 5) + (6 * 2)))
                    {
                        resultado = true;
                    }



                    break;


            }


            return resultado;
        }

       public bool validarDisponibilidadDeNichos(string cementerio, string estado, string etapa, string pared) 
        {
            bool resultado = false;

            //Columbario Ozaman y sus etapas
            var columbarioOzama = "Columbario Ozama";
            var columbarioOzama_etapa1 = "Etapa1";
            var columbarioOzama_etapa1_Pared1 = "1";
            //Columbario Ozaman y sus etapas

            //Columbario Santiago y sus etapas
            var columbarioSantiago = "Columbario Santiago";

            var columbarioSantiagoetapa1 = "Etapa1";
            var colubarioSantiagoIslaCentral = "Isla Central";

            var columbarioSantiago_etapa1_pared1 = "1";
            var columbarioSantiago_IslaCentral_pared6 = "6";
            var columbarioSantiago_IslaCentral_pared7 = "7";



            //Columbario Santiago y sus etapas

            //Colubario Luperon y sus etapas
            var columbarioLuperon = "Columbario Luperon";

            var columbarioLuperonExternos = "Externos";
            var columbarioLuperonInternos = "Internos";
            var columbarioLuperonFamiliar = "Familiar";

            var columbarioLuperon_Externos_Pared1 = "1";
            var columbarioLuperon_Externos_Pared2 = "2";
            var columbarioLuperon_Externos_Pared3 = "3";
            var columbarioLuperon_Externos_Pared4 = "4";

            var columbarioLuperon_Internos_Pared5 = "5";
            var columbarioLuperon_Internos_Pared6 = "6";

            var columbarioLuperon_Familiar_Pared7 = "6";
            //Colubario Luperon y sus etapas


            //Ingenio Santiago y sus etapas
            var ingenioSantiago = "Ingenio Santiago";
            var ingenioSantiagoEtapa1 = "Etapa1";

            var ingenioSantiago_Etapa1_pared1 = "1";
            //Ingenio Santiago y sus etapas

            //Maximo Gomez y sus etapas
            var maximoGomez = "Maximo Gomez";
            var maximoGomezEtapa1 = "Etapa1";
            var maximoGomezEtapa2 = "Etapa2";

            var maximoGomez_Etapa1_pared1 = "1";
            var maximoGomez_Etapa2_pared2 = "2";
            //Maximo Gomez y sus etapas

            //Cristo Salvador y sus etapas
            var cristoSalvador = "Cristo Salvador";
            var cristoSalvadorEtapa1 = "Etapa1";
            var cristoSalvadorEtapa2 = "Etapa2";
            var cristoSalvadorEtapa3 = "Etapa3";
            var cristoSalvadorEtapa4 = "Etapa4";
            var cristoSalvadorEtapa5 = "Etapa5";
            var cristoSalvadorEtapa6 = "Etapa6";
            var cristoSalvadorEtapa7 = "Etapa7";


            var cristoSalvador_Etapa1_pared1 = "1";
            var cristoSalvador_Etapa1_pared2 = "2";
            var cristoSalvador_Etapa1_pared3 = "3";

            var cristoSalvador_Etapa2_pared4 = "4";

            var cristoSalvador_Etapa3_pared5 = "5";

            var cristoSalvador_Etapa4_pared6 = "6";

            var cristoSalvador_Etapa5_pared7  = "7";
            var cristoSalvador_Etapa5_pared8  = "8";
            var cristoSalvador_Etapa5_pared9  = "9";
            var cristoSalvador_Etapa5_pared10 = "10";
            var cristoSalvador_Etapa5_pared11 = "11";
            var cristoSalvador_Etapa5_pared12 = "12";
            var cristoSalvador_Etapa5_pared13 = "13";

            var cristoSalvador_Etapa6_pared14 = "14";
            var cristoSalvador_Etapa6_pared15 = "15";
            var cristoSalvador_Etapa6_pared16 = "16";

            var cristoSalvador_Etapa7_pared17 = "17";
            var cristoSalvador_Etapa7_pared18 = "18";
            var cristoSalvador_Etapa7_pared19 = "19";
            //Cristo Salvador y sus etapas

            //Cristo Redentor y sus etapas
            var cristoRedentor = "Cristo Redentor";
            var cristoRedentorEtapa1 = "Etapa1";
            var cristoRedentorEtapa2 = "Etapa2";
            var cristoRedentorEtapa3 = "Etapa3";
            var cristoRedentorEtapa4 = "Etapa4";
            var cristoRedentorEtapa5 = "Etapa5";
            var cristoRedentorEtapa6 = "Etapa6";
            var cristoRedentorEtapa7 = "Etapa7";
            var cristoRedentorEtapa8 = "Etapa8";
            var cristoRedentorEtapa9 = "Etapa9";

            var cristoRedentor_Etapa1_Pared1 = "1";
            var cristoRedentor_Etapa1_Pared2 = "2";
            var cristoRedentor_Etapa1_Pared3 = "3";

            var cristoRedentor_Etapa2_Pared4 = "4";
            var cristoRedentor_Etapa2_Pared5 = "5";

            var cristoRedentor_Etapa3_Pared6 = "6";
            var cristoRedentor_Etapa3_Pared7 = "7";

            var cristoRedentor_Etapa4_Pared8 = "8";
            var cristoRedentor_Etapa4_Pared9 = "9";

            var cristoRedentor_Etapa5_Pared10 = "10";

            var cristoRedentor_Etapa6_Pared11 = "11";
            var cristoRedentor_Etapa6_Pared12 = "12";
            var cristoRedentor_Etapa6_Pared13 = "13";

            var cristoRedentor_Etapa7_Pared14 = "14";

            var cristoRedentor_Etapa8_Pared15 = "15";

            var cristoRedentor_Etapa9_Pared16 = "16";
            var cristoRedentor_Etapa9_Pared17 = "17";

            //Cristo Redentor y sus etapas

            var numeroNichosPorParedes = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower() && (c.Etapa == etapa) && (c.Pared == pared)).Count();


            switch (cementerio) 
            {
                case "Columbario Ozama":

                    if (cementerio.ToLower() == columbarioOzama.ToLower() && etapa.ToLower() == columbarioOzama_etapa1.ToLower()
                                                                          && columbarioOzama_etapa1_Pared1 == pared
                                                                          && numeroNichosPorParedes < (20*8)) 
                    {
                        resultado = true;
                    
                    }
                    
                    break;

                case "Columbario Santiago":

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == columbarioSantiagoetapa1.ToLower()
                                                                             && columbarioSantiago_etapa1_pared1 == pared
                                                                             && numeroNichosPorParedes < (61 * 7))
                    {
                        resultado = true;
                    }

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == colubarioSantiagoIslaCentral.ToLower()
                                                                             && columbarioSantiago_IslaCentral_pared6 == pared
                                                                             && numeroNichosPorParedes < (4 * 10)) 
                    {
                        resultado = true;
                    }

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == colubarioSantiagoIslaCentral.ToLower()
                                                                             && columbarioSantiago_IslaCentral_pared7 == pared
                                                                             && numeroNichosPorParedes < (4 * 10))
                    {
                        resultado = true;
                    }

                    break;

                case "Columbario Luperon":

                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                          && columbarioLuperon_Externos_Pared1 == pared
                                                                          && numeroNichosPorParedes < (10 * 11))
                    {
                        resultado = true;

                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                            && columbarioLuperon_Externos_Pared2 == pared
                                                                            && numeroNichosPorParedes < (10 * 7)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                            && columbarioLuperon_Externos_Pared3 == pared
                                                                            && numeroNichosPorParedes < (11 * 8))  
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                            && columbarioLuperon_Externos_Pared4 == pared
                                                                            && numeroNichosPorParedes < (10 * 7)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonInternos.ToLower()
                                                                            && columbarioLuperon_Internos_Pared5 == pared
                                                                            && numeroNichosPorParedes < (22 * 6)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonInternos.ToLower()
                                                                            && columbarioLuperon_Internos_Pared6 == pared
                                                                            && numeroNichosPorParedes < (20 * 11)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonFamiliar.ToLower()
                                                                            && columbarioLuperon_Familiar_Pared7 == pared
                                                                            && numeroNichosPorParedes < (12 * 3)) 
                    {
                        resultado = true;
                    }

                    break;

                case "Ingenio Santiago":
                    if (cementerio.ToLower() == ingenioSantiago.ToLower() && etapa.ToLower() == ingenioSantiagoEtapa1.ToLower()
                                                                          && ingenioSantiago_Etapa1_pared1 == pared
                                                                          && numeroNichosPorParedes < (1 * 5)) 
                    {
                        resultado = true;
                    }
                    break;

                case "Maximo Gomez":
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa1.ToLower()
                                                                      && maximoGomez_Etapa1_pared1 == pared
                                                                      && numeroNichosPorParedes < (5 * 6)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa2.ToLower()
                                                                      && maximoGomez_Etapa2_pared2 == pared
                                                                      && numeroNichosPorParedes < (2 * 6)) 
                    {
                        resultado = true;
                    }
                    break;


                case "Cristo Salvador":
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && cristoSalvador_Etapa1_pared1 == pared
                                                                         && numeroNichosPorParedes< (1 * 5)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && cristoSalvador_Etapa1_pared2 == pared
                                                                         && numeroNichosPorParedes < (4 * 5)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && cristoSalvador_Etapa1_pared3 == pared
                                                                         && numeroNichosPorParedes < (1 * 5)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa2.ToLower()
                                                                         && cristoSalvador_Etapa2_pared4 == pared
                                                                         && numeroNichosPorParedes < (4 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa3.ToLower()
                                                                       && cristoSalvador_Etapa3_pared5 == pared
                                                                       && numeroNichosPorParedes < (6 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa4.ToLower()
                                                                      && cristoSalvador_Etapa4_pared6 == pared
                                                                      && numeroNichosPorParedes < (6 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                     && cristoSalvador_Etapa5_pared7 == pared
                                                                     && numeroNichosPorParedes < (13 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                   && cristoSalvador_Etapa5_pared8 == pared
                                                                   && numeroNichosPorParedes < (4 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                   && cristoSalvador_Etapa5_pared9 == pared
                                                                   && numeroNichosPorParedes < (12 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                   && cristoSalvador_Etapa5_pared10 == pared
                                                                   && numeroNichosPorParedes < (5 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                 && cristoSalvador_Etapa5_pared11 == pared
                                                                 && numeroNichosPorParedes < (12 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                               && cristoSalvador_Etapa5_pared12 == pared
                                                               && numeroNichosPorParedes < (10 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                            && cristoSalvador_Etapa5_pared13 == pared
                                                            && numeroNichosPorParedes < (4 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                           && cristoSalvador_Etapa6_pared14 == pared
                                                           && numeroNichosPorParedes < (7 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                          && cristoSalvador_Etapa6_pared15 == pared
                                                          && numeroNichosPorParedes < (9 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                        && cristoSalvador_Etapa6_pared16 == pared
                                                        && numeroNichosPorParedes < (7 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa7.ToLower()
                                                       && cristoSalvador_Etapa7_pared17 == pared
                                                       && numeroNichosPorParedes < (7 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa7.ToLower()
                                                       && cristoSalvador_Etapa7_pared18 == pared
                                                       && numeroNichosPorParedes < (7 * 5))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa7.ToLower()
                                                       && cristoSalvador_Etapa7_pared19 == pared
                                                       && numeroNichosPorParedes < (7 * 5))
                    {
                        resultado = true;
                    }
                    break;

                case "Cristo Redentor":
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                         && cristoRedentor_Etapa1_Pared1 == pared
                                                                         && numeroNichosPorParedes < (14 * 6)) 
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                        && cristoRedentor_Etapa1_Pared2 == pared
                                                                        && numeroNichosPorParedes < (5 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                      && cristoRedentor_Etapa1_Pared3 == pared
                                                                      && numeroNichosPorParedes < (14 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa2.ToLower()
                                                                     && cristoRedentor_Etapa2_Pared4 == pared
                                                                     && numeroNichosPorParedes < (7 * 7))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa2.ToLower()
                                                                   && cristoRedentor_Etapa2_Pared5 == pared
                                                                   && numeroNichosPorParedes < (7 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa3.ToLower()
                                                                  && cristoRedentor_Etapa3_Pared6 == pared
                                                                  && numeroNichosPorParedes < (4 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa3.ToLower()
                                                                 && cristoRedentor_Etapa3_Pared7 == pared
                                                                 && numeroNichosPorParedes < (1 * 1))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa4.ToLower()
                                                               && cristoRedentor_Etapa4_Pared8 == pared
                                                               && numeroNichosPorParedes < (6 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa4.ToLower()
                                                            && cristoRedentor_Etapa4_Pared9 == pared
                                                            && numeroNichosPorParedes < (6 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa5.ToLower()
                                                           && cristoRedentor_Etapa5_Pared10 == pared
                                                           && numeroNichosPorParedes < (7 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                         && cristoRedentor_Etapa6_Pared11 == pared
                                                         && numeroNichosPorParedes < (5 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                        && cristoRedentor_Etapa6_Pared12 == pared
                                                        && numeroNichosPorParedes < (6 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                     && cristoRedentor_Etapa6_Pared13 == pared
                                                     && numeroNichosPorParedes < (6 * 1))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa7.ToLower()
                                                    && cristoRedentor_Etapa7_Pared14 == pared
                                                    && numeroNichosPorParedes < (6 * 4))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa8.ToLower()
                                                   && cristoRedentor_Etapa8_Pared15 == pared
                                                   && numeroNichosPorParedes < (6 * 10))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa9.ToLower()
                                                  && cristoRedentor_Etapa9_Pared16 == pared
                                                  && numeroNichosPorParedes < (5 * 6))
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa9.ToLower()
                                                && cristoRedentor_Etapa9_Pared17 == pared
                                                && numeroNichosPorParedes < (2 * 6))
                    {
                        resultado = true;
                    }

                    break;
            }

            return resultado;
        
        }

        //Produccion
        ///TEST
     /*
        public bool validarDisponibilidadDeNichos(string cementerio)
        {
            bool resultado = false;
            //Cementerios
            var columbarioOzama = "Columbario Ozama";
            var columbarioSantiago = "Columbario Santiago";
            var columbarioLuperon = "Columbario Luperon";
            var ingenioSantiago = "Ingenio Santiago";
            var maximoGomez = "Maximo Gomez";
            var cristoSalvador = "Cristo Salvador";
            var cristoRedentor = "Cristo Redentor";

            var numeroDeNichos = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower()).Count();


            switch (cementerio)
            {
                case "Columbario Ozama":
                    //ColumbarioOzama
                    if (cementerio == columbarioOzama && numeroDeNichos < numeroTest)
                    {
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                    break;

                //Columbario Santiago
                case "Columbario Santiago":
                    if (cementerio == columbarioSantiago && numeroDeNichos < numeroTest)
                    {
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                    break;

                case "Columbario Luperon":
                    if (cementerio == columbarioLuperon && numeroDeNichos < numeroTest)
                    {
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                    break;

                case "Ingenio Santiago":
                    if (cementerio == ingenioSantiago && numeroDeNichos < numeroTest)
                    {
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                    break;


                case "Maximo Gomez":
                    if (cementerio == maximoGomez && numeroDeNichos < numeroTest)
                    {
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                    break;

                case "Cristo Salvador":
                    if (cementerio == cristoSalvador && numeroDeNichos < numeroTest)
                    {
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                    break;

                case "Cristo Redentor":
                    if (cementerio == cristoRedentor && numeroDeNichos < numeroTest)
                    {
                        resultado = true;
                    }
                    else
                    {
                        resultado = false;
                    }
                    break;
            }

            return resultado;
        }

        public bool validarDisponibilidadDeNichos(string cementerio, string estado, string etapa)
        {
            bool resultado = false;

            //Columbario Ozaman y sus etapas
            var columbarioOzama = "Columbario Ozama";
            var columbarioOzamaetapa1 = "Etapa1";
            //Columbario Ozaman y sus etapas

            //Columbario Santiago y sus etapas
            var columbarioSantiago = "Columbario Santiago";
            var columbarioSantiagoetapa1 = "Etapa1";
            var colubarioSantiagoIslaCentral = "Isla Central";
            //Columbario Santiago y sus etapas

            //Colubario Luperon y sus etapas
            var columbarioLuperon = "Columbario Luperon";
            var columbarioLuperonExternos = "Externos";
            var columbarioLuperonInternos = "Internos";
            var columbarioLuperonFamiliar = "Familiar";
            //Colubario Luperon y sus etapas


            //Ingenio Santiago y sus etapas
            var ingenioSantiago = "Ingenio Santiago";
            var ingenioSantiagoEtapa = "Etapa1";
            //Ingenio Santiago y sus etapas

            //Maximo Gomez y sus etapas
            var maximoGomez = "Maximo Gomez";
            var maximoGomezEtapa1 = "Etapa1";
            var maximoGomezEtapa2 = "Etapa2";
            //Maximo Gomez y sus etapas

            //Cristo Salvador y sus etapas
            var cristoSalvador = "Cristo Salvador";
            var cristoSalvadorEtapa1 = "Etapa1";
            var cristoSalvadorEtapa2 = "Etapa2";
            var cristoSalvadorEtapa3 = "Etapa3";
            var cristoSalvadorEtapa4 = "Etapa4";
            var cristoSalvadorEtapa5 = "Etapa5";
            var cristoSalvadorEtapa6 = "Etapa6";
            //Cristo Salvador y sus etapas

            //Cristo Redentor y sus etapas
            var cristoRedentor = "Cristo Redentor";
            var cristoRedentorEtapa1 = "Etapa1";
            var cristoRedentorEtapa2 = "Etapa2";
            var cristoRedentorEtapa3 = "Etapa3";
            var cristoRedentorEtapa4 = "Etapa4";
            var cristoRedentorEtapa5 = "Etapa5";
            var cristoRedentorEtapa6 = "Etapa6";
            //Cristo Redentor y sus etapas




            var numeroNichosPorParedes = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower() && (c.Etapa == etapa)).Count();



            switch (cementerio)
            {
                case "Columbario Ozama":
                    if (cementerio.ToLower() == columbarioOzama.ToLower() && etapa.ToLower() == columbarioOzamaetapa1.ToLower()
                                                                          && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    
                    break;

                case "Columbario Santiago":
                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == columbarioSantiagoetapa1.ToLower()
                                                                             && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == colubarioSantiagoIslaCentral.ToLower()
                                                                             && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }

                   
                    break;

                case "Columbario Luperon":
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                  && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonInternos.ToLower()
                                                                  && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonFamiliar.ToLower()
                                                                  && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                  

                    break;

                case "Ingenio Santiago":

                    if (cementerio.ToLower() == ingenioSantiago.ToLower() && etapa.ToLower() == ingenioSantiagoEtapa.ToLower()
                                                                          && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                   
                    break;

                case "Maximo Gomez":
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa1.ToLower()
                                                                      && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;

                    }
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa2.ToLower()
                                                                       && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;

                    }
                  
                    break;

                case "Cristo Salvador":
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa2.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa3.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa4.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                          && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                  
                    break;

                case "Cristo Redentor":
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa2.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa3.ToLower()
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa4.ToLower()
                                                                     && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa5.ToLower()
                                                                     && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cementerio.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                                     && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    
                    break;


            }


            return resultado;
        }

        public bool validarDisponibilidadDeNichos(string cementerio, string estado, string etapa, string pared)
        {
            bool resultado = false;

            //Columbario Ozaman y sus etapas
            var columbarioOzama = "Columbario Ozama";
            var columbarioOzama_etapa1 = "Etapa1";
            var columbarioOzama_etapa1_Pared1 = "1";
            //Columbario Ozaman y sus etapas

            //Columbario Santiago y sus etapas
            var columbarioSantiago = "Columbario Santiago";

            var columbarioSantiagoetapa1 = "Etapa1";
            var colubarioSantiagoIslaCentral = "Isla Central";

            var columbarioSantiago_etapa1_pared1 = "1";
            var columbarioSantiago_IslaCentral_pared6 = "6";
            var columbarioSantiago_IslaCentral_pared7 = "7";



            //Columbario Santiago y sus etapas

            //Colubario Luperon y sus etapas
            var columbarioLuperon = "Columbario Luperon";

            var columbarioLuperonExternos = "Externos";
            var columbarioLuperonInternos = "Internos";
            var columbarioLuperonFamiliar = "Familiar";

            var columbarioLuperon_Externos_Pared1 = "1";
            var columbarioLuperon_Externos_Pared2 = "2";
            var columbarioLuperon_Externos_Pared3 = "3";
            var columbarioLuperon_Externos_Pared4 = "4";

            var columbarioLuperon_Internos_Pared5 = "5";
            var columbarioLuperon_Internos_Pared6 = "6";

            var columbarioLuperon_Familiar_Pared7 = "6";
            //Colubario Luperon y sus etapas


            //Ingenio Santiago y sus etapas
            var ingenioSantiago = "Ingenio Santiago";
            var ingenioSantiagoEtapa1 = "Etapa1";

            var ingenioSantiago_Etapa1_pared1 = "1";
            //Ingenio Santiago y sus etapas

            //Maximo Gomez y sus etapas
            var maximoGomez = "Maximo Gomez";
            var maximoGomezEtapa1 = "Etapa1";
            var maximoGomezEtapa2 = "Etapa2";

            var maximoGomez_Etapa1_pared1 = "1";
            var maximoGomez_Etapa2_pared2 = "2";
            //Maximo Gomez y sus etapas

            //Cristo Salvador y sus etapas
            var cristoSalvador = "Cristo Salvador";
            var cristoSalvadorEtapa1 = "Etapa1";
            var cristoSalvadorEtapa2 = "Etapa2";
            var cristoSalvadorEtapa3 = "Etapa3";
            var cristoSalvadorEtapa4 = "Etapa4";
            var cristoSalvadorEtapa5 = "Etapa5";
            var cristoSalvadorEtapa6 = "Etapa6";

            var cristoSalvador_Etapa1_pared1 = "1";
            var cristoSalvador_Etapa1_pared2 = "2";
            var cristoSalvador_Etapa1_pared3 = "3";

            var cristoSalvador_Etapa2_pared4 = "4";

            var cristoSalvador_Etapa3_pared5 = "5";

            var cristoSalvador_Etapa4_pared6 = "6";

            var cristoSalvador_Etapa5_pared7 = "7";
            var cristoSalvador_Etapa5_pared8 = "8";
            var cristoSalvador_Etapa5_pared9 = "9";
            var cristoSalvador_Etapa5_pared10 = "10";
            var cristoSalvador_Etapa5_pared11 = "11";
            var cristoSalvador_Etapa5_pared12 = "12";
            var cristoSalvador_Etapa5_pared13 = "13";

            var cristoSalvador_Etapa6_pared14 = "14";
            var cristoSalvador_Etapa6_pared15 = "15";
            var cristoSalvador_Etapa6_pared16 = "16";
            //Cristo Salvador y sus etapas

            //Cristo Redentor y sus etapas
            var cristoRedentor = "Cristo Redentor";
            var cristoRedentorEtapa1 = "Etapa1";
            var cristoRedentorEtapa2 = "Etapa2";
            var cristoRedentorEtapa3 = "Etapa3";
            var cristoRedentorEtapa4 = "Etapa4";
            var cristoRedentorEtapa5 = "Etapa5";
            var cristoRedentorEtapa6 = "Etapa6";

            var cristoRedentor_Etapa1_Pared1 = "1";
            var cristoRedentor_Etapa1_Pared2 = "2";
            var cristoRedentor_Etapa1_Pared3 = "3";

            var cristoRedentor_Etapa2_Pared4 = "4";
            var cristoRedentor_Etapa2_Pared5 = "5";

            var cristoRedentor_Etapa3_Pared6 = "6";
            var cristoRedentor_Etapa3_Pared7 = "7";

            var cristoRedentor_Etapa4_Pared8 = "8";
            var cristoRedentor_Etapa4_Pared9 = "9";

            var cristoRedentor_Etapa5_Pared10 = "10";

            var cristoRedentor_Etapa6_Pared11 = "11";
            var cristoRedentor_Etapa6_Pared12 = "12";
            var cristoRedentor_Etapa6_Pared13 = "13";

            //Cristo Redentor y sus etapas

            var numeroNichosPorParedes = _context.INVCAM_Inventario.ToList().Where(c => c.Cementerio.ToLower() == cementerio.ToLower() && (c.Etapa == etapa) && (c.Pared == pared)).Count();


            switch (cementerio)
            {
                case "Columbario Ozama":

                    if (cementerio.ToLower() == columbarioOzama.ToLower() && etapa.ToLower() == columbarioOzama_etapa1.ToLower()
                                                                          && columbarioOzama_etapa1_Pared1 == pared
                                                                          && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;

                    }

                    break;

                case "Columbario Santiago":

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == columbarioSantiagoetapa1.ToLower()
                                                                             && columbarioSantiago_etapa1_pared1 == pared
                                                                             && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == colubarioSantiagoIslaCentral.ToLower()
                                                                             && columbarioSantiago_IslaCentral_pared6 == pared
                                                                             && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }

                    if (cementerio.ToLower() == columbarioSantiago.ToLower() && etapa.ToLower() == colubarioSantiagoIslaCentral.ToLower()
                                                                             && columbarioSantiago_IslaCentral_pared7 == pared
                                                                             && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }

                    break;

                case "Columbario Luperon":

                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                          && columbarioLuperon_Externos_Pared1 == pared
                                                                          && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;

                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                            && columbarioLuperon_Externos_Pared2 == pared
                                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                            && columbarioLuperon_Externos_Pared3 == pared
                                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonExternos.ToLower()
                                                                            && columbarioLuperon_Externos_Pared4 == pared
                                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonInternos.ToLower()
                                                                            && columbarioLuperon_Internos_Pared5 == pared
                                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonInternos.ToLower()
                                                                            && columbarioLuperon_Internos_Pared6 == pared
                                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == columbarioLuperon.ToLower() && etapa.ToLower() == columbarioLuperonFamiliar.ToLower()
                                                                            && columbarioLuperon_Familiar_Pared7 == pared
                                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }

                    break;

                case "Ingenio Santiago":
                    if (cementerio.ToLower() == ingenioSantiago.ToLower() && etapa.ToLower() == ingenioSantiagoEtapa1.ToLower()
                                                                          && ingenioSantiago_Etapa1_pared1 == pared
                                                                          && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    break;

                case "Maximo Gomez":
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa1.ToLower()
                                                                      && maximoGomez_Etapa1_pared1 == pared
                                                                      && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == maximoGomez.ToLower() && etapa.ToLower() == maximoGomezEtapa2.ToLower()
                                                                      && maximoGomez_Etapa2_pared2 == pared
                                                                      && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    break;


                case "Cristo Salvador":
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && cristoSalvador_Etapa1_pared1 == pared
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && cristoSalvador_Etapa1_pared2 == pared
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa1.ToLower()
                                                                         && cristoSalvador_Etapa1_pared3 == pared
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa2.ToLower()
                                                                         && cristoSalvador_Etapa2_pared4 == pared
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa3.ToLower()
                                                                       && cristoSalvador_Etapa3_pared5 == pared
                                                                       && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa4.ToLower()
                                                                      && cristoSalvador_Etapa4_pared6 == pared
                                                                      && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                     && cristoSalvador_Etapa5_pared7 == pared
                                                                     && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                   && cristoSalvador_Etapa5_pared8 == pared
                                                                   && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                   && cristoSalvador_Etapa5_pared9 == pared
                                                                   && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                   && cristoSalvador_Etapa5_pared10 == pared
                                                                   && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                                 && cristoSalvador_Etapa5_pared11 == pared
                                                                 && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                               && cristoSalvador_Etapa5_pared12 == pared
                                                               && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa5.ToLower()
                                                            && cristoSalvador_Etapa5_pared13 == pared
                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                           && cristoSalvador_Etapa6_pared14 == pared
                                                           && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                          && cristoSalvador_Etapa6_pared15 == pared
                                                          && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoSalvador.ToLower() && etapa.ToLower() == cristoSalvadorEtapa6.ToLower()
                                                        && cristoSalvador_Etapa6_pared16 == pared
                                                        && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    break;

                case "Cristo Redentor":
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                         && cristoRedentor_Etapa1_Pared1 == pared
                                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                        && cristoRedentor_Etapa1_Pared2 == pared
                                                                        && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa1.ToLower()
                                                                      && cristoRedentor_Etapa1_Pared3 == pared
                                                                      && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa2.ToLower()
                                                                     && cristoRedentor_Etapa2_Pared4 == pared
                                                                     && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa2.ToLower()
                                                                   && cristoRedentor_Etapa2_Pared5 == pared
                                                                   && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa3.ToLower()
                                                                  && cristoRedentor_Etapa3_Pared6 == pared
                                                                  && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa3.ToLower()
                                                                 && cristoRedentor_Etapa3_Pared7 == pared
                                                                 && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa4.ToLower()
                                                               && cristoRedentor_Etapa4_Pared8 == pared
                                                               && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa4.ToLower()
                                                            && cristoRedentor_Etapa4_Pared9 == pared
                                                            && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa5.ToLower()
                                                           && cristoRedentor_Etapa5_Pared10 == pared
                                                           && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                         && cristoRedentor_Etapa6_Pared11 == pared
                                                         && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                        && cristoRedentor_Etapa6_Pared12 == pared
                                                        && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }
                    if (cementerio.ToLower() == cristoRedentor.ToLower() && etapa.ToLower() == cristoRedentorEtapa6.ToLower()
                                                     && cristoRedentor_Etapa6_Pared13 == pared
                                                     && numeroNichosPorParedes < numeroTest)
                    {
                        resultado = true;
                    }

                    break;
            }

            return resultado;

        }
     */

        ////Test
    }
}
