﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_Estructura_ColumnaController : ControllerBase
    {
        private readonly IINVCAM_Estructura_ColumnaRepository _ecRepo;
        private readonly IMapper _mapper;

        public INVCAM_Estructura_ColumnaController(IINVCAM_Estructura_ColumnaRepository ecRepo,
                                                    IMapper mapper)
        {
            _ecRepo = ecRepo;
            _mapper = mapper;
        }

        [HttpGet("{cementerio}/{etapa}/{pared}", Name= "GetINVCAM_Estructura_Columna")]
        public IActionResult GetINVCAM_Estructura_Columna(string cementerio, string etapa, int pared) 
        {
            var INVCAM_Estructura_Columna = _ecRepo.GetINVCAM_Estructura_Columna(cementerio, etapa, pared);

            if (INVCAM_Estructura_Columna == null) 
            {
                return NotFound();

            }

            //var itemINVCAM_Estructura_ColumnaDto = _mapper.Map<INVCAM_Estructura_ColumnaDto>(INVCAM_Estructura_Columna);

            return Ok(INVCAM_Estructura_Columna);
            
        }

    }
}
