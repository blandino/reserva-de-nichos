﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_PRA_MASTERController : ControllerBase
    {
        private readonly IINVCAM_PRA_MASTERRepository _pmRepo;
        private readonly IMapper _mapper;
        public INVCAM_PRA_MASTERController(IINVCAM_PRA_MASTERRepository pmRepo,
                                            IMapper mapper)
        {
            _pmRepo = pmRepo;
            _mapper = mapper;        
        }

        //[HttpGet([]]
        //public IActionResult GetINVCAM_PRA_MASTERS(string PRANUM)
        //{
        //    var listaINVCAM_PRA_MASTERS = _pmRepo.GetINVCAM_PRA_MASTER(PRANUM);
        //    var listaINVCAM_PRA_MASTERSDto = new List<INVCAM_PRA_MASTERDto>();
                       
        //    return Ok(listaINVCAM_PRA_MASTERSDto);
        //}

        [HttpGet("{INVCAM_PRA_MASTERId:int}", Name = "GetINVCAM_PRA_MASTER")]
        public IActionResult GetINVCAM_PRA_MASTER(string INVCAM_PRA_MASTERId)
        {
            var itemINVCAM_PRA_MASTERS = _pmRepo.GetINVCAM_PRA_MASTER(INVCAM_PRA_MASTERId);

            if (itemINVCAM_PRA_MASTERS == null)
            {
                return NotFound();
            }

            var itemINVCAM_PRA_MASTERSDto = _mapper.Map<INVCAM_InventarioDto>(itemINVCAM_PRA_MASTERS);

            return Ok(itemINVCAM_PRA_MASTERSDto);
        }
    }
}
