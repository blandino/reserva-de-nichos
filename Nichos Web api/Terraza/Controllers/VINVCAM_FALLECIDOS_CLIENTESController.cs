﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VINVCAM_FALLECIDOS_CLIENTESController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IINVCAM_LogRepository _Ilog_Repo;

        INVCAM_Log log = new INVCAM_Log();
        DateTime fecha = DateTime.Now;   

        public VINVCAM_FALLECIDOS_CLIENTESController(ApplicationDbContext context,
                                                     IINVCAM_LogRepository Ilog_Repo) 
        {
            
            _context = context;
            _Ilog_Repo = Ilog_Repo;
             
        }

        [HttpGet]
        public ICollection<VINVCAM_FALLECIDOS_CLIENTES> VINVCAM_FALLECIDOS_CLIENTES() 
        {
            
            /*log.Usuario = _Ilog_Repo.ObtenerUsuario();
            log.Fecha = fecha;
            log.Detalles = "Datos obtenidos de manera correcta";
            _Ilog_Repo.GuardarLog(log);*/
             return _context.VINVCAM_FALLECIDOS_CLIENTES.ToList();

        }
        [HttpGet("{VINVCAM_CodigoInventario}", Name = "GetINVCAM_Fallecido_Clientes_ByCodigo")]
        public VINVCAM_FALLECIDOS_CLIENTES GetINVCAM_Fallecido_Clientes_ByCodigo(string VINVCAM_CodigoInventario)
        {
            //var codigoInventario = _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.codigoInventario == VINVCAM_CodigoInventario);

            //return _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.codigoInventario == VINVCAM_CodigoInventario);
            var codigoInventario = _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.codigoInventario == VINVCAM_CodigoInventario);
            if (codigoInventario == null ) 
            {
                codigoInventario = _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.Codigo == VINVCAM_CodigoInventario);
            }
            return codigoInventario;

        }
    }
}
