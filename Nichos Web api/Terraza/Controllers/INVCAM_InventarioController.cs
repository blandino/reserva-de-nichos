﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Data;
using Terraza.Models;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_InventarioController : Controller
    {
        private readonly IINVCAM_InventarioRepository _InRepo;
        private readonly IINVCAM_FallecidosRepository _fRepo;
        private readonly IINVCAM_LogRepository _ILog_Repo;
        private readonly IMapper _mapper;
        private readonly ApplicationDbContext _context;
        static string _usuario;
        INVCAM_Log log = new INVCAM_Log();
        DateTime fecha = DateTime.Now;

        public INVCAM_InventarioController(IINVCAM_InventarioRepository InRepo,
                                           IMapper mapper,
                                           ApplicationDbContext context,
                                           IINVCAM_LogRepository ILog_Repo
                                           )
        {
            _InRepo = InRepo;
            _mapper = mapper;
            _context = context;
            _ILog_Repo = ILog_Repo;
        }

        //[HttpGet("{Usuario}", Name = "ObtenerUsuario")]
     
        [Route(@"getUsuario/{Usuario}")]
        [HttpGet]    
        public void ObtenerUsuario(string Usuario) 
        {
            _usuario = Usuario;           
        } 
        

        [HttpGet]        
        public IActionResult GetINVCAM_Inventarios()
        {
             

          //  log.Usuario = ObtenerUsuario(_usuario);
           // log.Fecha = DateTime.Now;
           // log.Detalles = "primera prueba";
           // log.Usuario = _ILog_Repo.ObtenerUsuario(); ;

            var listaINVCAM_Inventarios = _InRepo.GetINVCAM_Inventarios();
            var listaINVCAM_InventariosDto = new List<INVCAM_InventarioDto>();

            foreach (var lista in listaINVCAM_Inventarios)
            {
                listaINVCAM_InventariosDto.Add(_mapper.Map<INVCAM_InventarioDto>(lista));
            }

            //Creacion del log
           // _ILog_Repo.GuardarLog(log);
            return Ok(listaINVCAM_InventariosDto);
        }

        [HttpGet("{INVCAM_InventarioId:int}", Name = "GetINVCAM_Inventario")]
        public IActionResult GetINVCAM_Inventario(int INVCAM_InventarioId)
        {
            var itemINVCAM_Inventario = _InRepo.GetINVCAM_Inventario(INVCAM_InventarioId);

            if (itemINVCAM_Inventario == null)
            {
                return NotFound();
            }

            var itemitemINVCAM_InventarioDto = _mapper.Map<INVCAM_InventarioDto>(itemINVCAM_Inventario);

            return Ok(itemitemINVCAM_InventarioDto);
        }

        

        [HttpGet("{INVCAM_InventarioCodigo}/{opcional}", Name = "GetINVCAM_InventarioByCodigo")]
        public bool GetINVCAM_InventarioByCodigo(string INVCAM_InventarioCodigo, string opcional)
        {
            var result = _InRepo.GetINVCAM_Inventarios().Where(x => x.Codigo == INVCAM_InventarioCodigo).SingleOrDefault();

            if (result != null)
            {
                return true;
            }

            return false;
        }

        public class ListaFallecidos
        {
            public string Codigo { get; set; }
            public string NombreCliente { get; set; }
            public string NombreFallecido { get; set; }
            public DateTime FechaNacimiento { get; set; }
            public DateTime FechaFallecimiento { get; set; }
            public string cementerio { get; set; }
            public string Tipo { get; set; }
            public string Contrato { get; set; }
            public string Panum { get; set; }
            public string Pared { get; set; }
            public string Fila { get; set; }
            public string Columna { get; set; }
            public string Etapa { get; set; }
            public string Estatus { get; set; }
            public string TipoColumbario { get; set; }
        }

        // [HttpGet("{nombreFallecido}/{condicion}/{opcional}", Name = "FiltrarFallecidos")]
        [HttpGet("{nombreFallecido}/{tipo:int}", Name = "FiltrarFallecidos")]
        public List<ListaFallecidos> FiltrarFallecidos(string nombreFallecido, int tipo/*, string condicion, string opcional*/)
        {
            List<ListaFallecidos> lista = new List<ListaFallecidos>();
   

            // 1 = fallecido, 2 = cliente, 3 = contrato
            if (tipo == 1) 
            {
                log.Usuario = _ILog_Repo.ObtenerUsuario();
                log.Fecha = fecha;
                log.Detalles = "El usuario: " + log.Usuario + "ha ejecutado una busqueda filtrada por el fallecido";
                _ILog_Repo.GuardarLog(log);


                var result = from c in _context.VINVCAM_FALLECIDOS_CLIENTES
                             where c.fallecido1.Contains(nombreFallecido.ToLower().Trim()) || c.fallecido2.Contains(nombreFallecido.ToLower().Trim())
                             || c.fallecido3.Contains(nombreFallecido.ToLower().Trim()) || c.fallecido4.Contains(nombreFallecido.ToLower().Trim())
                               || c.fallecido5.Contains(nombreFallecido.ToLower().Trim()) || c.fallecido6.Contains(nombreFallecido.ToLower().Trim())
                                 || c.fallecido7.Contains(nombreFallecido.ToLower().Trim()) || c.fallecido8.Contains(nombreFallecido.ToLower().Trim())
                                 //|| c.Contrato.Contains(nombreFallecido) || c.NombreCliente.Contains(nombreFallecido)
                             select c;

                foreach (var item in result)
                {
                    ListaFallecidos simple = new ListaFallecidos();
                    if (!string.IsNullOrEmpty(item.fallecido1))
                    {

                        //if (item.fallecido1.ToLower().ToString().Contains(nombreFallecido.ToLower()) || item.Contrato.ToString() == condicion.ToString() || item.NombreCliente.ToString() == condicion.ToString())
                        if (item.fallecido1.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))

                        {
                            simple.NombreFallecido = item.fallecido1;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento1;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento1;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            //simple.Codigo = item.codigoInventario;
                            simple.Codigo = item.Codigo;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);
                           
                        }
                    }

                    if (!string.IsNullOrEmpty(item.fallecido2))
                    {

                        if (item.fallecido2.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))
                        {
                            simple.NombreFallecido = item.fallecido2;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento2;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento2;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            //simple.Codigo = item.codigoInventario;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);

                        }
                    }
                    if (!string.IsNullOrEmpty(item.fallecido3))
                    {
                        if (item.fallecido3.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))
                        {
                            simple.NombreFallecido = item.fallecido3;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento3;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento3;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            // simple.Codigo = item.codigoInventario;
                            simple.Codigo = item.Codigo;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);

                        }
                    }
                    if (!string.IsNullOrEmpty(item.fallecido4))
                    {
                        if (item.fallecido4.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))
                        {
                            simple.NombreFallecido = item.fallecido4;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento4;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento4;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            //simple.Codigo = item.codigoInventario;
                            simple.Codigo = item.Codigo;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);

                        }
                    }
                    if (!string.IsNullOrEmpty(item.fallecido5))
                    {
                        if (item.fallecido5.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))
                        {
                            simple.NombreFallecido = item.fallecido5;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento5;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento5;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            //simple.Codigo = item.codigoInventario;
                            simple.Codigo = item.Codigo;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);

                        }
                    }
                    if (!string.IsNullOrEmpty(item.fallecido6))
                    {
                        if (item.fallecido6.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))
                        {
                            simple.NombreFallecido = item.fallecido6;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento6;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento6;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            //simple.Codigo = item.codigoInventario;
                            simple.Codigo = item.Codigo;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);

                        }
                    }
                    if (!string.IsNullOrEmpty(item.fallecido7))
                    {
                        if (item.fallecido7.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))
                        {
                            simple.NombreFallecido = item.fallecido7;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento7;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento7;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            //simple.Codigo = item.codigoInventario;
                            simple.Codigo = item.Codigo;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);

                        }
                    }
                    if (!string.IsNullOrEmpty(item.fallecido8))
                    {
                        if (item.fallecido8.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().Trim()))
                        {
                            simple.NombreFallecido = item.fallecido8;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento8;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento8;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                            //simple.Codigo = item.codigoInventario;
                            simple.Codigo = item.Codigo;
                            simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);

                        }

                    }
                    simple = null;
                }

                return lista;

            }


            if (tipo == 2) 
            {

                log.Usuario = _ILog_Repo.ObtenerUsuario();
                log.Fecha = fecha;
                log.Detalles = "El usuario: " + log.Usuario + "ha ejecutado una busqueda filtrada por Contrato";
                _ILog_Repo.GuardarLog(log);

                var result = from c in _context.VINVCAM_FALLECIDOS_CLIENTES
                             where c.Contrato.Trim().Contains(nombreFallecido.Trim()) 
                             select c;

                foreach (var item in result) 
                {
                    ListaFallecidos simple = new ListaFallecidos();
                //    if (!string.IsNullOrEmpty(item.fallecido1))
                  //  {
                         if ( item.Contrato.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().ToString().Trim()) )

                        {
                            simple.NombreFallecido = item.fallecido1;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento1;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento1;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                        //simple.Codigo = item.codigoInventario;
                        simple.Codigo = item.Codigo;
                        simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);
                        simple = null;
                    }
                   
                }

                //}
                return lista;
            }


            if (tipo == 3)
            {
                log.Usuario = _ILog_Repo.ObtenerUsuario();
                log.Fecha = fecha;
                log.Detalles = "El usuario: " + log.Usuario+ " ha ejecutado una busqueda filtrada por Cliente";
                _ILog_Repo.GuardarLog(log);
                var result = from c in _context.VINVCAM_FALLECIDOS_CLIENTES
                             where c.NombreCliente.Trim().Contains(nombreFallecido.Trim())
                             select c;

                foreach (var item in result)
                {
                    ListaFallecidos simple = new ListaFallecidos();
                    // if (!string.IsNullOrEmpty(item.fallecido1))
                    // {
                    if (item.NombreCliente.ToLower().ToString().Trim().Contains(nombreFallecido.ToLower().ToString().Trim()))

                        {
                            simple.NombreFallecido = item.fallecido1;
                            simple.FechaFallecimiento = (DateTime)item.fechaFallecimiento1;
                            simple.FechaNacimiento = (DateTime)item.fechaFallecimiento1;
                            simple.cementerio = item.Cementerio;
                            simple.NombreCliente = item.NombreCliente;
                        //simple.Codigo = item.codigoInventario;
                        simple.Codigo = item.Codigo;
                        simple.Tipo = item.Tipo;
                            simple.Contrato = item.Contrato;
                            simple.Codigo = item.Codigo;
                            simple.Panum = item.Panum;
                            simple.Pared = item.Pared;
                            simple.Fila = item.Fila;
                            simple.Columna = item.Columna;
                            simple.Etapa = item.Etapa;
                            simple.Estatus = item.Estatus;
                            simple.TipoColumbario = item.TipoColumbario;
                            lista.Add(simple);
                        simple = null;
                        }
                    //}

                }
                return lista;
            }
            

            return lista;
        }
       

        [HttpPost]
         public IActionResult CrearINVCAM_Inventario([FromBody] INVCAM_InventarioDto iNVCAM_InventarioDto)
         {
             if (iNVCAM_InventarioDto == null)
             {
                 return BadRequest(ModelState);
             }
             if (_InRepo.ExisteINVCAM_Inventario(iNVCAM_InventarioDto.Codigo))
             {
                 ModelState.AddModelError("", "El codigo ya existe");
                 return StatusCode(500, ModelState);
             }




             var inventario = _mapper.Map<INVCAM_Inventario>(iNVCAM_InventarioDto);          

             if (!_InRepo.CrearINVCAM_Inventario(inventario))
             {
                 ModelState.AddModelError("", $"Algo salio mal guardando el registro{inventario.Codigo}");
                 return StatusCode(500, ModelState);
             }
            //return CreatedAtRoute("GetINVCAM_Inventario", new { inventarioId = inventario.Id }, inventario);

            log.Usuario = _ILog_Repo.ObtenerUsuario();
            log.Fecha = fecha;
            log.Detalles = "El usuario: " + log.Usuario + " ha creado el registro con el codigo: " +  iNVCAM_InventarioDto.Codigo + " y el estado: "+ iNVCAM_InventarioDto.Estatus;
            _ILog_Repo.GuardarLog(log);

            return Ok();

         }

        [HttpPatch("{INVCAM_InventarioId:int}", Name = "ActualizarINVCAM_Inventario")]
        public IActionResult ActualizarINVCAM_Inventario(int INVCAM_InventarioId, [FromBody] INVCAM_InventarioDto iNVCAM_InventarioDto)
        {
            if (iNVCAM_InventarioDto == null || INVCAM_InventarioId != iNVCAM_InventarioDto.Id)
            {
                return BadRequest(ModelState);
            }

            var inventario = _mapper.Map<INVCAM_Inventario>(iNVCAM_InventarioDto);

            if (!_InRepo.ActualizarINVCAM_Inventario(inventario))
            {
                ModelState.AddModelError("", $"Algo salio mal actualizando el Registro{inventario.Codigo}");
                return StatusCode(500, ModelState);
            }
            log.Usuario = _ILog_Repo.ObtenerUsuario();
            log.Fecha = fecha;
            log.Detalles = "El registro identificado con el codigo "+ iNVCAM_InventarioDto.Codigo + " ha sido modificado por el usuario: " + log.Usuario;
            _ILog_Repo.GuardarLog(log);

            return NoContent();
        }

        [HttpDelete("{INVCAM_InventarioId:int}", Name = "BorrarINVCAM_Inventario")]
        public IActionResult BorrarINVCAM_Inventario(int INVCAM_InventarioId)
        {
            if (!_InRepo.ExisteINVCAM_Inventario(INVCAM_InventarioId))
            {
                return NotFound();
            }

            var inventario = _InRepo.GetINVCAM_Inventario(INVCAM_InventarioId);

            if (!_InRepo.BorrarINVCAM_Inventario(inventario))
            {
                ModelState.AddModelError("", $"Algo salio mal Borrando el registro{inventario.Codigo}");
                return StatusCode(500, ModelState);
            }

            log.Usuario = _ILog_Repo.ObtenerUsuario();
            log.Fecha = fecha;
            log.Detalles = "El registro identificado con el codigo " + inventario.Codigo + " ha sido eliminado por el usuario: " + log.Usuario;
            _ILog_Repo.GuardarLog(log);

            return NoContent();
        }


        
    }
}
