﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_PermisosController : ControllerBase
    {
        private readonly IINVCAM_PermisosRepository _pmRepo;
        private readonly IMapper _mapper;
        private readonly IINVCAM_LogRepository _ILog_Repo;
        static string _usuario;
        INVCAM_Log log = new INVCAM_Log();
        DateTime fecha = DateTime.Now;

        public INVCAM_PermisosController(IINVCAM_PermisosRepository pmRepo,
                                           IMapper mapper,
                                           IINVCAM_LogRepository ILog_Repo)
        {
            _pmRepo = pmRepo;
            _mapper = mapper;
            _ILog_Repo = ILog_Repo;
        }

        [HttpGet]
        public IActionResult GetPermisos()
        {
            var listaINVCAM_Permisos = _pmRepo.GetPermisos();
            var listaINVCAM_PermisosDto = new List<INVCAM_Permisos>();

            foreach (var lista in listaINVCAM_Permisos)
            {
                listaINVCAM_PermisosDto.Add(_mapper.Map<INVCAM_Permisos>(lista));
            }
            return Ok(listaINVCAM_PermisosDto);
        }

        [HttpPost]
        public IActionResult CrearINVCAM_Permisos([FromBody] INVCAM_PermisosDto iNVCAM_PermisosDto)
        {
            if (iNVCAM_PermisosDto == null)
            {
                return BadRequest(ModelState);
            }
            if (_pmRepo.ExistePermiso(iNVCAM_PermisosDto.usuarioID))
            {
                ModelState.AddModelError("", "Elcodigo ya existe");
                return StatusCode(500, ModelState);
            }

            var permiso = _mapper.Map<INVCAM_Permisos>(iNVCAM_PermisosDto);

            if (!_pmRepo.CrearPermiso(permiso))
            {
                ModelState.AddModelError("", $"Algo salio mal guardando el registro{permiso.usuarioID}");
                return StatusCode(500, ModelState);
            }
            //return CreatedAtRoute("GetINVCAM_Inventario", new { inventarioId = inventario.Id }, inventario);
            
            log.Usuario = _ILog_Repo.ObtenerUsuario();
            log.Fecha = fecha;
            log.Detalles = "El usuario: " + log.Usuario + " ha otorgado el acceso sistema de nichos al usuario: " + permiso.usuarioID;
            _ILog_Repo.GuardarLog(log);

            return Ok();

        }

        [HttpDelete("{INVCAM_PermisosId:int}", Name = "BorrarINVCAM_Permiso")]
        public IActionResult BorrarINVCAM_Permiso(int INVCAM_PermisosId)
        {
            if (!_pmRepo.ExistePermiso(INVCAM_PermisosId))
            {
                return NotFound();
            }

            var inventario = _pmRepo.GetPermiso(INVCAM_PermisosId);

            if (!_pmRepo.BorrarPermiso(inventario))
            {
                ModelState.AddModelError("", $"Algo salio mal Borrando el registro{inventario.usuarioID}");
                return StatusCode(500, ModelState);
            }

            log.Usuario = _ILog_Repo.ObtenerUsuario();
            log.Fecha = fecha;
            log.Detalles = "El usuario: " + log.Usuario + " ha eliminado el acceso al sitema de nichos al usuario: " + inventario.usuarioID;
            _ILog_Repo.GuardarLog(log);

            return NoContent();
        }

        /* [HttpDelete("{INVCAM_PermisosId}", Name = "BorrarINVCAM_Permiso")]
         public IActionResult BorrarINVCAM_Permiso(string INVCAM_PermisosId)
         {
             if (!_pmRepo.ExistePermiso(INVCAM_PermisosId))
             {
                 return NotFound();
             }

             var inventario = _pmRepo.GetPermiso(INVCAM_PermisosId);

             if (!_pmRepo.BorrarPermiso(inventario))
             {
                 ModelState.AddModelError("", $"Algo salio mal Borrando el registro{inventario.usuarioID}");
                 return StatusCode(500, ModelState);
             }
             return NoContent();
         }*/

        [HttpPatch("{INVCAM_PermisosId:int}", Name = "ActualizarINVCAM_Permisos")]
        public IActionResult ActualizarINVCAM_Permisos(int INVCAM_PermisosId, [FromBody] INVCAM_PermisosDto iNVCAM_PermisosDto)
        {
            if (iNVCAM_PermisosDto == null || INVCAM_PermisosId != iNVCAM_PermisosDto.permisoID)
            {
                return BadRequest(ModelState);
            }

            var permiso = _mapper.Map<INVCAM_Permisos>(iNVCAM_PermisosDto);

            if (!_pmRepo.ActualizarPermiso(permiso))
            {
                ModelState.AddModelError("", $"Algo salio mal actualizando el Registro{permiso.permisoID}");
                return StatusCode(500, ModelState);
            }

            log.Usuario = _ILog_Repo.ObtenerUsuario();
            log.Fecha = fecha;
            log.Detalles = "El usuario: " + log.Usuario + " ha modificado los permisos al usuario: " + permiso.usuarioID;
            _ILog_Repo.GuardarLog(log);
            return NoContent();
        }
    }
}
