﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Terraza.Data;
using Terraza.Models;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_PRA_MASTER_CONTRACTController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public INVCAM_PRA_MASTER_CONTRACTController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/INVCAM_PRA_MASTER_CONTRACT
        [HttpGet]
        public async Task<ActionResult<IEnumerable<INVCAM_PRA_MASTER>>> GetINVCAM_PRA_MASTER()
        {
            return await _context.INVCAM_PRA_MASTER.ToListAsync();
        }

        // GET: api/INVCAM_PRA_MASTER_CONTRACT/5
        [HttpGet("{PRACONTRACT}")]
        public INVCAM_PRA_MASTER GetINVCAM_PRA_MASTER_bycontract(string PRACONTRACT)
        {
            var iNVCAM_PRA_MASTER = _context.INVCAM_PRA_MASTER.Where(x => x.PRACONTRACT == PRACONTRACT).FirstOrDefault();

            if (iNVCAM_PRA_MASTER == null)
            {
                return null;
            }

            return iNVCAM_PRA_MASTER;
        }
    }
}
