﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_ParametrosController : ControllerBase
    {
        private readonly IINVCAM_ParametrosRepository _pRepo;
        private readonly IMapper _mapper;

        public INVCAM_ParametrosController(IINVCAM_ParametrosRepository pRepo,
                                           IMapper mapper)
        {
            _pRepo = pRepo;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetINVCAM_Columnas()
        {
            var listaINVCAM_Parametros = _pRepo.GetINVCAM_parametros();
            var listaINVCAM_ParametrosDto = new List<INVCAM_ParametrosDto>();

            foreach (var lista in listaINVCAM_Parametros)
            {
                var fechaVencimiento = DateTime.Now.AddDays(lista.diasReserva);
                
                

                listaINVCAM_ParametrosDto.Add(_mapper.Map<INVCAM_ParametrosDto>(lista));
            }
            return Ok(listaINVCAM_ParametrosDto);
        }
    }
}
