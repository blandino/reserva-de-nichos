﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_ColumnasController : ControllerBase
    {
        private readonly IINVCAM_ColumnasRepository _clRepo;
        private readonly IMapper _mapper;

        public INVCAM_ColumnasController(IINVCAM_ColumnasRepository clRepo,
                                           IMapper mapper)
        {
            _clRepo = clRepo;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetINVCAM_Columnas()
        {
            var listaINVCAM_Columnas = _clRepo.GetINVCAM_Columnas();
            var listaINVCAM_ColumnasDto = new List<INVCAM_ColumnasDto>();

            foreach (var lista in listaINVCAM_Columnas)
            {
                listaINVCAM_ColumnasDto.Add(_mapper.Map<INVCAM_ColumnasDto>(lista));
            }
            return Ok(listaINVCAM_ColumnasDto);
        }
    }
}

