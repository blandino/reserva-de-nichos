﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Data;
using Terraza.Models;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_TipoColumbarioController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private readonly IINVCAM_TipoColumbarioRepository _tCRepo;
        private readonly IMapper _mapper; 
        public INVCAM_TipoColumbarioController( ApplicationDbContext db,
                                                IINVCAM_TipoColumbarioRepository tCRepo,
                                                IMapper mapper)
        {
            _db = db;
            _tCRepo = tCRepo;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetINVCAM_TipoColumbario() 
        {
            var listaINVCAM_TipoColumbario = _tCRepo.GetINVCAM_TipoColumbario();
            var listaINVCAM_TipoColumbaDto = new List<INVCAM_TipoColumbarioDto>();

            foreach (var lista in listaINVCAM_TipoColumbario) 
            {
                listaINVCAM_TipoColumbaDto.Add(_mapper.Map<INVCAM_TipoColumbarioDto>(lista));            
            }
            return Ok(listaINVCAM_TipoColumbaDto);
        }


    }
}
