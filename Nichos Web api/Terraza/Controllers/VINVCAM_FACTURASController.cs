﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VINVCAM_FACTURASController : ControllerBase
    {
        private readonly IVINVCAM_FACTURASRepository _vfRepo;
        private readonly IMapper _mapper;

        public VINVCAM_FACTURASController(IVINVCAM_FACTURASRepository vfRepo,
                                           IMapper mapper)
        {
            _vfRepo = vfRepo;
            _mapper = mapper;
        }


        /*  [HttpGet("{SOPNUMBE}", Name = "GetINVCAM_FACTURAS")]
          public IActionResult GetINVCAM_FACTURAS(string SOPNUMBE)
          {
              var VINVCAM_FACTURAS = _vfRepo.GetVINVCAM_FACTURA(SOPNUMBE);

              if (VINVCAM_FACTURAS == null)
              {
                  return NotFound();
              }

              var itemVINVCAM_FACTURASDto = _mapper.Map<VINVCAM_FACTURASDto>(VINVCAM_FACTURAS);

              return Ok(itemVINVCAM_FACTURASDto);
          }*/

        [HttpGet("{SOPNUMBE}", Name = "GetINVCAM_FACTURAS")]
        public IActionResult GetINVCAM_FACTURAS(string SOPNUMBE)
        {
            var VINVCAM_FACTURAS = _vfRepo.GetVINVCAM_FACTURA(SOPNUMBE);

            if (VINVCAM_FACTURAS == null)
            {
                 var VINVCAM_FACTURAS_OLD = _vfRepo.GetVINVCAM_FACTURA_OLD(SOPNUMBE);
                var itemVINVCAM_FACTURAS_OLDDto = _mapper.Map<VINVCAM_FACTURAS_OLDDto>(VINVCAM_FACTURAS_OLD);
                return Ok (itemVINVCAM_FACTURAS_OLDDto);
            }

            var itemVINVCAM_FACTURASDto = _mapper.Map<VINVCAM_FACTURASDto>(VINVCAM_FACTURAS);

            return Ok(itemVINVCAM_FACTURASDto);
        }
    }
}
