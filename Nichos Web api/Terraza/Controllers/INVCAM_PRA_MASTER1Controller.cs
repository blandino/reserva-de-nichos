﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Terraza.Data;
using Terraza.Models;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_PRA_MASTER1Controller : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public INVCAM_PRA_MASTER1Controller(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/INVCAM_PRA_MASTER1
        [HttpGet]
        public async Task<ActionResult<IEnumerable<INVCAM_PRA_MASTER>>> GetINVCAM_PRA_MASTER()
        {
            return await _context.INVCAM_PRA_MASTER.ToListAsync();
        }

        // GET:     
        [HttpGet("{PRANUM}")]
        public INVCAM_PRA_MASTER  GetINVCAM_PRA_MASTER(string PRANUM)
        {
            var iNVCAM_PRA_MASTER =  _context.INVCAM_PRA_MASTER.Where(x=>x.PRANUM==PRANUM).FirstOrDefault();

            if (iNVCAM_PRA_MASTER == null)
            {
                return null;
            }

            return iNVCAM_PRA_MASTER;
        }

       


    }
}
