﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Data;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_LogController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IINVCAM_LogRepository _ILog_Repo;

        public INVCAM_LogController(IINVCAM_LogRepository ILog_Repo)
        {
            _ILog_Repo = ILog_Repo;
        }

        [HttpGet("{Usuario}", Name = "GuardarUsuario")]
        public IActionResult GuardarUsuario(string Usuario) {
            _ILog_Repo.GuardarUsuario(Usuario);

            return Ok();
        }

        [HttpGet]
        public string ObtenerUsuario() {

            var usuario =_ILog_Repo.ObtenerUsuario();

            return usuario;
            
        }

        [Route(@"Get_Log")]
        [HttpGet]
        public IActionResult Get_Log() 
        {
            var log = _ILog_Repo.GetINVCAM_Log();
            
            return Ok(log);
        }

        [HttpGet("{fechaInicial:DateTime}/{fechaFinal:DateTime}", Name = "Get_Log_By_Date")]
        public IActionResult Get_Log_By_Date(DateTime fechaInicial, DateTime fechaFinal) 
        {
            var log_By_Date = _ILog_Repo.GetINVCAM_LogByDate(fechaInicial, fechaFinal);
            return Ok(log_By_Date);

            
        }
    }
}
