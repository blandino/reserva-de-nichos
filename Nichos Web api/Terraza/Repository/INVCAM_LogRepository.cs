﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_LogRepository : IINVCAM_LogRepository
    {
        public readonly ApplicationDbContext _context;
        static string _usuario;

        public INVCAM_LogRepository(ApplicationDbContext context) {
            _context = context;
        }

        public bool GuardarLog(INVCAM_Log INVCAM_Log) {
            _context.INVCAM_Log.Add(INVCAM_Log);
            return Guardar();
        }

        public bool Guardar()
        {
            return _context.SaveChanges() >= 0 ? true : false;
        }      

        public string ObtenerUsuario()
        {
            return _usuario;
        }

        public void GuardarUsuario(string Usuario)
        {
            _usuario = Usuario;
        }

        public ICollection<INVCAM_Log> GetINVCAM_Log()
        {
            return _context.INVCAM_Log.OrderBy(i => i.Id).ToList();            
        }

       /* public ICollection<INVCAM_Log> GetINVCAM_LogByDate(DateTime date)
        {           
            return _context.INVCAM_Log.Where(i => i.Fecha.Date == date.Date).ToList();       
        }*/

        public ICollection<INVCAM_Log> GetINVCAM_LogByDate(DateTime Fecha_Desde, DateTime Fecha_hasta)
        {
            return _context.INVCAM_Log.Where(i => i.Fecha.Date >= Fecha_Desde && i.Fecha.Date <= Fecha_hasta).ToList(); 
        }
    }
}
