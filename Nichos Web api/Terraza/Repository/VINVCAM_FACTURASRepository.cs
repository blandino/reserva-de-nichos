﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class VINVCAM_FACTURASRepository : IVINVCAM_FACTURASRepository
    {
        private readonly ApplicationDbContext _db;

        public VINVCAM_FACTURASRepository(ApplicationDbContext db)
        {
            _db = db;
        }
     
        public VINVCAM_FACTURAS GetVINVCAM_FACTURA(string VINVCAM_FACTURAS)
        {
            return _db.VINVCAM_FACTURAS.FirstOrDefault(I => I.SOPNUMBE == VINVCAM_FACTURAS);
        }

        public VINVCAM_FACTURAS_OLD GetVINVCAM_FACTURA_OLD(string VINVCAM_FACTURAS)
        {
            return _db.VINVCAM_FACTURAS_OLD.FirstOrDefault(I => I.SOPNUMBE == VINVCAM_FACTURAS);
        }
    }
}
