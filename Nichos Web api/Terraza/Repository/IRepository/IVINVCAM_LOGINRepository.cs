﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IVINVCAM_LOGINRepository
    {
        ICollection<VINVCAM_LOGIN> GetVINVCAM_LoginUsers();
        VINVCAM_LOGIN GetVINVCAM_LoginUser(string VINVCAM_LOGINId);
        bool ExistLoginUser(string name);
        bool ExistLoginUser(int Id);
        bool CreateVINVCAM_LOGINId(VINVCAM_LOGIN VINVCAM_LOGIN);
        bool UpdateVINVCAM_LOGIN(VINVCAM_LOGIN VINVCAM_LOGIN);
        bool DeleteVINVCAM_LOGIN(VINVCAM_LOGIN VINVCAM_LOGIN);
        bool Save();
    }
}
