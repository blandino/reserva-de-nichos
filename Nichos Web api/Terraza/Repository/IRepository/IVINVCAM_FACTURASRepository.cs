﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IVINVCAM_FACTURASRepository
    {
        VINVCAM_FACTURAS GetVINVCAM_FACTURA(string VINVCAM_FACTURAS);
        VINVCAM_FACTURAS_OLD GetVINVCAM_FACTURA_OLD(string VINVCAM_FACTURAS);
    }
}
