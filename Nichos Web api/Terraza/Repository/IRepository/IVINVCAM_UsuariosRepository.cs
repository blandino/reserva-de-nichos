﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IVINVCAM_UsuariosRepository
    {
        ICollection<VINVCAM_Usuarios> GetUsers();
    }
}
