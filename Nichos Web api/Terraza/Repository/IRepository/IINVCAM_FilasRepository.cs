﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
   public interface IINVCAM_FilasRepository
    {
        ICollection<INVCAM_Filas> GetINVCAM_Filas();
    }
}
