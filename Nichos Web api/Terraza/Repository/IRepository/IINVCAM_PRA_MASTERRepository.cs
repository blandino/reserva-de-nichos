﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_PRA_MASTERRepository
    {
        ICollection<INVCAM_PRA_MASTER> GetINVCAM_PRA_MASTERS();
        INVCAM_PRA_MASTER GetINVCAM_PRA_MASTER(string INVCAM_PRA_MASTERId);
    }
}
