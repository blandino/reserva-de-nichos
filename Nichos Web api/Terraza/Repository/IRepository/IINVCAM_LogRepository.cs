﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_LogRepository
    {
        bool GuardarLog(INVCAM_Log INVCAM_Log);
        bool Guardar();
        string ObtenerUsuario();
        void GuardarUsuario(string Usuario);
        ICollection<INVCAM_Log> GetINVCAM_Log();
        ICollection<INVCAM_Log> GetINVCAM_LogByDate(DateTime Fecha_Desde, DateTime Fecha_hasta);

    }
}
