﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class VINVCAM_InventarioRepository : IVINVCAM_InventarioRepository
    {
        private readonly ApplicationDbContext _db;

        public VINVCAM_InventarioRepository(ApplicationDbContext db) 
        {
            _db = db;
        
        }
        public VINVCAM_Inventario GetINVCAM_InventarioByCodigo(string VINVCAM_CodigoInventario)
        {
            return _db.VINVCAM_Inventario.FirstOrDefault(I => I.Codigo == VINVCAM_CodigoInventario);
        }

        public ICollection<VINVCAM_Inventario> GetVINVCAM_Inventarios()
        {
            return _db.VINVCAM_Inventario.OrderBy(i => i.Id).ToList();
        }              
    }
}
