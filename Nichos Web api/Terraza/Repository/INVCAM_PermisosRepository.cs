﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_PermisosRepository : IINVCAM_PermisosRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_PermisosRepository( ApplicationDbContext db)
        {
            _db = db;
        }


        public bool ActualizarPermiso(INVCAM_Permisos INVCAM_Permisos)
        {
            _db.INVCAM_Permisos.Update(INVCAM_Permisos);
            return Guardar();
        }

        public bool BorrarPermiso(INVCAM_Permisos INVCAM_Permisos)
        {
            _db.INVCAM_Permisos.Remove(INVCAM_Permisos);
            return Guardar();
        }

        public bool CrearPermiso(INVCAM_Permisos INVCAM_Permisos)
        {
            _db.INVCAM_Permisos.Add(INVCAM_Permisos);
            return Guardar();
        }

        public bool ExistePermiso(string _usuarioID)
        {
            bool valor = _db.INVCAM_Permisos.Any(I => I.usuarioID.ToLower().Trim() == _usuarioID.ToLower().Trim());
            return valor;
        }

        public bool ExistePermiso(int Id)
        {
            return _db.INVCAM_Permisos.Any(I => I.permisoID == Id);
        }

        public INVCAM_Permisos GetPermiso(int INVCAM_InventarioId)
        {
            return _db.INVCAM_Permisos.FirstOrDefault(I => I.permisoID == INVCAM_InventarioId);
        }

        /*public INVCAM_Permisos GetPermiso(string INVCAM_PermisosId)
        {
            return _db.INVCAM_Permisos.FirstOrDefault(I => I.usuarioID == INVCAM_PermisosId);
        }*/

        public ICollection<INVCAM_Permisos> GetPermisos()
        {
            return _db.INVCAM_Permisos.OrderBy(i => i.permisoID).ToList();
        }

        public bool Guardar()
        {
            return _db.SaveChanges() >= 0 ? true : false;
        }
    }
}
