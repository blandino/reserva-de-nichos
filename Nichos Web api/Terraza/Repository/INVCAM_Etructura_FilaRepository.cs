﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_Etructura_FilaRepository : IINVCAM_Etructura_FilaRepository
    {
        private readonly ApplicationDbContext _db;
        public INVCAM_Etructura_FilaRepository( ApplicationDbContext db) 
        {
            _db = db;
        }

        public List<string> GetINVCAM_Etructura_FilaRepository(string cementerio, string etapa, int pared)
        {
            List<string> items = new List<string>();
            var result = _db.INVCAM_Etructura_Fila.Where(x => x.Cementerio == cementerio && x.Etapa == etapa && x.Pared == pared).SingleOrDefault();

            if (result == null)
            {
                return null;
            }

            /*for (var i = Convert.ToChar(result.Desde); i <= Convert.ToChar(result.Hasta); i++)
            {
                
                items.Add((char)i + "");
            }*/
           
            for ( var i = Convert.ToChar(result.Hasta); i >= Convert.ToChar(result.Desde); i--)
            {
                items.Add((char)i + "");
            }
            return items;
        }
    }
}
