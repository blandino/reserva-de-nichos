﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{    
    public class INVCAM_ParametrosRepository : IINVCAM_ParametrosRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_ParametrosRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public ICollection<INVCAM_Parametros> GetINVCAM_parametros()
        {
            return _db.INVCAM_Parametros.OrderBy(i => i.parametroID).ToList();
        }
    }
}
