﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_ParedesRepository : IINVCAM_ParedesRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_ParedesRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public ICollection<INVCAM_Paredes> GetINVCAM_Paredes()
        {
            return _db.INVCAM_Paredes.OrderBy(i => i.Id).ToList();
        }
    }
}
