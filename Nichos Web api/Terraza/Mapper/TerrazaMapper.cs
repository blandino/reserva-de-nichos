﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;
using Terraza.Models.Dtos;

namespace Terraza.Mapper
{
    public class TerrazaMapper : Profile
    {
        public TerrazaMapper() 
        {
            CreateMap<INVCAM_Inventario, INVCAM_InventarioDto>().ReverseMap();
            CreateMap<INVCAM_Filas, INVCAM_FilasDto>().ReverseMap();
            CreateMap<INVCAM_Columnas, INVCAM_ColumnasDto>().ReverseMap();
            CreateMap<INVCAM_Paredes, INVCAM_ParedesDto>().ReverseMap();
            CreateMap<INVCAM_PRA_MASTER, INVCAM_PRA_MASTERDto>().ReverseMap();
            CreateMap<INVCAM_Parametros, INVCAM_ParametrosDto>().ReverseMap();
            CreateMap<VINVCAM_Usuarios, VINVCAM_UsuariosDto>().ReverseMap();
            CreateMap<VINVCAM_LOGIN, VINVCAM_LOGINDto>().ReverseMap();
            CreateMap<INVCAM_Permisos, INVCAM_PermisosDto>().ReverseMap();
            CreateMap<VINVCAM_FACTURAS, VINVCAM_FACTURASDto>().ReverseMap();     
            CreateMap<INVCAM_Estructura_Columna, INVCAM_Estructura_ColumnaDto>().ReverseMap();
            CreateMap<INVCAM_Etructura_Fila, INVCAM_Etructura_FilaDto>().ReverseMap();
            CreateMap<INVCAM_TipoColumbario, INVCAM_TipoColumbarioDto>().ReverseMap();
            CreateMap<INVCAM_Fallecidos, INVCAM_FallecidosDto>().ReverseMap();
            CreateMap<VINVCAM_Inventario, VINVCAM_InventarioDto>().ReverseMap();
            CreateMap<VINVCAM_FACTURAS_OLD, VINVCAM_FACTURAS_OLDDto>().ReverseMap();



        }
    }
}
