using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Terraza.Data;
using Terraza.Mapper;
using Terraza.Repository;
using Terraza.Repository.IRepository;
using Microsoft.OpenApi.Models;
using Terraza.Models;

namespace Terraza
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(Options => Options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddScoped<IINVCAM_InventarioRepository, INVCAM_InventarioRepository>();
            services.AddScoped<IINVCAM_FilasRepository, INVCAM_FilasRepository>();
            services.AddScoped<IINVCAM_ColumnasRepository, INVCAM_ColumnasRepository>();
            services.AddScoped<IINVCAM_ParedesRepository, INVCAM_ParedesRepository>();
            services.AddScoped<IINVCAM_PRA_MASTERRepository, INVCAM_PRA_MASTERRepository>();
            services.AddScoped<IINVCAM_ParametrosRepository, INVCAM_ParametrosRepository>();
            services.AddScoped<IVINVCAM_UsuariosRepository, VINVCAM_UsuariosRepository>();
            services.AddScoped<IVINVCAM_LOGINRepository, VINVCAM_LOGINRepository>();
            services.AddScoped<IINVCAM_PermisosRepository, INVCAM_PermisosRepository>();
            services.AddScoped<IVINVCAM_FACTURASRepository, VINVCAM_FACTURASRepository>();
            services.AddScoped<IINVCAM_Estructura_ColumnaRepository, INVCAM_Estructura_ColumnaRepository>();
            services.AddScoped<IINVCAM_Etructura_FilaRepository, INVCAM_Etructura_FilaRepository>();
            services.AddScoped<IINVCAM_TipoColumbarioRepository, INVCAM_TipoColumbarioRepository>();
            services.AddScoped<IINVCAM_FallecidosRepository, INVCAM_FallecidosRepository>();
            services.AddScoped<IVINVCAM_InventarioRepository, VINVCAM_InventarioRepository>();
            services.AddScoped<IINVCAM_LogRepository, INVCAM_LogRepository>();



            services.AddAutoMapper(typeof(TerrazaMapper));

            services.AddCors(Options =>
           {
               Options.AddPolicy("CorsPolicy", builder => builder.AllowAnyOrigin()
                                                                 .AllowAnyHeader()
                                                                 .AllowAnyMethod());
           }

            );

            services.AddControllers();
            AddSwagger(services);



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CorsPolicy");

            //app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Foo API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

            });

        }

        private void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(options =>
            {
                var groupName = "v1";

                options.SwaggerDoc(groupName, new OpenApiInfo
                {
                    Title = $"Foo {groupName}",
                    Version = groupName,
                    Description = "Foo API",
                    Contact = new OpenApiContact
                    {
                        Name = "Foo Company",
                        Email = string.Empty,
                        Url = new Uri("https://foo.com/"),
                    }
                });
            });
        }
    }
}
