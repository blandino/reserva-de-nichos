﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_LogRepository : IINVCAM_LogRepository
    {
        public readonly ApplicationDbContext _context;

        public INVCAM_LogRepository(ApplicationDbContext context) {
            _context = context;
        }

        public bool GuardarLog(INVCAM_Log INVCAM_Log) {
            _context.INVCAM_Log.Add(INVCAM_Log);
            return Guardar();
        }

        public bool Guardar()
        {
            return _context.SaveChanges() >= 0 ? true : false;
        }

        public string ObtenerUsuario(string Usuario)
        {
            return Usuario;
        }
    }
}
