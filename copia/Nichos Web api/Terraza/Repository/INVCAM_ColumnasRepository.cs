﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_ColumnasRepository : IINVCAM_ColumnasRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_ColumnasRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public ICollection<INVCAM_Columnas> GetINVCAM_Columnas()
        {
            return _db.INVCAM_Columnas.OrderBy(i => i.Id).ToList();
        }
    }
}
