﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_Estructura_ColumnaRepository
    {
        public List<string> GetINVCAM_Estructura_Columna(string cementerio, string etapa, int pared);
    }
}
