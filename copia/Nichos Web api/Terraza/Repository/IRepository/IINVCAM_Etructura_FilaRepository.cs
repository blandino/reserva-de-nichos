﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_Etructura_FilaRepository
    {
        public List<string> GetINVCAM_Etructura_FilaRepository(string cementerio, string etapa, int pared);
    }
}
