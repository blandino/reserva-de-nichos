﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_ParametrosRepository
    {
        ICollection<INVCAM_Parametros> GetINVCAM_parametros();
    }
}
