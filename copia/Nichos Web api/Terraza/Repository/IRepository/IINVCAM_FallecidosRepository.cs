﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_FallecidosRepository
    {
        ICollection<INVCAM_Fallecidos> GetINVCAM_Fallecidos();
        INVCAM_Fallecidos GetINVCAM_Fallecido(int INVCAM_FallecidosId);
        INVCAM_Fallecidos GetINVCAM_FallecidosByCodigo(string INVCAM_FallecidosCodigo);
        bool ExisteINVCAM_Fallecidos(string nombre);
        bool ExisteINVCAM_Fallecidos(int Id);
        bool CrearINVCAM_Fallecidos(INVCAM_Fallecidos INVCAM_Fallecidos);
        bool ActualizarINVCAM_Fallecidos(INVCAM_Fallecidos INVCAM_Fallecidos);
        bool BorrarINVCAM_Fallecidos(INVCAM_Fallecidos INVCAM_Fallecidos);
        bool Guardar();
    }
}
