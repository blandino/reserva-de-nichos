﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_LogRepository
    {
        bool GuardarLog(INVCAM_Log INVCAM_Log);
        bool Guardar();
        string ObtenerUsuario(string Usuario);
        
    }
}
