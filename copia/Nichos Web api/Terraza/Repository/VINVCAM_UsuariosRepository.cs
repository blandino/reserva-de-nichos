﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class VINVCAM_UsuariosRepository : IVINVCAM_UsuariosRepository
    {
        private readonly ApplicationDbContext _db;

        public VINVCAM_UsuariosRepository (ApplicationDbContext db)
        {
            _db = db;
        }

       
        public ICollection<VINVCAM_Usuarios> GetUsers()
        {
            return _db.VINVCAM_Usuarios.OrderBy(i => i.USRID).ToList();
        }
    }
}
