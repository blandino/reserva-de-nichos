﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class INVCAM_PermisosDto
    {
        public int permisoID { get; set; }
        public string usuarioID { get; set; }
        public Boolean estatus { get; set; }
        public Boolean isAdmin { get; set; }
    }
}
