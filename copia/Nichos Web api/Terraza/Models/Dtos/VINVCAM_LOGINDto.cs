﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class VINVCAM_LOGINDto
    {
        public string USRID { get; set; }
        public string USRNAME { get; set; }
        public string PWL { get; set; }
        public Boolean isAdmin { get; set; }
        public Boolean estatus { get; set; }
        public int permisoID { get; set; }
    }
}
