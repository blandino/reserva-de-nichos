﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class INVCAM_ParametrosDto
    {
        public int parametroID { get; set; }
        public int diasReserva { get; set; }
    }
}
