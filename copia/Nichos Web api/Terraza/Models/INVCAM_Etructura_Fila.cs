﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Etructura_Fila
    {
        [Key]
        public int ID { get; set; }
        public string Type { get; set; }
        public string Desde { get; set; }
        public string Hasta { get; set; }
        public string Cementerio { get; set; }
        public string Etapa { get; set; }
        public int Pared { get; set; }
    }
}
