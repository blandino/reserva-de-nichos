﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class VINVCAM_FACTURAS
    {
        [Key]
        public string   SOPNUMBE  { get; set; }
        public string   ORIGNUMB  { get; set; }
        public DateTime DOCDATE   { get; set; }
        public string   CUSTNMBR  { get; set; } 
        public string   CUSTNAME  { get; set; }

    }
}
