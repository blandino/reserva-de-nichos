﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_FilasController : ControllerBase
    {
        private readonly IINVCAM_FilasRepository _flRepo;
        private readonly IMapper _mapper;

        public INVCAM_FilasController(IINVCAM_FilasRepository FlRepo,
                                           IMapper mapper)
        {
            _flRepo = FlRepo;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetINVCAM_Filas()
        {
            var listaINVCAM_Filas = _flRepo.GetINVCAM_Filas();
            var listaINVCAM_FilasDto = new List<INVCAM_FilasDto>();

            foreach (var lista in listaINVCAM_Filas)
            {
                listaINVCAM_FilasDto.Add(_mapper.Map<INVCAM_FilasDto>(lista));
            }
            // return Ok(listaINVCAM_FilasDto);
            return Ok(listaINVCAM_FilasDto.OrderByDescending(x => x.Fila).ToList());
        }
    }
}
