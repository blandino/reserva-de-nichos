﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Data;
using Terraza.Models;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VINVCAM_FALLECIDOS_CLIENTESController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public VINVCAM_FALLECIDOS_CLIENTESController(ApplicationDbContext context) 
        {
            _context = context;
        }

        [HttpGet]
        public ICollection<VINVCAM_FALLECIDOS_CLIENTES> VINVCAM_FALLECIDOS_CLIENTES() 
        {
             return _context.VINVCAM_FALLECIDOS_CLIENTES.ToList();

        }
        [HttpGet("{VINVCAM_CodigoInventario}", Name = "GetINVCAM_Fallecido_Clientes_ByCodigo")]
        public VINVCAM_FALLECIDOS_CLIENTES GetINVCAM_Fallecido_Clientes_ByCodigo(string VINVCAM_CodigoInventario)
        {
            //var codigoInventario = _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.codigoInventario == VINVCAM_CodigoInventario);

            //return _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.codigoInventario == VINVCAM_CodigoInventario);
            var codigoInventario = _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.codigoInventario == VINVCAM_CodigoInventario);
            if (codigoInventario == null ) 
            {
                codigoInventario = _context.VINVCAM_FALLECIDOS_CLIENTES.FirstOrDefault(I => I.Codigo == VINVCAM_CodigoInventario);
            }
            return codigoInventario;

        }
    }
}
