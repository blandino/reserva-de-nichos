﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Terraza.Data;
using Terraza.Models;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VINVCAM_SalesPersonController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public VINVCAM_SalesPersonController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/VINVCAM_SalesPerson
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VINVCAM_SalesPerson>>> GetVINVCAM_SalesPerson()
        {
            return await _context.VINVCAM_SalesPerson.ToListAsync();
        }

        // GET: api/VINVCAM_SalesPerson/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VINVCAM_SalesPerson>> GetVINVCAM_SalesPerson(int id)
        {
            var vINVCAM_SalesPerson = await _context.VINVCAM_SalesPerson.FindAsync(id);

            if (vINVCAM_SalesPerson == null)
            {
                return NotFound();
            }

            return vINVCAM_SalesPerson;
        }

        // PUT: api/VINVCAM_SalesPerson/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVINVCAM_SalesPerson(int id, VINVCAM_SalesPerson vINVCAM_SalesPerson)
        {
            if (id != vINVCAM_SalesPerson.DEX_ROW_ID)
            {
                return BadRequest();
            }

            _context.Entry(vINVCAM_SalesPerson).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VINVCAM_SalesPersonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/VINVCAM_SalesPerson
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<VINVCAM_SalesPerson>> PostVINVCAM_SalesPerson(VINVCAM_SalesPerson vINVCAM_SalesPerson)
        {
            _context.VINVCAM_SalesPerson.Add(vINVCAM_SalesPerson);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetVINVCAM_SalesPerson", new { id = vINVCAM_SalesPerson.DEX_ROW_ID }, vINVCAM_SalesPerson);
        }

        // DELETE: api/VINVCAM_SalesPerson/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<VINVCAM_SalesPerson>> DeleteVINVCAM_SalesPerson(int id)
        {
            var vINVCAM_SalesPerson = await _context.VINVCAM_SalesPerson.FindAsync(id);
            if (vINVCAM_SalesPerson == null)
            {
                return NotFound();
            }

            _context.VINVCAM_SalesPerson.Remove(vINVCAM_SalesPerson);
            await _context.SaveChangesAsync();

            return vINVCAM_SalesPerson;
        }

        private bool VINVCAM_SalesPersonExists(int id)
        {
            return _context.VINVCAM_SalesPerson.Any(e => e.DEX_ROW_ID == id);
        }
    }
}
