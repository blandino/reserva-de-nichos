﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VINVCAM_LOGINController : ControllerBase
    {
        
        private readonly IVINVCAM_LOGINRepository _lgRepo;
        private readonly IMapper _mapper;

        public VINVCAM_LOGINController(IVINVCAM_LOGINRepository lgRepo,
                                           IMapper mapper)
        {
            _lgRepo = lgRepo;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetVINVCAM_LoginUsers()
        {
            var VINVCAM_LoginUserList = _lgRepo.GetVINVCAM_LoginUsers();
            var VINVCAM_LoginUserListDto = new List<VINVCAM_LOGINDto>();

            foreach (var list in VINVCAM_LoginUserList)
            {
                VINVCAM_LoginUserListDto.Add(_mapper.Map<VINVCAM_LOGINDto>(list));
            }
            return Ok(VINVCAM_LoginUserListDto);
        }

        [HttpGet("{usrid}/{pwl}", Name = "Login")]
        public bool Login(string usrid, string pwl)
        { 
            var result = _lgRepo.GetVINVCAM_LoginUsers().Where(x=>x.USRID.ToLower()==usrid.ToLower() && x.PWL==pwl).SingleOrDefault();
            if (result != null)
            {
                return true;

            }
           
            return false;     
        }


        [HttpGet("{usrid}", Name = "Admin")]
        public bool Admin(string usrid)
        {
            var result = _lgRepo.GetVINVCAM_LoginUsers().Where(x => x.USRID.ToLower() == usrid.ToLower()).SingleOrDefault();
            
            return result.isAdmin;  
        }
        [HttpPost]
        public IActionResult CreateLoginUser([FromBody] VINVCAM_LOGINDto vINVCAM_LOGINDto)
        {
            if (vINVCAM_LOGINDto == null)
            {
                return BadRequest(ModelState);
            }
            if (_lgRepo.ExistLoginUser(vINVCAM_LOGINDto.USRID))
            {
                ModelState.AddModelError("", "Elcodigo ya existe");
                return StatusCode(500, ModelState);
            }

            var loginUser = _mapper.Map<VINVCAM_LOGIN>(vINVCAM_LOGINDto);

            if (!_lgRepo.CreateVINVCAM_LOGINId(loginUser))
            {
                ModelState.AddModelError("", $"Algo salio mal guardando el registro{loginUser.USRID}");
                return StatusCode(500, ModelState);
            }
            //return CreatedAtRoute("GetINVCAM_Inventario", new { inventarioId = inventario.Id }, inventario);
            return Ok();

        }



    }
}
