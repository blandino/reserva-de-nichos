﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<INVCAM_Inventario> INVCAM_Inventario       { get; set; }
        public DbSet<INVCAM_Filas> INVCAM_Filas                 { get; set; }
        public DbSet<INVCAM_Columnas> INVCAM_Columnas           { get; set; }
        public DbSet<INVCAM_Paredes> INVCAM_Paredes             { get; set; }
        public DbSet<INVCAM_PRA_MASTER> INVCAM_PRA_MASTER       { get; set; }
        public DbSet<VINVCAM_SalesPerson> VINVCAM_SalesPerson   { get; set; }
        public DbSet<INVCAM_Parametros> INVCAM_Parametros       { get; set; }
        public DbSet<VINVCAM_Usuarios> VINVCAM_Usuarios         { get; set; }
        public DbSet<VINVCAM_LOGIN> VINVCAM_LOGIN               { get; set; }
        public DbSet<INVCAM_Permisos> INVCAM_Permisos           { get; set; }
        public DbSet<VINVCAM_FACTURAS> VINVCAM_FACTURAS         { get; set; }
        public DbSet<INVCAM_Estructura_Columna> INVCAM_Estructura_Columna { get; set; }
        public DbSet<INVCAM_Etructura_Fila> INVCAM_Etructura_Fila { get; set; }
        public DbSet<INVCAM_TipoColumbario> INVCAM_TipoColumbario { get; set; }
        public DbSet<INVCAM_Fallecidos> INVCAM_Fallecidos { get; set; }
        public DbSet<VINVCAM_Inventario> VINVCAM_Inventario { get; set; }
        public DbSet<VINVCAM_FALLECIDOS_CLIENTES> VINVCAM_FALLECIDOS_CLIENTES { get; set; }
        public DbSet<VINVCAM_FACTURAS_OLD> VINVCAM_FACTURAS_OLD { get; set; }



    }
}
