﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Filas
    {
        [Key]
        public int Id { get; set; }
        public string Fila { get; set; }
        public string IdCementerio { get; set; }
    }
}
