﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Permisos
    {
        [Key]
        public int    permisoID { get; set; }
        public string usuarioID { get; set; }
        public Boolean estatus  { get; set; }
        public Boolean isAdmin  { get; set; }

    }
}
