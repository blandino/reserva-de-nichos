﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    
    public class VINVCAM_SalesPerson
    {
        
        public string vendedorID { get; set; }
        public string Nombre { get; set; }
        [Key]
        public int DEX_ROW_ID { get; set; }

    }
}
