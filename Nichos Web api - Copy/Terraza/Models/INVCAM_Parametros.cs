﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Parametros
    {
        [Key]
        public int parametroID { get; set; }
        public int diasReserva { get; set; }
    }
}
