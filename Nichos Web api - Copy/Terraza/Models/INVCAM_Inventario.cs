﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Inventario
    {
        [Key]
        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Contrato { get; set; }
        public string Panum { get; set; }
        public string Cementerio { get; set; }
        public string Pared { get; set; }
        public string Fila { get; set; }
        public string Columna { get; set; }
        public string CodigoAnterior { get; set; }
        public string? Etapa { get; set; }
        public string Estatus { get; set; }
        public string Vendedor { get; set; }
        public DateTime FechaReserva { get; set; }
        public string IdCliente { get; set; }
        public string NombreCliente { get; set; }
        //public string NombreFallecido { get; set; }
        public DateTime FechaRegistro { get; set; }
        //public DateTime FechaNacimiento { get; set; }
       // public DateTime FechaFallecimiento { get; set; }
        public string Tipo { get; set; }        
        public string TipoColumbario { get; set; }


    }
}
