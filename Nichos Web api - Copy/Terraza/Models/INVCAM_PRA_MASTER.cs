﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_PRA_MASTER
    {
        public string PRANUM { get; set; }
        public string PRACLASS { get; set; }
        public string PRACONTRACT { get; set; }
        public string PRACUSTNMBR { get; set; }
        public string PRACUSTNAME { get; set; }
        public string PRACUSTID { get; set; }
        public string PRACUSTPHONE { get; set; }
        public string PRACUSTPHONE2 { get; set; }
        public string PRACUSTEMAIL { get; set; }
        public int Id { get; set; }
        public string ESTADOCOBRO{get;set;}

    }
}
