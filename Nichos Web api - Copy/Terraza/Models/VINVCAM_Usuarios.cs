﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class VINVCAM_Usuarios
    {
        [Key]
        public string USRID { get; set; }
        public string UsrName { get; set; }
        public string Pwl    { get; set; }

    }
}
