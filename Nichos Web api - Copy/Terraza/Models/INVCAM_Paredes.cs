﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models
{
    public class INVCAM_Paredes
    {
        [Key]
        public int Id { get; set; }
        public string Pared { get; set; }
    }
}
