﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Terraza.Models.Dtos
{
    public class VINVCAM_UsuariosDto
    {
        public string USRID { get; set; }
        public string UsrName { get; set; }
        public string Pwl { get; set; }
    }
}
