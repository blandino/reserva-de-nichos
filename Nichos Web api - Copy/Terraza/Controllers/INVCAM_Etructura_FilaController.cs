﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_Etructura_FilaController : ControllerBase
    {
        public readonly IINVCAM_Etructura_FilaRepository _efRepo;
        public readonly IMapper _mapper;

        public INVCAM_Etructura_FilaController(IINVCAM_Etructura_FilaRepository efRepo
                                               ,IMapper mapper )
        {
            _efRepo = efRepo;
            _mapper = mapper;
        
        }

        [HttpGet("{cementerio}/{etapa}/{pared}", Name = "GetINVCAM_Estructura_Fila")]
        public IActionResult GetINVCAM_Estructura_Fila(string cementerio, string etapa, int pared)
        {
            var INVCAM_Estructura_fila = _efRepo.GetINVCAM_Etructura_FilaRepository(cementerio, etapa, pared);

            if (INVCAM_Estructura_fila == null)
            {
                return NotFound();

            }

            //var itemINVCAM_Estructura_ColumnaDto = _mapper.Map<INVCAM_Estructura_ColumnaDto>(INVCAM_Estructura_Columna);

            return Ok(INVCAM_Estructura_fila);
            //return Ok(listaINVCAM_FilasDto.OrderByDescending(x => x.Fila).ToList());

        }


    }
}
