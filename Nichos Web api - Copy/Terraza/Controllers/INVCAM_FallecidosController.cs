﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Data;
using Terraza.Models;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_FallecidosController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IINVCAM_FallecidosRepository _fRepo;
        private readonly IMapper _mapper;

        public INVCAM_FallecidosController(ApplicationDbContext context,
                                           IINVCAM_FallecidosRepository fRepo,
                                           IMapper mapper)
        {
            _context = context;
            _fRepo = fRepo;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetINVCAM_Fallecidos()
        {
            var listaINVCAM_Fallecidos = _fRepo.GetINVCAM_Fallecidos();
            var listaINVCAM_FallecidosDto = new List<INVCAM_FallecidosDto>();

            foreach (var lista in listaINVCAM_Fallecidos)
            {
                listaINVCAM_FallecidosDto.Add(_mapper.Map<INVCAM_FallecidosDto>(lista));
            }
            return Ok(listaINVCAM_FallecidosDto);
        }
        [HttpGet("{INVCAM_FallecidosId:int}", Name = "GetINVCAM_Fallecido")]
        public IActionResult GetINVCAM_Fallecido(int INVCAM_FallecidosId)
        {
            var itemINVCAM_Fallecido = _fRepo.GetINVCAM_Fallecido(INVCAM_FallecidosId);

            if (itemINVCAM_Fallecido == null)
            {
                return NotFound();
            }
            var itemINVCAM_FallecidoDto = _mapper.Map<INVCAM_FallecidosDto>(itemINVCAM_Fallecido);
            return Ok(itemINVCAM_FallecidoDto);

        }       

        [HttpGet("{INVCAM_FallecidosCodigo}/{opcional}", Name = "GetINVCAM_FallecidoByCodigo")]
        public IActionResult GetINVCAM_FallecidoByCodigo(string INVCAM_FallecidosCodigo, string opcional)
        {
            var itemINVCAM_Fallecido = _fRepo.GetINVCAM_Fallecidos().Where(x => x.codigoInventario == INVCAM_FallecidosCodigo).SingleOrDefault();

            if (itemINVCAM_Fallecido == null)
            {
                return NotFound();
            }

            var itemINVCAM_FallecidoDto = _mapper.Map<INVCAM_FallecidosDto>(itemINVCAM_Fallecido);

            return Ok(itemINVCAM_FallecidoDto);
        }

        [HttpPost]
         public IActionResult CrearGetINVCAM_Fallecido([FromBody] INVCAM_FallecidosDto iNVCAM_FallecidosDto)
         {
             if (iNVCAM_FallecidosDto == null)
             {
                 return BadRequest(ModelState);
             }
             if (_fRepo.ExisteINVCAM_Fallecidos(iNVCAM_FallecidosDto.codigoInventario))
             {
                 ModelState.AddModelError("", "El codigo ya existe");
                 return StatusCode(500, ModelState);
             }

             var fallecido = _mapper.Map<INVCAM_Fallecidos>(iNVCAM_FallecidosDto);

             if (!_fRepo.CrearINVCAM_Fallecidos(fallecido))
             {
                 ModelState.AddModelError("", $"Algo salio mal guardando el registro{fallecido.codigoInventario}");
                 return StatusCode(500, ModelState);
             }
             return Ok();
         }

        [HttpPatch("{iNVCAM_FallecidosId:int}", Name = "ActualizarINVCAM_Fallecidos")]
        public IActionResult ActualizarINVCAM_Fallecidos(int iNVCAM_FallecidosId, [FromBody] INVCAM_FallecidosDto iNVCAM_FallecidosDto)
        {
            if (iNVCAM_FallecidosDto == null || iNVCAM_FallecidosId != iNVCAM_FallecidosDto.Id)
            {
                return BadRequest(ModelState);
            }

            var fallecido = _mapper.Map<INVCAM_Fallecidos>(iNVCAM_FallecidosDto);

            if (!_fRepo.ActualizarINVCAM_Fallecidos(fallecido))
            {
                ModelState.AddModelError("", $"Algo salio mal actualizando el Registro{fallecido.codigoInventario}");
                return StatusCode(500, ModelState);
            }

            return NoContent();
        }

        [HttpDelete]
        public IActionResult BorrarINVCAM_Fallecidos( int INVCAM_FallecidosId) 
        {
            if (!_fRepo.ExisteINVCAM_Fallecidos(INVCAM_FallecidosId)) 
            {
                return NotFound();
            }

            var fallecido = _fRepo.GetINVCAM_Fallecido(INVCAM_FallecidosId);

            if (!_fRepo.BorrarINVCAM_Fallecidos(fallecido)) 
            {
                ModelState.AddModelError("", $"Algo salio mal borrando el registro{fallecido.codigoInventario}");
                return StatusCode(500, ModelState);            
            }

            return NoContent();
        }




    }   


}
