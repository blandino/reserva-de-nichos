﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class INVCAM_ParedesController : ControllerBase
    {
        private readonly IINVCAM_ParedesRepository _prRepo;
        private readonly IMapper _mapper;

        public INVCAM_ParedesController(IINVCAM_ParedesRepository prRepo,
                                           IMapper mapper)
        {
            _prRepo = prRepo;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetINVCAM_Paredes()
        {
            var listaINVCAM_Paredes = _prRepo.GetINVCAM_Paredes();
            var listaINVCAM_ParedesDto = new List<INVCAM_ParedesDto>();

            foreach (var lista in listaINVCAM_Paredes)
            {
                listaINVCAM_ParedesDto.Add(_mapper.Map<INVCAM_ParedesDto>(lista));
            }
            return Ok(listaINVCAM_ParedesDto);
        }
    }
}
