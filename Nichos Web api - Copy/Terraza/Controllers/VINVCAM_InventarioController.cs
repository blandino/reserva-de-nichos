﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VINVCAM_InventarioController : ControllerBase
    {

        private readonly IVINVCAM_InventarioRepository _vIRepo;
        private readonly IMapper _mapper;

        public VINVCAM_InventarioController(IVINVCAM_InventarioRepository vIRepo,
                                           IMapper mapper)
        {
            _vIRepo = vIRepo;
            _mapper = mapper;
        }


        [HttpGet]
        public IActionResult GetINVCAM_Inventarios()
        {
            var listaVINVCAM_Inventarios = _vIRepo.GetVINVCAM_Inventarios();
            var listaVINVCAM_InventariosDto = new List<VINVCAM_InventarioDto>();

            foreach (var lista in listaVINVCAM_Inventarios)
            {
                listaVINVCAM_InventariosDto.Add(_mapper.Map<VINVCAM_InventarioDto>(lista));
            }
            return Ok(listaVINVCAM_InventariosDto);
        }


        [HttpGet("{codigoInventario}", Name = "GetVINVCAM_Inventario")]
        public IActionResult GetVINVCAM_Inventario(string codigoInventario)
        {
            var VINVCAM_Inventario = _vIRepo.GetINVCAM_InventarioByCodigo(codigoInventario);

            if (VINVCAM_Inventario == null)
            {
                return NotFound();
            }

            var itemVINVCAM_InventarioDto = _mapper.Map<VINVCAM_InventarioDto>(VINVCAM_Inventario);

            return Ok(itemVINVCAM_InventarioDto);
        }
    }
}
