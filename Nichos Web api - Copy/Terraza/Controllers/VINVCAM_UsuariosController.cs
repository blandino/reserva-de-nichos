﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Terraza.Models.Dtos;
using Terraza.Repository.IRepository;

namespace Terraza.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VINVCAM_UsuariosController : ControllerBase
    {
        private readonly IVINVCAM_UsuariosRepository _usRepo;
        private readonly IMapper _mapper;

        public VINVCAM_UsuariosController(IVINVCAM_UsuariosRepository usRepo,
                                           IMapper mapper)
        {
            _usRepo = usRepo;
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult GetUsers()
        {
            var listaUsuarios = _usRepo.GetUsers();
            var listaUsuariosDto = new List<VINVCAM_UsuariosDto>();

            foreach (var lista in listaUsuarios)
            {
                listaUsuariosDto.Add(_mapper.Map<VINVCAM_UsuariosDto>(lista));
            }
            return Ok(listaUsuariosDto);
        }
    }
}
