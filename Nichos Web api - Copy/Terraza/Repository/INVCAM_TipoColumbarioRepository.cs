﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_TipoColumbarioRepository : IINVCAM_TipoColumbarioRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_TipoColumbarioRepository( ApplicationDbContext db) 
        {
            _db = db;
        
        }

        public ICollection<INVCAM_TipoColumbario> GetINVCAM_TipoColumbario()
        {
            return _db.INVCAM_TipoColumbario.OrderBy(x => x.Id).ToList();
        }
    }
}
