﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_FilasRepository : IINVCAM_FilasRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_FilasRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public ICollection<INVCAM_Filas> GetINVCAM_Filas()
        {
            return _db.INVCAM_Filas.OrderBy(i => i.Id).ToList();
        }  


       
    }
}
