﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_InventarioRepository : IINVCAM_InventarioRepository
    {
        private readonly ApplicationDbContext _db;

        public  INVCAM_InventarioRepository( ApplicationDbContext db) 
        {
            _db = db;
        }
        public bool ActualizarINVCAM_Inventario(INVCAM_Inventario INVCAM_Inventario)
        {
            _db.INVCAM_Inventario.Update(INVCAM_Inventario);
            return Guardar();
        }

        public bool BorrarINVCAM_Inventario(INVCAM_Inventario INVCAM_Inventario)
        {
            _db.INVCAM_Inventario.Remove(INVCAM_Inventario);
            return Guardar();
        }

        public bool CrearINVCAM_Inventario(INVCAM_Inventario INVCAM_Inventario)
        {
            _db.INVCAM_Inventario.Add(INVCAM_Inventario);
            return Guardar();
        }
               

        public bool ExisteINVCAM_Inventario(string nombre)
        {
            bool valor = _db.INVCAM_Inventario.Any(I => I.Codigo.ToLower().Trim() ==  nombre.ToLower().Trim());
            return valor;

           
        }

        public bool ExisteINVCAM_Inventario(int Id)
        {
            return _db.INVCAM_Inventario.Any(I => I.Id == Id);
        }

        public INVCAM_Inventario GetINVCAM_Inventario(int INVCAM_InventarioId)
        {
            return _db.INVCAM_Inventario.FirstOrDefault(I => I.Id == INVCAM_InventarioId);
        }

        public INVCAM_Inventario GetINVCAM_InventarioByCodigo(string INVCAM_InventarioCodigo)
        {
            return _db.INVCAM_Inventario.FirstOrDefault(I => I.Codigo == INVCAM_InventarioCodigo);
        }

        public ICollection<INVCAM_Inventario> GetINVCAM_Inventarios()
        {
            return _db.INVCAM_Inventario.OrderBy(i => i.Id).ToList();
        }

        public bool Guardar()
        {
            return _db.SaveChanges() >= 0 ? true : false;
        }
    }
}
