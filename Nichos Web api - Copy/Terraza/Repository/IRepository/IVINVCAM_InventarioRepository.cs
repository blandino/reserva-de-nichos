﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public  interface IVINVCAM_InventarioRepository
    {
        VINVCAM_Inventario GetINVCAM_InventarioByCodigo(string VINVCAM_CodigoInventario);
        ICollection<VINVCAM_Inventario> GetVINVCAM_Inventarios();
    }
}
