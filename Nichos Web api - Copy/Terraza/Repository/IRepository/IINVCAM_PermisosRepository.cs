﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_PermisosRepository
    {
        ICollection<INVCAM_Permisos> GetPermisos();
        INVCAM_Permisos GetPermiso(int INVCAM_InventarioId);
        //INVCAM_Permisos GetPermiso(string INVCAM_PermisosId);
        bool ExistePermiso(string _usuarioID);
        bool ExistePermiso(int Id);
        bool CrearPermiso(INVCAM_Permisos INVCAM_Permisos);
        bool ActualizarPermiso(INVCAM_Permisos INVCAM_Permisos);
        bool BorrarPermiso(INVCAM_Permisos INVCAM_Permisos);
        bool Guardar();
    }
}
