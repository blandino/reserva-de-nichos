﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Models;

namespace Terraza.Repository.IRepository
{
    public interface IINVCAM_InventarioRepository
    {
        ICollection<INVCAM_Inventario> GetINVCAM_Inventarios();
        INVCAM_Inventario GetINVCAM_Inventario(int INVCAM_InventarioId);
        INVCAM_Inventario GetINVCAM_InventarioByCodigo(string INVCAM_InventarioCodigo);
        bool ExisteINVCAM_Inventario(string nombre);
        bool ExisteINVCAM_Inventario(int Id);
        bool CrearINVCAM_Inventario(INVCAM_Inventario INVCAM_Inventario);
        bool ActualizarINVCAM_Inventario(INVCAM_Inventario INVCAM_Inventario);
        bool BorrarINVCAM_Inventario(INVCAM_Inventario INVCAM_Inventario);
        bool Guardar();
    }
}
