﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_FallecidosRepository : IINVCAM_FallecidosRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_FallecidosRepository(ApplicationDbContext db) 
        {
            _db =db;
        }
        public bool ActualizarINVCAM_Fallecidos(INVCAM_Fallecidos INVCAM_Fallecidos)
        {
            _db.INVCAM_Fallecidos.Update(INVCAM_Fallecidos);
            return Guardar();
        }

        public bool BorrarINVCAM_Fallecidos(INVCAM_Fallecidos INVCAM_Fallecidos)
        {
            _db.INVCAM_Fallecidos.Remove(INVCAM_Fallecidos);
            return Guardar();
        }

        public bool CrearINVCAM_Fallecidos(INVCAM_Fallecidos INVCAM_Fallecidos)
        {
             _db.INVCAM_Fallecidos.Add(INVCAM_Fallecidos);
             return Guardar();

            /*try
            {
                _db.INVCAM_Fallecidos.Add(INVCAM_Fallecidos);
                return Guardar();
            }
            catch (Exception e) 
            {
                return false;
            }*/
        }

        public bool ExisteINVCAM_Fallecidos(string codigo)
        {
            bool valor = _db.INVCAM_Fallecidos.Any(I => I.codigoInventario.ToLower().Trim() == codigo.ToLower().Trim());
            return valor;
        }

        public bool ExisteINVCAM_Fallecidos(int Id)
        {
            return _db.INVCAM_Fallecidos.Any(I => I.Id == Id);
        }

        public INVCAM_Fallecidos GetINVCAM_Fallecido(int INVCAM_FallecidosId)
        {
            return _db.INVCAM_Fallecidos.FirstOrDefault(I => I.Id == INVCAM_FallecidosId);
        }

        public ICollection<INVCAM_Fallecidos> GetINVCAM_Fallecidos()
        {
            var data = _db.INVCAM_Fallecidos.OrderBy(I => I.Id).ToList();
            return data;
        }

        public INVCAM_Fallecidos GetINVCAM_FallecidosByCodigo(string INVCAM_FallecidosCodigo)
        {
            return _db.INVCAM_Fallecidos.FirstOrDefault(I => I.codigoInventario == INVCAM_FallecidosCodigo); 
                
        }

        public bool Guardar()
        {
           return _db.SaveChanges() >=0 ?true: false;
        }
    }
}
