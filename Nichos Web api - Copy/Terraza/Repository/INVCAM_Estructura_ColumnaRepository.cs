﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_Estructura_ColumnaRepository : IINVCAM_Estructura_ColumnaRepository
    {
        private readonly ApplicationDbContext _db;
        public INVCAM_Estructura_ColumnaRepository(ApplicationDbContext db) {
            _db = db;
        }

        public List<string> GetINVCAM_Estructura_Columna(string cementerio, string etapa, int pared)
        {
            List <string> items = new List<string>();
            var result = _db.INVCAM_Estructura_Columna.Where(x => x.Cementerio == cementerio && x.Etapa == etapa && x.Pared == pared).SingleOrDefault();
            
            if (result == null)
            {
                return null;
            }

            for (int i = result.Desde; i <= result.Hasta; i++) 
            {
                items.Add(i.ToString());
            }
        
           return items;
        }
    }
}
