﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class INVCAM_PRA_MASTERRepository : IINVCAM_PRA_MASTERRepository
    {
        private readonly ApplicationDbContext _db;

        public INVCAM_PRA_MASTERRepository(ApplicationDbContext db)
        {
            _db = db;
        }
        public ICollection<INVCAM_PRA_MASTER> GetINVCAM_PRA_MASTERS()
        {
            return _db.INVCAM_PRA_MASTER.OrderBy(i => i.Id).ToList();
        }
        public INVCAM_PRA_MASTER GetINVCAM_PRA_MASTER(string INVCAM_PRA_MASTERId)
        {
            return _db.INVCAM_PRA_MASTER.FirstOrDefault(I => I.PRANUM == INVCAM_PRA_MASTERId);
        }       
    }
}
