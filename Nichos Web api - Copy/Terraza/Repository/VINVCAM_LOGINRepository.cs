﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Terraza.Data;
using Terraza.Models;
using Terraza.Repository.IRepository;

namespace Terraza.Repository
{
    public class VINVCAM_LOGINRepository : IVINVCAM_LOGINRepository
    {
        private readonly ApplicationDbContext _db;

        public VINVCAM_LOGINRepository( ApplicationDbContext db ) 
        {
            _db = db;
        }

        public bool CreateVINVCAM_LOGINId(VINVCAM_LOGIN VINVCAM_LOGIN)
        {
            _db.VINVCAM_LOGIN.Add(VINVCAM_LOGIN);
            return Save();
        }

        public bool DeleteVINVCAM_LOGIN(VINVCAM_LOGIN VINVCAM_LOGIN)
        {
            _db.VINVCAM_LOGIN.Remove(VINVCAM_LOGIN);
            return Save();
        }

        public bool ExistLoginUser(string name)
        {
            bool valor = _db.VINVCAM_LOGIN.Any(I => I.USRID.ToLower().Trim() == name.ToLower().Trim());
            return valor;
        }

        public bool ExistLoginUser(int Id)
        {
            throw new NotImplementedException();
        }

        public VINVCAM_LOGIN GetVINVCAM_LoginUser(string VINVCAM_LOGINId)
        {
            return _db.VINVCAM_LOGIN.FirstOrDefault(I => I.USRID == VINVCAM_LOGINId);
        }      

        public ICollection<VINVCAM_LOGIN> GetVINVCAM_LoginUsers()
        {
            return _db.VINVCAM_LOGIN.OrderBy(i => i.USRID).ToList();
        }

        public bool Save()
        {
            return _db.SaveChanges() >= 0 ? true : false;
        }

        public bool UpdateVINVCAM_LOGIN(VINVCAM_LOGIN VINVCAM_LOGIN)
        {
            _db.VINVCAM_LOGIN.Update(VINVCAM_LOGIN);
            return Save();
        }
    }
}
